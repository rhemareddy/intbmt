<?php

/**
 * Class contains of some common helper functions which are used across the site.
 */
class CommonHelper {

    /**
     * Global function to send an email from the application.
     * @param  array $config Accepts an details of the mail like to, from body...
     * @return boolean       Mail sending status.
     */
    public static function sendMail($config) { 
        $systemWinnerSettingsObject = SystemSetting::model()->findByAttributes(array('name'=>'EmailServer','value'=>0));//0:OFF
        if($systemWinnerSettingsObject){
            return true;
        }
        if (isset($config['file_path'])) {
            if (!empty($config) && !empty($config['to']) && !empty($config['subject']) && !empty($config['body'])) {
                Yii::import('application.extensions.yii-mail.YiiMailMessage');
                $message = new YiiMailMessage();
                $message->setTo($config['to']);
                if (empty($config['from'])) {
                    $message->setFrom(array(Yii::app()->params['senderMail'] => Yii::app()->params['senderName']));
                } else {
                    $message->setFrom($config['from']);
                }
                $message->setSubject($config['subject']);
                $message->setBody($config['body'], 'text/html');

                if (isset($config['file_path'])) {
                    $swiftAttachment = Swift_Attachment::fromPath($config['file_path']);
                    $message->attach($swiftAttachment);
                }
                if (Yii::app()->mail->send($message) > 0) {
                    return true;
                }
            }
            return false;
        }else {
            self::sendMailDkim($config);
        }
        return true;
    }
    
    public static function sendMailDkim($config) {  
        $sender = Yii::app()->params['senderMail'];
        $to = $config['to'];
        $subject = $config['subject'];
        $body = $config['body'];
        $headers = "From:$sender\r\n".
        "To: \r\n".
            "Reply-To: $sender\r\n".
            "Content-Type: text/html\r\n".
            "MIME-Version: 1.0";
        $headers = Yii::app()->dkim->add($headers,$subject,$body) . $headers;
        $result = mail($to,$subject,$body,$headers,"-f $sender");
        
        if($result){
            $status = "done";
        }else{
            $status = "error";
        }
        return $status ;
    }

    /**
     * 
     * @param type $length
     * @return \lengthGenerate unique id
     * @param int limit
     * 
     * @return length
     */
    public static function generateUniqueId($length = 10) {
        return substr(md5(uniqid(mt_rand(), true)), 0, $length);
    }

    /**
     * Returns the array differece, if both the array are differs in any way
     * @param  Array $arr1 Array1 to compare
     * @param  Array $arr2 Array2 to compare
     * @return Boolean Status weather both array are different at any point.
     */
    public static function arrayDifference($arr1, $arr2) {
        if (!count(array_diff($arr1, $arr2)) && !count(array_diff($arr2, $arr1))) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Generates unique name for the images
     * @param  String $image_name actual name of the Image
     * @return String New name of the image.
     */
    public static function generateNewNameOfImage($image_name) {
        $extension = pathinfo($image_name, PATHINFO_EXTENSION);
        $name = md5(uniqid(microtime(), true));
        return $name . "." . strtolower($extension);
    }

    /**
     * Function to generate resize the images based on passed resolutions
     * @param  string $inputFilePath  abs input file path
     * @param  string $inputFileName  file name
     * @param  string $outputFilePath abs output file path
     * @param  string $outputFileName file name
     * @param  array  $options        array of dimenstions
     */
    public static function generateResizeImage($inputFilePath, $inputFileName, $outputFilePath, $outputFileName, $options) {
        Yii::import('application.extensions.EWideImage.EWideImage');
        $inputImage = $inputFilePath . "/" . $inputFileName;
        $outputImage = $outputFilePath . "/" . $outputFileName;
        $extension = pathinfo($inputImage, PATHINFO_EXTENSION);
        $quality = (strtolower($extension) == 'png') ? 9 : 90;
        list($imageWidth, $imageHeight) = getimagesize($inputImage);
        if (is_array($options) && !empty($options)) {
            foreach ($options as $key => $value) {
                $imageSize = array();
                $imageSize = explode('_', $value);
                $imageSizeNew = $imageSize;
                if (!is_dir($outputFilePath . "/" . $value)) {
                    mkdir($outputFilePath . "/" . $value, 0777, true);
                }
                if (($imageWidth == $imageSize[0]) && ($imageHeight == $imageSize[1])) {
                    $cpSrc = $_SERVER['DOCUMENT_ROOT'] . '/' . $inputFilePath . $inputFileName;
                    $cpDes = $_SERVER['DOCUMENT_ROOT'] . '/' . $outputFilePath . "/" . $value . "/" . $inputFileName;
                    copy($cpSrc, $cpDes);
                } else {
                    EWideImage::load($inputImage)->resize($imageSizeNew[0], $imageSizeNew[1], 'inside', 'down')->saveToFile($outputFilePath . "/" . $value . "/" . $inputFileName, $quality);
                }
            }
        }
    }
    
    /**
     * Function to query string var.
     * @param type $url
     * @param type $key
     * @return type
     */
    public static function remove_querystring_var($url, $key) {
        $url = preg_replace('/(.*)(\?|&)' . $key . '=[^&]+?(&)(.*)/i', '$1$2$4', $url . '&');
        $url = substr($url, 0, -1);
        return $url;
    }

    public static function generateCropImage($inputFilePath, $inputFileName, $outputFilePath, $outputFileName, $options) {
        try {
            Yii::import('application.extensions.EWideImage.EWideImage');
            $inputImage = $inputFilePath . "/" . $inputFileName;
            $outputImage = $outputFilePath . "/" . $outputFileName;
            $extension = pathinfo($inputImage, PATHINFO_EXTENSION);

            $quality = (strtolower($extension) == 'png') ? 9 : 90;
            list($imageWidth, $imageHeight) = getimagesize($inputImage);

            if ($imageWidth && $imageHeight) {
                if (is_array($options) && !empty($options)) {
                    foreach ($options as $key => $value) {
                        $imageSize = array();
                        if (!is_dir($outputFilePath . "/" . $value)) {
                            mkdir($outputFilePath . "/" . $value, 0777, true);
                            chmod($outputFilePath . "/" . $value, 0777);
                        }
                        $imageSize = explode('_', $value);
                        if (($imageWidth == $imageSize[0]) && ($imageHeight == $imageSize[1])) {
                            $cpSrc = $_SERVER['DOCUMENT_ROOT'] . '/' . $inputFilePath . '/' . $inputFileName;
                            $cpDes = $_SERVER['DOCUMENT_ROOT'] . '/' . $outputFilePath . "/" . $value . "/" . $outputFileName;
                            copy($cpSrc, $cpDes);
                        } else {
                            //EWideImage::load($inputImage)->crop('center', 'center', $imageSize[0], $imageSize[1])->saveToFile($outputFilePath."/".$value."/".$inputFileName, $quality);
                            EWideImage::load($inputImage)->resize($imageSize[0], $imageSize[1], 'outside')->crop("center", "center", $imageSize[0], $imageSize[1])->saveToFile($outputFilePath . "/" . $value . "/" . $outputFileName, $quality);
                        }
                        chmod($outputFilePath . "/" . $value . "/" . $outputFileName, 0777);
                    }
                }
            }
        } catch (Exception $e) {
            error_log(print_r($e->getMessage(), true));
        }
    }

    public static function logMessage($message, $logLevel, $category) {
        $micro_date = microtime(true);
        $date_array = explode(".", $micro_date);
        $date = date("Y-m-d H:i:s", $date_array[0]) . "." . isset($date_array[1]) && !empty($date_array[1]) ? $date_array[1] : "";
        Yii::log("[$date] " . $message, $logLevel, $category);
    }

    public static function generateResizeImageHomeSlider($inputFilePath, $inputFileName, $outputFilePath, $outputFileName, $options) {
        Yii::import('application.extensions.EWideImage.EWideImage');
        $inputImage = $inputFilePath . "/" . $inputFileName;
        $outputImage = $outputFilePath . "/" . $outputFileName;
        $extension = pathinfo($inputImage, PATHINFO_EXTENSION);
        list($imageWidth, $imageHeight) = getimagesize($inputImage);
        $quality = (strtolower($extension) == 'png') ? 9 : 90;
        if (is_array($options) && !empty($options)) {
            foreach ($options as $key => $value) {
                $imageSize = array();
                if (!is_dir($outputFilePath . "/" . $value)) {
                    mkdir($outputFilePath . "/" . $value, 0777, true);
                }
                $imageSize = explode('_', $value);
                //error_log("REQUIRED SIZE: $value");
                $newWidth = $imageSize[0];
                $newHeight = $imageSize[1];

                if ($imageHeight > $newHeight) {
                    //error_log('RESIZING respect to height');			
                    EWideImage::load($inputImage)->resize(99999, $newHeight, 'inside', 'down')->saveToFile($outputFilePath . "/" . $value . "/" . $inputFileName, $quality);
                    $tempInputImage = $outputFilePath . "/" . $value . "/" . $inputFileName;
                    list($resizedImageWidth, $resizedImageHeight) = getimagesize($tempInputImage);
                    //error_log("new image width: $resizedImageWidth  , height: $resizedImageHeight");	
                    if ($resizedImageWidth >= $newWidth) {
                        //error_log("crop newly resize image");	
                        EWideImage::load($tempInputImage)->crop('center', 'center', $newWidth, $newHeight)->saveToFile($outputFilePath . "/" . $value . "/" . $inputFileName, $quality);
                    } else {
                        //error_log('RESIZING respect to width');			
                        EWideImage::load($inputImage)->resize($newWidth, 99999, 'inside', 'down')->saveToFile($outputFilePath . "/" . $value . "/" . $inputFileName, $quality);
                        $tempInputImage = $outputFilePath . "/" . $value . "/" . $inputFileName;
                        list($resizedImageWidth, $resizedImageHeight) = getimagesize($tempInputImage);
                        //error_log("new image width: $resizedImageWidth  , height: $resizedImageHeight");	
                        if ($resizedImageHeight >= $newHeight) {
                            //error_log("crop newly resize image");	
                            EWideImage::load($tempInputImage)->crop('center', 'center', $newWidth, $newHeight)->saveToFile($outputFilePath . "/" . $value . "/" . $inputFileName, $quality);
                        } else {
                            //error_log("crop original image");	
                            EWideImage::load($inputImage)->crop('center', 'center', $newWidth, $newHeight)->saveToFile($outputFilePath . "/" . $value . "/" . $inputFileName, $quality);
                        }
                    }
                } else {
                    if ($imageWidth > $newWidth) { // width is more. Crop out the excessive width
                        EWideImage::load($inputImage)->crop('center', 'center', $newWidth, $newHeight)->saveToFile($outputFilePath . "/" . $value . "/" . $inputFileName, $quality);
                    } else { // width is less then or equal to required image. Dont do anything, just copy the image
                        $cpSrc = $_SERVER['DOCUMENT_ROOT'] . '/' . $inputFilePath . $inputFileName;
                        $cpDes = $_SERVER['DOCUMENT_ROOT'] . '/' . $outputFilePath . "/" . $value . "/" . $inputFileName;
                        copy($cpSrc, $cpDes);
                    }
                }
            }
        }
    }

    /**
     * Function to generate random password for the account.
     * @param  integer $length Password Length.
     * @return string          Returns generated password.
     */
    function generatePassword($length = 8) {
        $chars = '0123456789@#$!()*^{}[]abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $char_length = strlen($chars);
        srand((double) microtime() * 1000000);
        for ($i = 0; $i < $length; $i++) {
            $num = rand() % $char_length;
            $password .= $chars[$num];
        }
        return $password;
    }

    /**
     * Function to generate the count of restaurants and avis (reviews)
     */
    public static function restoAndAvisCount() {

        $array = array();
        $array['restocount'] = Restaurant::Model()->count(array("condition" => "etat=1"));
        $array['aviscount'] = RestaurantReviews::Model()->count(array("condition" => "status!=4"));
        return $array;
    }

    /**
     * Function to get the Recent Search items
     */
    public static function getRecentSearchItems() {
        $array = array();

        $recentSearchDept = RecentSearch::model()->find(array("condition" => "type=1", "order" => "datetime DESC", "limit" => "1"));
        $recentSearchPoi = RecentSearch::model()->find(array("condition" => "type=3", "order" => "datetime DESC", "limit" => "1"));
        $recentSearchCity = RecentSearch::model()->find(array("condition" => "type=2", "order" => "datetime DESC", "limit" => "1"));
        $recentSearchResto = RecentSearch::model()->find(array("condition" => "type=4", "order" => "datetime DESC", "limit" => "1"));

        $counter = 0;
        if ($recentSearchPoi) {
            $object = Poi::model()->find("id='$recentSearchPoi->object_id'");
            if (!empty($object)) {
                $dateDiff = CommonHelper::getDateDifference(date("Y-m-d H:i:s"), $recentSearchPoi->datetime);
                $timeText = CommonHelper::getInterval($dateDiff);

                $array[$counter]['keyword'] = $object->name;
                $array[$counter]['url'] = $object->getSearchUrl();
                $array[$counter]['timeText'] = $timeText;
                $counter++;
            }
        }
        if ($recentSearchCity) {
            $object = City::model()->find("id='$recentSearchCity->object_id'");
            if (!empty($object)) {
                $dateDiff = CommonHelper::getDateDifference(date("Y-m-d H:i:s"), $recentSearchCity->datetime);
                $timeText = CommonHelper::getInterval($dateDiff);

                $array[$counter]['keyword'] = $object->name;
                $array[$counter]['url'] = $object->getSearchUrl();
                $array[$counter]['timeText'] = $timeText;
                $counter++;
            }
        }
        if ($recentSearchDept) {
            $object = Department::model()->find("id='$recentSearchDept->object_id'");
            if (!empty($object)) {
                $dateDiff = CommonHelper::getDateDifference(date("Y-m-d H:i:s"), $recentSearchDept->datetime);
                $timeText = CommonHelper::getInterval($dateDiff);

                $array[$counter]['keyword'] = $object->name;
                $array[$counter]['url'] = $object->getSearchUrl();
                $array[$counter]['timeText'] = $timeText;
                $counter++;
            }
        }
        if ($recentSearchResto) {
            $object = Restaurant::model()->find("id='$recentSearchResto->object_id'");
            if (!empty($object)) {
                $dateDiff = CommonHelper::getDateDifference(date("Y-m-d H:i:s"), $recentSearchResto->datetime);
                $timeText = CommonHelper::getInterval($dateDiff);

                $array[$counter]['keyword'] = $object->restaurant_name;
                $array[$counter]['url'] = $object->setRestUrl();
                $array[$counter]['timeText'] = $timeText;
                $counter++;
            }
        }

        return $array;
    }

        public static function search($value, $model, $columns, $with = array(), $selected = "", $pageSize = NULL,$conditionFieldQuery="") {
        $pageSize = isset($pageSize) ? $pageSize : Yii::app()->params['defaultPageSize'];
        if ($selected && $selected == "status_active") {
            $selected = "status";
            $value = 1;
        } elseif ($selected && $selected == "status_inactive") {
            $selected = "status";
            $value = 0;
        } 


        $condition = $selected . " like '%" . $value . "%' AND role_id != '2' " . $conditionFieldQuery;

        if (!$selected) {
            $condition = "";
            foreach ($columns as $column) {
                $condition .= " OR " . $column . " like '%" . $value . "%' AND role_id != '2' " . $conditionFieldQuery;
            }
            $condition = substr($condition, 3);
        }
        $criteria = new CDbCriteria(array(
            'condition' => $condition,
            'with' => $with,
        ));
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => $pageSize),
        ));
        return $dataProvider;
    }

    public static function getSearchCriteria($value, $model, $columns, $with = array(), $selected = "") {
        $pageSize = Yii::app()->params['defaultPageSize'];
        if ($selected && $selected == "status_active") {
            $selected = "status";
            $value = 1;
        } elseif ($selected && $selected == "status_inactive") {
            $selected = "status";
            $value = 0;
        }
        $condition = $selected . " like '%" . $value . "%'";

        if (!$selected) {
            $condition = "";
            foreach ($columns as $column) {
                $condition .= " OR " . $column . " like '%" . $value . "%' ";
            }
            $condition = substr($condition, 3);
        }
        $criteria = new CDbCriteria(array(
            'condition' => $condition,
            'with' => $with,
        ));

        return $criteria;
    }

    /**
     * Function to get the Recent Search items
     */
    public static function getDateDifference($date1, $date2) {
        $array = array();
        $date1 = new DateTime($date1);
        $date2 = new DateTime($date2);
        $interval = $date1->diff($date2);
        $array['y'] = $interval->y;
        $array['m'] = $interval->m;
        $array['d'] = $interval->d;
        $array['h'] = $interval->h;
        $array['i'] = $interval->i;
        $array['s'] = $interval->s;
        $array['type'] = ( $interval->invert ? ' n' : 'p' );
        return $array;
    }

    public static function getInterval($interval) {
        if ($interval['y'] >= 1) {
            return ($interval['y'] == 1) ? $interval['y'] . ' year' : $interval['y'] . ' years';
        }
        if ($interval['m'] >= 1) {
            return ($interval['m'] == 1) ? $interval['m'] . ' mois' : $interval['m'] . ' mois';
        }
        if ($interval['d'] >= 1) {
            return ($interval['d'] == 1) ? $interval['d'] . ' jour' : $interval['d'] . ' jours';
        }
        if ($interval['h'] >= 1) {
            return ($interval['h'] == 1) ? $interval['h'] . ' hour' : $interval['h'] . ' hours';
        }
        if ($interval['i'] >= 1) {
            return ($interval['i'] == 1) ? $interval['i'] . ' minute' : $interval['i'] . ' minutes';
        }
        return ($interval['s'] == 1) ? $interval['s'] . ' second' : $interval['s'] . ' seconds';
    }

    /**
     * Function to generate the count of menus, photos, vid�os, promotions, critiques, articles
     */
    public static function menuPhotoAndOtherCount() {

        $array = array();
        //$array['menucount'] = Restaurant::model()->count(array("condition"=> "etat=1 AND srv_menu_description IS NOT NULL AND srv_menu_description!=''"));
        $array['menucount'] = RestaurantMenu::model()->count("id");
        $array['photocount'] = RestaurantPhoto::model()->count("id");
        $array['videocount'] = RestaurantVideos::model()->count("id");
        $array['promotioncount'] = RestaurantPromotion::model()->count("id");
        $array['articlecount'] = Article::model()->count(array("condition" => "status=1"));
        $criteria = new CDbCriteria;
        $criteria->addCondition("t.status=1");
        $criteria->join = ' LEFT JOIN article_categories AC ON AC.article_id  = t.id ';
        $criteria->addCondition(" AC.category_id = '6'");
        $array['critiquecount'] = Article::model()->count($criteria);
        return $array;
    }

    public static function getCategories() {
        return Category::model()->findAll();
    }

    public static function usersInfo() {

        $Criteria = new CDbCriteria;
        $Criteria->limit = '3';
        $userInfo = User::Model()->findAll($Criteria);
        return $userInfo;
    }

    public static function formatRating($rating) {
        $rating = number_format($rating, 1, ',', ' ');
        $parts = explode(",", $rating);
        if (!(isset($parts[1]) && $parts[1] > 0)) {
            $rating = $parts[0];
        }
        return $rating;
    }

    public static function getRatingCssBackground($params) {
        $result = '';
        $rating = isset($params['rating']) ? $params['rating'] : 0;
        $type = isset($params['type']) ? $params['type'] : 'css';  // type = css or image
        $parts = explode(".", $rating);
        if (isset($parts[1]) && $parts[1] >= 5) {
            $parts[0] = $parts[0] + 1;
        }

        // if rating is less then 1, set it to default 1
        if ($parts[0] < 1) {
            $parts[0] = 1;
        }

        if ($parts[0] > 5) {
            $parts[0] = 5;
        }

        if ($type == 'number') {
            $result = $parts[0];
        } elseif ($type == 'image') {
            $result = '/images/retina/pinmap' . $parts[0] . '.png';
        } elseif ($type == 'bgcolor') {
            $result = 'avis' . $parts[0];
        } else {
            $result = 'ratingBig' . $parts[0];
        }
        return $result;
    }

    public static function getFormatedRatingHtml($params) {
        $result = '';
        $rating = isset($params['rating']) ? $params['rating'] : 0;
        $includeMicrodata = isset($params['includeMicrodata']) ? $params['includeMicrodata'] : false;
        if ($rating > 0) {
            $parts = explode(".", $rating);
            $result = '<p class="ratingBlockWrapper">';
            $result .= ($includeMicrodata == true) ? '<span class="ratingBlock" itemprop="ratingValue" >' : '<span class="ratingBlock">';
            $result .= $parts[0];
            if (isset($parts[1])) {
                $result .= '<span class="pointTxt">' . Yii::t("restaurant", "key_seperator") . $parts[1] . '</span>';
            }
            $result .= '</span><span>/';
            $result .= ($includeMicrodata == true) ? '<span itemprop="bestRating">5</span>' : '5';
            $result .= '</span></p>';
        }
        return $result;
    }

    public function loadModelByName($id, $name) {
        $model = $name::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public static function formatContactNo($contactNo = null) {
        if (Yii::app()->language == 'en') {

            $tempArry = explode("/", $contactNo);
            if (!isset($tempArry[1])) {
                $tempArry = explode(",", $contactNo);
            }

            foreach ($tempArry as $key => $value) {
                //if(isset($tempArry[0])){
                $value = preg_replace('/[^0-9]/', '', $value);
                $value = preg_replace('/^91/', '0', $value);
                if ($value && !preg_match('/^[0]{1}/', $value)) {
                    if (strlen($value) > 8) {
                        $value = '0' . $value;
                    }
                }
                $tempArry[$key] = $value;
                //}
            }
            $contactNo = implode('/ ', $tempArry);
        }
        return $contactNo;
    }

    public static function ellipsisText($text, $offset, $byCharLength = false) {
        $length = 0;
        /*         * * explode the string ** */
        $arr = explode(' ', $text);
        /*         * * check the search is not out of bounds ** */
        if ($byCharLength) {
            if (strlen($text) > $offset) {
                $length = $offset;
            }
        } else {
            switch ($offset) {
                case $offset == 0:
                    //don't do anything
                    break;

                case $offset > max(array_keys($arr)):
                    //don't do anything
                    break;

                default:
                    $length = strlen(implode(' ', array_slice($arr, 0, $offset)));
            }
        }
        if ($length && strlen($text) > $length) {
            $text = substr($text, 0, $length);
            $text = substr($text, 0, strrpos($text, ' ')) . "...";
        }
        return $text;
    }

    public static function formatDate($date) {
        $dt = strtotime($date);
        return date("d", $dt) . " " . Yii::app()->params['months' . Yii::app()->language][date("m", $dt)] . " " . date("Y", $dt);
    }

    public static function formatTime($time) {
        $formattedTime = "";
        $expl = explode(':', $time);

        if (isset($expl[0])) {
            if ($expl[0] < 10) {
                $expl[0] = substr($expl[0], 1);
            }
            $formattedTime.="$expl[0]h";
        }
        if (isset($expl[1]) && $expl[1] != "00") {
            $formattedTime.="$expl[0]";
        }

        return $formattedTime;
    }

    public static function filterLinkUrl($url = '') {
        if ($url != '') {
            if (strpos($url, '.') !== false) {
                if (strpos($url, 'http://') === false && strpos($url, 'https://') === false) {
                    $url = 'http://' . $url;
                }
            }
        }
        return $url;
    }

    public static function getNewDBConnection($dbOptions) {
        $dsn = "mysql:host=$dbOptions[DBHOST];dbname=$dbOptions[DBNAME]";
        return new CDbConnection($dsn, $dbOptions['DBUSER'], $dbOptions['DBPASS']);
    }

    public static function encryptAndDecrypt($action, $string) {
        $output = false;

        $key = 'My strong random secret key';

        // initialization vector 
        $iv = md5(md5($key));

        if ($action == 'encrypt') {

            $output = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, $iv);
            echo $output;
            exit;
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, $iv);
            $output = rtrim($output, "");
        }

        return $output;
    }

    /*
     * Get all temparary bids.
     */

    public static function getTempararyBids($userId, $auctionId) {
        $tempAuctionBidsListObject = TempAuctionBids::model()->findAllByAttributes(
                array('user_id' => $userId, 'auction_id' => $auctionId)
        );
        return $tempAuctionBidsListObject;
    }

    /*
     * Get count temparary bids.
     */

    public static function getCountTempararyBids($userId, $auctionId) {
        $bidCount = TempAuctionBids::model()->count('user_id = :user_id AND auction_id = :auction_id', array(':user_id' => $userId, ':auction_id' => $auctionId));
        return $bidCount;
    }

    /*
     * Get count temparary bids.
     */

    public static function deleteTempararyBids($userId, $auctionId) {
        $tempAuctionBidsObject = TempAuctionBids::model()->deleteAll(
                "(user_id = :userId AND auction_id = :auctionId)", array(':userId' => $userId, ':auctionId' => $auctionId)
        );

        return $tempAuctionBidsObject;
    }

  
    /**
     * Get Time Ago function.
     */
    public static function getTimeago($ptime) {
        $etime = time() - $ptime;

        if ($etime < 1) {
            return 'Just Now';
        }

        $a = array(12 * 30 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;

            if ($d >= 1) {
                $r = round($d);
                return '' . $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
    }

    /**
     * return count of directories.
     * @param type $dirPath
     * @return int
     */
    public static function countDirectories($dirPath = NULL) {
        if (!$dirPath)
            return 0;

        $folderCount = 0;
        $cdir = scandir($dirPath);
        foreach ($cdir as $key => $value) {
            if (!in_array($value, array(".", ".."))) {
                if (is_dir($dirPath . $value)) {
                    $folderCount++;
                }
            }
        }
        return $folderCount;
    }

    public static function extractZipFiles($source = NULL, $targetDir = NULL, $fileName = NULL) {
        $path = Yii::getPathOfAlias('webroot') . "/landingpage/";
        
        if (!$source || !$targetDir || !$fileName) return false;
               
        $timeAppend = time() . "_";
        $filenoext = basename($fileName, '.zip');
        $filenoext = basename($filenoext, '.ZIP');

        $targetzip = $path . $timeAppend . $fileName; // target zip file

        /* create directory if not exists', otherwise overwrite */
        /* target directory is same as filename without extension */
        if (is_dir($targetDir)) {
            $this->rmdir_recursive($targetDir);
        }

        /* here it is really happening */
        if (move_uploaded_file($source, $targetzip)) {
            $zip = new ZipArchive();
            $x = $zip->open($targetzip);  // open the zip file to extract
            if ($x === true) {
                $zip->extractTo($targetDir); // place in the directory with same name  
                $zip->close();
                unlink($targetzip);
            }
            return true;
        } else {
            return false;
        }       
    }
    
     /**
     *  Get client IP address.
     * @return string
     */
    public static function get_client_ip_server() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
    
    public static function getOS() {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $os_platform    =   "Unknown OS Platform";

        $os_array       =   array(
                            '/windows nt 10/i'     =>  'Windows 10',
                            '/windows nt 6.3/i'     =>  'Windows 8.1',
                            '/windows nt 6.2/i'     =>  'Windows 8',
                            '/windows nt 6.1/i'     =>  'Windows 7',
                            '/windows nt 6.0/i'     =>  'Windows Vista',
                            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                            '/windows nt 5.1/i'     =>  'Windows XP',
                            '/windows xp/i'         =>  'Windows XP',
                            '/windows nt 5.0/i'     =>  'Windows 2000',
                            '/windows me/i'         =>  'Windows ME',
                            '/win98/i'              =>  'Windows 98',
                            '/win95/i'              =>  'Windows 95',
                            '/win16/i'              =>  'Windows 3.11',
                            '/macintosh|mac os x/i' =>  'Mac OS X',
                            '/mac_powerpc/i'        =>  'Mac OS 9',
                            '/linux/i'              =>  'Linux',
                            '/ubuntu/i'             =>  'Ubuntu',
                            '/iphone/i'             =>  'iPhone',
                            '/ipod/i'               =>  'iPod',
                            '/ipad/i'               =>  'iPad',
                            '/android/i'            =>  'Android',
                            '/blackberry/i'         =>  'BlackBerry',
                            '/webos/i'              =>  'Mobile'
                        );

        foreach ($os_array as $regex => $value) { 

            if (preg_match($regex, $user_agent)) {
                $os_platform    =   $value;
            }

        }   

        return $os_platform;
    }
    
    public static function getBrowser() {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        
        $browser        =   "Unknown Browser";

        $browser_array  =   array(
                                '/MSIE/i'       =>  'Internet Explorer',
                                '/firefox/i'    =>  'Firefox',
                                '/safari/i'     =>  'Safari',
                                '/chrome/i'     =>  'Chrome',
                                '/edge/i'       =>  'Edge',
                                '/Opera/i'      =>  'Opera',
                                '/netscape/i'   =>  'Netscape',
                                '/maxthon/i'    =>  'Maxthon',
                                '/konqueror/i'  =>  'Konqueror',
                                '/mobile/i'     =>  'Handheld Browser'
                            );

        foreach ($browser_array as $regex => $value) { 

            if (preg_match($regex, $user_agent)) {
                $browser    =   $value;
            }

        }

    return $browser;
    }
    
    public static function getCountry($ip) {
        $country = "";
        if (!empty($ip)) { // the IP address to country
            $countryByIp = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
            if ($countryByIp && $countryByIp['status'] == 'success') {
                $country = $countryByIp['country'];
            } else {
                $country = "NA";
            }
        }

    return $country;
    }
    
       public static function timeElapsed($time){
        $time = time() - $time; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }

    }
    
    public static function isFileValid($fileName, $size) {
        $error = "";
        $fileNameArray = explode(".", $fileName);
        $ext = strtolower(end($fileNameArray));

        $uploadSize = $_FILES['attachment']["size"];
        if ($uploadSize > $size) {
            $error .= "File can not be greater than 2MB";
        } elseif ($ext != "jpg" && $ext != "jpeg" && $ext != "png" && $ext != "pdf") {
            $error .= "Incorrect File Type";
        }
        return $error;
    }
    
    public function rmdir_recursive($dir) {
        foreach (scandir($dir) as $file) {
            if ('.' === $file || '..' === $file)
                continue;
            if (is_dir("$dir/$file"))
                $this->rmdir_recursive("$dir/$file");
            else
                unlink("$dir/$file");
        }

        rmdir($dir);
    }
    
}
