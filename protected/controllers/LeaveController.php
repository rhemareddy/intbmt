<?php

class LeaveController extends Controller {

    public function actionIndex() {
        $this->render('index');
    }

    public function actionList() {
        $listListObject = LeaveApplication::model()->findAll();
        $this->render('list', array('leavesListObject' => $listListObject));
    }
    
    public function actionHoliday() {
        $holidayCalendarListObject = HolidayCalendar::model()->findAll();
        $this->render('holiday', array('holidayCalendarListObject' => $holidayCalendarListObject));
    }
    
    public function actionApply() {
        $userId = 1;
        $reportingUser = User::readReportingUser($userId);
        $leaveObject = array();
        if(isset($_GET['lId']) && !empty($_GET['lId'])){
            $leaveObject = LeaveApplication::read($_GET['lId']);
        }
        
        if($_POST){
            LeaveApplication::create($_POST);
            Yii::app()->user->setFlash('success', "Leave Application submitted Successfully!");
            $this->redirect(array('leave/list'));
        }
        $this->render('apply', array('reportingUser' => $reportingUser,'leaveObject'=>$leaveObject));
    }
    
    public function actionUpdateComment(){
        if($_POST){
            $leaveObject = LeaveApplication::read($_POST['leave_id']);
            
            $leaveObject->status = $_POST['leave_status'];
            $leaveObject->comment = $_POST['leave_comment'];
            $leaveObject->update(false);
            Yii::app()->user->setFlash('success', "Leave Application Status submitted Successfully!");
            $this->redirect(array('leave/list'));
        }
    }
    
    public function actionAddHoliday(){
        $holidayObject = array();
        if(isset($_GET['hId']) && !empty($_GET['hId'])){
            $holidayObject = HolidayCalendar::read($_GET['hId']);
        }
        
        if($_POST){
            HolidayCalendar::create($_POST);
            Yii::app()->user->setFlash('success', "Holiday Event Submitted Successfully!");
            $this->redirect(array('/leave/holiday'));
        }
        $this->render('add_holiday', array('holidayObject'=>$holidayObject));
    }

}
