<?php

class UserController extends Controller
{
    public function actionIndex()
    {
        
            $this->render('index');
    }
    public function actionEmployee()
    {
        $employeeListObject = User::model()->findAll();
        $this->render('employee',array('employeeListObject'=>$employeeListObject));
    }
    
    public function actionAdd(){
        $departMentListObject = Department::readAll();
        $designationListObject = Designation::readAll();
        $branchListObject = Branch::readAll();
        $bloodGroupListObject = Branch::readAll();

        $userObject = array();
        if(isset($_GET['uId']) && $_GET['uId']){
            $userObject = User::readById($_GET['uId']);
        }
        
        if($_POST){
            User::model()->create($_POST);
            Yii::app()->user->setFlash('success', "Employee ".$_POST['full_name']." Profile Activity Successfully!");
            $this->redirect(array('user/employee'));
        }
        $this->render('add',array('designationListObject'=>$designationListObject,
            'departMentListObject'=>$departMentListObject,'branchListObject'=>$branchListObject,
            'bloodGroupListObject'=>$bloodGroupListObject,'userObject'=>$userObject));
    }
    
    public function actionChangeStatus(){
        if(isset($_GET['uId']) && $_GET['uId'] && !empty($_GET['action'])){
            $userObject = User::readByPrimaryId($_GET['uId']);
            $userObject->status = $_GET['action'];
            $userObject->update();
            Yii::app()->user->setFlash('success', "Action ".$_GET['action']." applied on ".$userObject->full_name." account!");
            $this->redirect(array('user/employee'));
        }
    }
    
}