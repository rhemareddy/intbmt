<?php

spl_autoload_unregister(array('YiiBase', 'autoload'));
require('autoload.php');
spl_autoload_register(array('YiiBase', 'autoload'));

    $google_client_id = Yii::app()->params['social']['googlelogin']['clientId']; //Google CLIENT ID
    $google_client_secret = Yii::app()->params['social']['googlelogin']['clientSecret']; //Google CLIENT SECRET
    $google_redirect_uri = Yii::app()->params['social']['googlelogin']['redirectUri'];  //return url (url to script)
    $homeUrl = Yii::app()->params['social']['googlelogin']['homeUrl'];  //return to home

//setup new google client
$client = new Google_Client();
$client->setApplicationName('Mavwealth');
$client->setClientid($google_client_id);
$client->setClientSecret($google_client_secret);
$client->setRedirectUri($google_redirect_uri);
$client->setAccessType('online');
$client->setScopes(array('https://www.googleapis.com/auth/userinfo.email','https://www.googleapis.com/auth/userinfo.profile','https://www.google.com/m8/feeds'));