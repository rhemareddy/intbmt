<?php

/**
 * This is the model class for table "holiday_calendar".
 *
 * The followings are the available columns in table 'holiday_calendar':
 * @property integer $id
 * @property string $year
 * @property string $date
 * @property string $event
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class HolidayCalendar extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'holiday_calendar';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date, event, status, created_at, updated_at', 'required'),
			array('year', 'length', 'max'=>4),
			array('event', 'length', 'max'=>50),
			array('status', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, year, date, event, status, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'year' => 'Year',
			'date' => 'Date',
			'event' => 'Event',
			'status' => 'Status',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('event',$this->event,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HolidayCalendar the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public static function create($postDataArray) {
        $leaveObject = new HolidayCalendar();
        if (!empty($postDataArray['holiday_id'])) {
            $leaveObject = self::read($postDataArray['holiday_id']);
        }
        $leaveObject->year = date('Y');
        $leaveObject->date = BaseClass::dateFormate($postDataArray['date']);
        $leaveObject->event = $postDataArray['event'];
        $leaveObject->status = "ACTIVE";
        $leaveObject->created_at = date('Y-m-d');
        $leaveObject->save(false);
        return $leaveObject;
    }

    public static function read($leaveId) {
        return self::model()->findByPk($leaveId);
    }
}
