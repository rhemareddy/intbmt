<?php

/**
 * This is the model class for table "user_profile".
 *
 * The followings are the available columns in table 'user_profile':
 * @property integer $id
 * @property integer $user_id
 * @property string $alternative_no
 * @property string $address
 * @property integer $department_id
 * @property integer $designation_id
 * @property string $joined_on
 * @property integer $branch_id
 * @property integer $blood_group_id
 * @property string $dob
 * @property string $avatar
 * @property string $profile
 */
class UserProfile extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_profile';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, address, department_id, designation_id, joined_on, branch_id, blood_group_id, dob', 'required'),
			array('user_id, department_id, designation_id, branch_id, blood_group_id', 'numerical', 'integerOnly'=>true),
			array('alternative_no', 'length', 'max'=>15),
			array('address, avatar, profile', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, alternative_no, address, department_id, designation_id, joined_on, branch_id, blood_group_id, dob', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'alternative_no' => 'Alternative No',
			'address' => 'Address',
			'department_id' => 'Department',
			'designation_id' => 'Designation',
			'joined_on' => 'Joined On',
			'branch_id' => 'Branch',
			'blood_group_id' => 'Blood Group',
			'dob' => 'Dob',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('alternative_no',$this->alternative_no,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('department_id',$this->department_id);
		$criteria->compare('designation_id',$this->designation_id);
		$criteria->compare('joined_on',$this->joined_on,true);
		$criteria->compare('branch_id',$this->branch_id);
		$criteria->compare('blood_group_id',$this->blood_group_id);
		$criteria->compare('dob',$this->dob,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return UserProfile the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
         * Create user Profile data
         * @param type $postDataArray
         * @return \UserProfile
         * @throws CHttpException
         */
        public static function create($postDataArray){
            
            $userProfileObject = new UserProfile();
            $userProfileObject->user_id = $postDataArray['user_id'];
            $userProfileObject->alternative_no = $postDataArray['alternative'];
            $userProfileObject->address = $postDataArray['address'];
            $userProfileObject->department_id = $postDataArray['department_id'];
            $userProfileObject->designation_id = $postDataArray['designation_id'];
            $userProfileObject->joined_on = BaseClass::dateFormate($postDataArray['joined']);
            $userProfileObject->branch_id = $postDataArray['branch_id'];
            $userProfileObject->blood_group_id = $postDataArray['blood_group_id'];
            $userProfileObject->dob = BaseClass::dateFormate($postDataArray['date_of_birth']);
            if (!$userProfileObject->save(false)) {
                throw new CHttpException(404, 'Unable to find the code you requested.');
            }
            return $userProfileObject;
        }
}
