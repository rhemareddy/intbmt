<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property integer $organization_id
 * @property string $full_name
 * @property string $email
 * @property string $password
 * @property integer $phone_no
 * @property string $gender
 * @property integer $reporting_to
 * @property string $avatar
 * @property string $role
 * @property string $status
 * @property string $status_comment
 * @property string $created_at
 * @property string $updated_at
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('organization_id, full_name, email, password, phone_no, status_comment, created_at, updated_at', 'required'),
			array('organization_id, phone_no, reporting_to', 'numerical', 'integerOnly'=>true),
			array('full_name', 'length', 'max'=>50),
			array('email, password, avatar, status_comment', 'length', 'max'=>100),
			array('gender', 'length', 'max'=>6),
			array('role', 'length', 'max'=>9),
			array('status', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, organization_id, full_name, email, password, phone_no, gender, reporting_to, avatar, role, status, status_comment, created_at, updated_at','available_leave', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'organization' => array(self::BELONGS_TO, 'Organization', 'organization_id'),
                    'reportingTo' => array(self::BELONGS_TO, 'User', 'reporting_to'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'organization_id' => 'Organization',
			'full_name' => 'Full Name',
			'email' => 'Email',
			'password' => 'Password',
			'phone_no' => 'Phone No',
			'gender' => 'Gender',
			'reporting_to' => 'Reporting To',
			'avatar' => 'Avatar',
			'role' => 'Role',
			'available_leave' => 'Available Leave',
			'status' => 'Status',
			'status_comment' => 'Status Comment',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('organization_id',$this->organization_id);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('phone_no',$this->phone_no);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('reporting_to',$this->reporting_to);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('role',$this->role,true);
		$criteria->compare('available_leave',$this->available_leave,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('status_comment',$this->status_comment,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
         * Create user 
         * @param array $postDataArray
         * @return \User
         */
        public static function create($postDataArray){
            $userObject = new User();
            if(isset($postDataArray['user_id']) && !empty($postDataArray['user_id'])){
                $userObject = self::readByPrimaryId($postDataArray['user_id']);
            }
            $userObject->organization_id= Yii::app()->session['orgId'];
            $userObject->available_leave= $postDataArray['available_leave'];
            $userObject->full_name= $postDataArray['full_name'];
            $userObject->email= $postDataArray['email'];
            $userObject->password= md5('intbmt123');
            $userObject->phone_no= $postDataArray['phone'];
            $userObject->reporting_to= 1;
            $userObject->gender= $postDataArray['gender'];
            $userObject->avatar= $postDataArray['avatar'];
            $userObject->role= 'USER';
            $userObject->created_at = date('Y-m-d');
            $userObject->save(false);

            $postDataArray['user_id'] = $userObject->id;
            UserProfile::model()->create($postDataArray);
            return $userObject;
        }
        
        public static function readById($userId){
            return UserProfile::model()->findByAttributes(array('user_id'=>$userId));
        }
        
        public static function readByPrimaryId($userId){
            return self::model()->findByPk($userId);
        }
        
        public static function readReportingUser($userId){
            $userObject = self::readByPrimaryId($userId);
            return $userObject->reportingTo();
        }
}
