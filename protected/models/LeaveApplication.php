<?php

/**
 * This is the model class for table "leave_application".
 *
 * The followings are the available columns in table 'leave_application':
 * @property integer $id
 * @property integer $user_id
 * @property integer $applied_to
 * @property string $to_date
 * @property string $from_date
 * @property string $reason
 * @property string $status
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 */
class LeaveApplication extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'leave_application';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, applied_to, to_date, from_date, reason, status, comment, created_at, updated_at', 'required'),
            array('user_id, applied_to', 'numerical', 'integerOnly' => true),
            array('status', 'length', 'max' => 8),
            array('comment', 'length', 'max' => 100),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, applied_to, to_date, from_date, reason, status, comment, created_at, updated_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'toUser' => array(self::BELONGS_TO, 'User', 'user_id'),
            'fromUser' => array(self::BELONGS_TO, 'User', 'applied_to'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'applied_to' => 'Applied To',
            'to_date' => 'To Date',
            'from_date' => 'From Date',
            'reason' => 'Reason',
            'status' => 'Status',
            'comment' => 'Comment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('applied_to', $this->applied_to);
        $criteria->compare('to_date', $this->to_date, true);
        $criteria->compare('from_date', $this->from_date, true);
        $criteria->compare('reason', $this->reason, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('comment', $this->comment, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LeaveApplication the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function create($postDataArray) {
        $leaveObject = new LeaveApplication();
        if (!empty($postDataArray['leave_id'])) {
            $leaveObject = self::read($postDataArray['leave_id']);
        }
        $leaveObject->user_id = Yii::app()->session['orgId'];
        $leaveObject->applied_to = $postDataArray['reporting_to'];
        $leaveObject->to_date = BaseClass::dateFormate($postDataArray['to_date']);
        $leaveObject->from_date = BaseClass::dateFormate($postDataArray['from_date']);
        $leaveObject->reason = $postDataArray['reason'];
        $leaveObject->status = "PENDING";
        $leaveObject->created_at = date('Y-m-d');
        $leaveObject->save(false);
        return $leaveObject;
    }

    public static function read($leaveId) {
        return self::model()->findByPk($leaveId);
    }

}
