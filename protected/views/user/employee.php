<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Employees
        <small><?php echo Yii::app()->session['orgName']; ?></small>
        <small><strong><i>
            <?php if(Yii::app()->user->hasFlash('success')):?>
                <div class="info text-green">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
                </i></strong>
        </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Employees</a></li>
        <li class="active">List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Employee List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Full Name</th>
                                <th>Reporting To</th>
                                <th>Email</th>
                                <th>Phone No</th>
                                <th>Available Leave</th>
                                <th>Gender</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($employeeListObject as $employeeObject) {  ?>
                            <tr>
                                <td><?php echo $employeeObject->full_name; ?></td>
                                <td><?php echo $employeeObject->reportingTo()->full_name; ?></td>
                                <td><?php echo $employeeObject->email; ?></td>
                                <td><?php echo $employeeObject->phone_no; ?></td>
                                <td><?php echo $employeeObject->available_leave; ?></td>
                                <td><?php echo $employeeObject->gender; ?></td>
                                <td><?php echo $employeeObject->role; ?></td>
                                <td><?php echo $employeeObject->status; ?></td>
                                <td>
                                    <select class="form-control input-sm" onchange="getAction(this.value,<?php echo $employeeObject->id; ?>);">
                                        <option value="">Select Action</option>
                                        <option value="Edit">Edit</option>
                                        <option value="ACTIVE">ACTIVE</option>
                                        <option value="EXIT">EXIT</option>
                                        <option value="TERMINATED">TERMINATED</option>
                                        <option value="FIRED">FIRED</option>
                                        <option value="SUSPENDED">SUSPENDED</option>
                                    </select>
                                </td>
                              
                            </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Full Name</th>
                                <th>Reporting To</th>
                                <th>Email</th>
                                <th>Phone No</th>
                                <th>Available Leave</th>
                                <th>Gender</th>
                                <th>Role</th>
                                <th>Status</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<script>
    function getAction(action,id){
        if(action=='Edit'){
            window.location.href = '/user/add?uId='+id; 
        } else {
            window.location.href = '/user/changestatus?uId='+id+"&action="+action; 
        }
    }
    </script>
    
