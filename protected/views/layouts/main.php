<?php $this->renderPartial('//layouts/header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php echo $content; ?>
</div>
<!-- /.content-wrapper -->
<?php $this->renderPartial('//layouts/footer'); ?>
