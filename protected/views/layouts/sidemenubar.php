
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
<!--        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
                <a href="/">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>Employee</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/user/add"><i class="fa fa-toggle-off"></i> Add</a></li>
                    <li><a href="/user/employee"><i class="fa fa-toggle-off"></i> List</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-sign-out"></i>
                    <span>Leaves / Holiday</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/leave/holiday"><i class="fa fa-toggle-off"></i>Holiday</a></li>
                    <li><a href="/leave/list"><i class="fa fa-toggle-off"></i> Leaves</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-flag"></i>
                    <span>Reports</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/report/employee"><i class="fa fa-toggle-off"></i>Employee</a></li>
                    <li><a href="/report/attendance"><i class="fa fa-toggle-off"></i>Attendance</a></li>
                    <li><a href="/report/announcements"><i class="fa fa-toggle-off"></i>Announcements</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/settings/designation"><i class="fa fa-toggle-off"></i>Manage Designation</a></li>
                    <li><a href="/settings/department"><i class="fa fa-toggle-off"></i>Manage Department</a></li>
                    <li><a href="/settings/branch"><i class="fa fa-toggle-off"></i>Mange Branch</a></li>
                    <li><a href="/settings/profile"><i class="fa fa-toggle-off"></i>Mange Profile</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>