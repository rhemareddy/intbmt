<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Manage Leave
        <small><?php echo Yii::app()->session['orgName']; ?></small>
        <small><strong><i>
            <?php if(Yii::app()->user->hasFlash('success')):?>
                <div class="info text-green">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
                </i></strong>
        </small>
    </h1>
    
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/leave/list">Leave / Holiday</a></li>
        <li class="active">Apply Leave</li>
    </ol>
</section>
<?php // echo "<pre>"; print_r();?>
<!-- Main content -->
<section class="content">
    
    <form name="add_emp_frm" id="add_emp_frm" method="post" action="">
        <input type="hidden" value="<?php echo (isset($leaveObject)&& !empty($leaveObject))?$leaveObject->id:""; ?>" name="leave_id" class="form-control">
    <div class="row">
        <!-- /.col (left) -->
        
        <div class="col-md-12">
          <div class="box box-primary col-md-8">
            <div class="box-header">
              <h3 class="box-title">Apply Leave</h3>
              <a href="/leave/list" class="btn btn-block btn-primary btn-sm pull-right" style="max-width: 100px;">List&nbsp;<i class="fa fa-bars"></i></a>
            </div>
            <div class="box-body col-md-8" >
              <!-- Date -->
              <div class="form-group">
                  <label>Reporting to: <strong class="text-red">&nbsp;<?php echo (isset($reportingUser) && !empty($reportingUser))?$reportingUser->full_name:""?></strong></label>
                  <input type="hidden" class="form-control pull-right"value="<?php echo (isset($reportingUser)&& !empty($reportingUser))?$reportingUser->id:""; ?>" name="reporting_to" required>

                <!-- /.input group -->
              </div>
             
            <div class="form-group">
                <label>To Date:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                    <input type="text" class="form-control pull-right datepicker"value="<?php echo (isset($leaveObject)&& !empty($leaveObject))?$leaveObject->to_date:""; ?>"  placeholder="To Date" name="to_date" required>
                </div>
                <!-- /.input group -->
              </div>
            <div class="form-group">
                <label>From Date:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                    <input type="text" class="form-control pull-right datepicker"value="<?php echo (isset($leaveObject)&& !empty($leaveObject))?$leaveObject->from_date:""; ?>"  placeholder="From Date" name="from_date" required>
                </div>
                <!-- /.input group -->
              </div>
            <div class="form-group">
                <label>Accession:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-file-text"></i>
                  </div>
                    <textarea type="text" class="form-control pull-right"  placeholder="Enter Reason"  name="reason" required><?php echo (isset($leaveObject)&& !empty($leaveObject))?$leaveObject->reason:""; ?></textarea>
                </div>
                <!-- /.input group -->
              </div>
          
              <div class="form-group  pull-right" style="max-width: 100px;">
                  <input type="submit" class="btn btn-block btn-info" value="Submit">
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- /.form group -->

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col (right) -->
      </div>
    </form>
    <!-- /.row -->
</section>
