<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Manage Holiday Calendar 
        <small><?php echo Yii::app()->session['orgName']; ?></small>
        <small><strong><i>
            <?php if(Yii::app()->user->hasFlash('success')):?>
                <div class="info text-green">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
                </i></strong>
        </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/leave/list">Leave / Holiday</a></li>
        <li class="active">Holiday Calendar</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Holiday Calendar List</h3>
                    <a href="/leave/addholiday" class="btn btn-block btn-primary btn-sm pull-right" style="max-width: 100px;">Add &nbsp;<i class="fa fa-plus"></i></a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Year</th>
                                <th>Date</th>
                                <th>Event</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($holidayCalendarListObject as $holidayCalendarObject) {  
//                                echo "<pre>"; print_r($holidayCalendarObject);exit;
                                ?>
                            <tr>
                                <td><?php echo $holidayCalendarObject->year; ?></td>
                                <td><?php echo $holidayCalendarObject->date; ?></td>
                                <td><?php echo $holidayCalendarObject->event; ?></td>
                                <td><?php echo $holidayCalendarObject->status; ?></td>
                                <td><a href="/leave/addholiday?hId=<?php echo $holidayCalendarObject->id; ?>"><i class="fa fa-edit"></i></a></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                             <tr>
                                <th>Year</th>
                                <th>Date</th>
                                <th>Event</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
