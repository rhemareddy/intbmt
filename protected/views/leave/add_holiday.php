<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Manage Holiday
        <small><?php echo Yii::app()->session['orgName']; ?></small>
        <small><strong><i>
            <?php if(Yii::app()->user->hasFlash('success')):?>
                <div class="info text-green">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
                </i></strong>
        </small>
    </h1>
    
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/leave/list">Leave / Holiday</a></li>
        <li class="active">Apply Leave</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    
    <form name="add_holiday_frm" id="add_holiday_frm" method="post" action="">
    <input type="hidden" value="<?php echo (isset($holidayObject)&& !empty($holidayObject))?$holidayObject->id:""; ?>" name="holiday_id" class="form-control">
    <div class="row">
        <!-- /.col (left) -->
        
        <div class="col-md-12">
          <div class="box box-primary col-md-8">
            <div class="box-header">
              <h3 class="box-title">Add Holiday</h3>
              <a href="/leave/holiday" class="btn btn-block btn-primary btn-sm pull-right" style="max-width: 100px;">List&nbsp;<i class="fa fa-bars"></i></a>
            </div>
            <div class="box-body col-md-8" >
           
            <div class="form-group">
            <label>Date:</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
                <input type="text" class="form-control pull-right datepicker"value="<?php echo (isset($holidayObject)&& !empty($holidayObject))?$holidayObject->date:""; ?>"  placeholder="Date" name="date" required>
            </div>
            <!-- /.input group -->
          </div>
            
            <div class="form-group">
                <label>Accession:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-file-text"></i>
                  </div>
                    <textarea type="text" class="form-control pull-right"  placeholder="Enter Accession"  name="event" required><?php echo (isset($holidayObject)&& !empty($holidayObject))?$holidayObject->event:""; ?></textarea>
                </div>
                <!-- /.input group -->
              </div>
          
              <div class="form-group  pull-right" style="max-width: 100px;">
                  <input type="submit" class="btn btn-block btn-info" value="Submit">
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- /.form group -->

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
        </div>
        <!-- /.col (right) -->
      </div>
    </form>
    <!-- /.row -->
</section>
