<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Manage Leaves
        <small><?php echo Yii::app()->session['orgName']; ?></small>
        <small><strong><i>
            <?php if(Yii::app()->user->hasFlash('success')):?>
                <div class="info text-green">
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
                </i></strong>
        </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Employees</a></li>
        <li class="active">Leave</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->

            <div class="box">
                <div class="box-header">  
                    <h3 class="box-title">Leaves List</h3>
                    <a href="/leave/apply" class="btn btn-block btn-primary btn-sm pull-right" style="max-width: 100px;">Apply&nbsp;<i class="fa fa-plus"></i></a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Employee</th>
                                <th>Applied To</th>
                                <th>To Date</th>
                                <th>From Date</th>
                                <th>Reason</th>
                                <th>comment</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($leavesListObject as $leaveObject) {
//                                echo "<pre>"; print_r($leaveObject);exit;
                                ?>
                                <tr>
                                    <td><?php echo $leaveObject->toUser()->full_name; ?></td>
                                    <td><?php echo $leaveObject->fromUser()->full_name; ?></td>
                                    <td><?php echo $leaveObject->to_date; ?></td>
                                    <td><?php echo $leaveObject->from_date; ?></td>
                                    <td><?php echo $leaveObject->reason; ?></td>
                                    <td><?php echo $leaveObject->comment; ?></td>
                                    <td><?php echo $leaveObject->status; ?></td>
                                    <td>
                                        <select class="form-control input-sm" onchange="getLeaveAction(this.value,<?php echo $leaveObject->id; ?>);">
                                            <option value="">Select Action</option>
                                            <option value="Edit">Edit</option>
                                            <option value="APPROVED">APPROVED</option>
                                            <option value="REJECTED">REJECTED</option>
                                            <option value="CANCEL">CANCEL</option> 
                                        </select>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Employee</th>
                                <th>Applied To</th>
                                <th>To Date</th>
                                <th>From Date</th>
                                <th>Reason</th>
                                <th>comment</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- Trigger the modal with a button -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <form name="leave_comment_frm" id="leave_comment_frm" method="post" action="/leave/updatecomment">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Leave Comment: <span id="selected_status" class="text-red"></span></h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-file-text"></i>
                  </div>
                    <input type="hidden" value="" name="leave_id" id="leave_id" />
                    <input type="hidden" value="" name="leave_status" id="leave_status" />
                    <textarea type="text" class="form-control pull-right" placeholder="Enter Comment"  id="leave_comment"  name="leave_comment" required></textarea>
                </div>
            <span id="comment_error" class="text-red"></span>
                <!-- /.input group -->
              </div>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-block btn-primary btn-sm pull-right" style="max-width: 60px;" onclick="leaveComment();">Submit</button>
      </div>
            </form>
    </div>

  </div>
</div>
<script>
    function getLeaveAction(action, id) {
        
        if (action == 'Edit') {
            window.location.href = '/leave/apply?lId=' + id;
        } else {
            $('#leave_id').val(id);
            $('#leave_status').val(action);
            $('#selected_status').html(action);
            $('#myModal').modal('show');
        }
    }
    
    function leaveComment(){
        $("#comment_error").html = "Please enter comment";
        if($("#leave_comment").val()==""){
            $("#comment_error").html("Please enter comment");
        } else {
            $("#leave_comment_frm").submit();
        }
    }
</script>
