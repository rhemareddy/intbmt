<?php 
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Promo Code'
);

Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/buttons.dataTables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');

if (isset($_GET['id'])) {
    $classAddEdit = "active"; //Add tab making as Edit
    $type = "EDIT";
    $actionUrl = "/admin/promo/index?id=" . $_GET['id'];
} else {
    $classList = "active"; 
    $actionUrl = "/admin/promo/index";
}

//echo '<pre>';print_r($promoObject->user());exit;

?>
<script src="/js/datatables.min.js"></script>

<?php if (Yii::app()->user->hasFlash('error')){ ?>
    <p class="alert alert-danger" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </span></p>
<?php } ?>
<?php if (Yii::app()->user->hasFlash('success')){ ?>
    <p class="alert alert-success" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </span></p>
<?php } ?>

<div class="row">
     <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
    <div class="col-md-12 margin-top-20 filter-toggle">
        
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?php echo isset($classList) ? $classList : ""; ?>"><a onclick="showAddEditTab();" href="#list" id="link-banner-list" aria-controls="profile" role="tab" data-toggle="tab">LIST</a></li>
            <li role="presentation" class="<?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>"><a href="#addedit" id="link-banner-addedit" aria-controls="home" role="tab" data-toggle="tab"><?php echo isset($type) ? $type : "ADD"; ?></a></li>
        </ul>
        
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>" id="addedit">
            <div class="col-md-9 col-sm-9">
                <form enctype="multipart/form-data" name="addeditpromo" action="<?php echo $actionUrl; ?>" method="post" onsubmit="return promoValidation();">
                    <div class="portlet box toe-blue">
                        <div class="portlet-title">
                            <div class="caption">
                            <h4><?php echo isset($type) ? $type : "ADD"; ?> Promo </h4>
                            </div>
                        </div>

                        <div class="portlet-body form padding15">
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Promo Code *</label>
                                    
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="promocode" id="promocode" value="<?php echo isset($promoObject->code) ? $promoObject->code : ""; ?>" onchange="promoExistCheck();">
                                        <span id="error_code" value="0" style="color:red;"></span>
                                        <span id="code_available" class="text-success"></span>
                                    </div>
                                    <div class="col-md-2">
                                       <input type="button" name="generatePromoCode" id="generatePromoCode" value="Generate Code" onClick="getRandomPromo();" class="btn btn-primary"> 
                                    </div>
                                    
                                </div>

                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Can Use *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="can_use" id="can_use" value="<?php echo !empty($_GET['id']) && isset($promoObject->can_use) ? $promoObject->can_use : ''; ?>" onkeyup="return isValidAmount(this);">
                                        <span id="error_can_use" style="color:red;"></span>
                                    </div>
                                </div>
                            
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Type *</label>
                                    <div class="col-sm-8">
                                        <select name="type" id="type" class="form-control">
                                            <option value="">Select</option>
                                            <option value="PERCENTAGE" <?php echo (!empty($_GET['id']) && $promoObject->type == "PERCENTAGE")?'selected="selected"':''; ?>>Percentage</option>
                                            <option value="VALUE" <?php echo (!empty($_GET['id']) && $promoObject->type == "VALUE")?'selected="selected"':''; ?>>Value</option>
                                        </select> 
                                        <span id="error_type" style="color:red;"></span>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Promo Value *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="promovalue" id="promovalue" value="<?php echo !empty($_GET['id']) && isset($promoObject->value) ? $promoObject->value : ""; ?>" onkeyup="return isValidAmount(this);">
                                        <span id="error_promovalue" style="color:red;"></span>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Start Date *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control onlydate" name="start_date" id="start_date" value="<?php echo !empty($_GET['id']) && isset($promoObject->start_date) ? $promoObject->start_date : ""; ?>">
                                        <span id="error_start_date" style="color:red;"></span>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Close Date </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control onlydate" name="close_date" id="close_date" value="<?php echo !empty($_GET['id']) && isset($promoObject->close_date) ? $promoObject->close_date : ""; ?>">
                                        <span id="error_close_date" style="color:red;"></span>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Username </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="user_name" id="user_name" value="<?php echo (!empty($_GET['id']) && !empty($promoObject->user->name)) ? $promoObject->user->name : ""; ?>" onchange="getFullName(this.value);">
                                        <input type="hidden" name="user_id" id="search_user_id" value="<?php echo (!empty($_GET['id']) && !empty($promoObject->user_id)) ? $promoObject->user_id : ""; ?>" />
                                        <span id="search_fullname">&nbsp;</span>
                                        <span id="search_user_error" style="color:red;"></span>
                                        <span id="error_user_id" style="color:red;"></span>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right mobile-left-15">Status </label>
                                    <div class="col-sm-8">
                                        <select name="status" id="status" class="form-control">
                                            <option value="1" <?php echo (!empty($_GET['id']) && $promoObject->status == 1)?'selected="selected"':''; ?>>Active</option>
                                            <option value="0" <?php echo (!empty($_GET['id']) && $promoObject->status == 0)?'selected="selected"':''; ?>>Inactive</option>
                                        </select> 
                                        <span id="error_status" style="color:red;"></span>
                                    </div>
                                </div>

                             <input type="submit" name="addedit_submit" id="addedit_submit" class="btn green pull-right">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php echo isset($classList) ? $classList : ""; ?>" id="list">
        
        <div class="user-status col-md-12">
            <form id="promo_filter_frm" name="promo_filter_frm" method="post" action="/admin/promo" >
                <div class="col-md-4"> 
                    <span id="date_error" style="color:red"></span>
                    <div class="input-group input-large date-picker input-daterange pull-left">
                        <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
                        <span class="input-group-addon"> to </span>
                        <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 bulkSelect">
                    <div class="form-group">
                        <label class="col-md-2 col-sm-2 padding0 top7 mobile-left-15"> Status </label>
                        <div class="col-md-4 col-sm-4">
                            <select class=" form-control " id="promoStatus" name="promoStatus">
                                <option value="active">Active</option>
                                <option value="inactive">InActive</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-sm-6 form-inline">
                    <input type="button" class="btn btn-success margintop3 mobile-left-15" value="Filter" name="promoCodeFilter" id="promoCodeFilter"/>
                </div>
            </form>
        </div>
        <div class="row ">
            <div class="col-md-12 responsiveTable margin-top-15">
                <table id="promocodelist" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Promo Code</th>
                            <th>Can Use</th>
                            <th>Value</th>
                            <th>Type</th>
                            <th>Start Date</th>
                            <th>Close Date</th>
                            <th>User</th>
                            <th>Status</th>
                            <th>Created Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>

                <script src="/js/dataTables.buttons.min.js"></script>
                <script src="/js/jszip.min.js"></script>
                <script src="/js/buttons.html5.min.js"></script>
                <script src="/js/buttons.print.min.js"></script>

                <script>
                    $(document).ready(function () {
                        oTable = $('#promocodelist').DataTable({
                            "responsive": true,
                            "iDisplayLength": 50,
                            "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                            "processing": true,
                            "serverSide": true,
                            "paging": true,
                            "bSort": true,
                            "pagingType": "full_numbers",
                            "order": [[9, "desc"]],
                            "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                            "buttons": [
                                {
                                    extend: 'csv',
                                    exportOptions: {
                                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                                    }
                                },
                                {
                                    extend: 'copyHtml5',
                                    exportOptions: {
                                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                                    }
                                },
                                {
                                    extend: 'excelHtml5',
                                    exportOptions: {
                                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                                    }
                                },
                                {
                                    extend: 'print',
                                    exportOptions: {
                                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                                    }
                                },
                            ],
                            
                            "ajax": {
                                "url": "/admin/promo/promolistdata",
                                "type": "POST"
                            },
                            "columns": [
                                {
                                    "data": "id",
                                    render: function (data, type, row, meta) {
                                        return meta.row + meta.settings._iDisplayStart + 1;
                                    }
                                },
                                {"data": "code"},
                                {"data": "canUse"},
                                {"data": "value"},
                                {"data": "type"},
                                {"data": "startDate"},
                                {"data": "closeDate"},
                                {"data": "userName"},
                                {"data": "promoStatus"},
                                {"data": "createdDate"},
                                {
                                    render: function (data, type, row) { 
                                            return '<a href="javascript:void(0);" onclick="getPromoEditLink(' + row.id + ')" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="getUsageLink(' + row.id + ')" title="Usage View"><i class="fa fa-eye"></i></a>';
                                    },
                                },
                            ],
                            
                        });
                        
                    });

                    $("#promoCodeFilter").click(function() {
                    $("#date_error").html("");
                    if($("#toDate").val()>$("#fromDate").val()){
                        $("#date_error").html("To Date should be greater then From Date!!!");
                        $("#fromDate").focus();
                        return false;
                    }
                    oTable.columns(7).search($("#promoStatus").val().trim());
                    oTable.columns(8).search($("#toDate").val().trim(),$("#fromDate").val().trim());

                    oTable.draw();
                    });
                </script>
            </div>
        </div>  
    </div>
</div>
        
</div>
</div>