<?php

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Promo Usage List'
);

    Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
    Yii::app()->clientScript->registerCssFile('/css/buttons.dataTables.min.css');
    Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');
?>

<a style="float:right;font-size:16px;color:#428bca;cursor:pointer;" onclick="window.history.back(-1);">Go Back &gt;&gt;</a>

<script src="/js/datatables.min.js"></script>

<div class="row ">
    <div class="col-md-12 responsiveTable margin-top-15">
        <table id="promousagelistdata" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Sl No</th>
                    <th>Promo Code</th>
                    <th>User</th>
                    <th>Transaction Id</th>
                    <th>Amount</th>
                    <th>Cashback</th>
                    <th>Used Date</th>
                </tr>
            </thead>
        </table>

        <script src="/js/dataTables.buttons.min.js"></script>
        <script src="/js/jszip.min.js"></script>
        <script src="/js/buttons.html5.min.js"></script>
        <script src="/js/buttons.print.min.js"></script>

        <script>
            $(document).ready(function () {
                oTable = $('#promousagelistdata').DataTable({
                    "responsive": true,
                    "iDisplayLength": 50,
                    "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                    "processing": true,
                    "serverSide": true,
                    "paging": true,
                    "bSort": true,
                    "pagingType": "full_numbers",
                    "order": [[6, "desc"]],
                    "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                    "buttons": [
                                    {
                                        extend: 'csv',
                                        exportOptions: {
                                            columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                        }
                                    },
                                    {
                                        extend: 'copyHtml5',
                                        exportOptions: {
                                            columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                        }
                                    },
                                    {
                                        extend: 'excelHtml5',
                                        exportOptions: {
                                            columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                        }
                                    },
                                    {
                                        extend: 'print',
                                        exportOptions: {
                                            columns: [ 0, 1, 2, 3, 4, 5, 6 ]
                                        }
                                    },
                                ],

                    "ajax": {
                        "url": "/admin/promo/usagelistdata?id=<?php echo isset($_GET['id']) ? $_GET['id'] : ''; ?>",
                        "type": "POST"
                    },
                    "columns": [
                        {
                            "data": "id",
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        {"data": "promoCode"},
                        {"data": "userName"},
                        {"data": "transactionId"},
                        {"data": "packagePrice"},
                        {"data": "promoCashback"},
                        {"data": "usedDate"},
                    ],

                });

            });
        </script>
    </div>
</div>