<?php
$this->breadcrumbs = array(
    'News' => array('news/list'),
    'News Edit',
);
?>
<div class="col-md-7 col-sm-7">
    <?php if(!empty($_GET['error'])){?><div class="error" id="error_msg"><?php echo $_GET['error'];?></div><?php }?>
    <?php if(!empty($_GET['success'])){?><div class="success" id="error_msg"><?php echo $_GET['success'];?></div><?php }?>
   <div class="caption">
           Edit News
    </div>
    <form action="/admin/news/edit?id=<?php echo $newsObject->id;?>" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validation();">
     
         <fieldset>
 <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">News Title<span class="require">*</span></label>
                <div class="col-lg-7">
                    <input type="text" name="title" id="title" class="form-control" value="<?php echo (!empty($newsObject))?$newsObject->title : "";?>"> 
                    <span style="color:red;" id="title_error"></span>
                </div>
 </div>  
 <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">News<span class="require">*</span></label>
                <div class="col-lg-7">
                    <textarea id="news" class="form-control" name="news"><?php echo (!empty($newsObject))?$newsObject->news : "";?></textarea>
                    <span style="color:red;" id="news_error"></span>
                </div>
 </div>
        </fieldset>

    <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Submit" class="btn red">
            </div>
        </div>
    </form>
</div>





<script type="text/javascript">
    function validation()
    {
        $("#title_error").html("");
        if ($("#title").val() == "") {
            $("#title_error").html("Enter News Title");
            $("#title").focus();
            return false;
        }
        
        $("#news_error").html("");
        if ($("#news").val() == "") {
            $("#news_error").html("Enter News Content");
            $("#news").focus();
            return false;
        }
         
    }
    </script>
     
     
