<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'News Report'
);
?>
<style>
    .confirmOk{left: 350px;
    position: absolute;
    top: 8px;}
    .confirmExport{left: 410px;
    position: absolute;
    top: 8px;}
    .confirmMenu{position: relative;}
</style>
<div class="col-md-12">
    
    <div class="expiration margin-topDefault confirmMenu">
                    
    <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/news/list" class="form-inline">
    <div class="input-group input-large date-picker input-daterange">
        <input type="text" name="from" placeholder="To Date" class="datepicker form-control" value="<?php echo (!empty($todayDate)) ?  $todayDate :  DATE('Y-m-d');?>">
        <span class="input-group-addon">
        to </span>
        <input type="text" name="to" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="<?php echo (!empty($fromDate)) ?  $fromDate :  DATE('Y-m-d');?>">
    </div>
     
    
    </div>
    <input type="submit" class="btn btn-primary confirmOk" value="OK" name="submit" id="submit"/>
   
    </form>

</div>
<!--+-->
<div class="row">
    <div class="col-md-12">
         <?php if (!empty($_GET['msg']) && $_GET['msg'] == '1') { ?> <div class="success"><?php echo "News Deleted Succesfully." ?></div> <?php } ?>
        <?php if (!empty($_GET['msg']) && $_GET['msg'] == '2') { ?> <div class="success"><?php echo "News Status Changed Succesfully." ?></div> <?php } ?>
         <?php if (!empty($_GET['success'])) { ?> <div class="success"><?php echo $_GET['success'];?></div> <?php } ?>
         <?php if (!empty($_GET['error'])) { ?> <div class="error"><?php $_GET['error'];?></div> <?php } ?>
         
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;color:#01b7f2">Sl.No</span>',
                ),
                 array(
                    'name' => 'title',
                    'header' => '<span style="white-space: nowrap;">Title &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->title',
                ),
                array(
                    'name' => 'news',
                    'header' => '<span style="white-space: nowrap;">News &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->news',
                ),
                
                array(
                    'name' => 'status',
                    'header' => '<span style="white-space: nowrap;">Status &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                ),
                
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Created At &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
               array(
                    'class' => 'CButtonColumn',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'template' => '{Change}{Edit}{Delete}',
                    'htmlOptions' => array('width' => '30%'),
                    'buttons' => array(
                         'Change' => array(
                            'label' => Yii::t('translation', 'Change Status'),
                            'options' => array('class' => 'fa fa-success btn default black delete'),
                            'url' => 'Yii::app()->createUrl("admin/news/changestatus", array("id"=>$data->id))',
                        ),
                        'Edit' => array(
                            'label' => 'Edit',
                            'options' => array('class' => 'fa fa-success btn default black delete blue'),
                            'url' => 'Yii::app()->createUrl("admin/news/edit", array("id"=>$data->id))',
                        ),
                        'Delete' => array(
                            'label' => Yii::t('translation', 'Delete'),
                            'options' => array('class' => 'fa fa-success btn default black delete red', 'onclick' => "js:alert('Do u want to delete this news?')"),
                            'url' => 'Yii::app()->createUrl("admin/news/deletenews", array("id"=>$data->id))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>
