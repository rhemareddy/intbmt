<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Resource Library' => '/admin/library/category',
    'Manage Category'
);

Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/buttons.dataTables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');

if (isset($_GET['id'])) {
    $classAddEdit = "active"; //Add tab making as Edit
    $type = "EDIT";
    $actionUrl = "/admin/library/category?id=" . $_GET['id'];
} else {
    $classList = "active";
    $actionUrl = "/admin/library/category";
}
?>
<script src="/js/datatables.min.js"></script>

        
<?php if (Yii::app()->user->hasFlash('success')) { 
    echo '<p class="alert alert-success" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">'.Yii::app()->user->getFlash('success').'</span></p>';
    }
    if (Yii::app()->user->hasFlash('error')) { 
        echo '<p class="alert alert-danger" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">'.Yii::app()->user->getFlash('error').'</span></p>';
    } ?>

<div class="row">
    <div class="col-md-12 margin-top-20">

        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?php echo isset($classList) ? $classList : ""; ?>"><a onclick="showAddEditTab();" href="#list" id="link-banner-list" aria-controls="profile" role="tab" data-toggle="tab">LIST</a></li>
            <li role="presentation" class="<?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>"><a href="#addedit" id="link-banner-addedit" aria-controls="home" role="tab" data-toggle="tab"><?php echo isset($type) ? $type : "ADD"; ?></a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>" id="addedit">
                <div class="col-md-7 col-sm-7">
                    <form enctype="multipart/form-data" action="<?php echo $actionUrl; ?>" method="post" onsubmit="return libraryCategoryValidation();">

                        <div class="portlet box toe-blue">
                            <div class="portlet-title">
                                <div class="caption">
                                    <h4><?php echo isset($type) ? $type : "ADD"; ?> Category/Subcategory</h4>
                                </div>
                            </div>

                            <div class="portlet-body form padding15">
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Type *</label>
                                    <div class="col-sm-8">
                                        <input type="radio" class="pro-radio" name="type" id="type" value="category" <?php echo ($mediaCenterCategoryObject->parent_id == 0) ? "checked" : ""; ?> checked="" onclick="showCategoryList(this.value);">Category
                                        <input type="radio" class="pro-radio" name="type" id="type" value="subcategory" <?php echo ($mediaCenterCategoryObject->parent_id != 0) ? "checked" : ""; ?> onclick="showCategoryList(this.value);">Subcategory
                                    </div>
                                </div>
                                <div class="row form-group" style="<?php echo (isset($_GET['id']) && ($mediaCenterCategoryObject->parent_id != 0))?"display:block":"display:none"; ?>" id="category_div">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Select Category *</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="parent_id" id="parent_id">
                                            <option value="">Select Category</option>
                                            <?php
                                            if (isset($allMediaCenterCategoryObject) && !empty($allMediaCenterCategoryObject)) {
                                                foreach ($allMediaCenterCategoryObject as $allMediaCenterCategory) {
                                                    ?>
                                                    <option  value="<?php echo $allMediaCenterCategory->id; ?>" <?php echo ($mediaCenterCategoryObject->parent_id == $allMediaCenterCategory->id)?'selected="selected"':''; ?>><?php echo $allMediaCenterCategory->name; ?></option>
                                                <?php }
                                            }
                                            ?>
                                        </select>
                                        <span id="error_parent_id" style="color:red;"></span>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Name *</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="name" id="name" maxlength="50" placeholder="Name" value="<?php echo isset($mediaCenterCategoryObject->name) ? $mediaCenterCategoryObject->name : ""; ?>">
                                        <span id="error_name" style="color:red;"></span>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Priority </label>
                                    <div class="col-sm-8">
                                        <input type="text" name="priority" id="priority" class="form-control" maxlength="10" placeholder="Priority" value="<?php echo (isset($mediaCenterCategoryObject->priority) && $mediaCenterCategoryObject->priority != 25) ? $mediaCenterCategoryObject->priority : ""; ?>"/>
                                        <span id="error_priority" style="color:red;"></span>
                                    </div>
                                </div>
                                <?php 
                                if (isset($_GET['id']) && isset($mediaCenterCategoryObject->image)) {
                                    ?>
                                    <div class="row form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right">Current Image </label>
                                        <div class="col-sm-8">
                                            <img src="<?php echo isset($mediaCenterCategoryObject->image) ? Yii::app()->params['media_center']['category'] . $mediaCenterCategoryObject->image : ""; ?>" style="max-height: 200px;max-width: 200px;" />  
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="row form-group" id="image_div" style="<?php echo (isset($_GET['id']) && ($mediaCenterCategoryObject->parent_id != 0))?"display:none":""; ?>">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Image</label>
                                    <div class="col-sm-8">
                                        <input type="file" name="image" id="image" class="form-control" />
                                        (Upload jpg, jpeg, png, gif files only and max file size is 2MB)
                                        <span id="error_image" style="color:red;"></span>
                                    </div>
                                </div>
                                
                                <div class="row form-group" id="icon_image_div" style="<?php echo ($mediaCenterCategoryObject->parent_id != 0) ? "display:none" : ""; ?> ">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Icon Class</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="icon_image" id="icon_image" class="form-control" maxlength="50" value="<?php echo isset($mediaCenterCategoryObject->icon_image) ? $mediaCenterCategoryObject->icon_image : ""; ?>"/>
                                        <span id="error_icon_image" style="color:red;"></span>
                                    </div>
                                </div>
                              
                                <input type="submit" name="addedit_submit" id="addedit_submit" value="Submit" class="btn green">
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane <?php echo isset($classList) ? $classList : ""; ?>" id="list">

                <form id="new_user_filter_frm" name="library_filter_frm" method="post" action="/admin/library/category" >
                     <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
                    <div class="user-status filter-toggle">
                        <div class="col-md-4 col-sm-4 bulkSelect">
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 padding0 top7 mobile-left-15"> Bulk Action</label>
                                <div class="col-md-5 col-sm-5">
                                    <select class=" form-control  " id="bulkaction" name="bulkaction">
                                        <option value="NA">Select</option>
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                        <option value="Remove">Remove</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-6 form-inline">
                            <input type="submit" class="btn btn-success mobile-left-15" value="OK" name="library_bulkaction_submit" id="library_bulkaction_submit"/>
                        </div>


                        <div class="col-md-5 mobile-margin">          
                            <div class="input-group input-large date-picker input-daterange pull-left">
                                <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
                                <span class="input-group-addon"> to </span>
                                <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
                            </div>
                            <input type="button" class="btn btn-primary f-left margin-left15 margintop3" value="Filter" name="submit" id="btnSearch">
                        </div>

                        <div class="col-md-4 col-sm-4 bulkSelect margin-top-10 m-clear">
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 padding0 top7 mobile-left-15">Status : </label>
                                <div class="col-md-5 col-sm-5">
                                    <select class=" form-control  " id="change_status" name="change_status">
                                        <option value="all" >All</option>
                                        <option value="1" selected="selected">Active</option>
                                        <option value="2">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 bulkSelect margin-top-10">
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 padding0 top7 mobile-left-15">Type: </label>
                                <div class="col-md-5 col-sm-5">
                                    <select class=" form-control  " id="change_category" name="change_category">
                                        <option value="all" selected="selected">All</option>
                                        <option value="1">Category</option>
                                        <option value="2">Subcategory</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                    </div> 

                    <div class="row">
                        <div class="col-md-12 responsiveTable">
                            <br><br>

                            <table id="allLibraryCategoryList" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="flowcheckall" class="editor-active"></th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Main Category</th>
                                        <th>Image</th>
                                        <th>Icon Class</th>
                                        <th>Priority</th>
                                        <th>Create Dt</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>

                            <script>
                                $(document).ready(function () {
                                    oTable = $('#allLibraryCategoryList').DataTable({
                                        "responsive": true,
                                        "iDisplayLength": 50,
                                        "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                                        "processing": true,
                                        "serverSide": true,
                                        "paging": true,
                                        "bSort": true,
                                        "pagingType": "full_numbers",
                                        "order": [[6, "desc"]],
                                        "aoColumnDefs": [{
                                                'bSortable': false,
                                                'aTargets': [0]
                                            }],
                                        "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                                        "ajax": {
                                            "url": "/admin/library/categorylistdata",
                                            "type": "POST"
                                        },
                                        "columns": [
                                            {
                                                data: "id",
                                                render: function (data, type, row) {
                                                    if (type === 'display') {
                                                        return '<input type="checkbox" class="editor-active" name="requestids[' + data + ']" value="' + data + '">';
                                                    }
                                                    return data;
                                                },
                                            },
                                            {"data": "name"},
                                            {"data": "categoryType"},
                                            {"data": "mainCategory"},
                                            {
                                                data: "image",
                                                render: function (data, type, row) {
                                                    if (data != null) {
                                                        return '<img src="<?php echo Yii::app()->params['media_center']['category']; ?>' + data + '" width="50"> ';
                                                    } else {
                                                        return '';
                                                    }
                                                },
                                            },
                                            {"data": "icon_image"},
                                            {
                                                data: "priority",
                                                render: function (data, type, row) {
                                                    if (data != "25") {
                                                        return data;
                                                    } else {
                                                        return '0';
                                                    }
                                                },
                                            },
                                            {"data": "created_at"},
                                            {"data": "categoryStatus"},
                                            {
                                                data: "id",
                                                searchable: false,
                                                orderable: false,
                                                render: function (data, type, row) {
                                                    if (type === 'display') {
                                                        return '<a href="javascript:void(0);" onclick="showLibraryCategoryLink(' + data + ')" title="Edit"><i class="fa fa-pencil-square-o"></i></a>';
                                                    }
                                                    return data;
                                                }
                                            },
                                      
                                        ],

                                        select: {
                                            style: 'os',
                                            selector: 'td:not(:first-child)' // no row selection on last column
                                        },
                                    });
                                });
                                $("#flowcheckall").click(function () {
                                    $('#allLibraryCategoryList tbody input[type="checkbox"]').prop('checked', this.checked);
                                });
                                $("#btnSearch").click(function () {
                                    if ($("#toDate").val() < $("#fromDate").val()) {
                                        alert("To Date should be greater then From Date!!!");
                                        return false;
                                    }

                                    oTable.columns(4).search($("#toDate").val().trim(), $("#fromDate").val().trim());
                                    oTable.draw();
                                });
                                $("#change_status").change(function () {
                                    oTable.columns(5).search($("#change_status").val().trim());
                                    oTable.draw();
                                });
                                $("#change_category").change(function () {
                                    oTable.columns(1).search($("#change_category").val().trim());
                                     oTable.draw();
                                });

                            </script>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>