<?php 
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Resource Library' => '/admin/library/category',
    'Manage Media'
);

Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/buttons.dataTables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');

if (isset($_GET['id'])) {
    $classAddEdit = "active"; //Add tab making as Edit
    $type = "EDIT";
    $actionUrl = "/admin/library/media?id=" . $_GET['id'];
} else {
    $classList = "active"; 
    $actionUrl = "/admin/library/media";
}
 
if (Yii::app()->user->hasFlash('success')) { 
    echo '<p class="alert alert-success" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">'.Yii::app()->user->getFlash('success').'</span></p>';
}
if (Yii::app()->user->hasFlash('error')) { 
    echo '<p class="alert alert-danger" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">'.Yii::app()->user->getFlash('error').'</span></p>';
} 
?>
<script src="/js/datatables.min.js"></script>
<div class="row">
    <div class="col-md-12 margin-top-20">
        
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?php echo isset($classList) ? $classList : ""; ?>"><a onclick="showAddEditTab();" href="#list" id="link-banner-list" aria-controls="profile" role="tab" data-toggle="tab">LIST</a></li>
            <li role="presentation" class="<?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>"><a href="#addedit" id="link-banner-addedit" aria-controls="home" role="tab" data-toggle="tab"><?php echo isset($type) ? $type : "ADD"; ?></a></li>
        </ul>
        
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>" id="addedit">
            <div class="col-md-7 col-sm-7">
                <form enctype="multipart/form-data" action="<?php echo $actionUrl; ?>" method="post" onsubmit="return libraryMediaValidation();">
                    <input type="hidden" name="subcategoryFlag" id="subcategoryFlag" value="0">
                    <div class="portlet box toe-blue">
                        <div class="portlet-title">
                            <div class="caption">
                            <h4><?php echo isset($type) ? $type : "ADD"; ?> Media </h4>
                            </div>
                        </div>

                            <div class="portlet-body form padding15">
                                <div class="row form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Select Category *</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" name="category" id="category" onchange="showSubcategoryList(this.value);">
                                            <option value="">Select Category</option>
                                            <?php if(isset($allMediaCenterCategoryObject) && !empty($allMediaCenterCategoryObject)){
                                            foreach($allMediaCenterCategoryObject as $allMediaCenterCategory){ ?>
                                            <option  value="<?php echo $allMediaCenterCategory->id; ?>" <?php echo ($mediaCenterDocumentObject->category == $allMediaCenterCategory->id)?'selected="selected"':''; ?>><?php echo $allMediaCenterCategory->name; ?></option>
                                            <?php } } ?>
                                        </select>
                                        <span id="error_category" style="color:red;"></span>
                                    </div>
                                </div>

                                <div class="row form-group" id="subcategory_div" style="<?php echo (isset($_GET['id']) && ($mediaCenterDocumentObject->subcategory != 0))?"display:block":"display:none"; ?>">
                                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Select Subcategory *</label>
                                    <div class="col-sm-8" id="subcategory_select">
                                        <?php
                                        if (isset($_GET['id']) && ($mediaCenterDocumentObject->subcategory != 0)) { 
                                            $subcategoryObject = MediaCenterCategory::model()->findAll(array('condition' => 'parent_id =' .$mediaCenterDocumentObject->category)); 
                                        ?>
                                            <select class="form-control" name="subcategory" id="subcategory" onchange="getSubCategoryData(this.value);" >
                                                <option value="">Select Subcategory</option>
                                                <?php 
                                                if(isset($subcategoryObject) && !empty($subcategoryObject)){
                                                foreach($subcategoryObject as $subcategory){ ?>
                                                <option  value="<?php echo $subcategory->id; ?>" <?php echo ($mediaCenterDocumentObject->subcategory == $subcategory->id)?'selected="selected"':''; ?>><?php echo $subcategory->name; ?></option>
                                                <?php } } ?>
                                            </select>
                                        <?php } ?>
                                    </div>
                                    <span id="error_subcategory" style="color:red;"></span>
                                </div>
                                    <div class="row form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right">Link Type *</label>
                                        <div class="col-sm-8">
                                            <select name="asset_type" id="asset_type" class="form-control">
                                                <option value="INTERNAL" <?php echo ($mediaCenterDocumentObject->asset_type == "INTERNAL")?'selected="selected"':''; ?>>Internal</option>
                                                <option value="EXTERNAL" <?php echo ($mediaCenterDocumentObject->asset_type == "EXTERNAL")?'selected="selected"':''; ?>>External</option>
                                            </select> 
                                            <span id="error_category" style="color:red;"></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right">Title *</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="title" id="title" placeholder="Title" maxlength="25" value="<?php echo isset($mediaCenterDocumentObject->title) ? $mediaCenterDocumentObject->title : ""; ?>">
                                            (Title max 25 characters)
                                            <span id="error_title" style="color:red;"></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right">Description </label>
                                        <div class="col-sm-8">
                                            <textarea name="description" id="description" class="form-control" placeholder="Description"><?php echo isset($mediaCenterDocumentObject->description) ? $mediaCenterDocumentObject->description : ""; ?></textarea>
                                             (Description max 105 characters)
                                            <span id="error_description" style="color:red;"></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right">Language*</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="language" id="language" class="form-control" placeholder="Language" value="<?php echo isset($mediaCenterDocumentObject->language) ? $mediaCenterDocumentObject->language : ""; ?>"/>
                                            <span id="error_language" style="color:red;"></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right">Is Preferred *</label>
                                        <div class="col-sm-8">
                                            <label class="checkbox-inline">
                                                <input type="radio" class="pro-radio" name="is_featured" id="is_featured" value="1" <?php echo ($mediaCenterDocumentObject->is_featured == 1) ? "checked" : ""; ?>>Yes
                                            </label>
                                            <label class="checkbox-inline">
                                                <input type="radio" class="pro-radio" name="is_featured" id="is_featured" value="0" <?php echo ($mediaCenterDocumentObject->is_featured == 0) ? "checked" : ""; ?>>No
                                            </label> 
                                        </div>
                                    </div>
                                
                                    <?php if (isset($_GET['id']) && isset($mediaCenterDocumentObject->image)) { ?>
                                        <div class="row form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right">Current Image </label>
                                        <div class="col-sm-8">
                                             <img src="<?php echo isset($mediaCenterDocumentObject->image) ? Yii::app()->params['media_center']['document'] . $mediaCenterDocumentObject->image : ""; ?>" style="max-height: 200px;max-width: 200px;" />  
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="row form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right"><span id="upload_fb_mp4_txt">Image</span> *</label>
                                        <div class="col-sm-8">
                                            <input type="file" name="image" id="image" class="form-control" />
                                            <span  id="upload_fb_mp4_file">(Upload jpg, jpeg, png, gif files only and max file size is 2MB)</span>
                                            <span id="error_image" style="color:red;"></span>
                                        </div>
                                    </div>
                                    <div class="row form-group" style="<?php echo (isset($_GET['id']) && ($mediaCenterDocumentObject->category()->name == "Videos" && $mediaCenterDocumentObject->asset_type == "EXTERNAL"))?"display:block":"display:none"; ?>" id="media_file_link">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right">File Link</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="filelink" id="filelink" class="form-control" value="<?php echo isset($mediaCenterDocumentObject->filelinks) ? $mediaCenterDocumentObject->filelinks : ""; ?>"/>
                                            <span id="error_filelinks" style="color:red;"></span>
                                        </div>
                                    </div>
                                    <div class="row form-group" style="<?php echo (isset($_GET['id']) && ($mediaCenterDocumentObject->category()->name != "Videos"))?"display:block":"display:none"; ?>" id="media_file_document">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right">File Document*</label>
                                        <div class="col-sm-8">
                                            <input type="file" name="filedocument" id="filedocument" class="form-control" />
                                            (Upload jpg, jpeg, png, gif, pdf, ppt, pptx files only and max file size is 5MB)
                                            <span id="error_filedocument" style="color:red;"></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label text-right"></label>
                                        <div class="col-sm-8">
                                           <span id="error_file" style="color:red;"></span>
                                        </div>
                                    </div>
                                     
                                 <input type="submit" name="addedit_submit" value="Submit" id="addedit_submit" class="btn green">
                            </div>
                    </div>
                   
                </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php echo isset($classList) ? $classList : ""; ?>" id="list">
        
            <form id="new_user_filter_frm" name="library_filter_frm" method="post" action="/admin/library/media" >
                <div class="user-status">
                    
                    <div class="col-md-4 col-sm-4 bulkSelect">
                    <div class="form-group">
                        <label class="col-md-3 col-sm-3 padding0 top7"> Bulk Action</label>
                        <div class="col-md-8 col-sm-8">
                            <select class=" form-control  " id="bulkaction" name="bulkaction">
                                <option value="NA">Select</option>
                                <option value="Active">Active</option>
                                <option value="Inactive">Inactive</option>
                                <option value="Remove">Remove</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-1 col-sm-6 form-inline">
                    <input type="submit" class="btn btn-success mobile-left-15" value="OK" name="library_bulkaction_submit" id="library_bulkaction_submit"/>
                </div>
                    

                    <div class="col-md-5 mobile-margin">          
                            <div class="input-group input-large date-picker input-daterange pull-left">
                                <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
                                <span class="input-group-addon"> to </span>
                                <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
                            </div>
                        <input type="button" class="btn btn-primary f-left margin-left15 margintop3" value="Filter" name="submit" id="btnSearch">
                    </div>

                    <div class="col-md-4 col-sm-4 bulkSelect margin-top-10 m-clear">
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 padding0 top7 mobile-left-15">Status : </label>
                            <div class="col-md-8 col-sm-8">
                                <select class=" form-control  " id="change_status" name="change_status">
                                    <option value="all" >All</option>
                                    <option value="1" selected="selected">Active</option>
                                    <option value="2">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-md-4 col-sm-4 bulkSelect margin-top-10">
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 padding0 top7 mobile-left-15">Category: </label>
                            <div class="col-md-8 col-sm-8">
                                <select class=" form-control  " id="change_category" name="change_category" onchange="showSearchSubcategoryList(this.value)">
                                    <option value="" selected="selected">All</option>
                                        <?php if(isset($allMediaCenterCategoryObject) && !empty($allMediaCenterCategoryObject)){
                                        foreach($allMediaCenterCategoryObject as $allMediaCenterCategory){ ?>
                                        <option  value="<?php echo $allMediaCenterCategory->id; ?>"><?php echo $allMediaCenterCategory->name; ?></option>
                                        <?php } } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 col-sm-4 bulkSelect" id="search_subactegory_div" style="display:none">
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 padding0 top7">Subcategory: </label>
                            <div class="col-md-8 col-sm-8" id="search_subcategory_select">
                             
                            </div>
                        </div>
                    </div>
                </div> 

                <div class="row">
                    <div class="col-md-12 responsiveTable">
                        <br><br>

                        <table id="allLibraryMediaList" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="flowcheckall" class="editor-active"></th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Language</th>
                                    <th>Is Preferred </th>
                                    <th>Status</th>
                                    <th>Created At</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                        
                        <script>
                            $(document).ready(function () {
                                oTable = $('#allLibraryMediaList').DataTable({
                                    "responsive": true,
                                    "iDisplayLength": 50,
                                    "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                                    "processing": true,
                                    "serverSide": true,
                                    "paging": true,
                                    "bSort": true,
                                    "pagingType": "full_numbers",
                                    "order": [[8, "desc"]],
                                    "aoColumnDefs": [ {
                                        'bSortable' : false,
                                        'aTargets' : [ 0 ]
                                    } ],
                                    "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                                    "ajax": {
                                        "url": "/admin/library/medialistdata",
                                        "type": "POST"
                                    },
                                    "columns": [
                                         {
                                            data: "id",
                                            render: function (data, type, row) { 
                                                if (type === 'display') {
                                                    return '<input type="checkbox" class="editor-active" name="requestids['+data+']" value="'+data+'">';
                                                }
                                                return data;
                                            },
                                        },
                                        {"data": "category"},
                                        {"data": "subcategory"},
                                        {"data": "title"},
                                        {"data": "description"},
                                        {"data": "language"},
                                        {"data": "isPreffered"},
                                        {"data": "mcdStatus"},
                                        {"data": "created_at"},
                                        
                                        {
                                            data: "id",
                                            render: function (data, type, row) { 
                                            var actionVal =""; 
                                            if (row.image != null) {
                                                var actionVal = '<a href="<?php echo Yii::app()->params['media_center']['document']; ?>'+row.image+'" title="View" target="_blank"><i class="fa fa-eye"></i></a>';
                                            }    
                                            return '<a href="javascript:void(0);" onclick="showLibraryMediaLink(' + row.id + ')" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;'+actionVal;
                                            },
                                        },
                                    ],
                                    select: {
                                        style: 'os',
                                        selector: 'td:not(:first-child)' // no row selection on last column
                                    },
                                });
                            });
                        
                        $("#flowcheckall").click(function () {
                            $('#allLibraryMediaList tbody input[type="checkbox"]').prop('checked', this.checked);
                        });
                       
                        $("#btnSearch").click(function() {
                            if($("#toDate").val()<$("#fromDate").val()){
                                alert("To Date should be greater then From Date!!!");
                                return false;
                            }

                            oTable.columns(8).search($("#toDate").val().trim(),$("#fromDate").val().trim());
                            oTable.draw();
                        });
                        $("#change_status").change(function() {
                            oTable.columns(7).search($("#change_status").val().trim());
                            oTable.draw();
                        });
                         $("#change_category").change(function() { 
                            oTable.columns(1).search($("#change_category").val().trim());
                            oTable.draw();
                        });
                        $("#search_subcategory_select").change(function() { 
                            oTable.columns(2).search($("#change_sub_category").val().trim());
                            oTable.draw();
                        });
                        var uploadmg4 = 0;
                    function getSubCategoryData(data){
                        if(data==11){ //FB vedios
                            $("#upload_fb_mp4_txt").html('Upload Vedio');
                            $("#upload_fb_mp4_file").html('(Upload mp4 files only and max file size is 10MB)');
                            $("#media_file_link").hide();
                            uploadmg4 = 1;
                        }
                    }
    
                    </script>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
        
    </div>

</div>