<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Deactive Bid List'
);

$statusId = "";
if (isset($_REQUEST['res_filter'])) {
    $statusId = $_REQUEST['res_filter'];
}
?>

<style>
    .confirmBtn{left: 333px;
                position: absolute;
                top: 0;}

    .confirmOk{left: 610px;
               position: absolute;
               top: 8px;}
    .confirmMenu{position: relative;}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" />

<script>
    $(function () {
        $("#name").autocomplete({
            source: "/admin/bid/autocompletebypid",
            minLength: 2,
            select: function (event, ui) {
                event.preventDefault();
                $("#name").val(ui.item.label);
                $("#searchPid").val(ui.item.value);
            },
            focus: function (event, ui) {
                event.preventDefault();
                $(this).val(ui.item.label);
            },
            html: true, // optional (jquery.ui.autocomplete.html.js required)

            // optional (if other layers overlap autocomplete list)
            open: function (event, ui) {
                $(".ui-autocomplete").css("z-index", 1000);
            }
        });

    });
</script>
<div class="col-md-12">

    <div class="expiration margin-topDefault confirmMenu">


    </div>

</form>

</div>
<div class="row">
    <div class="col-md-12">
        <?php if (!empty($_GET['msg']) && $_GET['msg'] == '1') { ?> <div class="success"><?php echo "Record Deleted Succesfully." ?></div> <?php } ?>
        <?php if (!empty($_GET['msg']) && $_GET['msg'] == '2') { ?> <div class="success"><?php echo "Status Changed Succesfully." ?></div> <?php } ?>
        <?php if (!empty($_GET['success'])) { ?> <div class="success"><?php echo $_GET['success']; ?></div> <?php } ?>
        <?php if (!empty($_GET['error'])) { ?> <div class="error"><?php $_GET['error']; ?></div> <?php } ?>

        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            //'rowCssClassExpression'=>'fa fa-success btn default black delete',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Sl. No &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$row+1',
                ),
                array(
                    'name' => 'productname',
                    'header' => '<span style="white-space: nowrap;">Package Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->auction->product->name;',
                ),
                array(
                    'name' => 'start_date',
                    'header' => '<span style="white-space: nowrap;">Start Date&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->auction->start_date',
                ),
                array(
                    'name' => 'close_date',
                    'header' => '<span style="white-space: nowrap;">Close Date&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->auction->close_date',
                ),
                array(
                    'name' => 'entry_fees',
                    'header' => '<span style="white-space: nowrap;">Entry Fees &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'number_format($data->auction->entry_fees,2)',
                ),
                array(
                    'name' => 'lowest_bid_price',
                    'header' => '<span style="white-space: nowrap;">Lowest Bid &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'number_format($data->auction->lowest_bid_price,2)',
                ),
                array(
                    'name' => 'lowest_bid_price',
                    'header' => '<span style="white-space: nowrap;">Total Bids &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this, 'getTotalBids'),
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Created Date&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
                array(
                    'class' => 'CButtonColumn',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'template' => '{Edit}',
                    'htmlOptions' => array('width' => '30%'),
                    'buttons' => array(
                        'Edit' => array(
                            'label' => 'View More',
                            'options' => array('class' => 'fa fa-success btn default black delete blue'),
                            'url' => 'Yii::app()->createUrl("admin/bid/listmore", array("auctionid"=>$data->auction_id))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>