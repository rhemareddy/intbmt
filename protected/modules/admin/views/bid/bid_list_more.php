<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Bids' => '/admin/bid/list',
    'Bid Detail Report'
);

Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');
Yii::app()->clientScript->registerCssFile('/css/buttons.dataTables.min.css');
?>

<script src="/js/datatables.min.js"></script>
<span class="success" id="bidSuccessMsg" style="display: none">Bid Created Successfully.</span>
<span class="error" id="bidErrorMsg" style="display: none">Failed to create bid.</span>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
<div class="user-status col-md-12 filter-toggle">
<form id="BidDetail_filter_frm" name="BidDetail_filter_frm" method="post" action="/admin/bid/listmore" >
    <div class="col-md-4">  
        <span id="date_error" style="color:red"></span>        
        <div class="input-group input-large date-picker input-daterange pull-left">
            <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
            <span class="input-group-addon"> to </span>
            <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
        </div>
    </div>
    
    <div class="col-md-3 col-sm-3 bulkSelect">
        <div class="form-group">
            <label class="col-md-4 col-sm-4 padding0 top7 mobile-left-15">Auction Status </label>
            <div class="col-md-7 col-sm-7">
                <select class=" form-control " id="actionStatus" name="actionStatus">
                        <option value="open">Open</option>
                        <option value="close">Close</option>
                </select>
            </div>
        </div>
    </div>
    
    <div class="col-md-4 col-sm-4 bulkSelect">
        <div class="form-group">
            <label class="col-md-3 col-sm-3 padding0 top7 mobile-left-15">Bid Status </label>
            <div class="col-md-6 col-sm-6">
                <select class=" form-control " id="bidStatus" name="bidStatus">
                        <option value="all">All</option>
                        <option value="ul">Bid Considered</option>
                        <!--<option value="unl">Unique But not lowest</option>-->
                        <option value="nu">Not Unique</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-1 col-sm-6 form-inline">
        <input type="button" class="btn btn-success margintop3 mobile-left-15" value="Filter" name="bid_Detail" id="bid_Detail"/>
    </div>
</form>
</div>
    <div class="row">
        <div class="col-md-12 responsiveTable">
            <?php
            if (Yii::app()->user->hasFlash('error')):
                echo '<div class="alert alert-danger">' . Yii::app()->user->getFlash('error') . '</div>';
            endif;
            if (Yii::app()->user->hasFlash('success')):
                echo '<div class="alert alert-success">' . Yii::app()->user->getFlash('success') . '</div>';
            endif;
            ?>
          
            
            <table id="biddetailreport" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Auction Id</th>
                        <th>End Date</th>
                        <th>Product Name</th>
                        <th>User Name</th>
                        <th>Bid Amt</th>
                        <th>Bid Status</th>
                        <th>User Type</th>
                        <th>Created Date</th>
                        <th>Auction Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>

            <script src="/js/dataTables.buttons.min.js"></script>
            <script src="/js/buttons.flash.min.js"></script>
            <script src="/js/jszip.min.js"></script>
            <script src="/js/vfs_fonts.js"></script>
            <script src="/js/buttons.html5.min.js"></script>
            <script src="/js/buttons.print.min.js"></script>
            
            <script>
                $(document).ready(function () {
                    oTable = $('#biddetailreport').DataTable({
                        "responsive": true,
                        "iDisplayLength": 50,
                        "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        "processing": true,
                        "serverSide": true,
                        "paging": true,
                        "bSort": true,
                        "pagingType": "full_numbers",
                        "order": [[7, "desc"]],
                        
                        "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                        "buttons": [
                                {
                                    extend: 'csv',
                                    exportOptions: {
                                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                                    }
                                },
                                {
                                    extend: 'copyHtml5',
                                    exportOptions: {
                                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                                    }
                                },
                                {
                                    extend: 'excelHtml5',
                                    exportOptions: {
                                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                                    }
                                },
                                {
                                    extend: 'print',
                                    exportOptions: {
                                        columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ]
                                    }
                                },
                            ],
                            
                        "ajax": {
                            "url": "biddetailreport",
                            "type": "POST"
                        },
                        "columns": [
                            {"data": "auctionId"},
                            {"data": "endDate"},
                            {"data": "productName"},
                            {"data": "userName"},
                            {"data": "bidAmt"},
                            {"data": "bidStatus"},
                            {"data": "userType"},
                            {"data": "updatedDate"},
                            {"data": "auctionStatus"},
                            { 
                                "data": null,
                                "render": function (data, type, full, meta) 
                                    { 
                                        if((full.userType != 'Preferential') && (full.bidStatus != 'Not Unique')) {
                                        return "<input id='generateBids' type='button' onclick='generateBids(" 
                                         + full.auctionId +" , " + full.userId + " , " + full.bidAmt + ")' value='Match This Bid' />"; } 
                                        else {
                                            return ''; 
                                        }
                                    }
                            } 
                        ],
                    });
                });
                
                $("#bid_Detail").click(function() {
                $("#date_error").html("");
                if($("#toDate").val()>$("#fromDate").val()){
                    $("#date_error").html("To Date should be greater than From Date!!!");
                    $("#fromDate").focus();
                    return false;
                }
                if($("#actionStatus").val() == 'active') {
                    oTable.column(9).visible(true);
                } else {
                    oTable.column(9).visible(false);
                }
                oTable.columns(8).search($("#actionStatus").val().trim());
                oTable.columns(5).search($("#bidStatus").val().trim());
                oTable.columns(7).search($("#toDate").val().trim(),$("#fromDate").val().trim());
                
                oTable.draw();
                });
                
                function generateBids(auctionId, userId, bidAmt) {
                    $.ajax({
                        type: "post",
                        url: "matchthisbid",
                        data: {auctionId: auctionId, userId: userId, bidAmt: bidAmt},
                        success: function (msg) {
                            $('#bidSuccessMsg').val("");
                            if(msg == 1) {
                                $("#bidSuccessMsg").css({ display: "block" });
                                oTable.draw();
                            } else if(msg == 0) { 
                                $("#bidErrorMsg").css({ display: "block" });
                            } else {
                                $("#bidErrorMsg").css({ display: "block" });
                            }
                        }
                    });
                }
                
            </script>
        </div>
    </div>