   <!-- BEGIN PAGE LEVEL STYLES --> 
<link rel="stylesheet" type="text/css" href="/metronic/assets/plugins/select2/select2.css"/>
<link rel="stylesheet" type="text/css" href="/metronic/assets/plugins/select2/select2-metronic.css"/>  
<div class="content">
      <!-- BEGIN LOGIN FORM -->
<?php if(Yii::app()->session['frontloggedIN']=='1'){?>
<h3 class="form-title">Sorry! someone already loggedIN. Please try in another browser.</h3>
<?php }else {?>
<div id="login">
     <form class="login-form" action="index.html" method="post">
         <h3 class="form-title">Admin Login</h3>
         <div class="alert alert-error hide">
            <button class="close" data-dismiss="alert"></button>
            <span>Enter the valid data</span>
         </div>
         <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Admin Name</label>
            <div class="input-icon">
               <i class="fa fa-user"></i>
               <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="username" name="username" id="username_id"/>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
               <i class="fa fa-lock"></i>
               <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="password"/>
            </div>
         </div>
         <div class="form-actions">
            <!--<label class="checkbox">
            <input type="checkbox" name="LoginForm[rememberMe]" value="1" id="on_off"/> Se souvenir de moi
            </label>-->
	    <div id="loader" class="load-img" style="display:none;">Please wait don't refresh or press back button...</div>		
            <button type="submit" class="btn blue pull-right">
            Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>            
         </div>
        <!-- <div class="forget-password">
            <h4>Mot de passe oublié ?</h4>
            <p>
               <a href="javascript:void(0);"  id="forget-password">Cliquez-ici pour récupérer votre mot de passe</a>
            </p>
         </div>-->
      </form>
</div>
<div id="authentication" style="display: none;">
    <form method="post" id="authentication" class="authentication">
        <h3 class="form-title">Admin Login</h3>
       <input type="hidden" name="username1" id="username1" value="">
       <div class="form-group" >
            <label class="control-label visible-ie8 visible-ie9">Master Pin</label>
            <div class="input-icon">
               <i class="fa fa-lock"></i>
               <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Master Pin" name="pin" id="pin"/>
            </div>
         </div>
         <div class="form-actions">
            <!--<label class="checkbox">
            <input type="checkbox" name="LoginForm[rememberMe]" value="1" id="on_off"/> Se souvenir de moi
            </label>-->
			
            <button type="submit" class="btn blue pull-right" onclick="submitForm();">
            Submit <i class="m-icon-swapright m-icon-white"></i>
            </button>            
         </div>
</form>
 </div>
  <?php }?>
   </div>
 
 <!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/metronic/assets/scripts/core/app.js" type="text/javascript"></script>
<script src="/metronic/custom/login-soft-admin.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
  
<script type="text/javascript">
  window.onload = function() {
	App.init();
	Login.init();	
  }
  </script>