<ul class="page-breadcrumb breadcrumb">
    <li>
        <div class="breadcrumbs">
            <a href="/admin/default/dashboard">Dashboard</a> »
            <a href="/admin/landingpage">Landing Pages</a> »
            <span>Create/View</span>
        </div>
    </li>
</ul>

<div class="expiration margin-topDefault">
    <a class="btn  green margin-right-20" style="float:left" href="<?php echo $this->createUrl('landingpage/add'); ?>">New LandingPage </a> 
</div>

<div class="row">
    <div class="col-md-12 blue-table">
        <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="success" id="error_msg">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <div class="expiration margin-topDefault confirmMenu">
            <form id="regervation_filter_frm" name="regervation_filter_frm"  method="POST" action="/admin/landingpage/" class="form-inline">            
                <div class="form-group">                 
                    <input type="text" name="from" id="from" placeholder="From Date" class="datepicker form-control" value="<?php echo $fromDate; ?>"> 
                </div>
                <div class="form-group">                 
                    <input type="text" name="to" id="to" placeholder="To Date" class="datepicker form-control" value="<?php echo $toDate; ?>">                                   
                </div>
                <input type="submit" class="btn btn-primary " value="OK" name="submit" id="submit"/>
            </form>
        </div>

        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;color:#01b7f2">Sl.No</span>',
                ),
                array(
                    'name' => 'title',
                    'header' => '<span style="white-space: nowrap;">Title</span>',
                    'value' => '$data->title',
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Screenshot &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this, 'getLandingPageScreenShot'),
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style=" color:#1F92FF;white-space: nowrap;">Created Date</span>',
                    'value' => function($data) {
                        $date = date("M j, Y ", strtotime($data->created_at));
                        echo $date;
                    }
                ),
                array(
                    'name' => 'status',
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                ),
                array(
                    'class' => 'CButtonColumn',
                    'template' => '{Change}{Edit}',
                    'htmlOptions' => array('width' => '20%'),
                    'buttons' => array(
                        'Change' => array(
                            'label' => Yii::t('translation', 'Change Status'),
                            'options' => array('class' => 'fa fa-success btn default black delete'),
                            'url' => 'Yii::app()->createUrl("admin/landingpage/changestatus", array("id"=>$data->id))',
                        ),
                        'Edit' => array(
                            'label' => Yii::t('translation', 'Edit'),
                            'options' => array('class' => 'fa fa-success btn default green delete'),
                            'url' => 'Yii::app()->createUrl("admin/landingpage/edit", array("id"=>$data->id))',
                        ),
                    /* 'Delete' => array(
                      'label' => Yii::t('translation', 'Delete'),
                      'options' => array('class' => 'fa fa-success btn default red delete', 'onclick' => 'return confirm("Do u want to delete this user?");'),
                      'url' => 'Yii::app()->createUrl("admin/user/deleteuser", array("id"=>$data->id))',
                      ), */
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>
