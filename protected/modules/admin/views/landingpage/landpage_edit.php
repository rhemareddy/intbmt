<?php
$this->breadcrumbs = array(
    'Operation' => '/admin/landingpage',
    'LandingPage Edit',
);
?>
<div class="col-md-7 col-sm-7">
    <?php if (isset($error) && !empty($error)) { ?><div class="error"><?php echo $error; ?></div><?php } ?>
    <?php if (isset($success) && !empty($success)) { ?>
        <div class="success"><?php echo $success; ?> &nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo $this->createUrl('landingpage/index'); ?>">Click here to view. </a></div> 
    <?php } ?>

    <form action="" method="post" action="/landingpage/edit" class="form-horizontal" onsubmit="return validateForm();" enctype="multipart/form-data">
        <div class="portlet box toe-blue top10">
      <div class="portlet-title">
                    <div class="caption">
                  Edit Landing Page
                    </div>
                </div>
     <div class="portlet-body form padding15 f-left full-width">
            <p> <?php echo "Css/Js file path : " . $filePath; ?>  </p>
            <div class="form-group">
                <label for="lpage_title" class="col-lg-4 control-label">Title <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" maxlength="50" id="lpage_title" name="title" value="<?php echo isset($lpageObject->title) ? $lpageObject->title : ''; ?>">
                    <span id="lpage_title_error" class="clrred"></span>                    
                </div>
            </div>

            <div class="form-group">
                <label for="lpage_desc" class="col-lg-4 control-label">Content <span class="require">*</span></label>
                <div class="col-lg-8">
                    <textarea class="form-control dvalid" name="html" id="lpagedescription" rows="10" cols="50" maxlength="250"><?php echo isset($lpageObject->html) ? $lpageObject->html : ''; ?></textarea>
                    <span id="lpagedescription_error" class="clrred"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="lpage-zip" class="col-lg-4 control-label">Upload(Zip File) <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="file" class="form-control" name="upload" id="lpage_upload" accept="application/zip">                    
                    <span id="lpage_upload_error" class="clrred"></span>
                </div>
            </div>
            <input type="hidden" value="<?php echo isset($lpageObject->id) ? $lpageObject->id : ''; ?>" id="lpage_id" name="lpage_id" />
 <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Submit" class="btn green " onclick="return confirm('Are you sure updated all JS and CSS path to : <?php echo $filePath;?>')">
            </div>
   
        </div>
       
</div>
    </form>

</div> 


<script>
    function validateForm() {
        var title = $("#lpage_title").val();
        var lpage_desc = $("#lpagedescription").val();
        var lpage_upload = $("#lpage_upload").val();

        $("#lpage_title_error").html("");
        $("#lpagedescription_error").html("");
        $("#lpageed_bid_points_error").html("");

        if (title === '') {
            $("#lpage_title_error").html("Please enter title.");
            $("#lpage_title").focus();
            return false;
        }

        if (lpage_desc === '') {
            $("#lpagedescription_error").html("Please enter html content.");
            $("#lpagedescription").focus();
            return false;
        }

        if (lpage_upload === '') {
            $("#lpage_upload_error").html("Please upload Zip File.");
            $("#lpage_upload").focus();
            return false;
        }

        return true;
    }
</script>

<script type="text/javascript">
    CKEDITOR.replace('html', {
        filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?type=Flash',
        filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
    CKFinder.setupCKEditor(editor, '../');
</script>
