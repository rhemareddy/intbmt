<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Mail' => '/admin/mail',
    'View'
);
?>


    <div class="col-md-12 user-status mailBtns">
        <?php echo CHtml::link(Yii::t('translation', 'Compose') . ' <i class="fa fa-edit"></i>', '/admin/mail/compose', array("class" => "btn btn-xs green")); ?>
        <?php echo CHtml::link(Yii::t('translation', 'Inbox'). ' <i class="fa fa-envelope"></i>', '/admin/mail', array("class" => "btn btn-xs green")); ?>
        <?php echo CHtml::link(Yii::t('translation', 'Sent'). ' <i class="fa fa-share"></i>', '/admin/mail/sent', array("class" => "btn btn-xs green")); ?>
    </div>

<div class="row col-md-9 top10">
    <div class="portlet box toe-blue mailView ">
        <div class="portlet-title">
            <div class="caption">
                <?php if(!empty($conversationMailObject)) echo $conversationMailObject->subject; else "View Message"; ?>
            </div>
        </div>
        <div class="portlet-body form">

                <div class="form-body">
                    <!-- Start User Conversations -->
                    <?php if (isset($conversationObject) && !empty($conversationObject)) { ?>
                        <div class="margin">
                            <div class="chat">
                                <?php foreach ($conversationObject as $object) { ?>
                                    <?php if ($object->fromuser->role_id == 2) { ?>
                                        <div class="bubble me">
                                            <?php echo $object->message; ?><br>  
                                            <?php if (!empty($object->attachment)) { ?>
                                                <p><a download="<?php echo $object->attachment; ?>" href="<?php echo Yii::app()->getBaseUrl(true); ?>/upload/attachement/<?php echo $object->attachment; ?>" target="_blank">Click here to download</a> / <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/upload/attachement/<?php echo $object->attachment; ?>" target="_blank">Preview</a></p>
                                            <?php } ?>
                                            <p class="pull-right"><?php echo $object->fromuser()->name; ?> </p><br>
                                            <p class="pull-right"><?php echo date("d M Y g:i A", strtotime($object->updated_at)); ?> </p>
                                        </div>
                                        <div class="clearfix"> </div>
                                    <?php } else { ?>                
                                        <div class="bubble you"><?php echo $object->message; ?> <br>  
                                            <?php if (!empty($object->attachment)) { ?>
                                                <p><a download="<?php echo $object->attachment; ?>" href="<?php echo Yii::app()->getBaseUrl(true); ?>/upload/attachement/<?php echo $object->attachment; ?>" target="_blank">Click here to download</a> / <a href="<?php echo Yii::app()->getBaseUrl(true); ?>/upload/attachement/<?php echo $object->attachment; ?>" target="_blank">Preview</a></p>                            
                                            <?php } ?>
                                                <p class="pull-right"><?php echo $object->fromuser()->name; ?> </p><br>
                                            <p class="pull-right"><?php echo date("d M Y g:i A", strtotime($object->updated_at)); ?> </p>
                                        </div>    
                                        <div class="clearfix"> </div>
                                    <?php } ?>                

                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- End User Conversations -->
                     <?php if(!isset($_GET['sent']) && empty($_GET['sent']))
                     {?>
                    <div class="col-md-12">
                        <label class="col-md-1">&nbsp; </label>
                        <div class="form-actions right">
                            <a href="/admin/mail/reply?id=<?php echo(!empty($mailObject->mail_id)) ? BaseClass::mgEncrypt($mailObject->mail_id) : ""; ?>&department_id=<?php echo(!empty($mailObject->department_id)) ? BaseClass::mgEncrypt($mailObject->department_id) : ""; ?>" class="replyConversionBtn">Reply</a>
                        </div>
                    </div>
                     <?php }?>
                </div>
      
        </div>


