<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Mail' => '/admin/mail',
    'Compose'
);
?>
<?php
if (!empty($error)) {
    echo "<p class='error'>" . $error . "</p>";
}
$_GET['successMsg'] = 0;
if(!empty($_GET) && $_GET['successMsg']=='1'){
    echo "<div class='success'>Your message sent successfully.</div>";
}
?>

    <div class="col-md-12 user-status mailBtns">
        <?php echo CHtml::link(Yii::t('translation', 'Compose') . ' <i class="fa fa-edit"></i>', '/admin/mail/compose', array("class" => "btn btn-xs green")); ?>
        <?php echo CHtml::link(Yii::t('translation', 'Inbox'). ' <i class="fa fa-envelope"></i>', '/admin/mail', array("class" => "btn btn-xs green")); ?>
        <?php echo CHtml::link(Yii::t('translation', 'Sent'). ' <i class="fa fa-share"></i>', '/admin/mail/sent', array("class" => "btn btn-xs green")); ?>
    </div>

<div class="col-md-6  portlet box toe-blue top10">
    <div class="portlet-title">
                    <div class="caption">
                     Compose Mail
                    </div>
                </div>
    <div class="portlet-body form padding15">
<form class="form-horizontal" role="form" id="form_admin_reservation" enctype="multipart/form-data" action="/admin/mail/compose" method="post" onsubmit="return validateComposeMailForm()">
    <input type="hidden" value="<?php echo (!empty($_GET['id'])) ? $_GET['id'] : ""; ?>" name="id">
    <input type="hidden" value="<?php echo (!empty($_GET['department_id'])) ? $_GET['department_id'] : ""; ?>" name="department_id">
    <div class=" form-group">
        <label class="padding-right0  col-md-3">Department *</label>
        <div class="col-md-7 ">

            <select name="department_id" id="department_id"  class="form-control ">
                <option value="">Select Department</option>
                <?php
                foreach ($departmentObject as $department) {
                    if (!empty($departmentObject)) {
                        $varID = (Yii::app()->session['userid'] == 1) ? $department->id : $department->department_id;
                        $name = (Yii::app()->session['userid'] == 1) ? $department->name : $department->department->name;
                        $department_id = (!empty($mailObject)) ? $mailObject->department_id : "";
                    } else {
                        $varID = "";
                    }
                    ?>
                    <option value="<?php echo $varID; ?>" <?php if ($department_id == $varID) { ?> selected="selected" <?php } ?>><?php echo ucwords($name); ?></option>
                <?php } ?>
            </select>

            <span style="color:red"  id="department_id_error"></span>
        </div>
    </div> 
    <div class="form-group">
        <label class="padding-right0  col-md-3">To *</label>
        <div class="col-md-7">
            <input type="text" class="form-control dvalid" name="to_email[]"  onchange="getUserFullName(this.value);" id="search_username" size="60" maxlength="75" value="<?php echo (isset($mailObject->from_user_id)) ? $receiverName : ""; ?>" />
            <span id="search_fullname">&nbsp;</span>
            <span id="search_user_error" style="color:red"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="padding-right0  col-md-3">Subject *</label>
        <div class="col-md-7">
            <input type="text" class="form-control dvalid" name="email_subject" id="email_subject" size="60" maxlength="75" class="textBox" value="<?php
                if (!empty($_POST)) {
                    echo $_POST['email_subject'];
                } elseif (!empty($mailObject)) {
                    echo $mailObject->to_mail()->subject;
                } else {
                    echo "";
                }
                ?>" />
            <span class="clrred" style="color:red" id="email_subject_error"></span>
        </div>
    </div>
    <div class=" form-group">
        <label class="padding-right0  col-md-3">Message *</label>
        <div class="col-md-7"><textarea class="form-control dvalid" name="email_body" id="email_body" rows="10" cols="50" maxlength="1000"></textarea>
             <div>Maximum Size 1000 Letters</div>
            <span class="clrred" style="color:red"  id="email_body_error"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="padding-right0  col-md-3">Attachement &nbsp;</label>
    <div class="col-md-7">
        <input type="hidden" name="attachment1" id="" value="<?php echo (!empty($mailObject)) ? $mailObject->attachment : "";?>"/>
        <input type="file" name="attachment" id="attachement"/>
        (Please upload jpg, png, pdf files only and max upload size should be 2MB)
        <?php 
        $action = Yii::app()->controller->action->id;
        if(!empty($mailObject) && $mailObject->attachment !='' && $action != 'reply'){ ?>
        <?php echo $mailObject->attachment;?>
        <?php }?>
        <span style="color:red"  id="attachement_error"></span>
    </div>
</div>
    <div class="col-md-12 form-group">
        <label class="padding-right0 col-md-3"></label>
        <div class="col-md-7">
            <input type="submit" class="btn green" name="submit" id="submit" size="60" maxlength="75" class="textBox" value="Submit" />
        </div>
    </div> 
</form>
        <input type="hidden" value="<?php echo (isset($mailObject->from_user_id)) ? "1" : "0"; ?>" id="search_user_id" />
        </div>
    </div>