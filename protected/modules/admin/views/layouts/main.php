<?php
//if(isset(Yii::app()->session['userId'])){
$curController = @Yii::app()->controller->id;
$curAction = @Yii::app()->getController()->getAction()->controller->action->id;
$curControllerLower = strtolower($curController);
$curActionLower = strtolower($curAction);

$curControllerDisplay = $curController;
$curActionDisplay = $curAction;
if ($curControllerLower == 'user') {
	$curControllerDisplay = 'Alert';
}

if ($curActionLower == 'index') {
	$curActionDisplay = 'Listing';
}
$access = Yii::app()->user->getState('access');
$menusections = ''; //BaseClass::getmenusections ( Yii::app ()->user->getState ( 'username' ) );
$adImg = ''; //BaseClass::getadminImg ( Yii::app ()->user->getState ( 'username' ) );
$menusections ['psections'] = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
$baseURL = "http://localhost";
$accessArr = BaseClass::getMemberAccess();
$userName = BaseClass::getUserName();
/* Getting Promo wallet info */
$getPromoStatus = BaseClass::getWalletInfo(3);
$getCashStatus = BaseClass::getWalletInfo(4);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <meta name="google-site-verification" content="jAHPKLQC6dkwvDiZihMAmPdmOXCN9N6oi8XG2k0r854" />
        <title>Mavwealth | Admin</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="icon" type="image/ico" href="/images/favicon.ico">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />-->
        <link href="/metronic/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!--<link href="/metronic/assets/plugins/bootstrap-datepicker/datepicker.css" rel="stylesheet" type="text/css" />-->    
        <!--<link href="/metronic/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />-->
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="/metronic/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/assets/css/style-responsive.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/assets/css/plugins.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/assets/css/themes/default.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/custom/custom.css" rel="stylesheet" rtype="text/css" />
        <link href="/metronic/custom/custom-pagination.css" rel="stylesheet" type="text/css" />
        <link href="/metronic/custom/custom-pagination.css" rel="stylesheet" type="text/css" />
		<link href="/metronic/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" type="text/css" href="/css/admintable.css">   
        <!-- END THEME STYLES -->
        <!--<link rel="stylesheet" type="text/css" href="/metronic/assets/plugins/jquery-notific8/jquery.notific8.min.css" />-->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
           <script src="/metronic/assets/plugins/respond.min.js"></script>
           <script src="/metronic/assets/plugins/excanvas.min.js"></script> 
           <![endif]-->
        <script src="/metronic/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="/metronic/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        
        <!-- END CORE PLUGINS -->
        <script type="text/javascript" src="/metronic/assets/scripts/core/app.js"></script>
        <script type="text/javascript" src="/js/custom_admin_validation.js"></script>
        <script type="text/javascript" src="/js/transaction.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                App.init();
                //checkLoginTime();
            });
        </script>
        <!-- END JAVASCRIPTS -->
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-closed">
        <div class="fade-screen"></div>
        <div id="loader" class="text-center" style="display:none;"><img class="load-img" src="/images/loader.gif" alt="loader-image"></div>

        <!-- BEGIN HEADER -->
        <div class="header navbar navbar-fixed-top" style="height:45px!important;">
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="header-inner">
                <a class="navbar-brand" href="<?php echo Yii::app()->getBaseUrl(true); ?>" style="padding:10px;">
                    <img width="82px" src="/images/logo.png" class="img-responsive ">
                </a> 
                
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="navbar-toggle" data-toggle="collapse"  data-target=".navbar-collapse"> 
                    <img src="/metronic/assets/img/menu-toggler.png" alt="" />
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-right topWallet">

                    <li class="cash">
                        <a href="/admin/wallet/walletsummary" data-toggle="tooltip" data-placement="bottom" title="Bid Wallet"> 
                            <i class="fa fa-money"></i>
                            <span class="badge badge-default">
                                <?php
                                $arrayRP = BaseClass::walletAmount('1');
                                echo (!empty($arrayRP)) ? number_format($arrayRP->fund, 2) : "0.00";
                                ?>
                            </span>
                        </a>
                    </li>

                    <li class="cash">
                        <a href="/admin/wallet/walletsummary" data-toggle="tooltip" data-placement="bottom" title="Cashback Wallet"> 
                            <i class="fa fa-credit-card"></i>
                            <span class="badge badge-default">
                                <?php
                                $arrayRP = BaseClass::walletAmount('2');
                                echo (!empty($arrayRP)) ? number_format($arrayRP->fund, 2) : "0.00";
                                ?>
                            </span>
                        </a>
                    </li>
                    <?php if($getPromoStatus == 1) { ?>
                    <li class="cash">
                        <a href="/admin/wallet/walletsummary" data-toggle="tooltip" data-placement="bottom" title="Promo Wallet"> 
                            <i class="glyphicon glyphicon-briefcase"></i>
                            <span class="badge badge-default">
                                <?php
                                $arrayRP = BaseClass::walletAmount('3');
                                echo (!empty($arrayRP)) ? number_format($arrayRP->fund, 2) : "0.00";
                                ?>
                            </span>
                        </a>
                    </li>
                    <?php } ?>
                    
                    <?php if($getCashStatus == 1) { ?>
                    <li class="cash">
                        <a href="/admin/wallet/walletsummary" data-toggle="tooltip" data-placement="bottom" title="Cash Wallet"> 
                            <i class="glyphicon glyphicon-briefcase"></i>
                            <span class="badge badge-default">
                                <?php
                                $arrayRP = BaseClass::walletAmount('4');
                                echo (!empty($arrayRP)) ? number_format($arrayRP->fund, 2) : "0.00";
                                ?>
                            </span>
                        </a>
                    </li>
                    <?php } ?>

                    <li class="dropdown user main-dd mobile-dropdown">
                        <?php
                        $condition = "";
                        $departmentIds = "";
                        $userIds = "";
                        if (isset(Yii::app()->session['userid'])) {
                            $userDepartment = DepartmentMapping::model()->findAll(array('condition' => 'user_id = ' . Yii::app()->session['userid'] . ' AND status = 1'));

                            if (isset($userDepartment) && !empty($userDepartment)) {
                                foreach ($userDepartment as $department) {
                                    $departmentIds[] = (int) $department->department_id;

                                    $userListDepartment = DepartmentMapping::model()->findAll(array('condition' => 'department_id IN (' . implode(",", $departmentIds) . ') AND status = 1'));
                                    if (isset($userListDepartment) && !empty($userListDepartment)) {
                                        foreach ($userListDepartment as $userList) {
                                                $userIds[] = (int) $userList->user_id;
                                        }
                                    }
                                }
                                $condition = 'department_id IN (' . implode(",", $departmentIds) . ') AND (to_user_id IN (' . implode(",", $userIds) . ')) AND from_user_id != ' . Yii::app()->session['userid'];
                            } else {
                                $condition = ' to_user_id =' . Yii::app()->session['userid'];
                            }

                            $unreadMailObject = UserHasConversations::model()->count(array('condition' => $condition . ' AND status = 0', 'order' => 'updated_at DESC'));
                            $mailObject = UserHasConversations::model()->findAll(array('condition' => $condition, 'order' => 'updated_at DESC', 'limit' => 3));
                            if (count($mailObject) > 0) {
                                    ?>
                                <a href="javascript:;" class="dropdown-toggle mobile-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge badge-default admindd-badge"><?php if ($unreadMailObject > 0) echo $unreadMailObject; ?></span>
                                </a>

                                <ul class="dropdown-menu admin-ddmenu list-unstyled">

                                    <li class="external">
                                    <?php if ($unreadMailObject > 0) { ?>
                                            <p class="dd-head-p"> You have<span class="bold"> <?php echo $unreadMailObject ?> New</span> Mails
                                                <span class="pull-right"><a href="/admin/mail">View All</a></span></p>
                                    <?php } else { ?>
                                            <p class="dd-head-p"> You Have Mails in Inbox
                                                <span class="pull-right"><a href="/admin/mail">View Inbox</a></span></p>
                                    <?php } ?>
                                    </li>
                                    <li>
                                        <ul class="toeadmin-dd dropdown-menu-list list-unstyled">
                                            <?php
                                            $time = "";
                                            if (isset($mailObject) && !empty($mailObject)) {
                                                foreach ($mailObject as $mail) {
                                                    $time = CommonHelper::timeElapsed(strtotime($mail->updated_at));
                                                    ?>
                                                    <li>
                                                        <a href="/admin/mail"> 
                                                            <div class="subject">
                                                                <span class="from bold msg-name"><?php echo $mail->fromuser->name; ?><span class="adinbox-time"><?php if (isset($time)) echo $time . " ago"; ?></span></span>
                                                                <span class="time pull-right"></span>
                                                            </div>
                                                            <span class="message"><?php echo substr($mail->message, 0, 100);
                                                            if (strlen($mail->message) > 100) echo "..."; ?></span>
                                                        </a>
                                                    </li>
                                                <?php }
                                            } ?>
                                        </ul>
                                    </li>
                                </ul>
                    <?php }
                    } ?>
                    </li>

                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username">
                            <?php
                            $userObject = User::model()->findByPk(Yii::app()->session['userid']);
                            if ($userObject) {
                                echo ucfirst($userObject->name);
                            }
                            ?>
                            </span> <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="/site/logout?type=mwadmin"> <i class="fa fa-key"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>

                    <span class="home-link" style="font-size:12px;color:#fff"><?php echo date('Y-m-d H:i:s', strtotime('now')) . "\n"; ?></span>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>

                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END TOP NAVIGATION BAR -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu" data-auto-scroll="true"
                        data-slide-speed="200">
                        <li class="sidebar-toggler-wrapper">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler hidden-phone"></div> <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <?php
                        $primary_menu = 1;
                        if (in_array('dashboard', $accessArr)) {
                            if ((in_array($primary_menu, $menusections ['psections'])) || (in_array($primary_menu, $menusections ['section_ids']))) {
                                $activecls = '';
                                if ($curControllerLower == "default") {
                                    $activecls = 'active';
                                }
                                ?>
                                <li class="<?php echo $activecls; ?>">
                                    <a href="/admin/default/dashboard"><i class="fa fa-home"></i>  <span class="title">Dashboard</span>
                                        <span class="selected"></span> 
                                    </a>                					
                                </li>	
                                <?php
                            }
                        }

                        if (in_array('user', $accessArr)) {
                                $primary_menu = 1;
                            if ((in_array($primary_menu, $menusections ['psections'])) || (in_array($primary_menu, $menusections ['section_ids']))) {
                                    if (in_array('usermg', $accessArr)) {
                                        $menu_subsection1 = array(
                                            "user" => "All Member",
                                            "user?status=" . BaseClass::mgEncrypt(0) => "InActive Members",
                                        );
                                    } else {
                                        $menu_subsection1 = array();
                                    }
                                    if (in_array('wallet', $accessArr)) {
                                        $menu_subsection2 = array(
                                            "user/wallet" => "Wallet",
                                        );
                                    } else {
                                        $menu_subsection2 = array();
                                    }
                                   
                                    if (in_array('genealogy', $accessArr)) {
                                        $menu_subsection3 = array(
                                            "user/genealogy" => "Genealogy",
                                        );
                                    } else {
                                        $menu_subsection3 = array();
                                    }
                                    if (in_array('binarycommission', $accessArr)) {
                                        $menu_subsection4 = array(
                                            "user/binarycalculation" => "Generate Binary Commission",
                                        );
                                    } else {
                                        $menu_subsection4 = array();
                                    }
                                    if (in_array('broadcast', $accessArr)) {
                                        $menu_subsection5 = array(
                                            "user/broadcast" => "Broadcast Message",
                                        );
                                    } else {
                                        $menu_subsection5 = array();
                                    }
                                    if (in_array('powerline', $accessArr)) {
                                        $menu_subsection6 = array(
                                            "user/powerline" => "Powerline",
                                        );
                                    } else {
                                        $menu_subsection6 = array();
                                    }
                                    if (in_array('manageagent', $accessArr)) {
                                        $menu_subsection7 = array(
                                            "user/manageagent" => "Manage Agent",
                                        );
                                    } else {
                                        $menu_subsection7 = array();
                                    }
                                    if (in_array('manageusergroup', $accessArr)) {
                                        $menu_subsection8 = array(
                                            "user/usergroup" => "Manage User Group",
                                        );
                                    } else {
                                        $menu_subsection8 = array();
                                    }

                                    $menu_subsection = array_merge($menu_subsection1, $menu_subsection2, $menu_subsection3, $menu_subsection4, $menu_subsection5, $menu_subsection6, $menu_subsection7, $menu_subsection8);
                                    $activecls ="";
                                    if ($curControllerLower == "user" && $curAction != "dashboard" && $curAction != 'resetpassword' &&  $curAction != 'supportcreditwallet'  && $curAction != 'dashboard' && $curAction != 'add' && $curAction != 'edit' && $curAction != 'changepassword' && $curAction != 'uplinedownline' && $curAction != 'verificationapproval' && $curAction != 'testimonialapproval' && $curAction != 'profileimageapproval' || $curControllerLower == "admin" || $curAction == 'binarycalculation') {
                                        $activecls = 'active';
                                    }
                                    if ($curActionLower == isset($_GET['mode']))
                                        $activecls = '';
                                    ?>
                                    <li class="<?php echo $activecls; ?>">
                                        <a href="javascript:;"> <i class="fa fa-cog"></i>
                                            <span class="title">Operation</span>
                                            <span class="selected"></span> 
                                            <span class="arrow <?php echo ($curControllerLower == 'user') ? "open" : ''; ?>"></span>
                                        </a>
                                            <?php
                                            $menusections ['sections'] = $menu_subsection;
                                            echo '<ul class="sub-menu">';
                                            foreach ($menu_subsection as $hotName => $hotTitle) {
                                                if (in_array($hotTitle, $menusections ['sections'])) {
                                                    if ($hotName == 'admin') {
                                                        $hotName = 'admin/index';
                                                    }
                                                    if ($curActionLower == 'create') {
                                                        $curActionLower = 'create/type/details';
                                                    }
                                                    $class_content = ($curControllerLower . "/" . $curActionLower == $hotName) ? 'class="active"' : '';
                                                    echo '<li ' . $class_content . '>';
                                                    echo '<a href="/admin/' . $hotName . '">' . Yii::t('translation', $hotTitle) . '</a>';
                                                    echo '</li>';
                                                    if ($hotName == 'admin/index') {
                                                        $hotName = 'admin';
                                                    }
                                                }
                                            }
                                            echo '</ul>';
                                            ?>					
                                    </li>	
                                    <?php
                                }
                        }

                        if (in_array('financereports', $accessArr)) {
                            $main_pmenu = 2;
                            if ((in_array($main_pmenu, $menusections ['psections'])) || (in_array($main_pmenu, $menusections ['section_ids']))) {

                                if (in_array('reporttransaction', $accessArr)) {
                                    $main_subsection1 = array(
                                        "report/transaction" => "Transaction",
                                    );
                                } else {
                                    $main_subsection1 = array();
                                }

                                if (in_array('packagepurchased', $accessArr)) {
                                    $main_subsection2 = array(
                                        "report/orderlist" => "Order List",
                                    );
                                } else {
                                    $main_subsection2 = array();
                                }

                                if (in_array('generatebinaryreport', $accessArr)) {
                                    $main_subsection3 = array(
                                        "report/binaryreport" => "Generated Binary Report",
                                    );
                                } else {
                                    $main_subsection3 = array();
                                }

                                if (in_array('downlinereport', $accessArr)) {
                                    $main_subsection4 = array(
                                        "report/downlinereport" => "Down Line Purchase Report",
                                    );
                                } else {
                                    $main_subsection4 = array();
                                }

                                if (in_array('offlinepayment', $accessArr)) {
                                    $main_subsection5 = array(
                                        "report/requestpayment" => "Offline Payment",
                                    );
                                } else {
                                    $main_subsection5 = array();
                                }

                                if (in_array('walletsummary', $accessArr)) {
                                    $main_subsection6 = array(
                                        "wallet/walletsummary" => "Wallet Summary",
                                    );
                                } else {
                                    $main_subsection6 = array();
                                }

                                if (in_array('profitability', $accessArr)) {
                                    $main_subsection7 = array(
                                        "report/profitability" => "Profitability",
                                    );
                                } else {
                                    $main_subsection7 = array();
                                }

                                $main_subsection = array_merge($main_subsection1, $main_subsection2, $main_subsection3, $main_subsection4, $main_subsection5, $main_subsection6, $main_subsection7);
                                ?>
                                <li class="<?php echo (($curControllerLower == 'report' && $curAction == 'transaction') || ($curControllerLower == 'report' && $curAction == 'orderlist') || ($curControllerLower == 'report' && $curAction == 'binaryreport') || ($curControllerLower == 'report' && $curAction == 'downlinereport') || ($curControllerLower == 'report' && $curAction == 'requestpayment') || ($curControllerLower == 'wallet' && $curAction == 'walletsummary') || ($curControllerLower == 'report' && $curAction == 'profitability')) ? "active" : ''; ?>">
                                    <a href="javascript:;"> <i class="fa fa-file-text-o"></i>
                                        <span class="title">Finance Reports</span>
                                        <span class="selected"></span> 
                                        <span class="arrow <?php echo (($curControllerLower == 'report' && $curAction == 'transaction') || ($curControllerLower == 'report' && $curAction == 'orderlist') || ($curControllerLower == 'report' && $curAction == 'binaryreport') || ($curControllerLower == 'report' && $curAction == 'downlinereport') || ($curControllerLower == 'report' && $curAction == 'requestpayment') || ($curControllerLower == 'wallet' && $curAction == 'walletsummary')) ? "open" : ''; ?>">
                                        </span>
                                    </a>

                                <?php
                                echo '<ul class="sub-menu">';
                                foreach ($main_subsection as $ctName => $ctTitle) {
                                    if ($ctName == "search/create") {
                                        $ctName = "search/create/type/details";
                                    }
                                    if ($ctName == "report" && $curControllerLower == "report")
                                        $class_content = 'class="active"';
                                    else
                                        $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                    echo '<li ' . $class_content . '>';
                                    echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                    echo '</li>';
                                    if ($ctName == "search/create/type/details") {
                                        $ctName = "search/create";
                                    }
                                }
                                echo '</ul>';
                                ?>			
                </li>
                                <?php
                            }
                        }

                        if (in_array('support', $accessArr) || in_array('support_recharge_wallet', $accessArr) || in_array('support_transaction_search', $accessArr)) {
                            $primary_menu = 7;
                            if ((in_array($primary_menu, $menusections ['psections'])) || (in_array($primary_menu, $menusections ['section_ids']))) {
                                if (in_array('support_recharge_wallet', $accessArr)) {
                                    $main_subsection1 = array(
                                "user/creditwallet?mode=AddFund" => "Recharge Wallet",
                            );
                            } else {
                                $main_subsection1 = array();
                            }
                            if (in_array('support_transaction_search', $accessArr)) {
                                $main_subsection2 = array(
                                    "report/supporttransaction" => "Transaction Search",
                                );
                            } else {
                                $main_subsection2 = array();
                            }
                            if (in_array('support_inactive_users', $accessArr)) {
                                $main_subsection3 = array(
                                    "user?status=supportusers&" => "Inactive Users",
                                );
                            } else {
                                $main_subsection3 = array();
                            }
                            if (in_array('change_password', $accessArr)) {
                                $main_subsection4 = array(
                                    "user/changepassword" => "Change Password",
                                );
                            } else {
                                $main_subsection4 = array();
                            }
                            
                            if (in_array('change_email', $accessArr)) {
                                $main_subsection9 = array(
                                    "user/changeemail" => "Change Email",
                                );
                            } else {
                                $main_subsection9 = array();
                            }
                            
                            if (in_array('upline_downline', $accessArr)) {
                                $main_subsection5 = array(
                                    "user/uplinedownline" => "Upline/Downline",
                                );
                            } else {
                                $main_subsection5 = array();
                            }
                            if (in_array('document', $accessArr)) {
                                $main_subsection6 = array(
                                    "user/verificationapproval" => "Document Approval",
                                );
                            } else {
                                $main_subsection6 = array();
                            }
                            if (in_array('testimonial', $accessArr)) {
                                $main_subsection7 = array(
                                    "user/testimonialapproval" => "Testimonial Approval",
                                );
                            } else {
                                $main_subsection7 = array();
                            }
                            if (in_array('profileimage', $accessArr)) {
                                $main_subsection8 = array(
                                    "user/profileimageapproval" => "Profile Image Approval",
                                );
                            } else {
                                $main_subsection8 = array();
                            }
                                    
                            $subsection = array_merge($main_subsection1, $main_subsection2, $main_subsection3, $main_subsection4, $main_subsection9 , $main_subsection5, $main_subsection6, $main_subsection7, $main_subsection8);

                            $activecls = '';

                            if ((isset($_GET['mode']) && ($_GET['mode'] == Transaction::$_MODE_ADDFUND)) || (isset($_GET['status']) && ($_GET['status'] == 'supportcreditwallet')) || ($curControllerLower == "report" && $curAction == 'supporttransaction') || ($curControllerLower == "user" && $curAction == 'changepassword') || ($curControllerLower == "user" && (isset($_GET['status']) && $_GET['status'] == 'supportusers')) || ($curControllerLower == "user" && $curAction == 'uplinedownline') || ($curControllerLower == "user" && $curAction == 'verificationapproval') || ($curControllerLower == "user" && $curAction == 'testimonialapproval') || ($curControllerLower == "user" && $curAction == 'profileimageapproval')) {
                                $activecls = 'active';
                            }
        ?>
                                                                
                            <li class="<?php echo $activecls; ?>">
                                    <a href="javascript:;"> <i class="fa fa-cog"></i>
                                            <span class="title">Support</span>
                                            <span class="selected"></span> <span
                                                    class="arrow <?php echo ($curControllerLower == 'user') ? "open" : ''; ?>">
                                            </span>
                                    </a>
                                    <?php
                                    $menusections ['sections'] = $subsection;
                                    echo '<ul class="sub-menu">';
                                    foreach ($subsection as $hotName => $hotTitle) {
                                        if (in_array($hotTitle, $menusections ['sections'])) {
                                            if ($hotName == 'admin') {
                                                    $hotName = 'admin/index';
                                            }
                                            if ($curActionLower == 'create') {
                                                    $curActionLower = 'create/type/details';
                                            }
                                            $class_content = ($curControllerLower . "/" . $curActionLower == $hotName) ? 'class="active"' : '';
                                            echo '<li ' . $class_content . '>';
                                            echo '<a href="/admin/' . $hotName . '">' . Yii::t('translation', $hotTitle) . '</a>';

                                            echo '</li>';
                                            if ($hotName == 'admin/index') {
                                                    $hotName = 'admin';
                                            }
                                        }
                                    }
                                    echo '</ul>';
                                    ?>

                            </li>
                                <?php
                        }
                }

                        if (in_array('site', $accessArr)) {
                            $reservation_pmenu = 4;
                            if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                                $main_subsection = array(
                                    "site/index" => "Create",
                                    "site/list" => "List",
                                );
                                ?>
                                <li class="<?php echo ($curControllerLower == 'site') ? "active" : ''; ?>">
                                    <a href="javascript:;"> <i class="fa fa-building-o"></i>
                                        <span class="title">Site Settings</span>
                                        <span class="selected"></span> <span
                                                class="arrow <?php //echo ($curControllerLower == 'buildtemp') ? "open" : '';    ?>">
                                        </span>
                                    </a>

                                    <?php
                                    echo '<ul class="sub-menu">';
                                    foreach ($main_subsection as $ctName => $ctTitle) {
                                        if ($ctName == "search/create") {
                                            $ctName = "search/create/type/details";
                                        }
                                        if ($ctName == "site" && $curControllerLower == "site")
                                            $class_content = 'class="active"';
                                        else
                                            $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                        echo '<li ' . $class_content . '>';
                                        echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                        echo '</li>';
                                        if ($ctName == "search/create/type/details") {
                                            $ctName = "search/create";
                                        }
                                    }
                                    echo '</ul>';
                                    ?>			
                                </li>
                                <?php
                            }
                        }

                    if (in_array('offer', $accessArr)) {
                        $reservation_pmenu = 5;
                        if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                            $main_subsection = array(
                                    "offer/index" => "Create/View",
                            );
                            ?>
                                <li class="<?php echo ($curControllerLower == 'offer') ? "active" : ''; ?>">
                                    <a href="javascript:;"> <i class="fa fa-building-o"></i>
                                        <span class="title">Manage Offers</span>
                                        <span class="selected"></span> <span
                                                class="arrow <?php //echo ($curControllerLower == 'buildtemp') ? "open" : '';    ?>">
                                        </span>
                                    </a>

                                        <?php
                                        echo '<ul class="sub-menu">';
                                        foreach ($main_subsection as $ctName => $ctTitle) {
                                            if ($ctName == "search/create") {
                                                    $ctName = "search/create/type/details";
                                            }
                                            if ($ctName == "offer" && $curControllerLower == "offer")
                                                    $class_content = 'class="active"';
                                            else
                                                    $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                            echo '<li ' . $class_content . '>';
                                            echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                            echo '</li>';
                                            if ($ctName == "search/create/type/details") {
                                                    $ctName = "search/create";
                                            }
                                        }
                                        echo '</ul>';
                                        ?>			
                                </li>
                                <?php
                        }
                    }

                    if (in_array('landingpage', $accessArr)) {
                            $reservation_pmenu = 6;
                            if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                                    $main_subsection = array(
                                            "landingpage/index" => "Create/View",
                                    );
                                    ?>

                                    <li class="<?php echo ($curControllerLower == 'landingpage') ? "active" : ''; ?>">
                                        <a href="javascript:;"> <i class="fa fa-building-o"></i>
                                            <span class="title">Landing Pages</span>
                                            <span class="selected"></span> 
                                            <span class="arrow "> </span>
                                        </a>

                                            <?php
                                            echo '<ul class="sub-menu">';
                                            foreach ($main_subsection as $ctName => $ctTitle) {
                                                if ($ctName == "search/create") {
                                                    $ctName = "search/create/type/details";
                                                }
                                                if ($ctName == "offer" && $curControllerLower == "landingpage")
                                                    $class_content = 'class="active"';
                                                else
                                                        $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                                echo '<li ' . $class_content . '>';
                                                echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                                echo '</li>';
                                                if ($ctName == "search/create/type/details") {
                                                    $ctName = "search/create";
                                                }
                                            }
                                            echo '</ul>';
                                            ?>			
                                    </li>
                                    <?php
                            }
}

                            if (in_array('mail', $accessArr)) {
                                $billing_pmenu = 7;
                                if ((in_array($billing_pmenu, $menusections ['psections'])) || (in_array($billing_pmenu, $menusections ['section_ids']))) {
                                    ?>
                                    <li class="<?php echo ($curControllerLower == 'mail') ? "active" : ''; ?>">
                                        <a href="/admin/mail"> <i class="fa fa-envelope-o"></i> <span class="title">Mail</span>
                                                <span class="selected"></span> 
                                        </a>
                                    </li>	
                                    <?php
                                }
                            }

                            if (in_array('reports', $accessArr)) {
                                $reservation_pmenu = 8;
                                if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {

                                    if (in_array('userreport', $accessArr)) {
                                        $main_subsection1 = array(
                                            "report/" => "Registration",
                                        );
                                    } else {
                                        $main_subsection1 = array();
                                    }
                                    if (in_array('reportaddress', $accessArr)) {
                                        $main_subsection2 = array(
                                            "report/address" => "Member Address",
                                        );
                                    } else {
                                        $main_subsection2 = array();
                                    }
                                    if (in_array('reportsponsor', $accessArr)) {
                                        $main_subsection3 = array(
                                            "report/companysponsor" => "Company Sponsor",
                                        );
                                    } else {
                                        $main_subsection3 = array();
                                    }

                                    if (in_array('reportcontact', $accessArr)) {
                                        $main_subsection7 = array(
                                            "report/contact" => "Contact",
                                        );
                                    } else {
                                        $main_subsection7 = array();
                                    }

                                    if (in_array('reportreferral', $accessArr)) {
                                        $main_subsection9 = array(
                                            "report/trackreferral" => "Referral Invite",
                                        );
                                    } else {
                                        $main_subsection9 = array();
                                    }

                                    if (in_array('binaryreport', $accessArr)) {
                                        $main_subsection10 = array(
                                            "report/binaryreport" => "Binary Report",
                                        );
                                    } else {
                                        $main_subsection10 = array();
                                    }

                                    if (in_array('userloginactivities', $accessArr)) {
                                        $main_subsection11 = array(
                                            "report/loginactivities" => "User Login Activities",
                                        );
                                    } else {
                                        $main_subsection11 = array();
                                    }

                                    if (in_array('agentcontact', $accessArr)) {
                                        $main_subsection12 = array(
                                            "report/agentcontact" => "Agent Contacted",
                                        );
                                    } else {
                                        $main_subsection12 = array();
                                    }

                                    if (in_array('agentrequest', $accessArr)) {
                                        $main_subsection13 = array(
                                            "report/agentrequest" => "Agent Request",
                                        );
                                    } else {
                                        $main_subsection13 = array();
                                    }

                                    $main_subsection = array_merge($main_subsection1, $main_subsection2, $main_subsection3, $main_subsection7, $main_subsection9, $main_subsection11,$main_subsection12,$main_subsection13);
                                        ?>
                                        <li class="<?php echo ($curControllerLower == 'report' && $curAction != 'supporttransaction' && $curAction != 'transaction' && $curAction != 'orderlist' && $curAction != 'binaryreport' && $curAction != 'downlinereport' && $curAction != 'requestpayment' && $curAction != 'profitability') ? "active" : ''; ?>">
                                            <a href="javascript:;"> <i class="fa fa-file-text-o"></i>
                                            <span class="title">Reports</span>
                                            <span class="selected"></span> <span
                                                    class="arrow <?php echo ($curControllerLower == 'report') ? "open" : ''; ?>">
                                            </span>
                                            </a>

                                            <?php
                                            echo '<ul class="sub-menu">';
                                            foreach ($main_subsection as $ctName => $ctTitle) {
                                                if ($ctName == "search/create") {
                                                    $ctName = "search/create/type/details";
                                                }
                                                if ($ctName == "report" && $curControllerLower == "report")
                                                    $class_content = 'class="active"';
                                                else
                                                        $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                                echo '<li ' . $class_content . '>';
                                                echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                                echo '</li>';
                                                if ($ctName == "search/create/type/details") {
                                                    $ctName = "search/create";
                                                }
                                            }
                                            echo '</ul>';
                                            ?>			
                                        </li>
                                        <?php
                                }
                        }

                        if (in_array('withdrawal', $accessArr)) {
                            $reservation_pmenu = 9;
                            if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                                if (in_array('request', $accessArr)) {
                                    $main_subsection1 = array(
                                        "withdrawal/request" => "Pending Request",
                                    );
                                } else {
                                    $main_subsection1 = array();
                                }
                                if (in_array('payment', $accessArr)) {
                                    $main_subsection2 = array(
                                        "withdrawal/payment" => "Pending Payment",
                                    );
                                } else {
                                    $main_subsection2 = array();
                                }
                                if (in_array('history', $accessArr)) {
                                    $main_subsection3 = array(
                                        "withdrawal/history" => "History",
                                    );
                                } else {
                                    $main_subsection3 = array();
                                }
                                $main_subsection = array_merge($main_subsection1, $main_subsection2, $main_subsection3);
        ?>
                                <li class="<?php echo (($curControllerLower == 'withdrawal') || ($curControllerLower == 'withdrawal')) ? "active" : ''; ?>">
                                    <a href="javascript:;"> <i class="fa fa-file-text-o"></i>
                                        <span class="title">Withdrawal</span>
                                        <span class="selected"></span> <span class="arrow <?php echo ($curControllerLower == 'withdrawal') ? "open" : ''; ?>">
                                        </span>
                                    </a>

                                        <?php
                                        echo '<ul class="sub-menu">';
                                        foreach ($main_subsection as $ctName => $ctTitle) {
                                                if ($ctName == "search/create") {
                                                        $ctName = "search/create/type/details";
                                                }
                                                if ($ctName == "withdrawal" && $curControllerLower == "withdrawal")
                                                        $class_content = 'class="active"';
                                                else
                                                        $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                                echo '<li ' . $class_content . '>';
                                                echo '<a href="/admin/' . $ctName . '">' . Yii::t('withdrawal', $ctTitle) . '</a>';
                                                echo '</li>';
                                                if ($ctName == "search/create/type/details") {
                                                        $ctName = "search/create";
                                                }
                                        }
                                        echo '</ul>';
                                        ?>			
                                </li>
                                <?php
                        }
                }

                if (in_array('transaction', $accessArr)) {
                    $reservation_pmenu = 10;
                    if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                        if (in_array('transaction', $accessArr)) {
                            $main_subsection1 = array(
                                "transaction/order" => "Order Transaction",
                            );
                        } else {
                            $main_subsection1 = array();
                        }
                        if (in_array('transaction', $accessArr)) {
                            $main_subsection2 = array(
                                "transaction/transaction" => "Wallet Transaction",
                            );
                        } else {
                            $main_subsection2 = array();
                        }
                        if (in_array('transaction', $accessArr)) {
                            $main_subsection3 = array(
                                "transaction/bid" => "Bid Transaction",
                            );
                        } else {
                            $main_subsection3 = array();
                        }

                        $main_subsection = array_merge($main_subsection1, $main_subsection2, $main_subsection3);
                                ?>
                                <li class="<?php echo (($curControllerLower == 'transaction') || ($curControllerLower == 'transaction')) ? "active" : ''; ?>">
                                    <a href="javascript:;"> <i class="fa fa-file-text-o"></i>
                                        <span class="title">Transaction Reports</span>
                                        <span class="selected"></span> <span class="arrow <?php echo ($curControllerLower == 'transaction') ? "open" : ''; ?>">
                                        </span>
                                    </a>

                                    <?php
                                    echo '<ul class="sub-menu">';
                                    foreach ($main_subsection as $ctName => $ctTitle) {
                                        if ($ctName == "search/create") {
                                                $ctName = "search/create/type/details";
                                        }
                                        if ($ctName == "transaction" && $curControllerLower == "transaction")
                                                $class_content = 'class="active"';
                                        else
                                                $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                        echo '<li ' . $class_content . '>';
                                        echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                        echo '</li>';
                                        if ($ctName == "search/create/type/details") {
                                                $ctName = "search/create";
                                        }
                                    }
                                        echo '</ul>';
                                        ?>			
                                </li>
                                <?php
                        }
                }

                if (in_array('product', $accessArr) || in_array('product_add', $accessArr) || in_array('product_list', $accessArr)) {
                    $reservation_pmenu = 11;
                    if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                        $main_subsection = array(
                            "product/add" => "Add Product",
                            "product/list" => "Product List",
                                //"product/vendorlist" => "Vendor Product List",
                        );
        ?>
                    <li class="<?php echo (($curControllerLower == 'product') || ($curControllerLower == 'product')) ? "active" : ''; ?>">
                        <a href="javascript:;"> <i class="fa fa-archive"></i>
                            <span class="title">Product</span>
                            <span class="selected"></span> <span
                                    class="arrow <?php echo ($curControllerLower == 'product') ? "open" : ''; ?>">
                            </span>
                        </a>
        <?php
            echo '<ul class="sub-menu">';
            foreach ($main_subsection as $ctName => $ctTitle) {
                if ($ctName == "search/create") {
                    $ctName = "search/create/type/details";
                }
                if ($ctName == "report" && $curControllerLower == "package")
                    $class_content = 'class="active"';
                else
                    $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                echo '<li ' . $class_content . '>';
                echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                echo '</li>';
                if ($ctName == "search/create/type/details") {
                    $ctName = "search/create";
                }
        }
        echo '</ul>';
                                ?>			
                        </li>
                        <?php
                }
        }

        if (in_array('auction', $accessArr)) {
    $reservation_pmenu = 12;
    if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
        $main_subsection = array(
            "auction/add" => "Add Auction",
            "auction/list" => "Auction List",
            "auction/declarewinner" => "Declare Winner",
            "auction/winnerpayment" => "Winner Payment",
        );
        ?>
        <li
                class="<?php echo (($curControllerLower == 'auction') || ($curControllerLower == 'auction')) ? "active" : ''; ?>">
                <a href="javascript:;"> <i class="fa fa-archive"></i>
                    <span class="title">Auction</span>
                    <span class="selected"></span> <span
                            class="arrow <?php echo ($curControllerLower == 'auction') ? "open" : ''; ?>">
                    </span>
            </a>

        <?php
        echo '<ul class="sub-menu">';
        foreach ($main_subsection as $ctName => $ctTitle) {
            if ($ctName == "search/create") {
                $ctName = "search/create/type/details";
            }
            if ($ctName == "report" && $curControllerLower == "auction")
                $class_content = 'class="active"';
            else
                $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

            echo '<li ' . $class_content . '>';
            echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
            echo '</li>';
            if ($ctName == "search/create/type/details") {
                $ctName = "search/create";
            }
        }
        echo '</ul>';
        ?>			
            </li>
        <?php
    }
}

            if (in_array('bids', $accessArr)) {
                $reservation_pmenu = 10;
                if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                    if (in_array('bidsummary', $accessArr)) {
                            $main_subsection1 = array(
                                    "bid/list" => "Bid Summary",
                            );
                    } else {
                            $main_subsection1 = array();
                    }
                    if (in_array('bidsdetail', $accessArr)) {
                            $main_subsection2 = array(
                                    "bid/listmore" => "Bid Detail Report",
                            );
                    } else {
                            $main_subsection2 = array();
                    }
                    
                    $main_subsection = array_merge($main_subsection1, $main_subsection2);
								?>
                        <li class="<?php echo (($curControllerLower == 'bid') || ($curControllerLower == 'list')) ? "active" : ''; ?>">
                                <a href="javascript:;"> <i class="fa fa-file-text-o"></i>
                                        <span class="title">Bids</span>
                                        <span class="selected"></span> <span
                                                class="arrow <?php echo ($curControllerLower == 'bid') ? "open" : ''; ?>">
                                        </span>
                                </a>

                                        <?php
                                        echo '<ul class="sub-menu">';
                                        foreach ($main_subsection as $ctName => $ctTitle) {
                                            if ($ctName == "search/create") {
                                                    $ctName = "search/create/type/details";
                                            }
                                            if ($ctName == "bid" && $curControllerLower == "bid")
                                                    $class_content = 'class="active"';
                                            else
                                                    $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                            echo '<li ' . $class_content . '>';
                                            echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                            echo '</li>';
                                            if ($ctName == "search/create/type/details") {
                                                    $ctName = "search/create";
                                            }
                                        }
                                        echo '</ul>';
                                        ?>			
                                </li>
                                <?php
                                }
                            }

                            if (in_array('promocode', $accessArr)) {
                                $reservation_pmenu = 11;
                                if ((in_array($reservation_pmenu, $menusections ['psections']))) {
                                    ?>
                                    <li class="<?php echo ($curControllerLower == 'promo') ? "active" : ''; ?>">
                                        <a href="/admin/promo"> <i class="fa fa-archive"></i> <span class="title" >Promo</span>
                                                <span class="selected"></span> 
                                        </a>
                                    </li>	
                                    <?php
                                }
                            }

                            if (in_array('category', $accessArr)) {
                                        $reservation_pmenu = 14;
                                        if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                                                $main_subsection = array(
                                                        "category/categoryadd" => "Category Add",
                                                        "category/categorylist" => "Category List",
                                                );
                                                ?>
                                                <li
                                                        class="<?php echo ($curControllerLower == 'category') ? "active" : ''; ?>">
                                                        <a href="javascript:;"> <i class="fa fa-building-o"></i>
                                                                <span >Category</span>
                                                                <span class="selected"></span> <span
                                                                        class="arrow <?php //echo ($curControllerLower == 'buildtemp') ? "open" : '';    ?>">
                                                                </span>
                                                        </a>

                                                        <?php
                                                        echo '<ul class="sub-menu">';
                                                        foreach ($main_subsection as $ctName => $ctTitle) {
//                                        if (in_array($ctTitle, $menusections ['sections'])) {
                                                                if ($ctName == "search/create") {
                                                                        $ctName = "search/create/type/details";
                                                                }
                                                                if ($ctName == "category" && $curControllerLower == "category")
                                                                        $class_content = 'class="active"';
                                                                else
                                                                        $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                                                echo '<li ' . $class_content . '>';
                                                                echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                                                echo '</li>';
                                                                if ($ctName == "search/create/type/details") {
                                                                        $ctName = "search/create";
                                                                }
//                                        }
                                                        }
                                                        echo '</ul>';
                                                        ?>			
                                                </li>
                                                <?php
                                        }
                                }

                                $bases_pmenu = 4;

                                if (in_array('memberaccess', $accessArr)) {
                                        /* access menu start */
                                        $reservation_pmenu = 15;
                                        if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                                                $main_subsection = array(
                                                        "UserHasAccess/members" => "Members",
                                                        "Department" => "Department",
                                                );
                                                ?>

                                                <li
                                                        class="<?php echo (($curControllerLower == 'userhasaccess') || ($curControllerLower == 'department')) ? "active" : ''; ?>">
                                                        <a href="javascript:;"> <i class="fa fa-user"></i>
                                                                <span class="title" >Member Access</span>
                                                                <span class="selected"></span> <span
                                                                        class="arrow <?php echo ($curControllerLower == 'userhasaccess') ? "open" : ''; ?>">
                                                                </span>
                                                        </a>

                                                        <?php
                                                        echo '<ul class="sub-menu">';
                                                        foreach ($main_subsection as $ctName => $ctTitle) {
//                                        if (in_array($ctTitle, $menusections ['sections'])) {
                                                                if ($ctName == "search/create") {
                                                                        $ctName = "search/create/type/details";
                                                                }
                                                                if ($ctName == "BuildTemp" && $curControllerLower == "userhasaccess")
                                                                        $class_content = 'class="active"';
                                                                else
                                                                        $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                                                echo '<li ' . $class_content . '>';
                                                                echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                                                echo '</li>';
                                                                if ($ctName == "search/create/type/details") {
                                                                        $ctName = "search/create";
                                                                }
//                                        }
                                                        }
                                                        echo '</ul>';
                                                        ?>			
                                                </li>
                                                <?php
                                        }
                                }

                                    $reservation_pmenu = 16;
                                    if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                                            ?>

                                            <li class="<?php echo ($curAction == 'resetpassword') ? "active" : ''; ?>">
                                                    <a href="/admin/User/resetpassword"> <i class="fa fa-user"></i>
                                                            <span class="title">Change Password</span>
                                                            <span class="selected"></span>
                                                    </a>			
                                            </li>
                                            <?php
                                    }

                                    if (in_array('settings', $accessArr)) {
                                            if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                                                    if (in_array('packagesettings', $accessArr)) {
                                                            $reservation_subsection1 = array(
                                                                    "package/packagesettings" => "Package Settings",
                                                            );
                                                    } else {
                                                            $reservation_subsection1 = array();
                                                    }
                                                    if (in_array('walletsettings', $accessArr)) {
                                                            $reservation_subsection2 = array(
                                                                    "wallet/walletsettings" => "Wallet Settings",
                                                            );
                                                    } else {
                                                            $reservation_subsection2 = array();
                                                    }
                                                    if (in_array('ecurrencysettings', $accessArr)) {
                                                            $reservation_subsection3 = array(
                                                                    "wallet/ecurrencysettings" => "Ecurrency Settings",
                                                            );
                                                    } else {
                                                            $reservation_subsection3 = array();
                                                    }
                                                    if (in_array('ecurrencysettings', $accessArr)) {
                                                            $reservation_subsection4 = array(
                                                                    "systemsetting/systemlist" => "System Settings",
                                                            );
                                                    } else {
                                                            $reservation_subsection4 = array();
                                                    }
                                                    $reservation_subsection = array_merge($reservation_subsection1, $reservation_subsection2, $reservation_subsection3, $reservation_subsection4);
                                                    ?>

                                                    <li class="<?php echo (($curAction == 'packagesettings') || ($curAction == 'walletsettings') || ($curAction == 'ecurrencysettings') || ($curAction == 'systemlist') ) ? "active" : ''; ?>">
                                                            <a href="javascript:;"> <i class="fa fa-cogs"></i>
                                                                    <span class="title">Settings</span>
                                                                    <span class="selected"></span> 
                                                                    <span class="arrow <?php echo (($curAction == 'packagesettings') || ($curAction == 'walletsettings') || ($curAction == 'ecurrencysettings') || ($curAction == 'systemlist ')) ? "open" : ''; ?>">
                                                                    </span>
                                                            </a>

                                                            <?php
                                                            echo '<ul class="sub-menu">';
                                                            foreach ($reservation_subsection as $ctName => $ctTitle) {
//                                        if (in_array($ctTitle, $menusections ['sections'])) {
                                                                    if ($ctName == "search/create") {
                                                                            $ctName = "search/create/type/details";
                                                                    }
                                                                    $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';
                                                                    echo '<li ' . $class_content . '>';
                                                                    echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                                                    echo '</li>';
                                                                    if ($ctName == "search/create/type/details") {
                                                                            $ctName = "search/create";
                                                                    }
//                                        }
                                                            }
                                                            echo '</ul>';
                                                            ?>			
                                                    </li>

                                                    <?php
                                            }
                                    }

                                    if (in_array('summary', $accessArr)) {
                                            $reservation_pmenu = 17;
                                            if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {

                                                    if (in_array('cashwallet', $accessArr)) {
                                                            $main_subsection1 = array(
                                                                    "wallet/fundwallet" => "Bid Wallet",
                                                            );
                                                    } else {
                                                            $main_subsection1 = array();
                                                    }
                                                    if (in_array('cashwallet', $accessArr)) {
                                                            $main_subsection2 = array(
                                                                    "wallet/cashbackwallet" => "CashBack Wallet",
                                                            );
                                                    } else {
                                                            $main_subsection2 = array();
                                                    }
                                                    $main_subsection = array_merge($main_subsection1, $main_subsection2);
                                                    ?>

                                                    <li
                                                            class="<?php echo (($curAction == 'fundwallet') || ($curAction == 'cashbackwallet')) ? "active" : ''; ?>">
                                                            <a href="javascript:;"> <i class="fa fa-user"></i>
                                                                    <span class="title">Summary</span>
                                                                    <span class="selected"></span> <span
                                                                            class="arrow <?php echo (($curAction == 'fundwallet') || ($curAction == 'cashbackwallet')) ? "open" : ''; ?>">
                                                                    </span>
                                                            </a>

                                                            <?php
                                                            echo '<ul class="sub-menu">';
                                                            foreach ($main_subsection as $ctName => $ctTitle) {
//                                        if (in_array($ctTitle, $menusections ['sections'])) {
                                                                    if ($ctName == "search/create") {
                                                                            $ctName = "search/create/type/details";
                                                                    }
                                                                    if ($curAction == "resetpassword" && $curControllerLower == "user")
                                                                            $class_content = 'class="active"';
                                                                    else
                                                                            $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                                                    echo '<li ' . $class_content . '>';
                                                                    echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                                                    echo '</li>';
                                                                    if ($ctName == "search/create/type/details") {
                                                                            $ctName = "search/create";
                                                                    }
//                                        }
                                                            }
                                                            echo '</ul>';
                                                            ?>			
                                                    </li>
                                                    <?php
                                            }
                                    }

                                    if (in_array('cms', $accessArr)) {
                                            $reservation_pmenu = 18;
                                            if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {
                                                    $main_subsection1 = array();    
                                                    $main_subsection2 = array();    
                                                    $main_subsection3 = array();    
                                                    $main_subsection4 = array();    
                                                    if (in_array('news', $accessArr)) {
                                                            $main_subsection1 = array(
                                                                    "cms/news" => "News",
                                                            );
                                                    } 
                                                    if (in_array('faq', $accessArr)) {
                                                            $main_subsection2 = array(
                                                                    "cms/faq" => "FAQ",
                                                            );
                                                    } 
                                                    if (in_array('popup', $accessArr)) {
                                                            $main_subsection3 = array(
                                                                    "cms/popup" => "Popup",
                                                            );
                                                    }
                                                    if (in_array('homebanner', $accessArr)) {
                                                            $main_subsection4 = array(
                                                                    "cms/homebanner" => "Home Banner",
                                                            );
                                                    } 
                                                    
                                                    $main_subsection = array_merge($main_subsection1, $main_subsection2, $main_subsection3, $main_subsection4);
                                                    ?>

                                                    <li
                                                            class="<?php echo (($curAction == 'news') || ($curAction == 'faq') || ($curAction == 'popup') || ($curAction == 'homebanner')) ? "active" : ''; ?>">
                                                            <a href="javascript:;"> <i class="fa fa-cog"></i>
                                                                    <span class="title">CMS</span>
                                                                    <span class="selected"></span> <span
                                                                            class="arrow <?php echo (($curAction == 'news') || ($curAction == 'faq') || ($curAction == 'popup') || ($curAction == 'homebanner')) ? "open" : ''; ?>">
                                                                    </span>
                                                            </a>

                                                            <?php
                                                            echo '<ul class="sub-menu">';
                                                            foreach ($main_subsection as $ctName => $ctTitle) {
//                                        if (in_array($ctTitle, $menusections ['sections'])) {
                                                                    if ($ctName == "search/create") {
                                                                            $ctName = "search/create/type/details";
                                                                    }
                                                                    if ($curAction == "resetpassword" && $curControllerLower == "user")
                                                                            $class_content = 'class="active"';
                                                                    else
                                                                            $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                                                    echo '<li ' . $class_content . '>';
                                                                    echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                                                    echo '</li>';
                                                                    if ($ctName == "search/create/type/details") {
                                                                            $ctName = "search/create";
                                                                    }
//                                        }
                                                            }
                                                            echo '</ul>';
                                                            ?>			
                                                    </li>
                                                    <?php
                                            }
                                    }
                                    
                                    if (in_array('library', $accessArr)) {
                                            $reservation_pmenu = 18;
                                            if ((in_array($reservation_pmenu, $menusections ['psections'])) || (in_array($reservation_pmenu, $menusections ['section_ids']))) {

                                                    if (in_array('library_category', $accessArr)) {
                                                            $main_subsection1 = array(
                                                                    "library/category" => "Manage Category",
                                                            );
                                                    } else {
                                                            $main_subsection1 = array();
                                                    }
                                                    if (in_array('library_media', $accessArr)) {
                                                            $main_subsection2 = array(
                                                                    "library/media" => "Manage Media",
                                                            );
                                                    } else {
                                                            $main_subsection2 = array();
                                                    }

                                                    $main_subsection = array_merge($main_subsection1, $main_subsection2);
                                                    ?>

                                                    <li
                                                            class="<?php echo ($curControllerLower == 'library') ? "active" : ''; ?>">
                                                            <a href="javascript:;"> <i class="fa fa-picture-o"></i>
                                                                    <span class="title">Resource Library</span>
                                                                    <span class="selected"></span> <span
                                                                            class="arrow <?php echo ($curControllerLower == 'library') ? "open" : ''; ?>">
                                                                    </span>
                                                            </a>

                                                            <?php
                                                            echo '<ul class="sub-menu">';
                                                            foreach ($main_subsection as $ctName => $ctTitle) {
//                                        if (in_array($ctTitle, $menusections ['sections'])) {
                                                                    if ($ctName == "search/create") {
                                                                            $ctName = "search/create/type/details";
                                                                    }
                                                                    if ($curAction == "resetpassword" && $curControllerLower == "user")
                                                                            $class_content = 'class="active"';
                                                                    else
                                                                            $class_content = ($curControllerLower . "/" . $curActionLower == $ctName) ? 'class="active"' : '';

                                                                    echo '<li ' . $class_content . '>';
                                                                    echo '<a href="/admin/' . $ctName . '">' . Yii::t('translation', $ctTitle) . '</a>';
                                                                    echo '</li>';
                                                                    if ($ctName == "search/create/type/details") {
                                                                            $ctName = "search/create";
                                                                    }
//                                        }
                                                            }
                                                            echo '</ul>';
                                                            ?>			
                                                    </li>
                                                    <?php
                                            }
                                    }

                                    $bases_pmenu = 4;
                                    ?>

                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">

                    <?php
                    $header_curController = @Yii::app()->controller->id;
                    $header_curAction = @Yii::app()->getController()->getAction()->controller->action->id;
                    $menu_cond = ($header_curAction == "index") ? false : true;
                    if ($menu_cond) {
                        ?>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                                <ul class="page-breadcrumb breadcrumb">

                                    <li>
                                        <?php
                                        $this->widget('zii.widgets.CBreadcrumbs', array(
                                            'homeLink' => false,
                                            'links' => $this->breadcrumbs
                                        ));
                                        ?>
                                    </li>
                                </ul>
                                <!-- END PAGE TITLE & BREADCRUMB-->
                            </div>
                        </div>
                    <?php } ?>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12 scroll-row">
                            <?php echo $content; ?>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="footer">
            <div class="footer-inner">
		<?php echo date("Y"); ?> &copy; Mavwealth
            </div>
            <div class="footer-tools">
                <span class="go-top"> <i class="fa fa-angle-up"></i>
                </span>
            </div>
        </div>
        <!-- END FOOTER -->
        <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" />
        <script src="/js/jquery.datetimepicker.full.min.js"></script>
        <script type="text/javascript">
        setTimeout(function () {
                $(".alert-success").hide();
        }, 3000);

        $('.datetimepicker').datetimepicker({
            format: 'Y-m-d H:i:s',
        });

        $('.onlydate').datetimepicker({
            format: 'Y-m-d',
            timepicker: false,
        });

        $('.datepicker').datetimepicker({
            format: 'Y-m-d',
            timepicker: false,
            maxDate: 0
        });

        function showError(msg) {
            bootbox.alert(msg, function () {
                //alert("Hello world callback");
            });
        }

        function showSucessMsg(msg, heading) {
            var settings = {
                theme: 'teal',
                // sticky: $('#notific8_sticky').is(':checked'),
                horizontalEdge: 'top',
                verticalEdge: 'right',
                heading: heading,
                life: 5000
            };
            $.notific8('zindex', 11500);
            $.notific8($.trim(msg), settings);
        }

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        $(".glyphicon-comment").click(function () {
            $(".chatuserList").toggle();
        });

        function selectAll(source) {
            checkboxes = document.getElementsByClassName('allcheckbox');
            for (var i in checkboxes)
                checkboxes[i].checked = source.checked;
        }
    </script>
    </body>
    <!-- END BODY -->
</html>
<?php // }  ?>
