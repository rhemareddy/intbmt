<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Finanace Reports' => '/admin/report/transaction',
    'Wallet Summary'
    );
$from ="";
if(isset($_GET)){
   $from = "?".$_SERVER['QUERY_STRING'];        
} 
if (isset($_GET['per_page']) && count($_GET) > 1) {
    $queryString = CommonHelper::remove_querystring_var($_SERVER["QUERY_STRING"], 'per_page');
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/wallet/walletsummary?' . $queryString . '&per_page=' . "'";
} else {
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/wallet/walletsummary?per_page=' . "'";
}
if (!empty($error)) {
    echo "<p class='alert alert-danger error-2'><i class='fa fa-times-circle icon-error'></i><span class='span-error-2'>" . $error . "</span></p>";
}
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">

<div class="col-md-12 walletSummery filter-toggle">
    <div class="expiration margin-topDefault confirmMenu row">
<?php if(isset($_GET) && !empty($_GET) && empty($error)) { ?>
<a class="export-csv" href="/admin/wallet/walletsummarycsv<?php echo $from; ?>"> CSV Export </a>
<?php } ?>
        <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/wallet/walletsummary">
            <div class="col-md-12 search-form">
                <div class="expiration confirmMenu">
                    <div class="expiration" id="walletreport">
                           <div class="col-md-3">
                               <div class="row">
                                   <div class="form-group">
                                        <label class="col-md-4 spl-label"> Search By</label> 
                                           <div class="col-md-8">
                                <input type="text" name="username" id="username" class="form-control search-group-input" placeholder="Enter Username.." value="<?php echo isset($_GET['username']) ? $_GET['username'] : ''; ?>" />        
                            </div>
                                        
                                   </div>
                               </div>
                                               
                         
                        
                           </div>
                            <div class="col-md-2">
                                    <?php $walletList = BaseClass::getWalletList(); ?>
                                <select class="customeSelect howDidYou form-control select2me confirmBtn" id="ui-id-5" name="walletType">
                                    <option value="">Select Wallet</option>
                                    <?php foreach ($walletList as $key => $value) { ?>
                                    <option value="<?php echo $value->id; ?>" <?php echo ($walletType == $value->id) ? "selected" : ""; ?> ><?php echo $value->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-3 no_pad_left">
                                <div class="dataTables_length" id="search_length">
                                    <label>Display&nbsp; 
                                        <select class="form-control customselect" id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                                            <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                                                <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                                            <?php } ?>
                                        </select>&nbsp; 
                                      </label>
                                </div>
                            </div>
                            <div class="col-md-1 col-sm-6">
                                <input type="submit" class="btn btn-success confirmOk margintop3" value="Search" name="submit" id="submit" >
                            </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
            if(isset($dataProvider) && !empty($dataProvider)) {
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'city-grid',
                'dataProvider' => $dataProvider,
                'enableSorting' => 'true',
                'ajaxUpdate' => true,
                'template' => "{pager}\n{items}\n{summary}\n{pager}",
                'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
                'pager' => array(
                    'header' => false,
                    'firstPageLabel' => "<<",
                    'prevPageLabel' => "<",
                    'nextPageLabel' => ">",
                    'lastPageLabel' => ">>",
                ),
                'columns' => array(
                    array(
                        'class' => 'IndexColumn',
                        'header' => '<span style="white-space: nowrap;">No.</span>',
                    ),
                     array(
                        'name' => 'transaction_id',
                        'header' => '<span style="white-space: nowrap;">TransactionID</span>',
                        'value' => '($data->transaction()->transaction_id) ? $data->transaction()->transaction_id : "N/A"',
                    ),
                    array(
                        'name' => 'created_at',
                        'header' => '<span style="white-space: nowrap;">Date &nbsp;</span>',
                        'value' => '$data->created_at',
                    ),
                    array(
                        'name' => 'id',
                        'header' => '<span style="white-space: nowrap;">Particulars</span>',
                        'value' => function($data) {
                            $walletType = isset($_GET['walletType']) ? $_GET['walletType'] : 1;
                            if(isset($_GET['username']) && !empty($_GET['username'])){
                            $userObject = User::model()->findByAttributes(array('name' => $_GET['username']));
                                 if(isset($userObject->id) && $userObject->id == $data->from_user_id && ($data->from_fund_type == $walletType)){
                                   echo "To:".$data->touser->name;
                                 }else {
                                   echo "From:".$data->fromuser->name;
                                 }
                            }
                         },
                    ),
                    array(
                        'name' => 'fund',
                        'header' => '<span style="white-space: nowrap;">Deposit(+)</span>',
                        'value' => function($data) {
                            $walletType = isset($_GET['walletType']) ? $_GET['walletType'] : 1; 
                            if(isset($_GET['username']) && !empty($_GET['username'])){
                            $userObject = User::model()->findByAttributes(array('name' => $_GET['username']));
                                 if(isset($userObject->id) && $userObject->id == $data->to_user_id && ($data->to_fund_type == $walletType)){
                                   echo $data->fund;
                                 }else {
                                    echo "-"; 
                                 }
                            }
                         },
                    ),
                     array(
                        'name' => 'fund',
                        'header' => '<span style="white-space: nowrap;">Withdraw(-)</span>',
                        'value' => function($data) {
                            $walletType = isset($_GET['walletType']) ? $_GET['walletType'] : 1; 
                            if(isset($_GET['username']) && !empty($_GET['username'])){
                            $userObject = User::model()->findByAttributes(array('name' => $_GET['username']));
                                 if(isset($userObject->id) && $userObject->id == $data->from_user_id && ($data->from_fund_type == $walletType)){
                                   echo $data->fund;
                                 }else {
                                    echo "-"; 
                                 }
                            }
                         },
                    ),
                     array(
                        'name' => 'id',
                        'header' => '<span style="white-space: nowrap;">Balance</span>',
                        'value' => function($data) {
                            $walletType = isset($_GET['walletType']) ? $_GET['walletType'] : 1; 
                            if(isset($_GET['username']) && !empty($_GET['username'])){
                            $userObject = User::model()->findByAttributes(array('name' => $_GET['username']));
                                 if(isset($userObject->id) && $userObject->id == $data->from_user_id && ($data->from_fund_type == $walletType)){
                                   echo $data->from_user_balance;
                                 }else {
                                   echo $data->to_user_balance;
                                 }
                            }
                         },
                    ),
                    array(
                        'name' => 'id',
                        'header' => '<span style="white-space: nowrap;">Mode &nbsp;</span>',
                        'value' => '($data->transaction()->mode) ? $data->transaction()->mode : "N/A"',
                    ),
                    array(
                        'name' => 'comment',
                        'header' => '<span style="white-space: nowrap;">Comment &nbsp;</span>',
                        'value' => '$data->comment',
                    ),
                ),
            ));
            } ?>
        </div>
    </div>
