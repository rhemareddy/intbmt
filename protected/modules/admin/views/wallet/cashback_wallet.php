<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Summary' => '/admin/wallet/fundwallet',
    'CashBack Wallet'
);
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	

<div class="user-status filter-toggle">
    <div class="expiration margin-topDefault confirmMenu">

        <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/wallet/cashbackwallet">
            <div class="input-group input-large date-picker input-daterange pull-left mobile-left-0">
                <input type="text" name="from" placeholder="To Date" class="datepicker form-control" value="<?php echo (isset($_GET['from']) && !empty($_GET['from']))?$_GET['from']:""; ?>">
                <span class="input-group-addon">
                    to </span>
                <input type="text" name="to" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="<?php echo (isset($_GET['to']) && !empty($_GET['to']))?$_GET['to']:""; ?>">
            </div>
            <?php /* $statusId =   1;
              if(isset($_REQUEST['res_filter'])){
              $statusId =   $_REQUEST['res_filter'];
              } ?>

              <select class="customeSelect howDidYou form-control input-medium select2me confirmBtn" id="ui-id-5" name="res_filter">
              <option value="1" <?php if($statusId == 1){ echo "selected"; } ?> >Active</option>
              <option value="0" <?php if($statusId == 3){ echo "selected"; } ?> >In Active</option>
              </select>
              </div> */ ?>
            
            <div class="col-md-3 ">
                <div class="dataTables_length" id="search_length">
                    <label>Display</label>
                    <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                            <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                            <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                            <?php } ?>
                    </select>   
                </div>
            </div>
            
            <input type="submit" class="btn btn-primary confirmOk mobile-left-15" value="OK" name="submit" id="submit" style="left:340px!important;top:0px!important;"/>
        </form>

    </div>
</div>

<div class="row">
    <div class="col-md-12 blue-table">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'city-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{pager} {items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',
                array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">Sl. No &nbsp; &nbsp; &nbsp;</span>',
                ),
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Transfer From &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->fromuser->name)? ucwords($data->fromuser->name):""',
                ),
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Transfer To &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->touser->name)? ucwords($data->touser->name):""',
                ),
                array(
                       'name' => 'id',
                       'header' => '<span style="white-space: nowrap;color: #01b7f2;">Transfer Status &nbsp; &nbsp; &nbsp;</span>',
                       'value' => '($data->status == 1) ? "Transfered" : "Pending"',
                   ),
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;color: #01b7f2;">Transferred Amount &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->fund != 0) ? $data->fund : "N/A"',
                ),
                array(
                    'name' => 'comment',
                    'header' => '<span style="white-space: nowrap;">Comment &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->comment',
                ),
                array(
                       'name' => 'id',
                       'header' => '<span style="white-space: nowrap;">Transfer Mode &nbsp; &nbsp; &nbsp;</span>',
                       'value' => '$data->transaction->mode',
                   ),
                array(
                       'name' => 'created_at',
                       'header' => '<span style="white-space: nowrap;">Transfer Date &nbsp; &nbsp; &nbsp;</span>',
                       'value' => '$data->created_at',
                ),
            ),
        ));
        ?>
    </div>
</div>
