<?php 
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Settings' => '/admin/package/packagesettings',
    'Ecurrency Setting',
); 
$count = 1 ;
?>
<p class="alert alert-success" id="success_msg" style="display:none;">E Currency Settings Updated Successfully.</p>
<p class="alert alert-error" id="error_msg" style="display:none;">Failed To Update Data</p>
<div class="portlet-body blue-table">
    <form>
            <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	

    <div class="user-status margin-bottom-10 filter-toggle">
        <div class="col-md-2 col-sm-6">
            <div class="form-group">
                <label class="col-md-3 margin-topDefault">Mode</label>
                 <div class="col-md-9 col-sm-9">
                    <select name="mode" class="customeSelect howDidYou form-control ">
                        <option value="">All</option>
                        <option value="ONLINE" <?php echo (($_GET) && $_GET['mode'] == 'ONLINE') ? 'selected' : ''; ?>>ONLINE</option>
                        <option value="OFFLINE" <?php echo (($_GET) && $_GET['mode'] == 'OFFLINE') ? 'selected' : ''; ?>>OFFLINE</option>
                    </select> 
                 </div>
            </div>
        </div>
        
        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <label class="col-md-3 margin-topDefault">Payment Status</label>
                 <div class="col-md-9 col-sm-9">
                    <select name="payment_status"  class="customeSelect howDidYou form-control ">
                        <option value="">All</option>
                        <option value="1" <?php echo (($_GET) && $_GET['payment_status'] == '1') ? 'selected' : ''; ?>>ACTIVE</option>
                        <option value="0" <?php echo (($_GET) && $_GET['payment_status'] == '0') ? 'selected' : ''; ?>>In ACTIVE</option>
                    </select> 
                 </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6">
            <div class="form-group">
                <label class="col-md-3 margin-topDefault">Withdrawal Status</label>
                 <div class="col-md-9 col-sm-9">
                    <select name="withdrawal_status"  class="customeSelect howDidYou form-control ">
                        <option value="">All</option>
                        <option value="1" <?php echo (($_GET) && $_GET['withdrawal_status'] == '1') ? 'selected' : ''; ?>>ACTIVE</option>
                        <option value="0" <?php echo (($_GET) && $_GET['withdrawal_status'] == '0') ? 'selected' : ''; ?>>In ACTIVE</option>
                    </select> 
                 </div>
            </div>
        </div>
        
        <div class="col-md-1 col-sm-6">
            <input type="submit"  name="submit" value="Filter" class="btn btn-primary f-left margin-left15 margin-topDefault">
        </div>  
    </div>
    
</form>
    
    <table class="table table-striped table-hover table-bordered customFluidTable" id="sample_editable_1">
        <thead>
            <tr>
                <th style="display:none;">id</th>
                <th>S.No</th>
                <th>Ecurrency Name</th>
                <th>Mode</th>
                <th>Title</th>
                <th style="width:10px">Description</th>
                <th>Min Payment</th>
                <th>Payment Status</th>
                <th>Min Withdrawal</th>
                <th>Withdrawal Status</th>
                <th>Priority</th>
                <th>Edit</th>
                <th>Cancel</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($gatewayObject as $gateway) { ?>
                <tr >
                    <td style="display:none;"><?php echo $gateway->id; ?></td>
                    <td ><?php echo $count; ?></td>
                    <td>
                        <p id="eccurency_name_<?php echo $gateway->id; ?>"><?php echo $gateway->name; ?></p>
                    </td>
                    <td>
                        <p id="mode_<?php echo $gateway->id; ?>"><?php echo ($gateway->mode == 'ONLINE') ? 'ONLINE' : 'OFFLINE'; ?></p>
                        <p id="input_mode_<?php echo $gateway->id; ?>" style="display:none;">
                            <select id="mode_value_<?php echo $gateway->id; ?>">
                                <option value="ONLINE" <?php echo ($gateway->mode == 'ONLINE') ? 'selected' : ''; ?> >ONLINE</option>
                                <option value="OFFLINE" <?php echo ($gateway->mode == 'OFFLINE') ? 'selected' : ''; ?> >OFFLINE</option>
                            </select>
                        </p>
                    </td>
                    <td>
                        <p id="title_<?php echo $gateway->id; ?>"><?php echo $gateway->title; ?></p>
                        <p id="input_title_<?php echo $gateway->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $gateway->title; ?>" name="title_<?php echo $gateway->id; ?>" id="title_value_<?php echo $gateway->id; ?>" />
                        </p>
                    </td>
                    <td>
                        <p id="description_<?php echo $gateway->id; ?>"><?php echo $gateway->description; ?></p>
                        <p id="input_description_<?php echo $gateway->id; ?>" style="display:none;">
                            <textarea name="description_<?php echo $gateway->id; ?>" cols="5" rows="5" id="description_value_<?php echo $gateway->id; ?>"><?php echo $gateway->description; ?></textarea>
                        </p>
                    </td>
                    <td>
                        <p id="min_deposit_<?php echo $gateway->id; ?>"><?php echo $gateway->min_deposit; ?></p>
                        <p id="input_min_deposit_<?php echo $gateway->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $gateway->min_deposit; ?>" name="min_deposit_<?php echo $gateway->id; ?>" id="min_deposit_value_<?php echo $gateway->id; ?>" />
                        </p>
                    </td>

                    <td>
                        <p id="paymentstatus_<?php echo $gateway->id; ?>"><?php echo ($gateway->payment_status == 1) ? 'Active' : 'In Active'; ?></p>
                        <p id="input_paymentstatus_<?php echo $gateway->id; ?>" style="display:none;">
                            <select id="paymentstatus_value_<?php echo $gateway->id; ?>">
                                <option value="1" <?php echo ($gateway->payment_status == 1) ? 'selected' : ''; ?> >Active</option>
                                <option value="0" <?php echo ($gateway->payment_status == 0) ? 'selected' : ''; ?> >In Active</option>
                            </select>
                        </p>
                    </td>
                    
                    <td>
                        <p id="min_withdrawal_<?php echo $gateway->id; ?>"><?php echo $gateway->min_withdrawal; ?></p>
                        <p id="input_min_withdrawal_<?php echo $gateway->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $gateway->min_withdrawal; ?>" name="min_withdrawal_<?php echo $gateway->id; ?>" id="min_withdrawal_value_<?php echo $gateway->id; ?>" />
                        </p>
                    </td>
                    
                    <td>
                        <p id="withdrawalstatus_<?php echo $gateway->id; ?>"><?php echo ($gateway->withdrawal_status == 1) ? 'Active' : 'In Active'; ?></p>
                        <p id="input_withdrawalstatus_<?php echo $gateway->id; ?>" style="display:none;">
                            <select id="withdrawalstatus_value_<?php echo $gateway->id; ?>">
                                <option value="1" <?php echo ($gateway->withdrawal_status == 1) ? 'selected' : ''; ?> >Active</option>
                                <option value="0" <?php echo ($gateway->withdrawal_status == 0) ? 'selected' : ''; ?> >In Active</option>
                            </select>
                        </p>
                    </td>
                    
                    <td>
                        <p id="priority_<?php echo $gateway->id; ?>"><?php echo $gateway->priority; ?></p>
                        <p id="input_priority_<?php echo $gateway->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $gateway->priority; ?>" name="priority_<?php echo $gateway->id; ?>" id="priority_value_<?php echo $gateway->id; ?>" />
                        </p>
                    </td>
                    
                    <td>
                        <a class="edit" href="javascript:;" id="editEcurrency_<?php echo $gateway->id; ?>" onclick="editEcurrencyData(<?php echo $gateway->id; ?>);">Edit</a>
                        <a class="save" style="display:none;" id="saveEcurrency_<?php echo $gateway->id; ?>" href="javascript:;" onclick="saveEcurrencyData(<?php echo $gateway->id; ?>);">Save</a>
                    </td>
                    <td><a class="cancel" href="javascript:;" style="display:none;" id="cancelEcurrency_<?php echo $gateway->id; ?>" onclick="restoreEcurrencyData(<?php echo $gateway->id; ?>);">Cancel</a></td>
                </tr>
            <?php $count++ ; } ?>
        </tbody>
    </table>
</div>
<script src="/js/ecurrencysettings.js" type="text/javascript"></script>