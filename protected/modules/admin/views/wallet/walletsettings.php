<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Settings' => '/admin/package/packagesettings',
    'Wallet Settings',
);
?>
<div class="alert alert-success" id="success_msg" style="display:none;"><strong>Information Updated Successfully.</strong></div>
<div class="alert alert-danger" id="error_msg" style="display:none;"><strong>Failed To Update Data.</strong></div>

<div class="portlet-body blue-table">
    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
        <thead>
            <tr>
                <th style="display:none;">id</th>
                <th>Wallet Name</th>
                <th>Withdraw Charges(%)</th>
                <th>Withdraw Status</th>
                <th>Transfer Charges(%)</th>
                <th>Transfer Status</th>
                <th>Purchase Percentage</th>
                <th>Edit</th>
                <th>Cancel</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($walletTypeObject as $walletType) { ?>
                <tr >
                    <td style="display:none;"><?php echo $walletType->id; ?></td>
                    <td>
                        <p id="wallet_name_<?php echo $walletType->id; ?>"><?php echo $walletType->name; ?></p>
                        <p id="input_wallet_name_<?php echo $walletType->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $walletType->name; ?>" id="wallet_name_value_<?php echo $walletType->id; ?>" name="wallet_name_<?php echo $walletType->id; ?>" />
                        </p>
                    </td>
                    <td>
                        <p id="withdrawal_charges_<?php echo $walletType->id; ?>"><?php echo $walletType->withdrawal_charges; ?></p>
                        <p id="input_withdrawal_charges_<?php echo $walletType->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $walletType->withdrawal_charges; ?>" name="withdrawal_charges_<?php echo $walletType->id; ?>" id="withdrawal_charges_value_<?php echo $walletType->id; ?>" />
                        </p>
                    </td>
                    <td>
                        <p id="withdraw_status_<?php echo $walletType->id; ?>"><?php echo ($walletType->withdraw_status == 1) ? 'Active' : 'InActive'; ?></p>
                        <p id="input_withdraw_status_<?php echo $walletType->id; ?>" style="display:none;">
                            <select id="withdraw_status_value_<?php echo $walletType->id; ?>">
                                <option value="1" <?php echo ($walletType->withdraw_status == 1) ? 'selected' : ''; ?> >Active</option>
                                <option value="0" <?php echo ($walletType->withdraw_status == 0) ? 'selected' : ''; ?> >InActive</option>
                            </select>
                        </p>
                    </td>
                    <td>
                        <p id="transfer_charges_<?php echo $walletType->id; ?>"><?php echo $walletType->transfer_charges; ?></p>
                        <p id="input_transfer_charges_<?php echo $walletType->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $walletType->transfer_charges; ?>" name="transfer_charges_<?php echo $walletType->id; ?>" id="transfer_charges_value_<?php echo $walletType->id; ?>" />
                        </p>
                    </td>
                    <td>
                        <p id="transfer_status_<?php echo $walletType->id; ?>"><?php echo ($walletType->transfer_status == 1) ? 'Active' : 'InActive'; ?></p>
                        <p id="input_transfer_status_<?php echo $walletType->id; ?>" style="display:none;">
                            <select id="transfer_status_value_<?php echo $walletType->id; ?>">
                                <option value="1" <?php echo ($walletType->transfer_status == 1) ? 'selected' : ''; ?> >Active</option>
                                <option value="0" <?php echo ($walletType->transfer_status == 0) ? 'selected' : ''; ?> >InActive</option>
                            </select>
                        </p>
                    </td>
                    <td>
                        <p id="purchase_percentage_<?php echo $walletType->id; ?>"><?php echo $walletType->purchase_percentage; ?></p>
                        <p id="input_purchase_percentage_<?php echo $walletType->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $walletType->purchase_percentage; ?>" name="purchase_percentage_<?php echo $walletType->id; ?>" id="purchase_percentage_value_<?php echo $walletType->id; ?>" />
                        </p>
                    </td>
                    <td>
                        <a class="edit" href="javascript:;" id="editWallet_<?php echo $walletType->id; ?>" onclick="editWalletData(<?php echo $walletType->id; ?>);">Edit</a>
                        <a class="save" style="display:none;" id="saveWallet_<?php echo $walletType->id; ?>" href="javascript:;" onclick="saveWalletData(<?php echo $walletType->id; ?>);">Save</a>
                    </td>
                    <td><a class="cancel" href="javascript:;" style="display:none;" id="cancelWallet_<?php echo $walletType->id; ?>" onclick="restoreWalletData(<?php echo $walletType->id; ?>);">Cancel</a></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script src="/js/walletsettings.js" type="text/javascript"></script>