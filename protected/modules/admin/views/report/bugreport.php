<?php 
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Bug Report'
);
$from = "";
if(isset($_GET['from']) && isset($_GET['to'])){
   $from = "?".$_SERVER['QUERY_STRING'];        
}

if (isset($_GET['per_page']) && count($_GET) > 1) {
    $queryString = CommonHelper::remove_querystring_var($_SERVER["QUERY_STRING"], 'per_page');
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/bugreport?' . $queryString . '&per_page=' . "'";
} else {
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/bugreport?per_page=' . "'";
}
?>

<div class="col-md-12">
    <div class="order-list-div col-md-12"> 
        <a class="export-csv" href="/admin/report/bugcsv<?php echo $from; ?>"> CSV Export </a>
        <div class="row">
            <div class="expiration margin-topDefault confirmMenu">

                <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report/bugreport" class="form-inline">
                    <div class="col-md-4  col-sm-6  margin-topDefault ">

                        <div class="input-group input-large date-picker input-daterange">
                            <input type="text" name="from" placeholder="From Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from'])) ? $_GET['from'] : DATE('Y-m-d'); ?>">
                            <span class="input-group-addon"> To </span>
                            <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to'])) ? $_GET['to'] : DATE('Y-m-d'); ?>">
                        </div>


                    </div>

                    <div class="col-md-3 no_pad_left">
                        <div class="dataTables_length" id="search_length">
                            <label>Display&nbsp; 
                                <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                                    <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                                        <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                                    <?php } ?>
                                </select>&nbsp; 
                                Records per page</label>
                        </div>

                    </div> 
                    <div class="col-md-5  col-sm-6 margin-topDefault ">
                        <input type="submit" class="btn btn-success confirmOk" value="OK" name="submit" id="submit" ></div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--+-->
<div class="row">
    <div class="col-md-12">
        <?php 
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            //'summaryText' => 'Showing {start} to {end} of {count} entries',
 
            //'template' => '<div class="table-responsive">{items}</div> {summary} {pager}',
            'template' => "{pager}\n{items}\n{summary}\n{pager}", //THIS DOES WHAT YOU WANT
 
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;">No.</span>',
                ),
                array(
                    'name' => 'name',
                    'header' => '<span style="white-space: nowrap;">Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->name) ? $data->name : "N/A"',
                ),
                
                array(
                    'name' => 'email',
                    'header' => '<span style="white-space: nowrap;">Email &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->email',
                ),
                array(
                    'name' => 'phone',
                    'header' => '<span style="white-space: nowrap;">Phone  &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->phone) ? $data->phone : "N/A"',
                ),
                array(
                    'name' => 'message',
                    'header' => '<span style="white-space: nowrap;">Message &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->message) ? $data->message : "N/A"',
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Date &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
                
            ),
        ));
        ?>
    </div>
</div>
