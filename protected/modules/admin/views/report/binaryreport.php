<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Finanace Reports' => '/admin/report/transaction',
    'Generated Binary Report'
);
$from ="";
if(isset($_GET['from']) && isset($_GET['to'])){
   $from = "?".$_SERVER['QUERY_STRING'];        
} 
if (isset($_GET['per_page']) && count($_GET) > 1) {
    $queryString = CommonHelper::remove_querystring_var($_SERVER["QUERY_STRING"], 'per_page');
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/binaryreport?' . $queryString . '&per_page=' . "'";
} else {
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/binaryreport?per_page=' . "'";
}
if(!empty($error)){
    echo "<p class='error-2'><i class='fa fa-times-circle icon-error'></i><span class='span-error-2'>".$error."</span></p>";
}
?> 

 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">

    <div class="col-md-12 user-status filter-toggle"> 
        <div class="expiration confirmMenu row">
            <form id="user_filter_frm" name="user_filter_frm" method="get" action="/admin/report/binaryreport" > 
            <div class="col-md-4 top7">
                <div class="input-group input-large date-picker input-daterange" style="z-index:9;">
                    <input type="text" id="txtFromDate" name="from" placeholder="To Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from'])) ? $_GET['from'] : date('Y-m-d', strtotime(' -1 day')); ?>">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" id="txtToDate" name="to" data-provide="datepicker" placeholder="From Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to'])) ? $_GET['to'] : DATE('Y-m-d'); ?>">
                </div>
            </div>
      
            <div class="col-md-3 top7 ">
                <input type="text" name="search" id="search" class="form-control" placeholder="Search By Username.." value="<?php echo (!empty($_GET['search'])) ? $_GET['search'] : ""; ?>" />
                <span class="clrred" id="search_error"></span>
            </div>
                      <div class="col-md-2 no_pad_left top7">
                <div class="dataTables_length" id="search_length">
                    <label>Display&nbsp; 
                        <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                            <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                                <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                            <?php } ?>
                        </select>&nbsp; 
                   </label>
                </div>
            </div> 
            <div class="col-md-1">
                <input type="submit" class="btn btn-success margin-top-10" value="OK" name="submit" id="submit"/>
            </div>
        </div>
    </div>
<a class="btn buttons-csv pull-right margin-top-10" href="/admin/report/binaryreportcsv<?php echo $from; ?>"> CSV Export </a>
    <div class="row">
        <div class="col-md-12 blue-table">
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'state-grid',
                'dataProvider' => $dataProvider,
                'enableSorting' => 'true',
                'ajaxUpdate' => true,
                'template' => "{pager}\n{items}\n{summary}\n{pager}",
                'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
                'pager' => array(
                    'header' => false,
                    'firstPageLabel' => "<<",
                    'prevPageLabel' => "<",
                    'nextPageLabel' => ">",
                    'lastPageLabel' => ">>",
                ),
                'columns' => array(
                    array(
                        'class' => 'IndexColumn',
                        'header' => '<span style="white-space: nowrap;">No.</span>',
                    ),                    
                    array(
                        'name' => 'date',
                        'header' => '<span style="white-space: nowrap;">Date &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->date',
                    ),
                    array(
                        'name' => 'user_id',
                        'header' => '<span style="white-space: nowrap;">User &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '(!empty($data->user)) ? $data->user->name : "NA"',
                    ),
                    array(
                        'name' => 'left_purchase',
                        'header' => '<span style="white-space: nowrap;">L. Purchase &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->left_purchase',
                    ),
                    array(
                        'name' => 'right_purchase',
                        'header' => '<span style="white-space: nowrap;">R. Purchase &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->right_purchase',
                    ),
                    array(
                        'name' => 'left_carry',
                        'header' => '<span style="white-space: nowrap;">L. Carry &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->left_carry',
                    ),
                    array(
                        'name' => 'right_carry',
                        'header' => '<span style="white-space: nowrap;">R. Carry &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->right_carry',
                    ),
                    array(
                        'name' => 'previous_left_carry',
                        'header' => '<span style="white-space: nowrap;">Prev L. Carry &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->previous_left_carry',
                    ),
                    array(
                        'name' => 'previous_right_carry',
                        'header' => '<span style="white-space: nowrap;">Prev R. Carry &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->previous_right_carry',
                    ),
                    array(
                        'name' => 'flush',
                        'header' => '<span style="white-space: nowrap;">flush &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->flush',
                    ),
                    array(
                        'name' => 'binary_commision',
                        'header' => '<span style="white-space: nowrap;">Commision &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->binary_commision',
                    ),
                    array(
                        'name' => 'created_at',
                        'header' => '<span style="white-space: nowrap;">Created At &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->created_at',
                    ),
                ),
            ));
            ?>
        </div>
    </div>