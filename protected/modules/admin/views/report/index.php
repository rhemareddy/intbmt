<ul class="page-breadcrumb breadcrumb">
    <li>
        <div class="breadcrumbs">
            <a href="/admin/default/dashboard">Dashboard</a> »
            <a href="/admin/report">Reports</a> »
            <span>Registration</span>
        </div>
    </li>
</ul>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">
<div class="col-md-12 user-status custom-padding filter-toggle">
    <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report">
        <div class="expiration margin-topDefault confirmMenu">
            <div class="col-md-3">          
                <div class="input-group  date-picker input-daterange">
                    <input type="text" id="form" name="from" data-provide="datepicker"  placeholder="From Date" class="datepicker form-control" value="<?php echo (!empty($_REQUEST) && $_REQUEST['from'] != '') ? $_REQUEST['from'] : ""; ?>">
                    <span class="input-group-addon"> to </span>
                    <input type="text" id="to" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="<?php echo (!empty($_REQUEST) && $_REQUEST['to'] != '') ? $_REQUEST['to'] : ""; ?>">
                </div>
            </div>
            <?php
            $statusId = 1;
            if (isset($_REQUEST['res_filter'])) {
                $statusId = $_REQUEST['res_filter'];
            }
            ?>
            <div class="col-md-2">  
                <select class="form-control " id="ui-id-5" name="res_filter">
                    <option value="1" <?php if($statusId == 1){ echo "selected"; } ?> >Active</option>
                    <option value="0" <?php if($statusId == 0){ echo "selected"; } ?> >In Active</option>
                </select>
            </div>
            <input type="submit" class="btn btn-primary f-left margintop3" value="OK" name="submit" id="submit"/>
        </div>
    </form>
  
</div>
<div class="row">
    <div class="col-md-12 blue-table">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;color:#01b7f2">No</span>',
                ),
                array(
                    'name' => 'name',
                    'header' => '<span style="white-space: nowrap;">Name &nbsp;</span>',
                    'value' => '$data->name',
                ),
                array(
                    'name' => 'full_name',
                    'header' => '<span style="white-space: nowrap;">Full Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->full_name',
                ),
                array(
                    'name' => 'phone',
                    'header' => '<span style="white-space: nowrap;">Phone &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this ,'gridProfilePhoneNumber'),
                ),
                array(
                    'name' => 'email',
                    'header' => '<span style="white-space: nowrap;">Email &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->email',
                ),
                array(
                    'name' => 'sponsor_id',
                    'header' => '<span style="white-space: nowrap;">Sponser Id &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->sponsor()->name)?$data->sponsor()->name:""',
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Registered Date &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
                array(
                    'name' => 'status',
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                ),
            ),
        ));
        ?>
    </div>
</div>
