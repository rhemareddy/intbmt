<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Reports' => '/admin/report',
    'Company Sponsor'
);
$from = "";
$csv = "?csv=1";
if (isset($_SERVER['QUERY_STRING'])) {
    $from = "?" . $_SERVER['QUERY_STRING'];
    $csv = '&csv=1';
}

if (Yii::app()->user->hasFlash('success')) { 
    echo '<p class="alert alert-success" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">'.Yii::app()->user->getFlash('success').'</span></p>';
}
if (Yii::app()->user->hasFlash('error')) { 
    echo '<p class="alert alert-danger" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">'.Yii::app()->user->getFlash('error').'</span></p>';
}  
    ?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">

<div class="user-status col-md-12 filter-toggle">
    <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report/companysponsor">
        <div class="expiration margin-topDefault confirmMenu">
            <div class="col-md-3 col-sm-3">
                <div class="input-group  date-picker ">
                    <input type="text" name="from" placeholder="From Date" class="datepicker form-control" value="<?php echo (!empty($_GET) && $_GET['from'] != '') ? $_GET['from'] : ""; ?>">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="<?php echo (!empty($_GET) && $_GET['to'] != '') ? $_GET['to'] : ""; ?>">
                </div>
            </div>
            <?php
            $statusId = 1;
            if (isset($_REQUEST['res_filter'])) {
                $statusId = $_REQUEST['res_filter'];
            }
            ?>
            <div class="col-md-3 col-md-offset-2">
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3  padding0 spl-label">User Status</label>
                        <div class="col-md-5">
                            <select class="form-control" id="ui-id-5" name="res_filter">
                                <option value="1" <?php if ($statusId == 1) { echo "selected"; } ?> >Active</option>
                                <option value="0" <?php if ($statusId == 0) { echo "selected"; } ?> >In Active</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-3 padding0 spl-label">Sponsor Id</label>
                        <div class="col-md-5">
                            <select class="form-control" id="ui-id-5" name="sponsor_list">
                                <option value="" <?php echo (isset($_GET['sponsor_list']) && ($_GET['sponsor_list'] == '')) ? "selected" : ""; ?>selected>All</option>
                                <option value="1" <?php echo (isset($_GET['sponsor_list']) && ($_GET['sponsor_list'] == 1)) ? "selected" : ""; ?>>Mavwealth</option>
                                <option value="2" <?php echo (isset($_GET['sponsor_list']) && ($_GET['sponsor_list'] == 2)) ? "selected" : ""; ?>>Social</option>
                            </select>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 top20">
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-4 padding-right-0 top7">Contact Status</label>
                        <div class="col-md-5">
                            <select class="form-control" id="ui-id-5" name="contact_status">
                                <option value="PENDING" <?php echo (isset($_GET['contact_status']) && ($_GET['contact_status'] == 'PENDING')) ? "selected" : ""; ?>>PENDING</option>
                                <option value="CONTACTED" <?php echo (isset($_GET['contact_status']) && ($_GET['contact_status'] == 'CONTACTED')) ? "selected" : ""; ?>>CONTACTED</option>
                            </select>  
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 top20 col-md-offset-1 padding-left-0">
                <div class="dataTables_length mobile-left-15" id="search_length">
                    <label>Display</label>
                    <select id="per_page" name="per_page" aria-controls="" class="displayRecords form-control" onchange="//window.location = <?php //echo $baseUrl;   ?> + this.value">
<?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                            <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
<?php } ?>
                    </select>   
                </div>
            </div> 
            <div class="col-md-2 top20">
                <input type="submit" class="btn btn-primary " value="OK" name="submit" id="submit"/>
            </div>
        </div>
    </form>
</div>
<a class="btn buttons-csv pull-right margin-top-10" href="/admin/report/companysponsor<?php echo $from . $csv; ?>"> CSV Export </a>
<div class="clrred"></div>
<form method="post" action="/admin/report/companysponsor">
    <div class="row"> 
        <div class="col-md-4">
            <div class="form-group top20">
                <label class="col-md-4 padding-right-0" >Bulk Action </label>
                <div class="col-md-8">
                    <select class="customeSelect howDidYou form-control input-medium select2me confirmBtn" id="bulkaction" name="bulkaction">
                        <option value="0" >Select</option>
                        <option value="1">Mark Contacted</option>
                        <option value="2">Mark Pending</option>
                        <option value="3">Update Comment</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-sm-6 top20 form-inline">
            <input type="submit" style="display: none" class="btn btn-success confirmOk" value="Apply Action" name="submit" id="apply_action">
        </div>
    </div> 
    <div class="row">
        <div class="col-md-12 blue-table">
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'state-grid',
                'dataProvider' => $dataProvider,
                'enableSorting' => 'true',
                'ajaxUpdate' => true,
                'summaryText' => 'Showing {start} to {end} of {count} entries',
                'template' => '{pager} {items} {summary} {pager}',
                'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
                'pager' => array(
                    'header' => false,
                    'firstPageLabel' => "<<",
                    'prevPageLabel' => "<",
                    'nextPageLabel' => ">",
                    'lastPageLabel' => ">>",
                ),
                'columns' => array(
                    array(
                        'name' => '',
                        'header' => '<span style="white-space: nowrap;"><input type="checkbox" onclick="selectAll(this)" id="selectall"></span>',
                        'value' => array($this, 'GetComSpnsrCheckbox'),
                    ),
                    array(
                        'class' => 'IndexColumn',
                        'header' => '<span style="white-space: nowrap;color:#01b7f2">No</span>',
                    ),
                    array(
                        'name' => 'name',
                        'header' => '<span style="white-space: nowrap;">Name &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data["name"]',
                    ),
                    array(
                        'name' => 'full_name',
                        'header' => '<span style="white-space: nowrap;">Full Name &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data["full_name"]',
                    ),
                    array(
                        'name' => 'sponsor_id',
                        'header' => '<span style="white-space: nowrap;">Sponsor &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data["sponsor_id"] == 1 ? "Admin" : "Social"  ',
                    ),
                    array(
                        'name' => 'phone',
                        'header' => '<span style="white-space: nowrap;">Country &nbsp; &nbsp; &nbsp;</span>',
                        'value' => array($this ,'userCountry'),
                    ),
                    array(
                        'name' => 'phone',
                        'header' => '<span style="white-space: nowrap;">Phone &nbsp; &nbsp; &nbsp;</span>',
                        'value' => array($this ,'userPhoneNumber'),
                    ),
                    array(
                        'name' => 'email',
                        'header' => '<span style="white-space: nowrap;">Email &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data["email"]',
                    ),
                    array(
                        'name' => 'uCreatedDate',
                        'header' => '<span style="white-space: nowrap;">Registered Date &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data["uCreatedDate"]',
                    ),
                    array(
                        'name' => 'sponsor_id',
                        'header' => '<span style="white-space: nowrap;">Registered via &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data["social"] != "" ? $data["social"] : "Website"  ',
                    ),
                    array(
                        'name' => 'unique_id',
                        'header' => '<span style="white-space: nowrap;">Unique Id &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data["unique_id"] != "" ? $data["unique_id"] : "NA"  ',
                    ),
                    array(
                        'name' => 'status',
                        'value' => '($data["status"] == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                    ),
                    array(
                        'name' => 'status',
                        'header' => '<span style="white-space: nowrap;">Contact Status &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '($data["contact_status"]) ? $data["contact_status"]:"PENDING"',
                    ),
                    array(
                        'name' => 'status',
                        'header' => '<span style="white-space: nowrap;">Comment &nbsp; &nbsp; &nbsp;</span>',
                        'value' => array($this, 'getComSpnsrComment'),
                    ),
                ),
            ));
            ?>
        </div>
    </div>
</form>
