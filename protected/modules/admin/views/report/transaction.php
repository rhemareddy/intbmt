<?php 
$this->breadcrumbs = array(
    'Finance Reports',
    'Transaction Report'
);
$from ="";
if(isset($_GET['from']) || isset($_GET['to']) || isset($_GET['searchvalue']) ){
   $from = "?".$_SERVER['QUERY_STRING'];        
}  
if (isset($_GET['per_page']) && count($_GET) > 1) {
    $queryString = CommonHelper::remove_querystring_var($_SERVER["QUERY_STRING"], 'per_page');
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/transaction?' . $queryString . '&per_page=' . "'";
} else {
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/transaction?per_page=' . "'";
}
if (Yii::app()->user->hasFlash('success')){
     echo '<div class="alert alert-success">'.Yii::app()->user->getFlash('success') .'</div>';
}
if (Yii::app()->user->hasFlash('error')){ 
   echo '<p class="error-2 alert alert-danger" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">'.Yii::app()->user->getFlash('error').'</span></p>';
} 
if (!empty($error)) {
    echo "<p class='error-2 alert alert-danger'><i class='fa fa-times-circle icon-error'></i><span class='span-error-2'>" . $error . "</span></p>";
}
if (!empty($success)) {
    echo '<p class=" success-2 alert alert-success"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">'. $success . '</span></p>';
}
?> 
<div class="orderTransaction">
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">
    <div class="order-list-div col-md-12 filter-toggle">    
        <div class="expiration margin-topDefault confirmMenu">
            <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report/transaction">
                <div class="col-md-2 col-sm-6 orderTransactionDate nopadleft">
                    <label>Date</label>
                    <div class="input-group date-picker input-daterange">
                        <input type="text" name="from" placeholder="From Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from'])) ? $_GET['from'] : DATE('Y-m-d'); ?>">
                        <span class="input-group-addon">
                            to </span>
                        <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to'])) ? $_GET['to'] : DATE('Y-m-d'); ?>">
                    </div>
                </div>
                <?php $statusId =   1;
                if(isset($_GET['res_filter'])){ $statusId = $_GET['res_filter']; } ?>
                <div class="col-md-2 col-sm-6 statusDrop form-inline">
                    <label>Status</label> <br>
                    <select class="customeSelect howDidYou form-control full-widthSelect " id="ui-id-5" name="res_filter">
                        <option value="1" <?php if($statusId == 1){ echo "selected"; } ?> >Completed</option>
                        <option value="0" <?php if($statusId == 0){ echo "selected"; } ?> >Pending</option>
                        <option value="2" <?php if($statusId == 2){ echo "selected"; } ?> >Cancelled</option>
                        <option value="3" <?php if($statusId == 3){ echo "selected"; } ?> >Processing</option>
                    </select>           
                </div>
                <div class="col-md-2 col-sm-6 form-inline">
                    <label>Ecurrency</label>
                    <select class="customeSelect howDidYou form-control full-widthSelect " id="ui-id-5" name="ecurrency">
                        <option value="0" >All</option>
                        <?php foreach($gatewayObject as $gateway) { ?>
                        <option value="<?php echo $gateway->id; ?>" <?php if(isset($_GET['ecurrency']) && $_GET['ecurrency'] == $gateway->id){ echo "selected"; } ?>><?php echo $gateway->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-2 col-sm-6 form-inline">
				  <label>Type</label> <br>
                    <select class="customeSelect howDidYou form-control full-widthSelect " id="mode" name="type">
                        <option value="0" >All</option>
                        <option value="<?php echo Transaction::$_MODE_ADMIN_RECHARGE; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_ADMIN_RECHARGE){ echo "selected"; } ?>><?php echo Transaction::$_MODE_ADMIN_RECHARGE; ?></option>
                        <option value="<?php echo Transaction::$_MODE_DAILY_CASHBACK; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_DAILY_CASHBACK){ echo "selected"; } ?>><?php echo Transaction::$_MODE_DAILY_CASHBACK; ?></option>
                        <option value="<?php echo Transaction::$_MODE_ADMIN_DEDUCTFUND; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_ADMIN_DEDUCTFUND){ echo "selected"; } ?>><?php echo Transaction::$_MODE_ADMIN_DEDUCTFUND; ?></option>
                        <option value="<?php echo Transaction::$_MODE_ADMINCHARGES; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_ADMINCHARGES){ echo "selected"; } ?>><?php echo Transaction::$_MODE_ADMINCHARGES; ?></option>
                        <option value="<?php echo Transaction::$_MODE_ADDFUND; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_ADDFUND){ echo "selected"; } ?>><?php echo Transaction::$_MODE_ADDFUND; ?></option>
                        <option value="<?php echo Transaction::$_MODE_BINARYCOMMISSION; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_BINARYCOMMISSION){ echo "selected"; } ?>><?php echo Transaction::$_MODE_BINARYCOMMISSION; ?></option>
                        <option value="<?php echo Transaction::$_MODE_CASHBACK; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_CASHBACK){ echo "selected"; } ?>><?php echo Transaction::$_MODE_CASHBACK; ?></option>
                        <option value="<?php echo Transaction::$_MODE_DIRECTCOMMISSION; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_DIRECTCOMMISSION){ echo "selected"; } ?>><?php echo Transaction::$_MODE_DIRECTCOMMISSION; ?></option>
                        <option value="<?php echo Transaction::$_MODE_TRANSFER; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_TRANSFER){ echo "selected"; } ?>><?php echo Transaction::$_MODE_TRANSFER; ?></option>
                        <option value="<?php echo Transaction::$_MODE_WITHDRAWAL; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_WITHDRAWAL){ echo "selected"; } ?>><?php echo Transaction::$_MODE_WITHDRAWAL; ?></option>
                        <option value="<?php echo Transaction::$_MODE_PROMO_POINTS; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_PROMO_POINTS){ echo "selected"; } ?>><?php echo Transaction::$_MODE_PROMO_POINTS; ?></option>
                        <option value="<?php echo Transaction::$_MODE_BIDWIN; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_BIDWIN){ echo "selected"; } ?>><?php echo Transaction::$_MODE_BIDWIN; ?></option>
                        <option value="<?php echo Transaction::$_MODE_POWERLINE; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_POWERLINE){ echo "selected"; } ?>><?php echo Transaction::$_MODE_POWERLINE; ?></option>
                        <option value="<?php echo Transaction::$_WALLET_LOAD; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_WALLET_LOAD){ echo "selected"; } ?>><?php echo Transaction::$_WALLET_LOAD; ?></option>
                        <option value="<?php echo Transaction::$_MODE_BID; ?>" <?php if(isset($_GET['type']) && $_GET['type'] == Transaction::$_MODE_BID){ echo "selected"; } ?>><?php echo Transaction::$_MODE_BID; ?></option>
                    </select>
                </div>
                <div class="col-md-2 col-sm-6 form-inline">
				  <label>Is Verified</label>
                    <select class="customeSelect howDidYou form-control full-widthSelect" id="ui-id-5" name="isverified">
                        <option value="0" >All</option>
                        <option value="<?php echo Transaction::$_STATUS_VERIFIED; ?>" <?php if(isset($_GET['isverified']) && $_GET['isverified'] == Transaction::$_STATUS_VERIFIED){ echo "selected"; } ?>><?php echo Transaction::$_STATUS_VERIFIED; ?></option>
                        <option value="<?php echo Transaction::$_STATUS_NOTVERIFIED; ?>" <?php if(isset($_GET['isverified']) && $_GET['isverified'] == Transaction::$_STATUS_NOTVERIFIED){ echo "selected"; } ?>><?php echo Transaction::$_STATUS_NOTVERIFIED; ?></option>
                    </select>           
                </div>
                <div class="col-md-1 form-inline">
				  <label>Records</label> <br>
                    <div class="" id="search_length">
                        <select id="per_page" name="per_page" aria-controls="" class="customeSelect howDidYou form-control " onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                        <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                            <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                        <?php } ?>
                        </select>
                    </div>                
                </div>
                
                <div class="col-md-1 form-inline">
				  <label style="visibility: hidden">A</label> <br>
                    <div class="" id="search_length">
                        <input type="submit" class="btn btn-success confirmOk" value="OK" name="submit" id="submit"/>
                    </div>                
                </div>
                
            </form>
        </div>
    </div>
    <a class="btn buttons-csv pull-right" href="/admin/report/transactioncsv<?php echo $from; ?>"> CSV </a>

    <div class="expiration margin-topDefault confirmMenu">
    <form id="user_filter_frm" class="form-inline" name="user_filter_frm" method="get" action="/admin/report/transaction" onsubmit="return validateSearch();">
        <div class="col-md-12 margin-topDefault search-form mobile-fullwidth">
            <div class="expiration margin-topDefault confirmMenu row">
                <div class="expiration margin-topDefault">
                    <div class="col-md-4">
                        <div class="form-group search-group">
                            <label> Search For : </label> 
                            <input type="text" name="searchvalue" id="transaction_search" class="form-control searchUserName search-group-input" placeholder="Enter search keyword.." onblur="return validateSearch();" value="<?php echo isset($_GET['searchvalue']) ? $_GET['searchvalue'] : ''; ?>" />  
                            <span class="cls-error" id="search-error"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group search-group">
                            <label> Search By : </label>
                            <select class="form-control search-group-input inlineSelects"  id="ui-id-5" name="searchby">
                                <option value="name" <?php if (isset($_GET['searchby']) && $_GET['searchby'] == 'name') echo "selected"; ?> >Name</option>
                                <option value="transaction_id" <?php if (isset($_GET['searchby']) && $_GET['searchby'] == 'transaction_id') echo "selected"; ?> >Transaction Id</option>
                                <option value="payment_transaction_id" <?php if (isset($_GET['searchby']) && $_GET['searchby'] == 'payment_transaction_id') echo "selected"; ?> >Payment Ref Id</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-6 form-inline">
                        <input type="submit" class="btn btn-success" value="Search" name="search" id="search"/>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
    <form method="POST" action="">
        <input type="submit" class="btn btn-success confirmOk margin-topDefault f-left" value="Mark as Verified" name="proceed">
    <div class="row">
        <div class="col-md-12">
            <?php 
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'state-grid',
                'dataProvider' => $dataProvider,
                'enableSorting' => 'true',
                'ajaxUpdate' => true,
                'template' => "{pager}\n{items}\n{summary}\n{pager}",   
                'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
                'pager' => array(
                    'header' => false,
                    'firstPageLabel' => "<<",
                    'prevPageLabel' => "<",
                    'nextPageLabel' => ">",
                    'lastPageLabel' => ">>",
                ),
                'columns' => array(
                    array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">No.</span>',
                    ),
                    array(
                        'name' => '',
                        'header' => '<span style="white-space: nowrap;"><input type="checkbox" onclick="selectAll(this)" id="selectall"></span>',
                        'value'=>array($this,'GetVerifyStatusCheckbox'),                    
                    ),

                    array(
                        'name' => 'transaction_id',
                        'header' => '<span style="white-space: nowrap;">Txn Id &nbsp; &nbsp;</span>',
                        'value' => 'isset($data->transaction_id)?$data->transaction_id:""',
                    ),
                    
                    array(
                        'name' => 'created_at',
                        'header' => '<span style="white-space: nowrap;">Date  &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->created_at',
                    ),
                    array(
                        'name' => 'user_name',
                        'header' => '<span style="white-space: nowrap;">From User&nbsp; &nbsp;  &nbsp;</span>',
                        'value' => array($this, 'getFromUserName'),
                    ),
                    array(
                        'name' => 'user_name',
                        'header' => '<span style="white-space: nowrap;">To User&nbsp; &nbsp;  &nbsp;</span>',
                        'value' => array($this, 'getToUserName'),
                    ),
                    array(
                        'name' => 'actual_amount',
                        'header' => '<span style="white-space: nowrap;">Amount &nbsp; &nbsp; &nbsp;</span>',
                        'value' => 'isset($data->actual_amount)?$data->actual_amount:""',
                    ),
                    array(
                        'name' => 'paid_amount',
                        'header' => '<span style="white-space: nowrap;">Paid Amt &nbsp; &nbsp;</span>',
                        'value' => 'isset($data->paid_amount)?$data->paid_amount:""',
                    ),
                    array(
                        'name' => 'used_rp',
                        'header' => '<span style="white-space: nowrap;">Wallet Amt &nbsp; &nbsp;</span>',
                        'value' =>  array($this, 'userWalletAmount'),
                    ),
                    array(
                        'name' => 'status',
                        'header' => '<span style="white-space: nowrap;">Wallet Type &nbsp; &nbsp;</span>',
                        'value' => array($this, 'GetWalletInfo'),
                    ),
                    array(
                        'name'=>'gateway_id',
                        'header'=>'<span style="white-space: nowrap;">Ecurrency</span>',
                        'value'=>array($this,'getEcurrancyStatusWithResponse'),
                    ),
                    array(
                        'name' => 'payment_transaction_id',
                        'header' => '<span style="white-space: nowrap;">Payment Ref Id &nbsp; &nbsp;</span>',
                        'value' => array($this, 'CheckPaymentRefId'),
                    ),
                    
                    array(
                        'name' => 'status',
                        'header' => '<span style="white-space: nowrap;">Status &nbsp; &nbsp; &nbsp;</span>',
                        'value' => array($this,'getTrasactionStatus'),
                    ),
                    array(
                        'name' => 'comment',
                        'header' => '<span style="white-space: nowrap;">Comment &nbsp; &nbsp;</span>',
                        'value' => array($this,'moneyTransferComment'),
                    ),
                    array(
                        'name' => 'mode',
                        'header' => '<span style="white-space: nowrap;">Type &nbsp; &nbsp; &nbsp;</span>',
                        'value' => array($this,'transferType'),
                    ),
                    array(
                        'name' => 'admin_mark_status',
                        'header' => '<span style="white-space: nowrap;">Is Verified</span>',
                        'value' => '$data->admin_mark_status',
                    ),
                    array(
                        'name' => 'admin_comment',
                        'header' => '<span style="white-space: nowrap;">Verification Comment</span>',
                        'value' => array($this,'getVerifyStatusComment'),
                    ),
                ),
            ));
            ?>
        </div>
    </div>
</form>
<script type="text/javascript" src="/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="/js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="/js/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
