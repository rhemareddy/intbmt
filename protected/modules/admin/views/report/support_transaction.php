<script src="/js/datatables.min.js"></script>
<?php
Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Support' => '/admin/user/creditwallet?mode=AddFund',
    'Transaction Search'
);
?>
 <form id="bulkaction" name="bulkaction" method="post" action="" >
      <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">
    <div class="user-status margin-bottom-10 filter-toggle">
        <input type="hidden" name="status" id="status" value="<?php echo (isset($_GET['status']) && !empty($_GET['status'])) ? $_GET['status'] : ""; ?>">

        <div class="col-md-4 col-sm-6">
            <div class="form-group">
                <label class="col-md-3 padding0 top7"> Search For : </label> 
                 <div class="col-md-8">
                <input type="text" name="transaction_search" id="transaction_search" class="form-control search-group-input" placeholder="Enter search keyword.."  />
                <span class="cls-error" id="search-error"></span>
                 </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6">
            <div class="form-group">
                <label class="col-md-2 padding0 margintop3">Search By :</label>
              <div class="col-md-9 padding0">
                <ul class="find_by_align list-inline list-unstyled">
                    <li><input type="radio" name="find_by" id="find_by" value="name" checked /> <label>Username</label></li>
                    <li><input type="radio" name="find_by" id="find_by" value="transaction_id" /> <label>Transaction Id </label></li>
                    <li><input type="radio" name="find_by" id="find_by" value="payment_transaction_id"  /> <label>Payment Ref Id </label></li>
                </ul>
              </div>
            </div>
        </div>

        <div class="col-md-1 col-sm-6 padding0 form-inline">
            <input type="button" class="btn btn-primary f-left margin-left15 margintop3" value="OK" name="submit" id="btnSearch">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 responsiveTable">
            <table id="supportTransactionList" class="table table-bordered no-footer support-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Txt Id</th>
                        <th>Date</th>
                        <th>From User</th>
                        <th>To User</th>
                        <th>Amount</th>
                        <th>Paid Amount</th>
                        <th>Wallet Type</th>
                        <th>Wallet Amount</th>
                        <th>Ecurrancy</th>
                        <th>Paymnet Ref Id</th>
                        <th>Is Verified</th>
                        <th>status</th>
                        <th>Mode</th>
                        <th>Verification Comment</th>
                        <th style="display:none">ID</th>
                    </tr>
                </thead>
            </table>
            <script>
                $(document).ready(function () {
                    oTable = $('#supportTransactionList').DataTable({
                        "responsive": true,
                        "iDisplayLength": 50,
                        "aLengthMenu": [[50, 100, 200, -1], [50, 100, 200, "All"]],
                        "processing": true,
                        "serverSide": true,
                        "paging": true,
                        "bSort": true,
                        scrollY: "100%",
                        scrollX: true,
                        scrollCollapse: true,
                        "pagingType": "full_numbers",
                        "order": [[14, "desc"]],
                        "aoColumnDefs": [{
                                'bSortable': false,
                                'aTargets': [0]
                            }],
                        "sDom": 'B<"top"lp>rt<"bottom"p>i<"clear">',
                        "buttons": [
                            'copy', 'csv', 'excel', 'print'
                        ],
                        "ajax": {
                            "url": "supportdata",
                            "type": "POST"
                        },
                        "columns": [                            
                            {"data": "transaction_id"},
                            {"data": "created_at"},
                            {"data": "fromname"},
                            {"data": "toname"},
                            {"data": "actual_amount"},
                            {"data": "paid_amount"},
                            {"data": "walletName"},
                            {"data": "used_rp"},
                            {"data": "gatewayName"},
                            {"data": "payment_transaction_id"},                            
                            {"data": "admin_mark_status"},
                            {"data": "txnStatus"},
                            {"data": "mode"},
                            {"data": "admin_comment"},
                            {
                                "data": "id",
                                className: "dt-body-center no-need-to-show"
                            },
                        ],
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)' // no row selection on last column
                        },
                    });
                });
                
                $("#btnSearch").click(function () {
                    oTable.columns(1).search($("#transaction_search").val());
                    oTable.columns(2).search($("#find_by:checked").val());
                    oTable.draw();
                });
            </script>
        </div>
    </div>
</form>

<!-- For import the data -->
<link href="/css/buttons.dataTables.min.css" rel="stylesheet">
<script src="/js/dataTables.buttons.min.js"></script>
<script src="/js/jszip.min.js"></script>
<script src="/js/buttons.html5.min.js"></script>
<script src="/js/buttons.print.min.js"></script>
<style>
    
    .no-need-to-show{
     display: none;   
    }
</style>