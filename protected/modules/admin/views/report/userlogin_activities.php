<?php
$this->breadcrumbs = array(
    'Reports',
    'User Login Activities Report'
);
$from = "";
if (isset($_GET['from']) && isset($_GET['to'])) {
    $from = "?" . $_SERVER['QUERY_STRING'];
}
if (!empty($error)) {
    echo "<p class='error-2'><i class='fa fa-times-circle icon-error'></i><span class='span-error-2'>" . $error . "</span></p>";
}
?> 
<style>
    #user_filter_frm > div.col-xs-12.adv-options > ul > li:nth-child(2) > div > span > input[type="radio"]
</style>
<div class="col-md-12 userLoginActivities">    
    <div class="expiration margin-topDefault confirmMenu row search-form">
        <form id="user_filter_frm" name="user_filter_frm" method="get" action="/admin/report/loginactivities" > 
        <div class="col-md-3 ">
            <div class="input-group input-large date-picker input-daterange" style="z-index:9;">
                <input type="text" id="txtFromDate" name="from" placeholder="From Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from'])) ? $_GET['from'] : DATE('Y-m-d'); ?>">
                <span class="input-group-addon">
                    to </span>
                <input type="text" id="txtToDate" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to'])) ? $_GET['to'] : DATE('Y-m-d'); ?>">
            </div>
        </div>
        <div class="col-md-3">
            <div class="dataTables_length" id="search_length">
                <label>Display&nbsp; 
                    <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;              ?> + this.value">
                        <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                            <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                        <?php } ?>
                    </select>&nbsp; 
                    Records per page</label>
            </div>
        </div> 
        <div class="col-md-3 no_pad_left">
            <div class="dataTables_length" id="search_length">
                <label>Status </label>
                <select id="success_failed" name="success_failed" aria-controls="" class="form-controll">
                    <option value="3" <?php if ($searchFlag == "3") echo "selected"; ?>>All</option>
                    <option value="1" <?php if ($searchFlag == '1') echo "selected"; ?>>Success</option>
                    <option value="0" <?php if ($searchFlag == '0') echo "selected"; ?>>Failed</option>
                </select>
            </div>
        </div> 
        <div class="col-md-3 form-inline ">
            <input type="text" name="search" id="search" class="form-control" placeholder="Enter keyword.." value="<?php echo (!empty($_GET['search'])) ? $_GET['search'] : ""; ?>" />
            <span class="clrred" id="search_error"></span>
            <input type="submit" class="btn btn-success confirmOk" value="OK" name="submit" id="submit"/>
        </div>
        <div class="col-xs-12 adv-options ">
            <h4 class="pull-left"> Search By : </h4> 
            <ul class="sort_by_align">
                <li>
                    <input style="margin-left:0px;" type="radio" name="sort_by" value="name" checked/> <label>Username </label>
                </li>
                <li>
                    <input style="margin-left:0px;" type="radio" name="sort_by" value="ip" <?php if (isset($_GET['sort_by']) && $_GET['sort_by'] == 'ip') echo "checked"; ?> /> <label>IP Address</label>
                </li>
            </ul>
        </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12 blue-table">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'template' => "{pager}\n{items}\n{summary}\n{pager}",
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">No.</span>',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">User &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(!empty($data->user)) ? $data->user->name : "NA"',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">IP &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(!empty($data->ip)) ? $data->ip : "NA"',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Country &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(!empty($data->country)) ? $data->country : "NA"',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Browser &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(!empty($data->browser)) ? $data->browser : "NA"',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Operating System &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(!empty($data->operating_system)) ? $data->operating_system : "NA"',
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Date</span>',
                    'value' => function($data) {
                        $date = date("F j, Y, g:i a", strtotime($data->log_at));
                        echo $date;
                    }
                ),
                array(
                    'name' => 'status',
                    'header' => '<span style="white-space: nowrap;">Status &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Success\') : Yii::t(\'translation\', \'Failed\')',
                ),
            ),
        ));
        ?>
    </div>
</div>