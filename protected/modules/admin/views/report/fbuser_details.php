<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'User Facebook Friends Details'
);
?>

<a href="/admin/report/featchfbusercsv"> CSV Export </a>
<br>
<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'state-grid',
                'dataProvider' => $dataProvider,
                'enableSorting' => 'true',
                'ajaxUpdate' => true,
                'summaryText' => 'Showing {start} to {end} of {count} entries',
                'template' => '<div class="table-responsive">{items}</div> {summary} {pager}',
                'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
                'pager' => array(
                    'header' => false,
                    'firstPageLabel' => "<<",
                    'prevPageLabel' => "<",
                    'nextPageLabel' => ">",
                    'lastPageLabel' => ">>",
                ),
                'columns' => array(
                    //'idJob',
                    array(
                        'name' => 'id',
                        'header' => 'No.',
                        'value' => '$row+1',
                    ),
                    array(
                        'name' => 'name',
                        'header' => '<span style="white-space: nowrap;">User Name &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '($data->fb_user_id) ? $data->ads->user->name : "N/A"',
                    ),
                    array(
                        'name' => 'name',
                        'header' => '<span style="white-space: nowrap;">Fb User Id &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '($data->fb_user_id) ? $data->fb_user_id : "N/A"',
                    ),
                    array(
                        'name' => 'name',
                        'header' => '<span style="white-space: nowrap;">Fb Name &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '($data->fb_user_name) ? $data->fb_user_name : "N/A"',
                    ),
                    array(
                        'name' => 'name',
                        'header' => '<span style="white-space: nowrap;">Profile Link&nbsp; &nbsp; &nbsp;</span>',
                        'value' => '($data->fb_user_profile_link) ? $data->fb_user_profile_link : "N/A"',
                    ),
                    array(
                        'name' => 'name',
                        'header' => '<span style="white-space: nowrap;">Shared Link&nbsp; &nbsp; &nbsp;</span>',
                        'value' => '($data->fbuser_shared_link) ? $data->fbuser_shared_link : "N/A"',
                    ),
                ),
            ));
            ?>
        </div>
    </div>
</div>
