<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
     'Verification Report'
);
?>

<div class="col-md-12 user-status">
    <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report/verification">
        <div class="expiration margin-topDefault confirmMenu">

            <div class="col-md-4">
                <div class="input-group input-large date-picker input-daterange">
                    <input type="text" name="from" placeholder="To Date" class="datepicker form-control" value="<?php echo (!empty($_REQUEST) && $_REQUEST['from'] != '') ? $_REQUEST['from'] : DATE('Y-m-d'); ?>">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" name="to" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="<?php echo (!empty($_REQUEST) && $_REQUEST['to'] != '') ? $_REQUEST['to'] : DATE('Y-m-d'); ?>">
                </div>
            </div>
            <?php
            $statusId = 0;
            if (isset($_REQUEST['res_filter'])) {
                $statusId = $_REQUEST['res_filter'];
            }
            ?>
            <div class="col-md-4">
                <select class="form-control " id="ui-id-5" name="res_filter">
                    <option value="1" <?php if ($statusId == 1) { echo "selected"; } ?> >Approved</option>
                    <option value="0" <?php if ($statusId == 0) { echo "selected"; } ?> >Pending</option>
                </select>
            </div>
        </div>
        <input type="submit" class="btn btn-primary " value="OK" name="submit" id="submit"/>
    </form>

</div>
<div class="row">
    <div class="col-md-12">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;color:#01b7f2">No</span>',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Full Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->user->full_name)?$data->user->full_name:""',
                ),
                
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Address Proof &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this,'gridAddressImagePopup'),
                ),
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Id Proof &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this,'gridIdImagePopup'),
                ),
                array(
                    'name' => 'country_id',
                    'header' => '<span style="white-space: nowrap;">Country &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->country->name)?$data->country->name:""',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Phone &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this ,'gridPhone'),
                ),
                array(
                    'name' => 'status',
                    'value' => '($data->document_status == 1) ? Yii::t(\'translation\', \'Approved\') : Yii::t(\'translation\', \'Pending\')',
                ),
            ),
        ));
        ?>
    </div>
</div>
