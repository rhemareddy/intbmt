    <?php 
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Finanace Reports' => '/admin/report/transaction',
    'Order List'
);
$status = "";
if (isset($_GET['status']) && !empty($_GET['status'])) {
    $status = $_GET['status'];
}

$userStatus = 1;
if (isset($_GET['user_status']) && !empty($_GET['user_status'])) {
    $userStatus = $_GET['user_status'];
}

$querystring = $_SERVER['QUERY_STRING'];
$filter = $querystring . "&csv=1";


Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/buttons.dataTables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');
?>
<script src="/js/datatables.min.js"></script>

<form id="new_user_filter_frm" name="user_filter_frm" method="post" action="/admin/user" >
     <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn" value="Filter" name="submit">
    <div class="user-status filter-toggle">
        <input type="hidden" name="status" id="status" value="<?php echo (isset($_GET['status']) && !empty($_GET['status'])) ? $_GET['status'] : ""; ?>">

 
        <div class="col-md-3">          
                <div class="input-group  date-picker input-medium input-daterange">
                    <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
                    <span class="input-group-addon"> to </span>
                    <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
                </div>
        </div>
      
        <div class="col-md-4 col-sm-4 bulkSelect">
            <div class="form-group">
                <label class="col-md-2 col-sm-2 padding0 top7">Status : </label>
                <div class="col-md-6 col-sm-6 mobile-padding-0">
                    <select class=" form-control  " id="change_status" name="change_status">
                        <option value="all" >All</option>
                        <option value="1" selected="selected">Completed</option>
                        <option value="2">Pending</option>
                    </select>
                </div>
            </div>
        </div>
                <input type="button" class="btn btn-primary f-left margin-left15 margintop3" value="Filter" name="submit" id="btnSearch">

        
    </div> 

    <div class="row">
        <div class="col-md-12 responsiveTable">
        
            
            <table id="allMemberList" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>TransactionId</th>
                        <th>User</th>
                        <th>Amount</th>
                        <th>Package</th>
                        <th>St Date</th>
                        <th>End Date</th>
                        <th>Create Dt</th>
                        <th>Status</th>
                        <th style="display: none;">Id</th>
                    </tr>
                </thead>
            </table>
            <script src="/js/dataTables.buttons.min.js"></script>
            <script src="/js/jszip.min.js"></script>
            <script src="/js/buttons.html5.min.js"></script>
            <script src="/js/buttons.print.min.js"></script>
            <script>
                $(document).ready(function () {
                    oTable = $('#allMemberList').DataTable({
//                        "sDom":'lfptip',
                        "responsive": true,
                        "iDisplayLength": 50,
                        "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        "processing": true,
                        "serverSide": true,
                        "paging": true,
                        "bSort": true,
                        "pagingType": "full_numbers",
                        "order": [[8, "desc"]],
                        "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                        "buttons": [ 'csv'],
                        "ajax": {
                            "url": "/admin/report/orderlistdata",
                            "type": "POST"
                        },
                        "columns": [
                            {"data": "transactionId"},
                            {"data": "userName"},
                            {"data": "package_price"},
                            {"data": "packageName"},
                            {"data": "start_date"},
                            {"data": "end_date"},
                            {"data": "created_at"},
                            {"data": "orderStatus"},
                            {
                                "data": "oid",
                                "className": "noneed"
                            },

                        ],
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)' // no row selection on last column
                        },
                    });
                });
            $("#flowcheckall").click(function () {
                $('#allMemberList tbody input[type="checkbox"]').prop('checked', this.checked);
            });
            $("#btnSearch").click(function() {
                if($("#toDate").val()<$("#fromDate").val()){
                    alert("To Date should be greater then From Date!!!");
                    return false;
                }
                
                oTable.columns(6).search($("#toDate").val().trim(),$("#fromDate").val().trim());
                oTable.draw();
            });
            $("#change_status").change(function() {
                oTable.columns(7).search($("#change_status").val().trim());
                oTable.draw();
            });

        </script>
        </div>
    </div>
</form>
<style>
    .noneed{
        display: none;
    }
</style>