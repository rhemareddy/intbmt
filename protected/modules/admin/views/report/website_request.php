<?php 
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Operation',
    'Website Approval'
);

if (isset($_GET['per_page']) && count($_GET) > 1) {
    $queryString = CommonHelper::remove_querystring_var($_SERVER["QUERY_STRING"], 'per_page');
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/websiterequest?' . $queryString . '&per_page=' . "'";
} else {
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/websiterequest?per_page=' . "'";
}

if (isset($_GET['success'])) { ?><span class="span-success-2"><?php echo $_GET['success']; ?></span><?php } ?>

<div class="row websiteRequest">
<div class="expiration confirmMenu col-md-12">
    <?php if ($success) { echo $success;  } ?>
    <div class="expiration margin-topDefault confirmMenu search-form">
        <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report/websiterequest">
            <div class="col-md-4">
                <div class="input-group input-large date-picker input-daterange" style="z-index:9;">
                    <input type="text" name="from" placeholder="To Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from']) && $_GET['from'] != '') ? $_GET['from'] : ""; ?>">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" name="to" data-provide="datepicker" placeholder="From Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to']) && $_GET['to'] != '') ? $_GET['to'] : ""; ?>">
                </div>
            </div>
            <div class="col-md-3 no_pad_left">
                <div class="dataTables_length" id="search_length">
                    <label>Display&nbsp; 
                        <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                            <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                                <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                            <?php } ?>
                        </select>&nbsp; 
                        Records per page</label>
                </div>
            </div> 
            <?php
            $statusId = 2;
            if (isset($_GET['res_filter'])) {
                $statusId = $_GET['res_filter'];
            }
            ?>
            <div class="col-md-5 form-inline ">
              <select class="customeSelect howDidYou form-control input-medium select2me confirmBtn " id="ui-id-5" name="res_filter">
                  <option value="2" <?php if ($statusId == 2) { echo "selected"; } ?> >Pending</option>                    
                  <option value="4" <?php if ($statusId == 4) { echo "selected"; } ?> >Approved</option> 
                  <option value="3" <?php if ($statusId == 3) { echo "selected"; } ?> >Pending For Deploy</option>
                </select>
                <input type="submit" class="btn btn-success confirmOk" value="OK" name="submit" id="submit"/>
            </div>
        </form>
    </div>
</div>
</div>
<div class="expiration margin-bottom-10 confirmMenu websiteRequestIn">
    <form id="user_filter_frm" class="form-inline" name="user_filter_frm" method="get" action="/admin/report/websiterequest" onsubmit="//return validateSearch();">
    <div class="col-md-12 search-form">
        <div class="confirmMenu row">
            <div class="expiration">
                <div class="col-md-3">
                    <div class="form-group search-group">
                        <label> Search For : </label> 
                        <input type="text" name="searchword" id="searchword" class="form-control search-group-input" placeholder="Enter search keyword.." value="<?php echo isset($_GET['searchword']) ? $_GET['searchword'] : ''; ?>" />        
                    </div>
                    <span class="cls-error" id="search-error"></span>
                </div>
                
                <div class="col-md-3 adv-options">
                    <h4 class="pull-left"> Search By : </h4> 
                    <ul class="sort_by_align">
                        <li><input type="radio" name="sort_by" value="name" checked /> <label>Username</label></li>
                        <li><input type="radio" name="sort_by" value="domain" <?php if (isset($_GET['sort_by']) && $_GET['sort_by'] == 'domain') echo "checked"; ?> /> <label>Domain </label></li>
                    </ul>
                </div>
                <div class="col-md-1 col-sm-6 form-inline">
                    <input type="submit" class="btn btn-success" value="Search" name="search" id="search"/>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
<div class="row">
    <div class="col-md-12 websiteApprovalTable">
        <?php 
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'template' => "{pager}\n{items}\n{summary}\n{pager}",
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;">No.</span>',
                ),
                array(
                    'name'=>'user_id',
                    'header'=>'<span style="white-space: nowrap;">User Name &nbsp; &nbsp; &nbsp;</span>',
                    'value'=> 'is_object($data->order())?$data->order()->user()->name:""' ,
		),
                array(
                    'name'=>'package_id',
                    'header'=>'<span style="white-space: nowrap;">Package &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'is_object($data->order())?$data->order()->package()->name:""',
		),
                array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Domain &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'is_object($data->order())?$data->order()->domain : "" ',
		),
                array(
			'name'=>'id',
                        'header'=>'<span style="white-space: nowrap;">Status &nbsp; &nbsp; &nbsp;</span>',
			'value'=>array($this,'GetStatus'),
		),
                array(
                    'name'=>'created_at',
                    'header'=>'<span style="white-space: nowrap;">Start Date &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'is_object($data->order())? $data->order()->start_date : "" ',
		),
                array(
                    'name'=>'created_at',
                    'header'=>'<span style="white-space: nowrap;">End Date &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'is_object($data->order())? $data->order()->end_date : ""',
		),
                array(
                    'name'=>'created_at',
                    'header'=>'<span style="white-space: nowrap;">Requested Date  &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'isset($data->created_at)? $data->created_at : ""',
		),
                array(
			'name'=>'builderpproval',
                        'header'=>'<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
			'value'=>array($this,'GetBuilderApproval'),
		),
            ),
        ));
        ?>
    </div>
</div>
<div id="ftpModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">FTP Details</h4>
      </div>
      <div class="modal-body">
           <div id="ftp_success_msg"></div>
            <div id="ftp_error_msg"></div>
           <form>
                <fieldset>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="language">Username *</label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" id="ftp_user_name" name="ftp_user_name">
                                <div id="ftp_user_name_error" class="form_error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="title">Password *</label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" id="ftp_password" name="ftp_password">
                                <div id="ftp_password_error" class="form_error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="title">Port *</label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" id="ftp_port_id" name="ftp_port_id">
                                <div id="ftp_port_id_error" class="form_error"></div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group">
                    <button id="ftp_submit" type="button" id="submit" name="submit" class="btn orange" onclick="return getFtpDetails();">Submit</button>
                    <div id="ftp_loader" style="display:none;"><center><img class="load-img" style="max-width:50px;" src="/images/new_loader.gif"></center></div>
                </div>
            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script>
function confirmComplete(id) { 
    var answer=confirm("Are you sure you want to approve this website ?");
    if (answer==true){
         $("#website_approvel_"+id).submit();
    }else{
        return false;
    }
}
function confirmReject(id) {
    var answer=confirm("Are you sure you want to Reject this website ?");
    if (answer==true){
        $("#website_reject_"+id).submit();
    }else{
        return false;
    }
}
function getFtpDetails(){
    $("#ftp_user_name_error").html("");
    if($("#ftp_user_name").val() == "") {
        $("#ftp_user_name_error").html("Please enter user name");
        $("#ftp_user_name").focus();
        return false;
    }
    $("#ftp_password_error").html("");
    if($("#ftp_password").val() == "") {
        $("#ftp_password_error").html("Please enter password");
        $("#ftp_password").focus();
        return false;
    }
    $("#ftp_port_id_error").html("");
    if($("#ftp_port_id").val() == "") {
        $("#ftp_port_id_error").html("Please enter port id");
        $("#ftp_port_id").focus();
        return false;
    }
        $('#ftp_submit').hide();
        $('#ftp_loader').show();
        $.ajax({
            type: "post",
            url: "/admin/report/enableftp",
            data: "username=" + $("#ftp_user_name").val() + "&password=" + $("#ftp_password").val() + "&port=" + $("#ftp_port_id").val() + "&userhastempid=" + $("#user_has_temp_id").val(),
            success: function (msg) {
                $('#ftp_submit').show();
                $('#ftp_loader').hide();
                $("#ftp_success_msg").html("");
                $("#ftp_error_msg").html(""); 
                if(msg == 1){
                 $("#ftp_success_msg").html("success");
                } else {
                 $("#ftp_error_msg").html("failed"); 
                }
            }
        });
    }
</script>   