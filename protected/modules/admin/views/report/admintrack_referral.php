    <?php
/* @var $this OrderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
        'Dashboard' => '/admin/default/dashboard',
        'Reports' => '/admin/report',
	'Referral Invite',
);
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
<div class="col-md-12 user-status filter-toggle
">
    
        <div class="expiration margin-topDefault confirmMenu">
                    
    <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report/trackreferral">
     <div class="col-md-4">
        <div class="input-group input-large date-picker input-daterange">
        <input type="text" name="from" placeholder="From Date" class="datepicker form-control" value="<?php echo (!empty($_REQUEST) && $_REQUEST['from'] !='') ?  $_REQUEST['from'] :  DATE('Y-m-d');?>">
        <span class="input-group-addon">
        to </span>
        <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="<?php echo (!empty($_REQUEST) && $_REQUEST['to'] !='') ?  $_REQUEST['to'] :  DATE('Y-m-d');?>">
    </div>
     </div>
 
        <input type="submit" class="btn btn-primary confirmOk mobile-left-15" value="OK" name="submit" id="submit" style="left:340px!important;top:0px!important;"/>
    </form>

</div>
</div>
<div class="row">
    <div class="col-md-12 blue-table"> 
<?php 
          
        $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'city-grid',
	'dataProvider'=>$dataProvider,
	'enableSorting'=>'true',
	'ajaxUpdate'=>true,
	'summaryText'=>'Showing {start} to {end} of {count} entries',
	'template'=>'{items} {summary} {pager}',
	'itemsCssClass'=>'table table-striped table-bordered table-hover table-full-width',
	'pager'=>array(
		'header'=>false,
		'firstPageLabel' => "<<",
		'prevPageLabel' => "<",
		'nextPageLabel' => ">",
		'lastPageLabel' => ">>",
	),	
	'columns'=>array(
		//'idJob',
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;color:#01b7f2">No</span>',
                ),
                array(
                    'name'=>'sponsor id',
                    'header'=>'<span style="white-space: nowrap;">Referred By &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'isset($data->sponsor_id)?$data->sponsor()->name:""',
		),
                array(
                    'name'=>'name',
                    'header'=>'<span style="white-space: nowrap;">Name &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'isset($data->name)?$data->name:""',
		),
		array(
                    'name'=>'full_name',
                    'header'=>'<span style="white-space: nowrap;">Full Name &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'isset($data->full_name)?$data->full_name:""',
		),
                array(
                    'name'=>'email',
                    'header'=>'<span style="white-space: nowrap;">Email &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'( $data->email) ? $data->email:""',
		),
               array(
                    'name'=>'phone',
                    'header'=>'<span style="white-space: nowrap;">Phone &nbsp; &nbsp; &nbsp;</span>',
                     'value' => array($this ,'gridPhone'),
		),
            
                array(
                    'name'=>'',
                    'header'=>'<span style="white-space: nowrap;">Referred Using &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'( $data->social=="fb") ? "facebook" : ($data->social=="tw" ? "twitter" : "email")',
		),
                
//		array(
//			'name'=>'id',
//                        'header'=>'<span style="white-space: nowrap;">Position &nbsp; &nbsp; &nbsp;</span>',
//			'value'=>'$data->position',
//		),
             
		 
	 ),
)); ?>
                    

      </div>
    </div>
<script>
    $(function () {
                $('.datepicker').datepicker({
                    format: 'yyyy-mm-dd'
                });
            });
    </script>
