<?php
 $queryStr = $_SERVER['QUERY_STRING'] ;
if (strpos($queryStr, 'transaction_id='.$_REQUEST['transaction_id'].'&') !== false) {
    $queryStr =  str_replace("transaction_id=".$_REQUEST['transaction_id']."&","",$queryStr);
}
?>
<div class="col-md-12 col-sm-12">
    <div class="portlet box orange   ">
        <div class="portlet-title">
            <div class="caption">Insert Payment Ref Id</div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post" action="/admin/report/requestpaymentrefid" autocomplete="off" id="emailcreation">
                <fieldset> 
                    <div class="form-body">
                        <div class="form-group">
                            <input type="hidden" name="transaction_id" id="transaction_id" value="<?php echo (isset($_GET['transaction_id']) ? $_GET['transaction_id'] : '' ); ?>"/>
                            <input type="hidden" name="queryStr" id="queryStr" value="<?php echo (isset($_SERVER['QUERY_STRING']) ? $queryStr : '' ); ?>"/>
                            <label for="payment-ref-id" class="col-lg-5 control-label">Payment Ref Id :</label>
                            <div class="col-lg-7">
                                <input type ="text" class="form-control" id="payment_ref_id" name="payment_ref_id" /><br>
                                <span id="payment_ref_id_error" style="color:red"></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-actions right db-action">                     
                        <input type="submit"  name="submit" id="submit" class="btn orange" value="Submit Request"/>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
