<?php 
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Finanace Reports' => '/admin/report/transaction',
    'Downline Purchase Report',
);
$this->menu = array(
    array('label' => 'Create Order', 'url' => array('create')),
    array('label' => 'Manage Order', 'url' => array('admin')),
);
if (isset($_GET['per_page']) && count($_GET) > 1) {
    $queryString = CommonHelper::remove_querystring_var($_SERVER["QUERY_STRING"], 'per_page');
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/order/downlinepurchase?' . $queryString . '&per_page=' . "'";
} else {
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/order/downlinepurchase?per_page=' . "'";
}
if(!empty($error)){
    echo "<p class='error-2'><i class='fa fa-times-circle icon-error'></i><span class='span-error-2'>".$error."</span></p>";
}
if (Yii::app()->user->hasFlash('error')){ 
   echo '<p class="error-2" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">'.Yii::app()->user->getFlash('error').'</span></p>';
} 
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">

    <div class="order-list-div col-md-12 margin-bottom-15  filter-toggle"> 
        <div class="expiration confirmMenu row">
            <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report/downlinereport">
                <div class="col-md-3">
                    <div class="input-group input-medium date-picker input-daterange">
                        <input type="text" name="search" id="search" placeholder="Search By Username" class="form-control" value="<?php echo (!empty($_GET['search'])) ? $_GET['search'] : ""; ?>" />
                    </div>
                </div>
                <div class="col-md-3 ">
                    <div class="input-group input-medium date-picker input-daterange">
                        <input type="text" name="from" placeholder="From Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from']) && $_GET['from'] != '') ? $_GET['from'] : ""; ?>">
                        <span class="input-group-addon">
                            to </span>
                        <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to']) && $_GET['to'] != '') ? $_GET['to'] : ""; ?>">
                    </div>
                </div>
                <div class="col-md-3 no_pad_left ">
                    <div class="dataTables_length" id="search_length">
                        <label>Display&nbsp; 
                            <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                                <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                                    <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                                <?php } ?>
                            </select>&nbsp; 
                            Records per page</label>
                    </div>
                </div> 
                <div class="col-md-1 col-sm-6">
                    <input type="submit" class="btn btn-success confirmOk top7" value="OK" name="submit" id="submit"/>
                </div>
            </form>
        </div>
    </div>
<div class="row">
    <div class="col-md-12 blue-table">
        <?php  
        if(!$dataProvider){
            echo "<p class='alert noresult'>No downline members under you </p>"; 
        } else {
        $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'city-grid',
	'dataProvider'=>$dataProvider,
	'enableSorting'=>'true',
	'ajaxUpdate'=>true,
        'template' => "{pager}\n{items}\n{summary}\n{pager}",
	'itemsCssClass'=>'table table-striped table-bordered table-hover table-full-width',
	'pager'=>array(
		'header'=>false,
		'firstPageLabel' => "<<",
		'prevPageLabel' => "<",
		'nextPageLabel' => ">",
		'lastPageLabel' => ">>",
	),	
	'columns'=>array(
               array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">Sl.No</span>',
                ),             
               array(
                    'name'=>'created_at',
                    'header'=>'<span>Date <i class="fa fa-sort pull-right"></i></span>',
                    'value'=>'$data->created_at',
		),
		array(
                    'name'=>'user_name',
                    'header'=>'<span>User Name <i class="fa fa-sort pull-right"></i></span>',
                    'value'=>'isset($data->user()->name)? ucwords($data->user()->name):""',
		), 
                array(
                    'name'=>'full_name',
                    'header'=>'<span>Full Name <i class="fa fa-sort pull-right"></i></span>',
                    'value'=>'isset($data->user()->full_name)? ucwords($data->user()->full_name):""',
		),
                array(
                    'name'=>'id',
                    'header'=>'<span>Phone Number <i class="fa fa-sort pull-right"></i></span>',
                    'value'=>array($this , 'gridOrderPhone'),
		),
                array(
                      'name'=>'package_price',
                      'header'=>'<span>Total Amount <i class="fa fa-sort pull-right"></i></span>',
                      'value'=>'$data->package_price',
                  ),
                array(
                    'name'=>'paid_amt',
                    'header'=>'<span>Paid Amount<i class="fa fa-sort pull-right"></i></span>',
                    'value'=>'$data->transaction()->paid_amount',
		), 
                array(
                    'name'=>'id',
                    'header'=>'<span>Position <i class="fa fa-sort pull-right"></i></span>',
                    'value'=>'isset($data->user()->position)? $data->user()->position :""',
		),
	),
        )); } ?>

    </div>
</div>