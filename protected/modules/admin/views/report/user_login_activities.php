<?php
 $this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Reports' => '/admin/report',
    'User Login Activities'
);
$from = "";
if (isset($_GET['from']) && isset($_GET['to'])) {
    $from = "?" . $_SERVER['QUERY_STRING'];
}
?> 
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
<div class="user-status col-md-12 filter-toggle">   
       <div class="expiration margin-topDefault confirmMenu">
        <form id="user_filter_frm" name="user_filter_frm" method="get" action="/admin/report/loginactivities"> 
         <div class="col-md-3 col-sm-3">
            <div class="input-group date-picker ">
                <input type="text" id="txtFromDate" name="from" placeholder="From Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from'])) ? $_GET['from'] : ""; ?>">
                <span class="input-group-addon">
                    to </span>
                <input type="text" id="txtToDate" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to'])) ? $_GET['to'] : ""; ?>">
            </div>
        </div>
      
        <div class="col-md-3 top7">
            <div class="row">
                <div class="form-group">
                    <label class="col-md-3" >Status </label>
                    <div class="col-md-9">
                          <select id="success_failed" name="success_failed" aria-controls="" class="">
                    <option value="3" <?php if ($searchFlag == "3") echo "selected"; ?>>All</option>
                    <option value="1" <?php if ($searchFlag == '1') echo "selected"; ?>>Success</option>
                    <option value="0" <?php if ($searchFlag == '0') echo "selected"; ?>>Failed</option>
                </select>
                    </div>
                </div>
            </div>
        </div> 
            
        <div class="col-md-3">
            
            <input type="text" name="search" id="search" class="form-control" placeholder="Search by Username or IP" value="<?php echo (!empty($_GET['search'])) ? $_GET['search'] : ""; ?>" />
            <span class="clrred" id="search_error"></span>
         
        </div>
              <div class="col-md-2 ">
            
            <div class="dataTables_length" id="search_length">
                <label>Display&nbsp; 
                    <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;              ?> + this.value">
                        <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                            <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                        <?php } ?>
                    </select>&nbsp; 
                  </label>
            </div>
        </div> 
        <div class="col-md-4 top20">
            <div class="row">
                <div class="form-group">
                     <label class="col-md-4 padding-right-0" >Search By </label>
                     <div class="col-md-8">
                         
            <ul class="sort_by_align list-inline list-unstyled">
                <li>
                    <input style="margin-left:0px;" type="radio" name="sort_by" value="name" checked/> <label>Username </label>
                </li>
                <li>
                    <input style="margin-left:0px;" type="radio" name="sort_by" value="ip" <?php if (isset($_GET['sort_by']) && $_GET['sort_by'] == 'ip') echo "checked"; ?> /> <label>IP Address</label>
                </li>
            </ul>
                     </div>
                </div>
            </div>
          
        </div>
               <input type="submit" class="btn btn-success margintop3 margin-left-15" value="OK" name="submit" id="submit"/>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12 blue-table">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'template' => "{pager}\n{items}\n{summary}\n{pager}",
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">No.</span>',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">User &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(!empty($data->user)) ? $data->user->name : "NA"',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">IP &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(!empty($data->ip)) ? $data->ip : "NA"',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Country &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(!empty($data->country)) ? $data->country : "NA"',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Browser &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(!empty($data->browser)) ? $data->browser : "NA"',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Operating System &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(!empty($data->operating_system)) ? $data->operating_system : "NA"',
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Date</span>',
                    'value' => function($data) {
                        $date = date("F j, Y, g:i a", strtotime($data->log_at));
                        echo $date;
                    }
                ),
                array(
                    'name' => 'status',
                    'header' => '<span style="white-space: nowrap;">Status &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Success\') : Yii::t(\'translation\', \'Failed\')',
                ),
            ),
        ));
        ?>
    </div>
</div>