<?php 
$this->breadcrumbs = array(
    'Reports',
    'Subscriber Report'
);
$from = "";
if(isset($_GET['from']) && isset($_GET['to'])){
   $from = "?".$_SERVER['QUERY_STRING'];        
}
if (isset($_GET['per_page']) && count($_GET) > 1) {
    $queryString = CommonHelper::remove_querystring_var($_SERVER["QUERY_STRING"], 'per_page');
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/subscriber?' . $queryString . '&per_page=' . "'";
} else {
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/report/subscriber?per_page=' . "'";
}
?>
<a class="export-csv" href="/admin/report/subscribecsv<?php echo $from; ?>"> CSV Export </a>
    <div class="order-list-div col-md-12 margin-bottom-15"> 
        <div class="row"> 
            <div class="expiration confirmMenu ">
                <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/report/subscriber" class="form-inline">
                    <div class="col-md-4 col-sm-6 ">
                        <div class="input-group input-large date-picker input-daterange">
                            <input type="text" name="from" placeholder="From Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from']) && $_GET['from'] != '') ? $_GET['from'] : DATE('Y-m-d'); ?>">
                            <span class="input-group-addon"> To </span>
                            <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to']) && $_GET['to'] != '') ? $_GET['to'] : DATE('Y-m-d'); ?>">
                        </div>    
                    </div>
                    <div class="col-md-3 no_pad_left">
                        <div class="dataTables_length" id="search_length">
                            <label>Display&nbsp; 
                                <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                                    <?php foreach (Yii::app()->params['recordsPerPage'] as $key => $pageNumber) { ?>
                                        <option value="<?php echo $key; ?>" <?php if ($pageNumber == $pageSize) echo "selected"; ?> ><?php echo $pageNumber; ?></option>
                                    <?php } ?>
                                </select>&nbsp; 
                                Records per page</label>
                        </div>
                    </div> 
                    <div class="col-md-5  col-sm-6 ">
                        <input type="submit" class="btn btn-success confirmOk" value="OK" name="submit" id="submit" >
                    </div>
                </form>
            </div>
        </div>
    </div>
<div class="row">
    <div class="col-md-12">
        <?php 
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'template' => "{pager}\n{items}\n{summary}\n{pager}",
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;">No.</span>',
                ),
                array(
                    'name' => 'email',
                    'header' => '<span style="white-space: nowrap;">Email &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->email',
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Date &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
            ),
        ));
        ?>
    </div>
</div>
