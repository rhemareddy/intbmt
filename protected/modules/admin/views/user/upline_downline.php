<?php 
 $this->breadcrumbs = array(
        'Dashboard' => '/admin/default/dashboard',
        'Support' => '/admin/user/creditwallet?mode=AddFund',
        'Upline/Downline'
    );
 if (Yii::app()->user->hasFlash('success')): 
     echo '<div class="alert alert-success">'.Yii::app()->user->getFlash('success') .'</div>';
endif;
if (Yii::app()->user->hasFlash('error')):
    echo '<div class="alert alert-danger">' . Yii::app()->user->getFlash('error') . '</div>';
endif;

$urlParameters = "";
if(!empty($_GET)){
    $urlParameters = $_SERVER['QUERY_STRING']."&csv=ok";
?>
    <a class="export-csv" href="/admin/user/uplinedownline?<?php echo $urlParameters; ?>"> CSV Export </a>
<?php } ?>
    <form id="new_user_filter_frm" name="user_filter_frm" method="get" action="/admin/user/uplinedownline" onsubmit="return validateUplineDownline();">
    <div class="user-status">
       
        <div class="col-md-3 col-sm-4 bulkSelect">
            <div class="form-group">
                <label class="col-md-4 col-sm-3 padding0 top7"> Search </label>
                <div class="col-md-8 col-sm-8">
                    <input type="text" name="search" id="search" class="form-control" placeholder="Search By Username" value="<?php echo isset($_GET['search'])?$_GET['search']:""; ?>">
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-4 bulkSelect">
            <div class="form-group">
                <label class="col-md-3 col-sm-3 padding0 top7"></label>
                <div class="col-md-8 col-sm-8">
                    <select class="form-control" id="searchMode" name="searchMode" onchange="unsetListSearch();">
                        <option value="Downline"  <?php if(isset($_GET['searchMode']) && $_GET['searchMode'] == "Downline") echo 'selected="selected"';  ?>>Downline</option>
                        <option value="Upline" <?php if(isset($_GET['searchMode']) && $_GET['searchMode'] == "Upline") echo 'selected="selected"';  ?>>Upline</option>
                    </select>
                </div>
            </div>
        </div> 
        
        <div class="col-md-3 col-sm-4 bulkSelect" style="<?php echo !empty($_GET)?'display:block':'display:none'; ?>" id="list_search_div">
            <div class="form-group">
                <label class="col-md-4 col-sm-3 padding0 top7"> Search from <?php echo isset($_GET['searchMode'])?$_GET['searchMode']:""; ?> List </label>
                <div class="col-md-8 col-sm-8">
                    <input type="text" name="listSearch" id="listSearch" class="form-control" value="<?php echo isset($_GET['listSearch'])?$_GET['listSearch']:""; ?>" placeholder="Search by user name">
                </div>
            </div>
        </div>
        
        <div class="col-md-1 col-sm-6 form-inline">
            <input type="submit" class="btn btn-success" value="OK" name="uplineDownlineSearch" id="uplineDownlineSearch"/>
        </div>
       
    </div>
</form>
     
    <div class="row">
        <div class="col-md-12 responsiveTable grid-view" id="uplineDownlineUserDetails" style="<?php echo!empty($userObject) ? "display:block" : "display:none"; ?> ">
            <div class="tableTitle">
                <span>User Details</span>
            </div>
            <table id="parentDetails" class="table table-striped table-bordered table-hover table-full-width" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>
                            <a class="sort-link" href="javascript:void(0)"><span style="white-space: nowrap;">Level</span></a>
                        </th>
                        <th>
                            <a class="sort-link" href="javascript:void(0)">
                                <span style="white-space: nowrap;">User Name</span>
                            </a></th>
                        <th>
                            <a class="sort-link" href="javascript:void(0)">
                                <span style="white-space: nowrap;">Sponsor Name</span>
                            </a>
                        </th>
                        <th>
                            <a class="sort-link" href="javascript:void(0)">
                                <span style="white-space: nowrap;">Kyc Verified</span>
                            </a>
                        </th>
                        <th>
                            <a class="sort-link" href="javascript:void(0)">
                                <span style="white-space: nowrap;">Country</span>
                            </a>
                        </th>
                        <th>
                            <a class="sort-link" href="javascript:void(0)">
                                <span style="white-space: nowrap;">Mobile No</span>
                            </a>
                        </th>
                        <th>
                            <a class="sort-link" href="javascript:void(0)">
                                <span style="white-space: nowrap;">Status</span>
                            </a>
                        </th>
                        <th>
                            <a class="sort-link" href="javascript:void(0)">
                                <span style="white-space: nowrap;">Left Count</span>
                            </a>
                        </th>
                        <th>
                            <a class="sort-link" href="javascript:void(0)">
                                <span style="white-space: nowrap;">Right Count</span>
                            </a>
                        </th>
                        <th>
                            <a class="sort-link" href="javascript:void(0)">
                                <span style="white-space: nowrap;">Position</span>
                            </a>
                        </th>
                    </tr>
                    <?php
                    if (isset($userObject) && !empty($userObject)) {
                        $genealogyObject = Genealogy::model()->findByAttributes(array('user_id' => $userObject->id));
                        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $userObject->id));
                        ?>
                        <tr>
                            <td><?php echo isset($genealogyObject->level) ? $genealogyObject->level : ""; ?></td>
                            <td><?php echo $userObject->name; ?></td>
                            <td><?php echo isset($userObject->sponsor()->name) ? $userObject->sponsor()->name : ""; ?></td>
                            <td><?php echo (isset($userProfileObject) && $userProfileObject->document_status == 1) ? "Verified" : "Not Verified"; ?></td>
                            <td><?php echo (isset($userProfileObject) && $userProfileObject->country_id != 0) ? $userProfileObject->country()->name : "NA"; ?></td>
                            <td><?php echo (isset($userProfileObject) && $userProfileObject->phone != 0) ? "+" . $userProfileObject->country_code . " - " . $userProfileObject->phone : "NA"; ?></td>
                            <td><?php echo ($userObject->status == 1) ? "Active" : "Inactive"; ?></td>
                            <td><?php echo isset($genealogyObject->left_count) ? $genealogyObject->left_count : ""; ?></td>
                            <td><?php echo isset($genealogyObject->right_count) ? $genealogyObject->right_count : ""; ?></td>
                            <td><?php echo isset($genealogyObject->position) ? $genealogyObject->position : ""; ?></td>
                        </tr>
                    <?php } ?>
                </thead>
            </table>
        </div>
    </div>

<div class="row">
    <div class="col-md-12 responsiveTable">
        <div class="tableTitle">
              <span> <?php echo isset($_GET['searchMode'])?$_GET['searchMode']." Result ":""; ?></span>
        </div>
            
        <?php  
        $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'city-grid',
	'dataProvider'=>$dataProvider,
	'enableSorting'=>'true',
	'ajaxUpdate'=>true,
        'template' => "{pager}\n{items}\n{summary}\n{pager}",
	'itemsCssClass'=>'table table-striped table-bordered table-hover table-full-width',
	'pager'=>array(
		'header'=>false,
		'firstPageLabel' => "<<",
		'prevPageLabel' => "<",
		'nextPageLabel' => ">",
		'lastPageLabel' => ">>",
	),	
	'columns'=>array(
		array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">Sl.No</span>',
                ),             
               array(
                    'name'=>'level',
                    'header'=>'<span style="white-space: nowrap;">Level &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'$data->level',
		),
		array(
                    'name'=>'user_name',
                    'header'=>'<span style="white-space: nowrap;">User Name &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'isset($data->user()->name)? ucwords($data->user()->name):""',
		), 
                array(
                    'name'=>'sponsor_name',
                    'header'=>'<span style="white-space: nowrap;">Sponsor Name &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'isset($data->user()->sponsor()->name)? ucwords($data->user()->sponsor()->name):""',
		), 
                array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Kyc Verified &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>array($this , 'iskycverified'),
		),
                 array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Country &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>array($this , 'getcountry'),
		),
                 array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Mobile No &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>array($this , 'getphone'),
		),
                array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Status &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'($data->user()->status == 1)?"Active":"Inactive"',
		),
                 array(
                    'name'=>'left_count',
                    'header'=>'<span style="white-space: nowrap;">Left Count &nbsp; &nbsp; &nbsp;</span>',
                     'value'=>'$data->left_count',
		),
                array(
                    'name'=>'right_count',
                    'header'=>'<span style="white-space: nowrap;">Right Count &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'$data->right_count',
		),
                array(
                    'name'=>'position',
                    'header'=>'<span style="white-space: nowrap;">Position&nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'$data->position',
		),
            ),
        )); ?>
      
    </div>
</div>