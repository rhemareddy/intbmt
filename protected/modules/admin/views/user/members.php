<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
       'Dashboard' => '/admin/default/dashboard',
       'Member Access' => '/admin/UserHasAccess/members',
       'Members'
);
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">

<div class="col-md-12 user-status filter-toggle">
    
    <?php if (Yii::app()->user->hasFlash('error')){ ?>
        <p class="error-2" id="error_msg"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2"><?php echo Yii::app()->user->getFlash('error'); ?></span></p>
    <?php } ?>

    <?php if (Yii::app()->user->hasFlash('success')){ ?>
        <p class="success-2" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2"><?php echo Yii::app()->user->getFlash('success'); ?></span></p>
    <?php } ?>

    <div class="expiration margin-topDefault confirmMenu">

        <a class="btn  green margin-right-20 margintop3" style="float:left" href="/admin/UserHasAccess/add">New User +</a> 
         
        <form id="regervation_filter_frm" name="regervation_filter_frm" method="post" action="/admin/UserHasAccess/members" class="form-inline">
            <div class="input-group form-group col-md-3" >
                <?php
                $statusId = 1;
                if (isset($_REQUEST['res_filter'])) {
                    $statusId = $_REQUEST['res_filter'];
                }
                ?>

                <select class="customeSelect howDidYou form-control input-small" id="ui-id-5" name="res_filter" style="margin-right: 10px;">
                    <option value="1" <?php if ($statusId == 1) { echo "selected"; } ?> >Active</option>
                    <option value="0" <?php if ($statusId == 0) { echo "selected"; } ?> >In Active</option>
                </select>
                
                <input type="submit" class="btn btn-primary margintop3" value="OK" name="submit" id="submit"/>
            </div>

        </form>
    </div>
    
</form>

</div>
<div class="row">
    <div class="col-md-12 blue-table">
        <?php if(isset($_GET['successMsg']) && $_GET['successMsg']=='1'){?><div class="success" id="error_msg"><?php echo "Status Changed Successfully";?></div><?php }?>
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',
                 array(
                   'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Sl.No &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$row+1',
                ),
                array(
                    'name' => 'name',
                    'header' => '<span style="white-space: nowrap;">User Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->name)?$data->name:""',
                ),
                
                array(
                    'name' => 'full_name',
                    'header' => '<span style="white-space: nowrap;">Full Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->full_name)? $data->full_name:""',
                ),
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Email &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->email)?$data->email:""',
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Created At &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->created_at)? $data->created_at : ""',
                ),
                 
                 
                array(
                    'name' => 'status',
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                ),
                 array(
                    'class' => 'CButtonColumn',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'template' => '{Change} {Access} {Edit}',
                    'htmlOptions' => array('width' => '10%'),
                    'buttons' => array(
                         'Change' => array(
                            'label' => '<i class="fa fa-retweet"></i>',
                            'options' => array('class' => 'action-icons', 'title' => 'Change Status'),
                            'url' => 'Yii::app()->createUrl("admin/UserHasAccess/changeapprovalstatus", array("id"=>$data->id))',
                        ),
                         'Access' => array(
                            'label' => '<i class="fa fa-key"></i>',
                            'options' => array('class' => 'action-icons', 'title' => 'Member Access'),
                            'url' => 'Yii::app()->createUrl("admin/UserHasAccess/memberaccess", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ),
                        'Edit' => array(
                            'label' => '<i class="fa fa-pencil-square-o">',
                            'options' => array('class' => 'action-icons', 'title' => 'Edit'),
                            'url' => 'Yii::app()->createUrl("admin/UserHasAccess/edit", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ),  
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>
