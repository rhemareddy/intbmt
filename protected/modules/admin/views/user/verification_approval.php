<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Support' => '/admin/user/creditwallet?mode=AddFund',
    'Document Approval'
);
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">
<div class="user-status col-md-12 filter-toggle">
    <div class="expiration confirmMenu">
        <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/user/verificationapproval">
            <div class="col-md-3 col-sm-3">
                <div class="input-group  date-picker input-daterange" style="z-index:9;">
                    <input type="text" name="from" placeholder="From Date" class="datepicker form-control" value="<?php echo (!empty($_REQUEST['from']) && $_REQUEST['from'] != '') ? $_REQUEST['from'] : ""; ?>">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="<?php echo (!empty($_REQUEST['to']) && $_REQUEST['to'] != '') ? $_REQUEST['to'] : ""; ?>">
                </div>
            </div>
    <?php 
    if(isset($_REQUEST['res_filter']) && $_REQUEST['res_filter'] !=''){
      $statusId = $_REQUEST['res_filter'];
    }else{
      $statusId =   "0";   
    } ?>
      <div class="col-md-2 col-sm-2">
        <select class="customeSelect  form-control" id="ui-id-5" name="res_filter">
            <option value="all" <?php if ($statusId == '') { ?> selected="selected"<?php } ?>>All</option> 
            <option value="1" <?php if ($statusId == '1') { ?> selected="selected"<?php } ?>>Approved</option>
            <option value="0" <?php if ($statusId == '0') { ?> selected="selected"<?php } ?>>Pending</option>
            <option value="2" <?php if ($statusId == '2') { ?> selected="selected"<?php } ?>>Rejected</option>
        </select>
      </div>
        <div class="col-md-2 col-sm-2">
            <div class="dataTables_length nomargin-btm" id="search_length">
                <label>Display&nbsp; 
                    <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;  ?> + this.value">
                        <option value="50" <?php if ($pageSize == 50) echo "selected"; ?>>50</option>
                        <option value="100" <?php if ($pageSize == 100) echo "selected"; ?>>100</option>
                        <option value="200"<?php if ($pageSize == 200) echo "selected"; ?>>200</option>
                    </select>
             </label>
            </div>
        </div>
    </div>
    <input type="submit" class="btn btn-primary margintop3 " value="OK" name="submit" id="submit"/>
    </form>
</div>
<div class="row">
    <div class="col-md-12 verificationApproval blue-table">
        <?php if(isset($_GET['successMsg']) && $_GET['successMsg']=='1'){?><div class="success" id="error_msg"><?php echo "Status Changed Successfully";?></div><?php }?>
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{pager} {items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
               array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;color:#01b7f2">No</span>',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->user->name)?$data->user->name:""',
                ),
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Address Proof &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this,'gridAddressImagePopup'),
                ),
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Id Proof &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this,'gridIdImagePopup'),
                ),
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Photo Proof &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this,'gridPhotoPopup'),
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Created At &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
                array(
                    'name' => 'country_id',
                    'header' => '<span style="white-space: nowrap;">Country &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->country->name)?$data->country->name:""',
                ),
                array(
                    'name' => 'phone',
                    'header' => '<span style="white-space: nowrap;">Phone &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this ,'gridPhone'),
                ),
                array(
                    'name' => 'status',
                    'value' => function($data) {
                        $status = $data->document_status == 1 ? ("Approved") : ($data->document_status == 0 ? "Pending" : "Rejected");
                        echo $status;
                    }
                ),
                 array(
                    'class' => 'CButtonColumn',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'template' => '{Change}{Reject}{Profile}',
                    'htmlOptions' => array('width' => '10%'),
                    'buttons' => array(
                         'Change' => array(
                            'label' => '<i class="fa fa-retweet"></i>',
                            'visible'=>'($data->document_status==2)?false:true;',                            
                            'options' => array('class' => 'action-icons', 'title' => 'Change Status'),
                            'url' => 'Yii::app()->createUrl("admin/user/changeapprovalstatus", array("id"=>$data->id,"status"=>$data->document_status))',
                        ),
                        'Reject' => array(
                            'label' => '<i class="fa fa-ban"></i>', 
                            'visible'=>'($data->document_status==2)?false:true;',
                            'options' => array('class' => 'action-icons', 'title' => 'Reject'),
                            'url' => 'Yii::app()->createUrl("admin/user/changetoreject", array("id"=>$data->id))',
                        ),
                        'Profile' => array(
                            'label' => '<i class="fa fa-eye"></i>',
                            'visible'=>'($data->document_status==2)?false:true;',
                            'options' => array('class' => 'action-icons' ,'title' => 'View Profile', 'target' => '_blank'),
                            'url' => 'Yii::app()->createUrl("admin/user/viewverificationprofile", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>
