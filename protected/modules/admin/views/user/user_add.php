<?php
$this->breadcrumbs = array(
    'Operation' => '/admin/user/index',
    'User Add',
);
?>
<div class="col-md-7 col-sm-7">
    <?php if ($error) { ?><div class="error"><?php echo $error; ?></div><?php } ?>
    <?php if ($success) { ?><div class="success"><?php echo $success; ?></div><?php } ?>

    <?php if (Yii::app()->user->hasFlash('error')){ ?>
        <p class="error-2" id="error_msg"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2"><?php echo Yii::app()->user->getFlash('error'); ?></span></p>
    <?php } ?>

    <?php if (Yii::app()->user->hasFlash('success')){ ?>
        <p class="success-2" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2"><?php echo Yii::app()->user->getFlash('success'); ?></span></p>
    <?php } ?>

    <form action="" method="post" class="form-horizontal" onsubmit="return validateUserAddFrm();">

        <fieldset>
            <legend>Add User</legend>
            <input type="hidden" id="admin" name="admin" value="1">
            <input type="hidden" id="social" name="social" value="">
            <input type="hidden" id="nameExistedErrorFlag" name="nameExistedErrorFlag" class="input-text full-width" value="0"/>
<!--            <div class="form-group">
                <label for="country" class="col-lg-4 control-label">Department</label>
                <div class="col-lg-8">
                    <select name="department[]" class="form-control" multiple>
                            <option value="">Select Department</option>
                            <?php
                            if(isset($departmentObject) && !empty($departmentObject)){
                            foreach ($departmentObject as $department) { ?>
                                <option value="<?php echo $department->id; ?>"><?php echo ucwords($department->name); ?></option>
                            <?php } } ?>
                     </select>

                    <span id="country_id_error" class="clrred"></span></div>
            </div>-->
            <div class="form-group">
                <label for="username" class="col-lg-4 control-label">User Name <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" onchange="isUserExisted()" id="name" name="name">
                    <span id="name_error" class="clrred"></span>
                    <span id="name_available" class="clr green"></span>
                </div>

                <div id="status"></div>
            </div>
            <div class="form-group">
                <label for="lastname" class="col-lg-4 control-label">Full Name <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="full_name" name="full_name">

                    <span id="full_name_error" class="clrred"></span></div>
            </div>
            <div class="form-group">
                <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="email" name="email" onchange="isEmailExisted()">

                    <span id="email_error" class="clrred"></span></div>
            </div>

            <div class="form-group">
                <label for="country" class="col-lg-4 control-label">Country <span class="require">*</span></label>
                <div class="col-lg-8">
                    <select name="country_id" id="country_id" onchange="getCountryCode(this.value)" class="form-control">
                        <option value="">Please Select Country</option>
                        <?php foreach ($countryObject as $country) { ?>
                            <option value="<?php echo $country->id; ?>"><?php echo $country->name; ?></option>
                        <?php } ?>
                    </select>

                    <span id="country_id_error" class="clrred"></span></div>
            </div>


            <div class="form-group">
                <label for="phone" class="col-lg-4 control-label">Mobile phone </label>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-xs-4">
                            <input  name="country_code" id="country_code" class="form-control" readonly="true"  >                                            </div>
                        <div class="col-lg-9 colo-sm-9 col-xs-8">

                            <input  name="phone" id="phone" maxlength="12" placeholder="phone number" class="form-control" value="<?php echo isset($_POST['phone']) ? $_POST['phone'] : ''; ?>" > <br>
                        </div>
                        <span id="phone_error" class="clrred"></span></div>
                </div>
            </div>

            <div class="form-group">
                <label for="firstname" class="col-lg-4 control-label">Referral Code </label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" value="<?php echo (isset($spnId)) ? $spnId : ""; ?>" name="sponsor_id" id="sponsor_id" onchange="isSponsorExisted()">                    
                    <span id="sponsor_id_error" class="clrred"></span>
                </div>
            </div>

        </fieldset>
        <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Submit" class="btn red">
            </div>
        </div>
    </form>
</div> 
