<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Support' => '/admin/user/creditwallet?mode=AddFund',
    'Change Password',
);
if (Yii::app()->user->hasFlash('error')):
    echo '<div class="alert alert-danger">' . Yii::app()->user->getFlash('error') . '</div>';
endif;
if (Yii::app()->user->hasFlash('success')):
    echo '<div class="alert alert-success">' . Yii::app()->user->getFlash('success') . '</div>';
endif;
?>
<div class="col-md-6 col-sm-6">

    <div class="portlet box toe-blue">
        <div class="portlet-title">
            <div class="caption">
                Change Password
            </div>
        </div>
        <div class="portlet-body form ">
            <form class="form-horizontal" role="form" id="form_admin_reservation" enctype="multipart/form-data" action="" method="post" onsubmit="return changepassword();">
                <input type="hidden" name="change_passwor_error_flag" id="change_passwor_error_flag" value="">
                <div class="form-body padding-right15">
                    <fieldset>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">User Name:<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" name="username" id="username" onblur = "return getUserDetails();" />
                                <span id="full_name"></span>
                                <span style="color:red"  id="name_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">New Password:<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="password" class="form-control" name="password" id="password" />
                                <span style="color:red"  id="password_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">New Master Pin:<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="password" class="form-control" name="master_pin" id="master_pin" />
                                <span style="color:red"  id="master_pin_error"></span>
                            </div>
                        </div>

                </div>
                </fieldset>
                <div class="form-actions right">                     
                    <input type="submit" name="submit" value="Submit" class="btn mav-blue-btn ">

                </div>

            </form>
        </div>
    </div>
</div>