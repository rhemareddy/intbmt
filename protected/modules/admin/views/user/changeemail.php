<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Support' => '/admin/user/creditwallet?mode=AddFund',
    'Update User Email',
);
if (Yii::app()->user->hasFlash('error')):
    echo '<div class="alert alert-danger">' . Yii::app()->user->getFlash('error') . '</div>';
endif;
if (Yii::app()->user->hasFlash('success')):
    echo '<div class="alert alert-success">' . Yii::app()->user->getFlash('success') . '</div>';
endif;
?>
<div class="col-md-6 col-sm-6">

    <div class="portlet box toe-blue">
        <div class="portlet-title">
            <div class="caption">
                Update User Email
            </div>
        </div>
        <div class="portlet-body form ">
            <form class="form-horizontal" role="form" action="" method="post" onsubmit="return changeEmail();">
                <input type="hidden" name="change_email_error_flag" id="change_email_error_flag" value="">
                <div class="form-body padding-right15">
                    <fieldset>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">User Name:<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" name="username" id="username" onblur = "return getUserFullDetails();" />
                                <span id="full_name"></span>
                                <span id="emailData"></span>
                                <span style="color:red"  id="name_error"></span>
                            </div>
                        </div>
                        
                        <div class="form-group" id="newemail-block" style="display: none">
                            <label class="col-lg-4 control-label" for="lastname">New Email:</label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control" name="newemail" id="newemail" onchange="isEmailExistedAjax();" />
                                <span style="color:red"  id="newemail_error"></span>
                                <span id="email_error" class="clrred"></span>
                            </div>
                        </div>
                    </fieldset>
                </div>
                
                <div class="form-actions right">                     
                    <input type="submit" name="submit" value="Submit" class="btn mav-blue-btn ">
                    <input type="hidden" id="invalidEmailExistedErrorFlag" value="0">
                </div>

            </form>
        </div>
    </div>
</div>
<style>
.col-lg-7 > span#full_name {display: block;}
</style>
    