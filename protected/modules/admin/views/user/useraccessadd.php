<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Member Access' => '/admin/UserHasAccess/members',
    'Add User',
);
?>
<div class="col-md-7 col-sm-7">

    <?php if (Yii::app()->user->hasFlash('error')){ ?>
        <p class="error-2" id="error_msg"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2"><?php echo Yii::app()->user->getFlash('error'); ?></span></p>
    <?php } ?>

    <?php if (Yii::app()->user->hasFlash('success')){ ?>
        <p class="success-2" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2"><?php echo Yii::app()->user->getFlash('success'); ?></span></p>
    <?php } ?>

    <form action="" method="post" class="form-horizontal" onsubmit="return validateMemberAccess();">

        <fieldset>
            <legend>Add User</legend>
            <input type="hidden" id="nameExistedErrorFlag" name="nameExistedErrorFlag" class="input-text full-width" value="0"/>
            <input type="hidden" id="emailExistedErrorFlag1" name="emailExistedErrorFlag" class="input-text full-width" value="0"/>
            <div class="form-group">
                <label for="department" class="col-lg-4 control-label">Department</label>
                <div class="col-lg-8">
                    <select name="department[]" class="form-control" multiple>
                            <option value="">Select Department</option>
                            <?php
                            if(isset($departmentObject) && !empty($departmentObject)){
                            foreach ($departmentObject as $department) { ?>
                                <option value="<?php echo $department->id; ?>"><?php echo ucwords($department->name); ?></option>
                            <?php } } ?>
                     </select>

                    <span id="department_error" style="color: red;"></span></div>
            </div>
            <div class="form-group">
                <label for="username" class="col-lg-4 control-label">Name <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" onchange="isUserExisted()" id="name" name="name">
                    <span id="name_error" style="color: red;"></span>
                    <span id="name_available" style="color: green;"></span>
                </div>

                <div id="status"></div>
            </div>

            <div class="form-group">
                <label for="email" class="col-lg-4 control-label">Email <span class="require">*</span></label>
                <div class="col-lg-8">
                    <input type="text" class="form-control" id="email" name="email" onchange="isEmailExisted()">

                    <span id="email_error"  style="color: red;"></span></div>
            </div>

            <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Password</label>
                <div class="col-lg-8">
                    <input type="password" id="zip_code" class="form-control" name="password" id="password">
                    <div style="color:red;" id="error_password"> </div> 
                </div>
            </div>

        </fieldset>
        <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Submit" class="btn red">
            </div>
        </div>
    </form>
</div>
