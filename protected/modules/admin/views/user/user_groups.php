<?php 
 $this->breadcrumbs = array(
        'Dashboard' => '/admin/default/dashboard',
        'Operation' => '/admin/user/index',
        'Manage User Group'
    );
 
if (isset($_GET['id'])) {
    $actionUrl = "/admin/user/usergroup?id=" . $_GET['id'];
    $title = "Update";
} else {
    $actionUrl = "/admin/user/usergroup";
    $title = "Create";
}
?>

<?php if (Yii::app()->user->hasFlash('error')){ ?>
     <div class="alert alert-danger"><?php echo Yii::app()->user->getFlash('error'); ?></div>
<?php } ?>
    
<?php if (Yii::app()->user->hasFlash('success')){ ?>
     <div class="alert alert-success"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php } ?>

<form id="usergroup_filter_frm" name="usergroup_filter_frm" method="post" action="<?php echo $actionUrl; ?>" onsubmit="return validateManageUserGroup();">
<div class="portlet box toe-blue col-md-6 col-sm-6">
    <div class="portlet-title">
        <div class="caption">
        <h4><?php echo $title; ?> Group</h4>
        </div>
    </div>

        <div class="portlet-body form padding15">

            <div class="row form-group">
                <label for="inputEmail3" class="col-sm-2 control-label text-right">Group Name *</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="name" id="name" placeholder="Group Name" value="<?php echo isset($userGroupsObject->name)?$userGroupsObject->name:"";?>" maxlength="100">
                    <span id="error_name" style="color:red;"></span>
                </div>
            </div>
            <div class="row form-group">
                <label for="inputEmail3" class="col-sm-2 control-label text-right">User Names *</label>
                <div class="col-sm-8">
                     <?php
                      $userNameList = "";
                      $userIdList = "";
                        if(isset($userGroupsObject) && !empty($userGroupsObject)){
                            $userHasGroupsObject = UserHasGroups::model()->findAll(array('condition' => 'user_groups_id='.$userGroupsObject->id));
                            if(isset($userHasGroupsObject) && !empty($userHasGroupsObject)){
                                foreach ($userHasGroupsObject as $userHasGroups) {
                                    $userIdList .= $userHasGroups->user_id . ",";
                                    $userObject = User::model()->findByPk($userHasGroups->user_id);
                                    if (isset($userObject) && !empty($userObject)) {
                                        $userNameList .= $userObject->name . ",";
                                    }
                                } 
                            }
                        }
                        ?>
                    <textarea name="users" id="users" class="form-control" placeholder="User Names" onblur="getUserGroupUserData(this.value);"><?php echo rtrim($userNameList,","); ?></textarea>
                    <input type="hidden" name="usersIdList" id="usersIdList" value="<?php echo rtrim($userIdList,","); ?>" />
                    (User Names should be comma(,) separated.)<br>
                    <span id="users_name_valid"></span><br>
                    <span id="users_name_invalid"></span><br>
                    <span id="error_users" style="color:red;"></span>
                </div>
            </div>
      
         <input type="submit" name="addedit" id="addedit" class="btn green" value="<?php echo $title; ?> Group">
        </div>
</div>
</form>   

<form id="new_user_filter_frm" name="user_filter_frm" method="post" action="/admin/user/usergroup" >
  

    <div class="row">
        <div class="col-md-12 responsiveTable">
         
            <?php
            if (Yii::app()->user->hasFlash('error')):
                echo '<div class="alert alert-danger top7">' . Yii::app()->user->getFlash('error') . '</div>';
            endif;
            if (Yii::app()->user->hasFlash('success')):
                echo '<div class="alert alert-success top7">' . Yii::app()->user->getFlash('success') . '</div>';
            endif;
            ?>
            <br><br>
            
          <?php  
        $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'city-grid',
	'dataProvider'=>$dataProvider,
	'enableSorting'=>'true',
	'ajaxUpdate'=>true,
        'template' => "{pager}\n{items}\n{summary}\n{pager}",
	'itemsCssClass'=>'table table-striped table-bordered table-hover table-full-width',
	'pager'=>array(
		'header'=>false,
		'firstPageLabel' => "<<",
		'prevPageLabel' => "<",
		'nextPageLabel' => ">",
		'lastPageLabel' => ">>",
	),	
	'columns'=>array(
		array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">Sl.No</span>',
                ),             
               array(
                    'name'=>'name',
                    'header'=>'<span style="white-space: nowrap;">Group Name &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'$data->name',
		),
                array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Users &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>array($this , 'getusers'),
		),
                array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'value' => function($data) {
                        echo '<a href="/admin/user/usergroup?id='.BaseClass::mgEncrypt($data->id).'"  title="Edit Group"><i class="fa fa-pencil-square-o"></i></a>&nbsp;&nbsp;<a href="/admin/user/trackusergroup?id='.BaseClass::mgEncrypt($data->id).'" title="Track Group Business" target="_blank"><i class="fa fa-eye"></i></a>';
                    },
		),
            ),
        )); ?>
            
        </div>
    </div>
</form>         



