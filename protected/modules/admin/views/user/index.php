<?php 
$bcrum = "All Member";
$dataUrl = 'user/readuserdata';
$mainOption = '<a href="/admin/user">Operation</a>';
if(isset($_GET['status']) && $_GET['status'] == 'supportusers') {
    $mainOption = '<a href="/admin/user/supportcreditwallet">Support</a>';
} 
if(isset($_GET['status'])){
    $dataUrl = 'user/readinactiveuserdata';
    $bcrum = 'InActive Members';
}
?>
<ul class="page-breadcrumb breadcrumb">
    <li>
        <div class="breadcrumbs">
            <a href="/admin/default/dashboard">Dashboard</a> »
            <?php echo $mainOption; ?> » <span><?php echo $bcrum ?></span>
        </div>
    </li>
</ul>
<?php
Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/buttons.dataTables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');
?>
<script src="/js/datatables.min.js"></script>
      


<?php
/* @var $this UserController */
/* @var $model User */


$status = "";
if (isset($_GET['status']) && !empty($_GET['status'])) {
    $status = $_GET['status'];
}

$userStatus = 1;
if (isset($_GET['user_status']) && !empty($_GET['user_status'])) {
    $userStatus = $_GET['user_status'];
}

$querystring = $_SERVER['QUERY_STRING'];
$filter = $querystring . "&csv=1";


?>

<form id="new_user_filter_frm" name="user_filter_frm" method="post" action="/admin/user" >
    <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">
    <div class="user-status filter-toggle">
        <input type="hidden" name="status" id="status" value="<?php echo (isset($_GET['status']) && !empty($_GET['status'])) ? $_GET['status'] : ""; ?>">
        
        <div class="col-md-3 col-sm-4 bulkSelect">
            <div class="form-group">
                <label class="col-md-4 col-sm-3 padding0 top7 margin-left15"> Bulk Action</label>
                <div class="col-md-7 col-sm-8">
                    <select class=" form-control  " id="bulkaction" name="bulkaction" onchange="showUserBulkactionButton(this.value);">
                        <option value="0" >Select</option>
                        <?php if (isset($status) && $status != "") { ?>
                            <option value="1">Resend Activation Email</option>
                            <option value="2">SMS Reminder - Activation</option>
                            <option value="3">Send Email to Sponsor</option>
                            <option value="6">Send Internal Mail to Sponsor</option>
                        <?php } else { ?>
                            <option value="4">Mobile Verified</option>
                            <option value="5">Mobile Not Verified</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-1 col-sm-6 form-inline">
            <input type="submit" class="btn btn-success" value="OK" name="user_bulkaction_submit" id="user_bulkaction_submit" style="display:none;"/>
        </div>
          <?php if(!isset($_GET['status'])){ ?>
        <div class="col-md-3 col-sm-4 bulkSelect">
            <div class="form-group">
                <label class="col-md-3 col-sm-3 padding0 top7 mobile-left-15">Status : </label>
                <div class="col-md-6 col-sm-6">
                    <select class=" form-control  " id="change_status" name="change_status">
                        <option value="all" >All</option>
                        <option value="1" selected="selected">Active</option>
                        <option value="2">InActive</option>
                    </select>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="col-md-5">          
                <div class="input-group input-large date-picker input-daterange pull-left">
                    <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
                    <span class="input-group-addon"> to </span>
                    <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
                </div>
            <input type="button" class="btn btn-primary f-left margin-left15 margintop3" value="Filter" name="submit" id="btnSearch">
        </div>
        <p id="date_error"></p>
      
        
    </div> 
    <div class="row">
        <div class="col-md-12 responsiveTable">
            <?php if (isset($_GET['successMsg']) && $_GET['successMsg'] == '1') { ?><div class="success" id="error_msg"><?php echo "User Added Successfully"; ?></div><?php } ?>
            <?php if (isset($_GET['successMsg']) && $_GET['successMsg'] == '2') { ?><div class="success" id="error_msg"><?php echo "Record Deleted Successfully"; ?></div><?php } ?>
            <?php if (isset($_GET['successMsg']) && $_GET['successMsg'] == '3') { ?><div class="success" id="error_msg"><?php echo "User Details Updated Successfully"; ?></div><?php } ?>

            <?php
            if (Yii::app()->user->hasFlash('error')):
                echo '<div class="alert alert-danger top7">' . Yii::app()->user->getFlash('error') . '</div>';
            endif;
            if (Yii::app()->user->hasFlash('success')):
                echo '<div class="alert alert-success top7">' . Yii::app()->user->getFlash('success') . '</div>';
            endif;
            ?>
        
            
            <table id="allMemberList" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="flowcheckall" class="editor-active"></th>
                        <th>Name</th>
                        <th>Full Name</th>
                        <th>Sponsor</th>
                        <th>L/R</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Ph.Verify</th>
                        <th>Doc.Verify</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Actions</th>

                    </tr>
                </thead>
            </table>
            <script src="/js/dataTables.buttons.min.js"></script>
            <script src="/js/jszip.min.js"></script>
            <script src="/js/buttons.html5.min.js"></script>
            <script src="/js/buttons.print.min.js"></script>
            <script>
                $(document).ready(function () {
                    oTable = $('#allMemberList').DataTable({
//                        "sDom":'lfptip',
                        "responsive": true,
                        "iDisplayLength": 50,
                        "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        "processing": true,
                        "serverSide": true,
                        "paging": true,
                        "bSort": true,
                        "pagingType": "full_numbers",
                        "order": [[9, "desc"]],
                        "aoColumnDefs": [ {
                            'bSortable' : false,
                            'aTargets' : [ 0 ]
                        } ],
                        "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                        "buttons": [ {
                                extend: 'csv',
                                footer: true,
                                exportOptions: {
                                     columns: [1,2,3,4,5,6,7,8,9,10]
                                 }
                            }],
                        "ajax": {
                            "url": "<?php echo $dataUrl; ?>",
                            "type": "POST"
                        },
                        "columns": [
                            {
                                data: "id",
                                render: function (data, type, row) { 
                                    if (type === 'display') {
                                        return '<input type="checkbox" class="editor-active" name="requestids['+data+']" value="'+data+'">';
                                    }
                                    return data;
                                },
                                className: "dt-body-center"
                            },
                            {"data": "name"},
                            {"data": "full_name"},
                            {"data": "sponsorName"},
                            {"data": "position"},
                            {"data": "email"},
                            {"data": "phoneNuber"},
                            {"data": "mobileStatus"},
                            {"data": "documentStatus"},
                            {"data": "created_at"},
                            {"data": "userStatus"},
                            {
                                data: "id",
                                render: function (data, type, row) { 
                                        return '<a href="user/changestatus?id='+data+'"  title="Change Status"><i class="fa fa-retweet"></i></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="getEncryptionId('+data+')" title="View"><i class="fa fa-eye"></i></a>';
                                },
                                className: "dt-body-center"
                            },

                        ],
                        select: {
                            style: 'os',
                            selector: 'td:not(:first-child)' // no row selection on last column
                        },
                    });
                });
            $("#flowcheckall").click(function () {
                $('#allMemberList tbody input[type="checkbox"]').prop('checked', this.checked);
            });
            $("#btnSearch").click(function() {
                if($("#toDate").val()<$("#fromDate").val()){
                    $("#date_error").html("To Date should be greater then From Date!!!");
                    return false;
                }
                
                oTable.columns(9).search($("#toDate").val().trim(),$("#fromDate").val().trim());
                oTable.draw();
            });
            $("#change_status").change(function() {
                oTable.columns(10).search($("#change_status").val().trim());
                oTable.draw();
            });

            function getEncryptionId(id){ 
                var dataString = 'id=' + id;
                $.ajax({
                    type: "POST",
                    url: "/site/encryptid",
                    data: dataString,
                    cache: false,
                    success: function (respoonseData) {
                        window.location.href = "user/edit?id="+respoonseData;
                        return false;
                    }
                });
            }
        </script>
        </div>
    </div>
</form>


