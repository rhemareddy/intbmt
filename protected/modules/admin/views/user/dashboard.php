<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Dashboard'
);

if (!empty($_GET['successMsg'])) { echo '<div class="success">'.$_GET['successMsg'].'</div>'; } 

$accessArr = BaseClass::getMemberAccess();
$oneMonthBackDate = date('Y-m-d', strtotime("-1 month"));
$currentDate = $week_end = date('Y-m-d');
$week_start = date('Y-m-d', strtotime('-6 days'));
if (in_array('dashboard', $accessArr)) {
	?>
    <div class="clearfix"></div>
    <div class="row homeDashboard">
        <div class="col-md-6 col-sm-6">
            <h4>Pending Action</h4>
            <div class="row dashboard-row">
                <?php
                if (in_array('user', $accessArr)) {
                    if (in_array('document', $accessArr)) {
                        ?>	
                        <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                            <div class="dashboard-stat yellow">
                                <div class="visual">
                                    <i class="fa fa-book"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><span class="small"></span>
                                    <?php echo $count['totalPendingDocuments']; ?>
                                    </div>
                                    <div class="desc"> Pending Document Approval </div>
                                </div>
                                <a href="/admin/user/verificationapproval?res_filter=0" class="more">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>						
                            </div>
                        </div>
                    <?php
                    }
                }

                if (in_array('user', $accessArr)) {
                    if (in_array('testimonial', $accessArr)) {
                        ?>	
                        <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-comments"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><span class="small"></span>
                                    <?php echo $count['totalPendingTestimonial']; ?>
                                    </div>
                                    <div class="desc"> Pending Testimonial Approval</div>
                                </div>
                                <a href="/admin/user/testimonialapproval?&res_filter=0" class="more">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>						
                            </div>
                        </div>
                    <?php
                    }
                }

                if (in_array('user', $accessArr)) {
                    if (in_array('profileimage', $accessArr)) {
                        ?>
                        <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="fa fa-picture-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><span class="small"></span>
                                    <?php echo $count['totalPendingProImage']; ?>
                                    </div>
                                    <div class="desc"> Profile Image Approval </div>
                                </div>
                                <a href="/admin/user/profileimageapproval?res_filter=0" class="more">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>						
                            </div>
                        </div>
                    <?php
                    }
                }

                if (in_array('withdrawal', $accessArr)) {
                    if (in_array('request', $accessArr)) {
                        ?>
                        <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                            <div class="dashboard-stat red">
                                <div class="visual">
                                    <i class="fa fa-hourglass-half"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><span class="small"></span>
                                    <?php echo $count['totalPendingWithdrawal']; ?>
                                    </div>
                                    <div class="desc"> Pending WithDrawal </div>
                                </div>
                                <a href="/admin/withdrawal/request" class="more">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>						
                            </div>
                        </div>
                    <?php
                    }
                }

                if (in_array('reports', $accessArr)) {
                    if (in_array('reportsponsor', $accessArr)) {
                        ?>
                        <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                            <div class="dashboard-stat purple">
                                <div class="visual">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="details">
                                    <div class="number"><span class="small"></span>
                                    <?php echo $count['totalPendingCompanySponsor']; ?>
                                    </div>
                                    <div class="desc">									
                                        <span class="company-span"> Company Sponsor : Pending contact</span>
                                    </div>
                                </div>
                                <a href="/admin/report/companysponsor?from=&to=&res_filter=1&sponsor_list=&contact_status=PENDING&per_page=50&submit=OK" class="more">
                                    View more <i class="m-icon-swapright m-icon-white"></i>
                                </a>						
                            </div>
                        </div>
        <?php
        }
    }
    if (in_array('reports', $accessArr)) {
        if (in_array('reporttransaction', $accessArr)) {
            ?>
            <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                <div class="dashboard-stat yellow">
                    <div class="visual">
                        <i class="fa fa-exchange"></i>
                    </div>
                    <div class="details">
                        <div class="number"><span class="small"></span>
                         <?php echo $count['totalPendingTransaction']; ?>
                        </div>
                        <div class="desc">									
                            Transaction: Pending Verification
                        </div>
                    </div>
                    <a href="/admin/report/transaction?from=<?php echo $week_start; ?>&to=<?php echo $week_end; ?>&res_filter=1&type=0&isverified=<?php echo Transaction::$_STATUS_NOTVERIFIED; ?>" class="more">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>						
                </div>
            </div>
    <?php
            }
        }
        ?>
        </div>

    <h4>Statistics</h4>
    <div class="row dashboard-row">
    <?php
    if (in_array('user', $accessArr)) {
        if (in_array('wallet', $accessArr)) {
            ?>
                <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                    <div class="dashboard-stat green">
                        <div class="visual">
                            <i class="fa fa-usd"></i>
                        </div>
                        <div class="details">
                            <div class="number"><span class="small"></span>
                            <?php echo isset($count['totalBidWalletBalance']) ? "$" . sprintf("%.2f", $count['totalBidWalletBalance']) : "$0"; ?>
                            </div>
                            <div class="desc"> Bid wallet Balance </div>
                        </div>
                        <a href="/admin/user/wallet?search=&res_filter=1&submit=OK" class="more">
                            View more <i class="m-icon-swapright m-icon-white"></i>
                        </a>						
                    </div>
                </div>
        <?php
        }
    }

    if (in_array('user', $accessArr)) {
        if (in_array('wallet', $accessArr)) {
            ?>
            <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                <div class="dashboard-stat blue">
                    <div class="visual">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="details">
                        <div class="number"><span class="small"></span>
                            <?php echo isset($count['totalCashBackWalletBalance']) ? "$" . sprintf("%.2f", $count['totalCashBackWalletBalance']) : "$0"; ?>
                            </div>
                            <div class="desc">									
                                CashBack Wallet Balance
                            </div>
                        </div>
                        <a href="/admin/user/wallet?search=&res_filter=2&submit=OK" class="more">
                            View more <i class="m-icon-swapright m-icon-white"></i>
                        </a>						
                    </div>
                </div>
        <?php
        }
    }

    if (in_array('withdrawal', $accessArr)) {
        if (in_array('history', $accessArr)) {
            ?>
            <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                <div class="dashboard-stat purple">
                    <div class="visual">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="details">
                        <div class="number"><span class="small"></span>
                        <?php echo isset($count['totalCompletedWithdrawal']) ? "$" . sprintf("%.2f", $count['totalCompletedWithdrawal']) : "$0"; ?>
                        </div>
                        <div class="desc">Completed WithDrawals </div>
                    </div>
                    <a href="/admin/withdrawal/history?from=&to=&res_filter=1&submit=OK" class="more">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>						
                </div>
            </div>
    <?php
        }
    }

    if (in_array('reports', $accessArr)) {
        if (in_array('reportsponsor', $accessArr)) {
            ?>
            <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                <div class="dashboard-stat yellow">
                    <div class="visual">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="details">
                        <div class="number"><span class="small"></span>
                        <?php echo $count['totalAdminSponsor']; ?>
                        </div>
                        <div class="desc">									
                            Admin Sponsor
                        </div>
                    </div>
                    <a href="/admin/report/companysponsor?from=&to=&res_filter=1&sponsor_list=1&contact_status=PENDING&per_page=50&submit=OK" class="more">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>						
                </div>
            </div>
        <?php
        }
    }

    if (in_array('reports', $accessArr)) {
        if (in_array('reportsponsor', $accessArr)) {
            ?>
            <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                <div class="dashboard-stat blue">
                    <div class="visual">
                        <i class="fa fa-user"></i>
                    </div>
                    <div class="details">
                        <div class="number"><span class="small"></span>
                        <?php echo $count['totalSocialSponsor']; ?>
                        </div>
                        <div class="desc">									
                            Social Sponsor
                        </div>
                    </div>
                    <a href="/admin/report/companysponsor?from=&to=&res_filter=1&sponsor_list=2&contact_status=PENDING&per_page=50&submit=OK" class="more">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>						
                </div>
            </div>
        <?php
        }
    }
    if (in_array('user', $accessArr)) {
        if (in_array('usermg', $accessArr)) {
            ?>
            <div data-desktop="span4" data-tablet="span6" class="col-sm-4 responsive padding-right0">
                <div class="dashboard-stat yellow">
                    <div class="visual">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="details">
                        <div class="number"><span class="small"></span>
                        <?php echo $count['totalActiveUser']; ?>
                        </div>
                        <div class="desc">									
                            Total Active Users
                        </div>
                    </div>
                    <a href="/admin/user" class="more">
                        View more <i class="m-icon-swapright m-icon-white"></i>
                    </a>						
                </div>
            </div>
        <?php
        }
    }
    ?> 
            </div>
        </div>
    <div class="col-md-6 col-sm-6">
            <div class="dashboard-row">
                <?php
                if (in_array('bids', $accessArr)) {
                    if (in_array('bidsummary', $accessArr)) {
                        ?>
                        <div class="portlet light col-md-12 activeBids">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-layers icons hide"></i>
                                    <h4>Active Bids</h4><a class="btn viewAll" href="/admin/bid/list">View All</a>
                                </div>
                            </div>
                            <?php
                            $this->widget('zii.widgets.grid.CGridView', array(
                                'id' => 'room-grid',
                                'dataProvider' => $dataProvider,
                                'enablePagination' => false,
                                'enableSorting' => false,
                                'ajaxUpdate' => true,
                                'summaryText' => 'Showing {start} to {end} of {count} entries',
                                'template' => '{pager} {items} {pager}',
                                'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
                                'pager' => array(
                                    'header' => false,
                                    'firstPageLabel' => "<<",
                                    'prevPageLabel' => "<",
                                    'nextPageLabel' => ">",
                                    'lastPageLabel' => ">>",
                                ),
                                'columns' => array(
                                    array(
                                        'class' => 'IndexColumn',
                                        'header' => '<span style="white-space: nowrap;color:#01b7f2">No</span>',
                                    ),
                                    array(
                                        'name' => 'id',
                                        'header' => '<span style=" color:#01b7f2;white-space: nowrap;">Auction (Product name )</span>',
                                        'value' => '$data->product->name',
                                    ),
                                    array(
                                        'name' => 'id',
                                        'header' => '<span style=" color:#01b7f2;white-space: nowrap;">Total Bids</span>',
                                        'value' => function($data) {
                                            $dataQuery = Yii::app()->db->createCommand()
                                                    ->select('count(ab.id) as $dataQuery')
                                                    ->from('auction_bids ab')
                                                    ->join('user u', 'u.id=ab.user_id AND u.role_id =1 AND auction_id = ' . $data->id);
                                            $userListObject = $dataQuery->queryRow();
                                            echo $userListObject['$dataQuery'];
                                        }
                                    ),
                                    array(
                                        'name' => 'close_date',
                                        'header' => '<span style=" color:#01b7f2;white-space: nowrap;">Closing date</span>',
                                        'value' => '$data->close_date',
                                    ),
                                ),
                            ));
                            ?>
                        <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="dashboard-row">

                <?php
                if (in_array('user', $accessArr)) {
                    if (in_array('usermg', $accessArr)) {
                        ?>
                        <div class="portlet light col-md-12 padding-left25">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-bar-chart font-green-sharp hide"></i>
                                    <h4>New Registrants</h4>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="user_site_statistics_loading">
                                    <img src="/metronic/assets/admin/layout/img/loading.gif" alt="loading"/>
                                </div>
                                <div id="user_site_statistics_content" class="display-none">
                                    <div id="user_site_statistics" class="chart">
                                    </div>
                                </div>
                            </div>
                        </div>
        <?php
            }
        }
        ?>
            </div>

        </div>
    <div class="clearfix"></div>
<div class="row">
    <div class="col-md-6 col-sm-6 padding-left30">
            <div class="dashboard-row">
                <?php
                if (in_array('financereports', $accessArr)) {
                    if (in_array('packagepurchased', $accessArr)) {
                        ?>
                        <div class="portlet light col-md-12 padding-left25 ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-shuffle hide"></i>
                                    <h4>Cash inflow</h4>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="cash_site_statistics_loading">
                                    <img src="/metronic/assets/admin/layout/img/loading.gif" alt="loading"/>
                                </div>
                                <div id="cash_site_statistics_content" class="display-none">
                                    <div id="cash_site_statistics" style="height: 228px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                }
                ?>
            </div> 
        </div>

    <div class="col-md-6 col-sm-6">
            <div class="dashboard-row">
                <?php
                if (in_array('bids', $accessArr)) {
                    if (in_array('bidsdetail', $accessArr)) {
                        ?>
                        <div class="portlet light col-md-12 padding-left25 ">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-share font-red-sunglo hide"></i>
                                    <h4>Bid Usage</h4>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div id="bid_site_statistics_loading">
                                    <img src="/metronic/assets/admin/layout/img/loading.gif" alt="loading"/>
                                </div>
                                <div id="bid_site_statistics_content" class="display-none">
                                    <div id="bid_site_statistics" style="height: 228px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                }
                ?>

            </div>
        </div>
    </div>
        </div>
    <?php } ?>

<script src="/metronic/assets/global/plugins/flot/jquery.flot.min.js?ver=<?php echo strtotime("now"); ?>" type="text/javascript"></script>
<script src="/metronic/assets/global/plugins/flot/jquery.flot.categories.min.js?ver=<?php echo strtotime("now"); ?>" type="text/javascript"></script>
<script src="/metronic/assets/admin/Chart.js?ver=<?php echo strtotime("now"); ?>"></script>
<script>
    jQuery(document).ready(function () {
        Metronic.init();
        Layout.init();
        QuickSidebar.init();
        Demo.init();
        Index.init();
        Index.initDashboardDaterange();
        Index.initJQVMAP();
        Index.initCalendar();
        Index.initCharts();
        Index.initChat();
        Index.initMiniCharts();
        Tasks.initDashboardWidget();
    });

    function showChartTooltip(x, y, xValue, yValue) {
            $('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 40,
                    left: x - 40,
                    border: '0px solid #ccc',
                    padding: '2px 6px',
                    'background-color': '#fff'
            }).appendTo("body").fadeIn(200);
    }
    var doughnutData = [
            {
                    value: 300,
                    color: "#efed6a",
                    highlight: "#efed6a",
                    label: "Excellent"
            },
            {
                    value: 250,
                    color: "#b1eac4",
                    highlight: "#b1eac4",
                    label: "Good"
            },
            {
                    value: 100,
                    color: "#fdc3af",
                    highlight: "#fdc3af",
                    label: "Start Up"
            },
            {
                    value: 40,
                    color: "#f7a589",
                    highlight: "#f7a589",
                    label: "Poor"
            }
    ];
    window.onload = function () {
            var ctx = document.getElementById("chart-area").getContext("2d");
            window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive: true});
    };

    if ($('#user_site_statistics').size() != 0) {
            $('#user_site_statistics_loading').hide();
            $('#user_site_statistics_content').show();
            var userCount = [<?php echo $chartUserCount; ?>];
            var plot_statistics = $.plot($("#user_site_statistics"),
                        [{
                            data: userCount,
                            lines: {
                                    fill: 0.6,
                                    lineWidth: 0
                            },
                            color: ['#f89f9f']
                        }, {
                            data: userCount,
                            points: {
                                    show: true,
                                    fill: true,
                                    radius: 5,
                                    fillColor: "#f89f9f",
                                    lineWidth: 3
                            },
                            color: '#fff',
                            shadowSize: 0
                            }],
                        {
                            xaxis: {
                                tickLength: 0,
                                tickDecimals: 0,
                                mode: "categories",
                                min: 0,
                                font: {
                                        lineHeight: 14,
                                        style: "normal",
                                        variant: "small-caps",
                                        color: "#6F7B8A"
                                }
                            },
                            yaxis: {
                                ticks: 5,
                                tickDecimals: 0,
                                tickColor: "#eee",
                                font: {
                                        lineHeight: 14,
                                        style: "normal",
                                        variant: "small-caps",
                                        color: "#6F7B8A"
                                }
                            },
                            grid: {
                                hoverable: true,
                                clickable: true,
                                tickColor: "#eee",
                                borderColor: "#eee",
                                borderWidth: 1
                            }
                        });

                            var previousPoint = null;
                            $("#user_site_statistics").bind("plothover", function (event, pos, item) {
                                $("#x").text(pos.x.toFixed(2));
                                $("#y").text(pos.y.toFixed(2));
                                if (item) {
                                    if (previousPoint != item.dataIndex) {
                                        previousPoint = item.dataIndex;

                                        $("#tooltip").remove();
                                        var x = item.datapoint[0].toFixed(2),
                                                        y = item.datapoint[1].toFixed(2);

                                        showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' Users');
                                    }
                                } else {
                                    $("#tooltip").remove();
                                    previousPoint = null;
                                }
                            });
			}
			if ($('#cash_site_statistics').size() != 0) {
				$('#cash_site_statistics_loading').hide();
				$('#cash_site_statistics_content').show();
				var cashFlow = [<?php echo $chartCashFlow; ?>];
				var plot_statistics = $.plot($("#cash_site_statistics"),
                                [{
                                    data: cashFlow,
                                    lines: {
                                            fill: 0.2,
                                            lineWidth: 0,
                                    },
                                    color: ['#BAD9F5']
                                }, {
                                    data: cashFlow,
                                    points: {
                                            show: true,
                                            fill: true,
                                            radius: 4,
                                            fillColor: "#9ACAE6",
                                            lineWidth: 2
                                    },
                                    color: '#9ACAE6',
                                    shadowSize: 1
                                    }, {
                                        data: cashFlow,
                                        lines: {
                                                show: true,
                                                fill: false,
                                                lineWidth: 3
                                        },
                                        color: '#9ACAE6',
                                        shadowSize: 0
                                    }],
                                {
                                    xaxis: {
                                            tickLength: 0,
                                            tickDecimals: 0,
                                            mode: "categories",
                                            min: 0,
                                            font: {
                                                    lineHeight: 18,
                                                    style: "normal",
                                                    variant: "small-caps",
                                                    color: "#6F7B8A"
                                            }
                                    },
                                    yaxis: {
                                            ticks: 5,
                                            tickDecimals: 0,
                                            tickColor: "#eee",
                                            font: {
                                                    lineHeight: 14,
                                                    style: "normal",
                                                    variant: "small-caps",
                                                    color: "#6F7B8A"
                                            }
                                    },
                                    grid: {
                                            hoverable: true,
                                            clickable: true,
                                            tickColor: "#eee",
                                            borderColor: "#eee",
                                            borderWidth: 1
                                    }
                                });

				var previousPoint2 = null;
				$("#cash_site_statistics").bind("plothover", function (event, pos, item) {
					$("#x").text(pos.x.toFixed(2));
					$("#y").text(pos.y.toFixed(2));
					if (item) {
						if (previousPoint2 != item.dataIndex) {
							previousPoint2 = item.dataIndex;
							$("#tooltip").remove();
							var x = item.datapoint[0].toFixed(2),
									y = item.datapoint[1].toFixed(2);
							showChartTooltip(item.pageX, item.pageY, item.datapoint[0], '$' + item.datapoint[1]);
						}
					}
				});
				$('#cash_site_statistics').bind("mouseleave", function () {
					$("#tooltip").remove();
				});
			}

			if ($('#bid_site_statistics').size() != 0) {
				$('#bid_site_statistics_loading').hide();
				$('#bid_site_statistics_content').show();
				var bidFlow = [<?php echo $chartBidFlow; ?>];
                                var plot_statistics = $.plot($("#bid_site_statistics"),
				[{
                                    data: bidFlow,
                                    lines: {
                                            fill: 0.2,
                                            lineWidth: 0,
                                    },
                                    color: ['#82E0AA']
                                }, {
                                        data: bidFlow,
                                        points: {
                                                show: true,
                                                fill: true,
                                                radius: 4,
                                                fillColor: "#98daae",
                                                lineWidth: 2
                                        },
                                        color: '#98daae',
                                        shadowSize: 1
                                }, {
                                        data: bidFlow,
                                        lines: {
                                                show: true,
                                                fill: false,
                                                lineWidth: 3
                                        },
                                        color: '#98daae',
                                        shadowSize: 0
                                }],
                                {
                                xaxis: {
                                    tickLength: 0,
                                    tickDecimals: 0,
                                    mode: "categories",
                                    min: 0,
                                    font: {
                                            lineHeight: 18,
                                            style: "normal",
                                            variant: "small-caps",
                                            color: "#6F7B8A"
                                    }
                                },
                                yaxis: {
                                    ticks: 5,
                                    tickDecimals: 0,
                                    tickColor: "#eee",
                                    font: {
                                            lineHeight: 14,
                                            style: "normal",
                                            variant: "small-caps",
                                            color: "#6F7B8A"
                                    }
                                },
                                grid: {
                                    hoverable: true,
                                    clickable: true,
                                    tickColor: "#eee",
                                    borderColor: "#eee",
                                    borderWidth: 1
                                }
                            });

		var previousPoint3 = null;
		$("#bid_site_statistics").bind("plothover", function (event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));
                    if (item) {
                        if (previousPoint3 != item.dataIndex) {
                            previousPoint3 = item.dataIndex;
                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(2),
                                            y = item.datapoint[1].toFixed(2);
                            showChartTooltip(item.pageX, item.pageY, item.datapoint[0], '$' + item.datapoint[1]);
                        }
                    }
		});
		$('#bid_site_statistics').bind("mouseleave", function () {
                    $("#tooltip").remove();
		});
	}
</script>
