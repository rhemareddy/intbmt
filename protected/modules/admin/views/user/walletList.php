<?php 
$this->breadcrumbs = array(
    'Operation' => '/admin/user',
    'Wallet'
);

if (isset($_GET['per_page']) && count($_GET) > 1) {
    $queryString = CommonHelper::remove_querystring_var($_SERVER["QUERY_STRING"], 'per_page');
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/user/wallet?' . $queryString . '&per_page=' . "'";
} else {
    $baseUrl = "'" . Yii::app()->params['baseUrl'] . '/admin/user/wallet?per_page=' . "'";
}
 
if(!empty($_GET['successmsg']) && $_GET['successmsg']=='1'){
    echo "<p class='alert alert-success success-2'><i class='fa fa-check-circle icon-success'></i><span class='span-success-2'>Wallet amount transferred successfully.</span></p>";
}
 
if(!empty($_GET['successmsg']) && $_GET['successmsg']=='2'){
    echo "<p class='alert alert-success success-2'><i class='fa fa-check-circle icon-success'></i><span class='span-success-2'>Wallet amount deducted successfully.</span></p>";
}
  
if(!empty($error)){
    echo "<p class='alert alert-danger error-2'><i class='fa fa-times-circle icon-error'></i><span class='span-error-2'>".$error."</span></p>";
}

if (Yii::app()->user->hasFlash('success')): 
     echo '<div class="alert alert-success">'.Yii::app()->user->getFlash('success') .'</div>';
endif;
if (Yii::app()->user->hasFlash('error')):
    echo '<div class="alert alert-danger">' . Yii::app()->user->getFlash('error') . '</div>';
endif;
?>
<div class="col-md-12 user-status">
    <div class="row">

        <form id="regervation_filter_frm" class="col-md-12" name="regervation_filter_frm" method="get" action="/admin/user/wallet">
            <div class="date-picker col-md-3 input-daterange">
                <input type="text" name="search" id="search" placeholder="Search by Name" class="form-control" value="<?php echo (!empty($_REQUEST['search']) && $_REQUEST['search'] !='') ?  $_REQUEST['search'] : '';?>" />
            </div>
            
            <?php 
            $statusId = "";
            if(isset($_REQUEST['res_filter']) && $_REQUEST['res_filter'] !=''){
              $statusId =   $_REQUEST['res_filter'];
            } ?>

        <div class="date-picker col-md-3 input-daterange">
            <?php $walletList = BaseClass::getWalletList(); ?>
            <select class="form-control" id="ui-id-5" name="walletType" >
                <option value="">All Wallet</option>
                <?php foreach ($walletList as $key => $value) { ?>
                    <option value="<?php echo $value->id; ?>" <?php echo ($walletType == $value->id) ? "selected" : ""; ?> ><?php echo $value->name; ?></option>
                <?php } ?>
            </select>
        </div>
            <input type="submit" class="btn btn-primary margin-right-10 confirmOk f-left margin-left-15" value="OK" name="submit" id="submit"/>
            
            <a class="btn btn-xs blue margin-right-10" onclick="walletTransactionStatus('enable')">Enable Wallet Transaction <i class="fa fa-check"></i></a>
            <a class="btn btn-xs red" onclick="walletTransactionStatus('disable')">Disable Wallet Transaction <i class="fa fa-times"></i></a>
            
        </form>    
    
    </div>
</div>
<form class="col-md-12" name="change_wallet_status" id="change_wallet_status" method="POST" action="/admin/user/wallet">
    <input type="hidden" value="" name="actiontype" id="actiontype"/>
    <div class="row">
        <div class="col-md-12">
            <?php 
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'state-grid',
                'dataProvider' => $dataProvider,
                'enableSorting' => 'true',
                'ajaxUpdate' => true,
                'template' => "{pager}\n{items}\n{summary}\n{pager}",
                'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
                'pager' => array(
                    'header' => false,
                    'firstPageLabel' => "<<",
                    'prevPageLabel' => "<",
                    'nextPageLabel' => ">",
                    'lastPageLabel' => ">>",
                ),
                'columns' => array(
                    array(
                        'name' => '',
                        'header' => '<span><input type="checkbox" onclick="selectAll(this)" id="selectall"></span>',
                        'value'=>array($this,'getUserCheckbox'),
                    ),
                    array(
                     'class' => 'IndexColumn',
                     'header' => '<span style="white-space: nowrap;">No.</span>',
                    ),
                    array(
                        'name' => 'user_id',
                        'header' => '<span style="white-space: nowrap;">Full Name &nbsp; &nbsp; &nbsp;</span>',
                        'value' => 'isset($data->user->full_name)?$data->user->full_name:""',
                    ),
                    array(
                        'name' => 'user_id',
                        'header' => '<span style="white-space: nowrap;">Name &nbsp; &nbsp; &nbsp;</span>',
                        'value' => 'isset($data->user->name)?$data->user->name:""',
                    ),
                    array(
                        'name' => 'fund',
                        'header' => '<span style="white-space: nowrap;">Fund &nbsp; &nbsp; &nbsp;</span>',
                        'value' => '$data->fund',
                    ),
                    array(
                        'name' => 'type',
                        'header' => '<span style="white-space: nowrap;">Type &nbsp; &nbsp; &nbsp;</span>',
                        'value' => 'isset($data->wallettype()->name)?$data->wallettype()->name:""',                    
                    ),
                    array(
                        'name' => 'wallet_transaction_status',
                        'header' => '<span style="white-space: nowrap;">Wallet Transaction Status</span>',
                        'value' => 'isset($data->wallet_transaction_status)?$data->wallet_transaction_status:""',                    
                    ),
                    array(
                        'name' => 'status',
                        'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                    ),                
                    array(
                        'name'=>'button',
                        'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                        'value'=>array($this,'GetWalletButtonTitle'),                    
                    ),               
                ),
            ));
            ?>
        </div>
    </div>
</form>