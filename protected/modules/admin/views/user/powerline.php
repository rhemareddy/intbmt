<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Operation' => '/admin/user',
    'Powerline'
);
?>
<?php  if(isset($_GET['successMsg']) && $_GET['successMsg']=='1'){?><p class="alert alert-success" id="error_msg"><span class="span-success-2"><?php  echo "Transaction created successfully for valid users..!!";?><span></p><?php  } ?>
<a class="btn  green margin-bottom-10" href="/admin/user/powerlinereport">View History</a>
<form class="form-horizontal" role="form" id="form_admin_reservation" enctype="multipart/form-data" action="" method="post" onsubmit="return getpackage();">
    <div class="col-md-6 portlet box orange blueLine">
        <div class="portlet-title margin-bottom-15">
            <div class="caption">
                Create Powerline
            </div>
        </div>
        <input type="hidden" name="userIdList" id="userIdList" value="" />
        <div class="col-md-12 form-group">
            <label class="col-md-3 control-label">User Name: *</label>
            <div class="col-md-7">
                <input type="text" class="form-control dvalid" name="name"  onblur="getUserData(this.value);" id="username" />
                <span id="user_name_valid"></span>
                <span id="user_name_invalid"></span>
                <span style="color:red"  id="user_name_error"></span>
            </div>
        </div>
        <div class="col-md-12 form-group">
            <label class="col-md-3 control-label">Bid Amount : *</label>
            <div class="col-md-7">
                <input type="text" class="form-control dvalid" name="package_value"  onblur="getpackage();" id="package_value" />
                <span id="package_name"></span>
                <span style="color:red"  id="Package_value_error"></span>
            </div>
        </div>
        <input type="hidden" name="package_id" id="package_id" value="" />
        <div class="col-md-12 form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-7">
                <input type="submit" class="btn orange pull-right" name="submit" id="submit" size="60" maxlength="75" class="textBox" value="Add Powerline" />
            </div>
        </div> 
    </div>
</form>

<script>
jQuery('#package_value').keyup(function () {
    this.value = this.value.replace(/[^0-9]/g, '');
});
function getpackage(){
    
        $("#user_name_error").html("");
        $("#package_name").html("");
        $("#Package_value_error").html("");
        $("#selected_package").val("");
        var username = $("#username").val();
        if(username==""){ 
            $("#user_name_error").html("Username should not be blank");    
            return false;
        }
        var bidVal = $("#package_value").val();
        if(bidVal==""){ 
            $("#Package_value_error").html("Amount should not be blank");    
            return false;
        }
        
        if(bidVal < 50 || bidVal > 99999){
            $("#Package_value_error").html("Invalid Package Price");   
            return false;
        }

        if(bidVal >= 50 && bidVal <= 9999){
            $("#package_name").html("Selected package is <strong>Explorer</strong>");  
            $("#package_id").val("1");
            return true;
        }
        
        if(bidVal >= 10000 && bidVal <= 49999){
            $("#package_name").html("Selected package is <strong>Pro</strong>");  
            $("#package_id").val("2");
            return true;
        }
        
        if(bidVal >= 50000 && bidVal <= 99999){
            $("#package_name").html("Selected package is <strong>Achiver</strong>"); 
            $("#package_id").val("3 ");
            return true;
        }
    }

</script>
