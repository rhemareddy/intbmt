<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Support' => '/admin/user/creditwallet?mode=AddFund',
    'Profile Image Approval'
);

?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">

<div class="user-status col-md-12 filter-toggle">
    <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/user/profileimageapproval">
        <div class="expiration margin-topDefault confirmMenu">
            <div class="col-md-4 col-sm-4">
                <div class="input-group input-large date-picker input-daterange" style="z-index:9;">
                    <input type="text" name="from" placeholder="From Date" class="datepicker form-control" value="<?php echo (!empty($_GET['from']) && $_GET['from'] != '') ? $_GET['from'] : ""; ?>">
                    <span class="input-group-addon">
                        to </span>
                    <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="<?php echo (!empty($_GET['to']) && $_GET['to'] != '') ? $_GET['to'] : ""; ?>">
                </div>
            </div>
        <?php 
        if(isset($_REQUEST['res_filter']) && $_REQUEST['res_filter'] !=''){
          $statusId =   $_REQUEST['res_filter'];
        }else{
          $statusId =   "0";   
        } ?>

        <div class="col-md-3 col-sm-3">
            <select class="form-control" id="ui-id-5" name="res_filter">
                <option value="all" <?php if ($statusId == '') { ?> selected="selected"<?php } ?>>All</option> 
                <option value="1" <?php if ($statusId == '1') { ?> selected="selected"<?php } ?>>Approved</option>
                <option value="0" <?php if ($statusId == '0') { ?> selected="selected"<?php } ?>>Pending</option>
                <option value="2" <?php if ($statusId == '2') { ?> selected="selected"<?php } ?>>Rejected</option>
            </select>
        </div>

        <div class="col-md-2">
            <div class="dataTables_length" id="search_length">
                <label>Display&nbsp; 
                    <select id="per_page" name="per_page" aria-controls="" class="" onchange="//window.location = <?php //echo $baseUrl;   ?> + this.value">
                        <option value="50" <?php if ($pageSize == 50) echo "selected"; ?>>50</option>
                        <option value="100" <?php if ($pageSize == 100) echo "selected"; ?>>100</option>
                        <option value="200"<?php if ($pageSize == 200) echo "selected"; ?>>200</option>
                    </select>&nbsp; 
                </label>
            </div>
        </div>

    </div>
        <input type="submit" class="btn btn-primary f-left margintop3" value="OK" name="submit" id="submit"/>
    </form>

</div>
<div class="row">
    <div class="col-md-12 blue-table">
        <?php if(isset($_GET['successMsg']) && $_GET['successMsg']=='1'){?><div class="success" id="error_msg"><?php echo "Status Changed Successfully";?></div><?php }?>
        <?php if(isset($_GET['successMsg']) && $_GET['successMsg']=='2'){?><div class="success" id="error_msg"><?php echo "Profile Image Rejected Successfully";?></div><?php }?>
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                //'idJob',
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;color:#01b7f2">Sl.No</span>',
                ),
                array(
                    'name' => 'user_id',
                    'header' => '<span style="white-space: nowrap;">Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->user->name)?$data->user->name:""',
                ),
                
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Profile Image &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this,'gridProfileImagePopup'),
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Created At &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
                array(
                    'name' => 'country_id',
                    'header' => '<span style="white-space: nowrap;">Country &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'isset($data->country->name)?$data->country->name:""',
                ),
                array(
                    'name' => 'phone',
                    'header' => '<span style="white-space: nowrap;">Phone &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this,'gridPhone'),
                ),
                array(
                    'name' => 'status',
                    //'value' => '($data->profile_image_status == 1) ? Yii::t(\'translation\', \'Approved\') : Yii::t(\'translation\', \'Pending\')',
                     'value' => function($data) {
                                if($data->profile_image_status == 1){
                                   echo "Approved";
                                } elseif($data->profile_image_status == 2){
                                   echo "Rejected";
                                } else {
                                   echo "Pending"; 
                                }
                            },
                ),
                 array(
                    'name'=>'button',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this, 'GetProfileImageAction'),
                ), 
            ),
        ));
        ?>
    </div>
</div>
