<?php 
 $this->breadcrumbs = array(
        'Dashboard' => '/admin/default/dashboard',
        'Operation' => '/admin/user/index',
        'Track Group Business'
    );
 
 $usergroupName = "";
 if (!empty($userGroupObject)) {
        $usergroupName = $userGroupObject->name;
} 
?>

<?php if (Yii::app()->user->hasFlash('error')){ ?>
     <div class="alert alert-danger"><?php echo Yii::app()->user->getFlash('error'); ?></div>
<?php } ?>
    
<?php if (Yii::app()->user->hasFlash('success')){ ?>
     <div class="alert alert-success"><?php echo Yii::app()->user->getFlash('success'); ?></div>
<?php } ?>
     
<form id="usergroup_filter_frm" name="usergroup_filter_frm" method="get" action="/admin/user/trackusergroup" onsubmit="return validateTrackUserGroup();">
    <div class="user-status">
       
        <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-2 col-sm-2 padding0 top7"> Search </label>
            <div class="col-md-8 col-sm-8">
               <input type="text" name="search_group" id="search_group" class="form-control" placeholder="Search By Group Name" value="<?php echo isset($_GET['search_group'])?$_GET['search_group']:$usergroupName; ?>">
            </div>
        </div>
        </div>
        
       <div class="col-md-4">    
            <span id="date_error" style="color:red"></span>
            <div class="input-group input-large date-picker input-daterange pull-left">
                <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="<?php echo isset($_GET['from'])?$_GET['from']:date('Y-m-d', strtotime("-1 month")); ?>">
                <span class="input-group-addon"> to </span>
                <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="<?php echo isset($_GET['to'])?$_GET['to']:date('Y-m-d'); ?>">
            </div>
        </div>
        
        <div class="col-md-1 col-sm-6 form-inline">
            <input type="submit" class="btn btn-success margintop3" value="View" name="trackgroupbusiness" id="trackgroupbusiness"/>
        </div>
       
    </div>
</form>

<div class="row">
    <div class="col-md-12 responsiveTable">
      
        <?php  
        $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'city-grid',
	'dataProvider'=>$dataProvider,
	'enableSorting'=>'true',
	'ajaxUpdate'=>true,
        'template' => "{pager}\n{items}\n{summary}\n{pager}",
	'itemsCssClass'=>'table table-striped table-bordered table-hover table-full-width trackUserGroupTable',
	'pager'=>array(
		'header'=>false,
		'firstPageLabel' => "<<",
		'prevPageLabel' => "<",
		'nextPageLabel' => ">",
		'lastPageLabel' => ">>",
	),	
	'columns'=>array(
		array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;">Sl.No</span>',
                ),             
               array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Name &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>'$data->user()->name',
		),
                array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Direct Business &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>array($this , 'getdirectbusiness'),
                    'htmlOptions'=>array('class'=>'directbusiness'),
		), 
                array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Total Business &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>array($this , 'getindirectbusiness'),
                    'htmlOptions'=>array('class'=>'indirectbusiness'),
		),
                array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Withdrawal &nbsp; &nbsp; &nbsp;</span>',
                    'value'=>array($this , 'getwithdrawal'),
                    'htmlOptions'=>array('class'=>'withdrawal'),
		),
                array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Left Count &nbsp; &nbsp; &nbsp;</span>',
                    'value' => function($data) {
                        $leftCount = "";
                        $genealogyObject = Genealogy::model()->findByAttributes(array('user_id' => $data->user_id));
                        if(isset($genealogyObject) && !empty($genealogyObject)){
                            $leftCount = $genealogyObject->left_count;
                        }
                        echo $leftCount;
                    },
                    'htmlOptions'=>array('class'=>'leftcount'),
		),
                array(
                    'name'=>'id',
                    'header'=>'<span style="white-space: nowrap;">Right Count &nbsp; &nbsp; &nbsp;</span>',
                    'value' => function($data) {
                        $rightCount = "";
                        $genealogyObject = Genealogy::model()->findByAttributes(array('user_id' => $data->user_id));
                        if(isset($genealogyObject) && !empty($genealogyObject)){
                            $rightCount = $genealogyObject->right_count;
                        }
                        echo $rightCount;
                    },
                    'htmlOptions'=>array('class'=>'rightcount'),
		),
            ),
        )); ?>
      
    </div>
</div>
<script>
    $(document).ready(function() {
        <?php //if search result not empty showing total
        if(isset($dataProvider) && $dataProvider->itemCount > 0) { ?>
        var sumdirectbusiness=0;
        var sumindirectbusiness=0;
        var sumwithdrawal=0;
        var sumleftcount=0;
        var sumrightcount=0;
        //iterate through each input and add to sum
        $('.directbusiness').each(function() {     
                sumdirectbusiness += parseInt($(this).text());                     
        }); 
         $('.indirectbusiness').each(function() {     
                sumindirectbusiness += parseInt($(this).text());                     
        }); 
        $('.withdrawal').each(function() {     
                sumwithdrawal += parseInt($(this).text());                     
        }); 
        $('.leftcount').each(function() {     
                sumleftcount += parseInt($(this).text());                     
        }); 
        $('.rightcount').each(function() {     
                sumrightcount += parseInt($(this).text());                     
        }); 
        
        $('.trackUserGroupTable tr:last').after('<tr><td colspan="2"><b>Total:</b></td><td><b>'+sumdirectbusiness+'</b></td><td><b>'+sumindirectbusiness+'</b></td><td><b>'+sumwithdrawal+'</b></td><td><b>'+sumleftcount+'</b></td><td><b>'+sumrightcount+'</b></td></tr>');
        <?php } ?>
    });
</script>
         



