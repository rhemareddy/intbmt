<?php
/* @var $this GenealogyController */
/* @var $model Genealogy */

$regVar = '#signup';
$treeVar = '/admin/user/genealogy/';
$registerPopUp = "soap-popupbox";

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Operation' => '/admin/user',
    'Genealogy'
);

?>
<link rel="stylesheet" href="/css/genealogy.css">
<link rel="stylesheet" type="text/css" href="/css/style.css" />

<script type="text/javascript" src="/js/transaction.js"></script>
<script type="text/javascript">
    function getSponsorPosition(id,positionId) {
        $('#sponsor_id').val(id);
        $("#"+positionId).prop("checked", true);
    }
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.myPop').popover({ 
          html : true,
          content: function() {
            return $('#popover_content_wrapper').html();
          }
        });
      });
</script>
<link rel="stylesheet" href="/css/jquery.webui-popover.min.css">
      <div class="row margin-bottom-100">
        <div class="col-md-6">
            <div class="expiration margin-topDefault confirmMenu" style="display:inline-block;">
                <form action="" class="form-inline">
                    <input type="text" class="form-control dvalid" name="name"  onchange="getFullName(this.value);" id="search_username"  value="<?php echo $userObject->full_name; ?>" />
                    <span id="search_fullname">&nbsp;</span>
                    <span id="search_user_error" style="color:red;">&nbsp;</span>
                    <input type="button" name="submit" value="Search" onclick="submitform();" class="btn btn-primary confirmOk">
                </form>
            </div>
        </div>
        <div class="col-md-2">
        <?php 
            if (!empty($_GET['id'])) {
                echo '<div class="row"><div class="col-md-12"><span><a onclick="window.history.back(-1);" style="float:right;font-size:16px;color:#428bca;cursor:pointer;">Go Back >></a></span></div></div>';
            }
        ?>
        </div>
    </div>



      <div class="row adminGenealogy">
          <div class="col-md-12">
          <div class="col-md-8 col-xs-12">
              <div class="myTree">
                  <div class="treeTop yellowbrdr">
                        <!-- Top root node start here -->
                        <?php $getDeatildTop = CJSON::decode(Genealogy::getAllDetailsOfCurrentNode($currentUserId)); ?>
                        <span  class="<?php echo $getDeatildTop['color'] ; ?>" data-placement="auto">
                            <span class="parent <?php echo $getDeatildTop['color'] ; ?>">
                                <a href="<?php echo '?id=' . $currentUserId ;?>" class="show-pop-table"><?php echo $userObject->name; ?></a>
                            </span>
                        </span>
                        <!-- Left node level 1 -->
                        <?php 
                            if (count($genealogyLeftListObject) > 0) {
                                $leftUserId = BaseClass::mgEncrypt($genealogyLeftListObject->user_id);
                                $getDeatildLeft = CJSON::decode(Genealogy::getAllDetailsOfCurrentNode($leftUserId));
                        ?>
                        <span data-placement="auto">
                            <span class=" child left <?php echo $getDeatildLeft['color'] ; ?>" >
                                <a  href="<?php echo '?id=' . $leftUserId; ?>" class="show-pop-table2"><?php echo $genealogyLeftListObject->user->name; ?></a>
                            </span>
                        </span>    
                        <?php }else{ 
                            if(!empty($userObject)){ $addForNewUser = "addnew"; }else{ $addForNewUser = "blank"; } ?>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#registrationInAdmin" onclick="getSponsorPosition('<?php echo !empty($userObject->name) ? $userObject->name : ""; ?>','positionLeft');"><span class="child left <?php echo $addForNewUser; ?>"></span></a>
                        <?php } ?>
                        <!-- Left node level 1 end -->
                        <!-- Right node level 1 start -->
                        <?php 
                        if (count($genealogyRightListObject) > 0) { 
                            $leftUserId = BaseClass::mgEncrypt($genealogyRightListObject->user_id);
                            $getDeatildRight = CJSON::decode(Genealogy::getAllDetailsOfCurrentNode($leftUserId));
                        ?>
                        <span  >
                            <span class="child right  <?php echo $getDeatildRight['color'] ; ?>">
                                <a href="<?php echo '?id=' . $leftUserId; ?>" data-placement="auto" class="show-pop-table3"><?php echo $genealogyRightListObject->user->name; ?></a>
                            </span>
                        </span>    
                        <?php }else{ 
                            if(!empty($userObject)){ $addForNewUser = "addnew"; }else{ $addForNewUser = "blank"; } ?>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#registrationInAdmin" onclick="getSponsorPosition('<?php echo !empty($userObject->name) ? $userObject->name : ""; ?>','positionRight');"><span class="child right <?php echo $addForNewUser; ?>"></span></a>
                        <?php } ?>
                  </div>
                  <!-- Right node level 1 end -->
                   <!-- Left to left node level 2 start -->
                    <div class="treeBot redbrdr pull-left">
                        <?php 
                            if ($genealogyLeftListLevalTwo) { 
                                $leftLeftUserId = BaseClass::mgEncrypt($genealogyLeftListLevalTwo->user_id);
                                $getDeatildleftLeft = CJSON::decode(Genealogy::getAllDetailsOfCurrentNode($leftLeftUserId));
                        ?>
                        <span >    
                                <span class="child left <?php echo $getDeatildleftLeft['color']; ?>">
                                    <a href="<?php echo '?id=' . $leftLeftUserId; ?>" data-placement="auto" class="show-pop-table4"><?php echo $genealogyLeftListLevalTwo->user->name; ?></a>
                                </span>
                            </span>
                        <?php }else{ 
                        /* Checking the this node should be blank or add new */
                        if(isset($genealogyLeftListObject)){ $addForNewUser = "addnew"; $url = "#registrationInAdmin"; }else{ $addForNewUser = "blank"; $url = "#"; } ?>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="<?php echo $url; ?>" onclick="getSponsorPosition('<?php echo !empty($genealogyLeftListObject->user->name) ? $genealogyLeftListObject->user->name : "" ; ?>','positionLeft');"><span class="child left <?php echo $addForNewUser; ?>"></span></a>
                        <?php }
                        if($genealogyRightListLevalTwo){  
                            $genealogyRightListLevalTwoId =  BaseClass::mgEncrypt($genealogyRightListLevalTwo->user_id) ;
                            $getDeatildRightLevalTwo = CJSON::decode(Genealogy::getAllDetailsOfCurrentNode($genealogyRightListLevalTwoId));
                            ?>
                            <span href="<?php echo '?id=' . $genealogyRightListLevalTwoId; ?>" data-placement="auto">
                                <span class="child right <?php echo $getDeatildRightLevalTwo['color']; ?>">
                                    <a href="<?php echo '?id=' . $genealogyRightListLevalTwoId; ?>" data-placement="auto" class="show-pop-table5"><?php echo $genealogyRightListLevalTwo->user->name; ?></a>
                                </span>
                            </span>
                        <?php }else{
                            /* Checking the this node should be blank or add new */
                            if(isset($genealogyLeftListObject)){ $addForNewUser = "addnew"; $url = "#registrationInAdmin"; }else{ $addForNewUser = "blank"; $url = "#";} ?>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="<?php echo $url; ?>" onclick="getSponsorPosition('<?php echo !empty($genealogyLeftListObject->user->name) ? $genealogyLeftListObject->user->name : ""; ?>','positionRight');"><span class="child right <?php echo $addForNewUser; ?>"></span></a>
                        <?php }  ?>
                    </div>
                  <!-- Right  node level 2 strat -->
                  <div class="treeBot right greenbrdr pull-right">
                    <?php if($genealogyRightLeftListLevalTwo){ 
                        $genealogyRightListLevalTwoId =  BaseClass::mgEncrypt($genealogyRightLeftListLevalTwo->user_id) ;
                        $getDeatildRightLeftLevalTwo = CJSON::decode(Genealogy::getAllDetailsOfCurrentNode($genealogyRightListLevalTwoId));
                            ?>
                    <span >  
                        <span class="child left <?php echo $getDeatildRightLeftLevalTwo['color']; ?>">
                            <a href="<?php echo '?id=' . $genealogyRightListLevalTwoId; ?>" data-placement="auto" class="show-pop-table6"><?php echo $genealogyRightLeftListLevalTwo->user->name; ?></a>
                        </span>
                    </span>    
                    <?php }else{ 
                        /* Checking the this node should be blank or add new */
                        if(isset($genealogyRightListObject)){ $addForNewUser = "addnew"; $url = "#registrationInAdmin"; }else{ $addForNewUser = "blank"; $url = "#";}
                        ?>
                      <a href="javascript:void(0)" data-toggle="modal" data-target="<?php echo $url; ?>" onclick="getSponsorPosition('<?php echo !empty($genealogyRightListObject->user->name) ? $genealogyRightListObject->user->name : ""; ?>','positionLeft');"><span class="child left <?php echo $addForNewUser ; ?>"></span></a>
                    <?php } ?>
                      
                     <?php if($genealogyRightRightListLevalTwo){ 
                        $genealogyRightListLevalTwoId =  BaseClass::mgEncrypt($genealogyRightRightListLevalTwo->user_id) ;
                        $getDeatildRightRightLevalTwo = CJSON::decode(Genealogy::getAllDetailsOfCurrentNode($genealogyRightListLevalTwoId));
                        
                         ?>
                        <span> 
                        <span class="child right <?php echo $getDeatildRightRightLevalTwo['color']; ?>">
                            <a href="<?php echo '?id=' . $genealogyRightListLevalTwoId; ?>" data-placement="auto" class="show-pop-table7"><?php echo $genealogyRightRightListLevalTwo->user->name; ?></a>
                        </span>
                        </span>
                    <?php }else{ 
                        /* Checking the this node should be blank or add new */
                        if(isset($genealogyRightListObject)){ $addForNewUser = "addnew"; $url = "#registrationInAdmin"; }else{ $addForNewUser = "blank"; $url = "#"; }
                        ?>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="<?php echo $url; ?>" onclick="getSponsorPosition('<?php echo !empty($genealogyRightListObject->user->name) ? $genealogyRightListObject->user->name : ""; ?>','positionRight');"><span class="child right <?php echo $addForNewUser ; ?>"></span></a>
                    <?php } ?>  
                  </div>
              </div>
          </div>
          <div class="col-md-4 col-xs-12">
              <div class="myTreeRight">
                  <div class="myTreeRightIn">
                      <h3>ICON INFORMATION</h3>
                       <div class="rightIcon addNew">
                          <span><img src="/images/genealogy/add-new.png"></span>
                          <span>Add New Member</span>
                      </div>
                       <div class="rightIcon addNew">
                          <span><img src="/images/genealogy/blank.png"></span>
                          <span>No Action</span>
                      </div>
                      <div class="rightIcon inactive">
                          <span><img src="/images/genealogy/icon_inactive.png"></span>
                          <span>User Inactive</span>
                      </div>
                      <div class="rightIcon iconActive">
                          <span><img src="/images/genealogy/icon_active.png"></span>
                          <span>User Active</span>
                      </div>
                      <div class="rightIcon explorer">
                          <span><img src="/images/genealogy/icon-explorer-small.png"></span>
                          <span>Explorer</span>
                      </div>
                      <div class="rightIcon pro">
                          <span><img src="/images/genealogy/icon-pro-small.png"></span>
                          <span>Pro</span>
                      </div>
                      <div class="rightIcon achiever">
                          <span><img src="/images/genealogy/icon-achiever-small.png"></span>
                          <span>Achiever</span>
                      </div>

                  </div>
              </div>
          </div>
        </div>
      </div>


<div id="registrationInAdmin" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="travelo-signup-box travelo-box" id="signup">
<?php $this->renderPartial('//layouts/signupform'); ?>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="tableContent" style="display:none;">
      <?php Genealogy::userTotalCountInfo($getDeatildTop,$userObject->name); ?>
</div>
<?php if(isset($getDeatildLeft)){  ?>
    <div id="tableContent2" style="display:none;">
        <?php Genealogy::userTotalCountInfo($getDeatildLeft,$genealogyLeftListObject->user->name); ?>
  </div>
<?php }
if(isset($getDeatildRight)){  ?>
  <div id="tableContent3" style="display:none;">
      <?php Genealogy::userTotalCountInfo($getDeatildRight,$genealogyRightListObject->user->name); ?>
  </div>
<?php }
if(isset($getDeatildleftLeft)){  ?>
  <div id="tableContent4" style="display:none;">
      <?php Genealogy::userTotalCountInfo($getDeatildleftLeft,$genealogyLeftListLevalTwo->user->name); ?>
  </div>
<?php }
if(isset($getDeatildRightLevalTwo)){  ?>
  <div id="tableContent5" style="display:none;">
      <?php Genealogy::userTotalCountInfo($getDeatildRightLevalTwo,$genealogyRightListLevalTwo->user->name); ?>
  </div>
<?php }
if(isset($getDeatildRightLeftLevalTwo)){  ?>
  <div id="tableContent6" style="display:none;">
      <?php Genealogy::userTotalCountInfo($getDeatildRightLeftLevalTwo,$genealogyRightLeftListLevalTwo->user->name); ?>
  </div>
<?php }
if(isset($getDeatildRightRightLevalTwo)){  ?>
  <div id="tableContent7" style="display:none;">
      <?php Genealogy::userTotalCountInfo($getDeatildRightRightLevalTwo,$genealogyRightRightListLevalTwo->user->name); ?>
  </div>
<?php }?> 
  <input type="hidden" name='userSearch' value='' id='search_user_id'>
 <script src="/js/jquery.webui-popover.min.js"></script>  
<script>
(function(){
  var settings = {
      trigger:'hover',
      // title:'Pop Title',
      content:'',
      width:250,            
      multi:true,           
      closeable:false,
      style:'',
      delay:300,
      padding:true
  };

  function initPopover(){         
    $('a.show-pop').webuiPopover('destroy').webuiPopover(settings);       
    
    var tableContent = $('#tableContent').html(),
      tableSettings = {content:tableContent};
    $('a.show-pop-table').webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings));
    
    var tableContent2 = $('#tableContent2').html(),
      tableSettings2 = {content:tableContent2};
    $('a.show-pop-table2').webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings2));

    var tableContent3 = $('#tableContent3').html(),
      tableSettings3 = {content:tableContent3};
    $('a.show-pop-table3').webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings3));

    var tableContent4 = $('#tableContent4').html(),
      tableSettings4 = {content:tableContent4};
    $('a.show-pop-table4').webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings4));
    
    var tableContent5 = $('#tableContent5').html(),
      tableSettings5 = {content:tableContent5};
    $('a.show-pop-table5').webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings5));
    
    var tableContent6 = $('#tableContent6').html(),
      tableSettings6 = {content:tableContent6};
    $('a.show-pop-table6').webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings6));
    
    var tableContent7 = $('#tableContent7').html(),
      tableSettings7 = {content:tableContent7};
    $('a.show-pop-table7').webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings7));
    
    
             }
  initPopover();
})();
function getFullName(userName) {
    fedIn(); 
    $.ajax({
        type: "post",
        url: "/user/getfullname",
        data: {'userName': userName},
        success: function (data) {
            var userData = jQuery.parseJSON(data);
            $("#search_user_error").html("");
            fedOut(); 
            if (userData) {                
                $("#search_username").val(userData.name);
                $("#search_fullname").html(userData.fullName);
                $("#search_user_id").val(userData.encid);
                $("#getId").val(userData.id);
                $("#search_user_error").html("");                 
            } else {
                $("#search_user_id").val(0);
                $("#search_fullname").html("");
                $("#search_user_error").html("No such user");
            }
        }
    });
}
function submitform() {
        var id = $('#search_user_id').val();
        if (id != '') {
            location.href = "/admin/user/genealogy?id=" + id;
        }
    }
</script>