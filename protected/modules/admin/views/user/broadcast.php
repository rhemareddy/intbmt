<?php 
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Operation' => '/admin/user',
    'Broadcast Message'
);
if (isset($_GET['id'])) {
    $classAddEdit = "active"; //Add tab making as Edit
    $type = "Edit";
    $actionUrl = "/admin/user/broadcast?id=" . $_GET['id'];
} else {
    $classList = "active"; 
    $actionUrl = "/admin/user/broadcast";
}
$bulkaction = "";
$statusId = 1;
if (isset($_POST['bulkaction'])) { $bulkaction = $_POST['bulkaction']; }
if (isset($_GET['res_filter'])) { $statusId = $_GET['res_filter']; }
    if (Yii::app()->user->hasFlash('error')){ 
        echo '<p class="alert alert-danger error-2" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">'.Yii::app()->user->getFlash('error').'</span></p>';
    } 
    if (Yii::app()->user->hasFlash('success')){ 
    echo '<p class="alert alert-success success-2" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">'.Yii::app()->user->getFlash('success').'</span></p>';
    } 
?>
    <div class="row" id="broadcast_filter_div">
        <div class="expiration margin-topDefault confirmMenu">
    <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/user/broadcast">
         <div class="col-md-1 padding-right0 margin-topDefault no-pad">
            <select class=" form-control" id="ui-id-5" name="res_filter">
                <option value="all">All</option>
                <option value="1" <?php if($statusId == "1"){ echo "selected"; } ?> >Active</option>
                <option value="0" <?php if($statusId == "0"){ echo "selected"; } ?> >Inactive</option>
            </select>
        </div>
        <div class="col-md-5 col-sm-6 margin-topDefault form-inline">      
            <input type="submit" class="btn btn-success confirmOk" value="OK" name="submit" id="submit"/>
        </div>
    </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12 margin-top-20">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?php echo isset($classList) ? $classList : ""; ?>"><a href="#list" id="link-broadcast-list" aria-controls="profile" role="tab" data-toggle="tab">LIST</a></li>
            <li role="presentation" class="<?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>"><a href="#addedit" id="link-broadcast-addedit" aria-controls="home" role="tab" data-toggle="tab"><?php echo isset($type) ? $type : "ADD"; ?></a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>" id="addedit">
            <div class="col-md-7 col-sm-7 portlet box toe-blue ">
                <div class="portlet-title">
                    <div class="caption">
                      <?php echo isset($type) ? $type : "Add"; ?> Broadcast Message
                    </div>
                </div>
                  <div class="portlet-body form padding15">
                <form action="<?php echo $actionUrl; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return validateBroadCastMessage();">
                    <fieldset>
                        <div class="form-group">
                            <label for="page_id" class="col-lg-4 control-label"> Select User <span class="require">*</span> </label>
                            <div class="col-lg-8">
                                <select name="user_type" id="user_type" class="form-control" onchange="showUserDiv(this.value);">
                                    <option value="">Select User</option>
                                    <option value="0" <?php echo (isset($broadcastMessageObject) && $broadcastMessageObject->users == "")?"selected":""; ?>>All User</option>
                                    <option value="1" <?php echo (isset($broadcastMessageObject) && $broadcastMessageObject->users != "")?"selected":""; ?>>Specific User</option>
                                </select>
                                <div id="user_type_error" class="form_error"></div>
                            </div>
                        </div>
                        <?php
                        $specificUserNames = "";
                        $specificUserIds = "";
                        if(isset($broadcastMessageObject) && $broadcastMessageObject->users != ""){
                        $specificUserIds = $broadcastMessageObject->users;
                            $userObject = User::model()->findAll(array('condition' => 'id IN (' . rtrim($broadcastMessageObject->users, ',') .')'));
                           
                            if(isset($userObject) && !empty($userObject)){
                                foreach($userObject as $user){
                                $specificUserNames .= $user->name.",";
                                }
                            }
                        }
                        ?>
                        <div class="form-group" id="user_div" style="display:none;">
                            <label for="page_id" class="col-lg-4 control-label"> User Name <span class="require">*</span> </label>
                            <div class="col-lg-8">
                                <input type="hidden" name="userIdList" id="userIdList" value="<?php echo isset($specificUserIds)?$specificUserIds:"";?>" />
                                <input type="text" class="form-control" name="user_name"  onblur="getUserData(this.value);" id="user_name" value="<?php echo isset($specificUserNames)?rtrim($specificUserNames,","):"";?>" />
                                <span>(Separate by comma's)</span><br>
                                <span id="user_name_valid"></span>
                                <span id="user_name_invalid"></span>
                                <span style="form_error"  id="user_name_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="page_id" class="col-lg-4 control-label"> Message <span class="require">*</span> </label>
                            <div class="col-lg-8">
                                <textarea name="text" id="summernote_1" class="broadcast_text"><?php echo isset($broadcastMessageObject->text) ? $broadcastMessageObject->text : ""; ?></textarea>
                                <div id="broadcast_text_error" class="form_error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="page_id" class="col-lg-4 control-label">Publish Date <span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" name="publish_date" id="broadcast_publish_date" class="onlydate form-control" value="<?php echo isset($broadcastMessageObject->publish_date) ? date('m/d/Y',strtotime($broadcastMessageObject->publish_date)) : ""; ?>"/>
                                <div id="broadcast_publish_date_error" class="form_error"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="page_id" class="col-lg-4 control-label">Expiry Date <span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" name="expiry_date" id="broadcast_expiry_date" class="onlydate form-control" value="<?php echo isset($broadcastMessageObject->expiry_date) ? date('m/d/Y',strtotime($broadcastMessageObject->expiry_date)) : ""; ?>"/>
                                <div id="broadcast_expiry_date_error" class="form_error"></div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="row">
                        <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                            <input type="submit" name="addedit_submit" id="addedit_submit" value="Submit" class="btn mav-blue-btn">
                            <a href="broadcast" class="btn orange-btn">Cancel</a>
                        </div>
                    </div>
                </form>
                    </div>
            </div>
        </div>
        <div role="tabpanel" class="blue-table tab-pane <?php echo isset($classList) ? $classList : ""; ?>" id="list">
        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
        <div class=" user-status">        
           <div class="col-md-4 col-sm-6 margin-topDefault form-inline">
               <div class="row">
                   <div class="col-md-3 padding0 top7">
                         <label>Bulk Action</label>
                   </div>
                   <div class="col-md-8 padding0">
                        <select class="customeSelect howDidYou form-control input-medium select2me confirmBtn" id="bulkaction" name="bulkaction">
                   <option value="NA">Select</option>
                   <option value="Active" <?php if($bulkaction == "Active"){ echo "selected"; } ?>>Active</option>
                   <option value="Inactive" <?php if($bulkaction == "Inactive"){ echo "selected"; } ?>>Inactive</option>
                   <option value="Remove" <?php if($bulkaction == "Remove"){ echo "selected"; } ?>>Remove</option>
               </select>
                   </div>
               </div>
           </div>
           <div class="col-md-2 col-sm-6  form-inline">
               <input type="submit" class="btn btn-success top7" value="OK" name="bulkaction_submit" id="bulkaction_submit"/>
           </div>   
       </div>
       <div class="clearfix"></div>
        <?php 
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'template' => "{pager}\n{items}\n{summary}\n{pager}", //THIS DOES WHAT YOU WANT    
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;">No.</span>',
                ),
                array(
                   'name' => '',
                   'header' => '<span style="white-space: nowrap;"><input type="checkbox" onclick="selectAll(this)" id="selectall"></span>',
                   'value'=> array($this,'GetBroadcastCheckbox'),
                   ),
                array(
                    'name' => 'text',
                    'header' => '<span style="white-space: nowrap;">Message &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'strip_tags($data->text)',
                ),
                 array(
                    'name' => 'users',
                    'header' => '<span style="white-space: nowrap;">Specified Users &nbsp; &nbsp; &nbsp;</span>',
                    'value'=> array($this,'GetBroadcastSpecifiedUsers'),
                ),
                 array(
                    'name' => 'publish_date',
                    'header' => '<span style="white-space: nowrap;">Publish Date &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->publish_date',
                ),
                array(
                    'name' => 'expiry_date',
                    'header' => '<span style="white-space: nowrap;">Expiry Date &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->expiry_date',
                ),
                array(
                    'name' => 'status',
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                ),
                array(
                    'class' => 'CButtonColumn',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'template' => '{Edit}',
                    'htmlOptions' => array('width' => '10%'),
                    'buttons' => array(
                        'Edit' => array(
                            'label' => '<i class="fa fa-pencil-square-o">',  
                            'options' => array('class' => 'action-icons', 'title' => 'Edit'),
                            'url' => 'Yii::app()->createUrl("admin/user/broadcast", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
        </form>
        </div>
    </div>
    </div>
</div>