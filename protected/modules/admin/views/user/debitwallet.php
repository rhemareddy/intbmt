<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Operation' => '/admin/user/index',
    'Debit Wallet'
);

$mailObject = array();
if (!empty($error)) {
    echo "<p class='alert alert-danger'>" . $error . "</p>";
}
?>
<div class="col-md-8 portlet debitWallet box toe-blue">
    <div class="portlet-title">
                <div class="caption">
                    Debit
                </div>
            </div>
    <div class="portlet-body form padding15 f-left">
<form class="form-horizontal" role="form" id="form_admin_reservation" enctype="multipart/form-data" action="/admin/user/debitwallet?id=<?php echo!empty($_GET['id']) ? $_GET['id'] : ""; ?>&type=<?php echo!empty($_GET['type']) ? $_GET['type'] : ""; ?>" method="post" onsubmit="return validateDebit()">
            
            <input type="hidden" name="cancelled" id="cancelled" value=""/>
            <input type="hidden" name="userId" id="userId" value="<?php echo (!empty($userObject)) ? $userObject->id : ""; ?>"/>
            <div class="col-md-12 form-group">
                <label class="col-md-3 control-label">User Name: </label>
                <div class="col-md-6">
                    <p><?php echo (!empty($userObject)) ? $userObject->name : ""; ?></p>
                    <span style="color:red"  id="first_name_error"></span>
                </div>
            </div>
            <?php $walletList = BaseClass::getWalletList(); ?>
            <div class="col-md-12 form-group">
                <label class="col-md-3 control-label">Wallet Type: </label>
                <div class="col-md-7">
                    <select name="towallettype" id="wallet_type" class="form-control dvalid"  onchange="getExistingFund('<?php echo $userObject->id; ?>', this.value);">
                        <option selected="selected" value="0"> Select </option>
                        <?php foreach ($walletList as $key => $value) { ?>
                            <option value="<?php echo $value->id; ?>" <?php echo (!empty($_GET['type']) && BaseClass::mgDecrypt($_GET['type']) == $value->id) ? "selected" : "" ?> ><?php echo $value->name; ?></option>
                        <?php } ?>
                    </select>
                    <span style="color:red"  id="select_wallet_error"></span>
                </div>
            </div>
            <div class="col-md-12 form-group" id="wallet_amount" >
                <label class="col-md-3 control-label">Existing Fund</label>
                <div class="col-md-7">
                    <span style="color:red" id="existing_fund" ><?php echo (!empty($walletObj) && ($walletObj->type == BaseClass::mgDecrypt($_GET['type']))) ? $walletObj->fund : '' ?></span>
                </div>
            </div>
            <div class="col-md-12 form-group">
                <label class="col-md-3 control-label">Deduct Fund *</label>
                <div class="col-md-7">
                    <input type="text" class="form-control dvalid" maxlength="5" onkeypress="javascript:return isNumberDecimal(event)" name="paid_amount" id="fund" size="60" maxlength="75" />
                    <span style="color:red"  id="fund_error"></span>
                </div>
            </div>

            <div class="col-md-12 form-group">
                <label class="col-md-3 control-label">Comment</label>
                <div class="col-md-7">
                    <textarea class="form-control dvalid" name="comment" id="fund" rows="10" cols="50"></textarea>
                </div>
            </div>

            <div class="col-md-12 form-group">
                <label class="col-md-3 control-label"></label>
                <div class="col-md-7">
                    <input type="submit" class="btn mav-blue-btn" name="submit" id="submit" size="60" maxlength="75" class="textBox" value="Submit" />
                    <input type="button" class="btn mav-blue-btn pull-right" name="cancel" onclick="location.href='/admin/user/wallet';" value="Cancel" />
                    <div id="loading2" style="display:none;" class="loader">Don't click back button or refresh page...your transaction is in process</div>
                </div>
            </div> 
        <input type="hidden" id="fundval" value="<?php echo (!empty($walletObj) && ($walletObj->type == BaseClass::mgDecrypt($_GET['type']))) ? $walletObj->fund : '' ?>">
    </form>
    </div>
</div>

<script>
function getExistingFund(userId, walletId) {
        $("#select_wallet_error").html("");
        $.ajax({
            type: "post",
            url: "/admin/wallet/getfundbyamount",
            data: {'userId': userId, 'walletId': walletId},
            success: function (amount) {
                $("#existing_fund").html("");
                $("#wallet_amount").show();
                if (amount != 0) {
                    $("#transaction_data_amt").val(amount);
                    $("#existing_fund").html(amount);
                    $("#fundval").val(amount.replace(/[^\d\.\-\ ]/g, ''));
                } else {
                    $("#transaction_data_amt").val(0);
                    $("#fundval").val("0.00");
                    $("#existing_fund").html("<b>0.00</b>");
                }
            }
        });
    }
</script>