<?php 

$this->breadcrumbs = array(
    'Withdrawal' => array(''),
    (isset($_GET['s']) && $_GET['s'] != 0) ? 'Payment' : 'Request' => (isset($_GET['s']) && $_GET['s'] != 0) ?  array('payment') :  array('request'),
    'Edit' ,
);
?>
<div class="col-md-6 col-sm-6">
    
    <div class="portlet box toe-blue ">
        <div class="portlet-title">
            <div class="caption">
                Withdrawal Edit
            </div>
        </div>
        <div class="portlet-body form">
            <form action="" method="post" class="form-horizontal">
                <fieldset>
                    <div class="form-body withdraw-body">
                        <div class="form-group">
                            <label class="col-md-4 control-label padding-top-0" for="wallet">Wallet Type</label>
                            <div class="col-md-7">
                                <?php echo $withdrawalObject->WalletType->name; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label padding-top-0" for="ecurrency">Ecurrency Type</label>
                            <div class="col-md-7">
                                <?php echo $withdrawalObject->gateway()->name; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label padding-top-0" for="wdamt">Amount</label>
                            <div class="col-md-7">
                                <?php echo $withdrawalObject->paid; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label " for="accnumber">Account Number</label>
                            <div class="col-md-6">
                                <input type="text" id="account_number" class="form-control" name="account_number" maxlength="30" OnKeypress="javascript:return isAlphaNumeric(event,this.value);" value="<?php echo !empty($withdrawalObject->account_number) ? $withdrawalObject->account_number : '' ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label padding-top-0" for="comment">Comment : </label>
                            <div class="col-md-6">
                                <textarea rows='4' class='form-control' cols='50'  name='comment'><?php echo $withdrawalObject->comment; ?></textarea>
                                <span id="comment" class="form_error"></span>
                            </div>
                        </div>
                        <div id="offline_account_details" style="display:<?php echo ($withdrawalObject->gateway()->id == 12 || $withdrawalObject->gateway()->id == 13 || $withdrawalObject->gateway()->id == 14) ? 'block' : 'none' ?>">
                              <h4 class="text-center">Beneficiary Details</h4>
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_name">Beneficiary Name : <span class="require">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" id="beneficiary_name" class="form-control" name="beneficiary_name" value="<?php echo !empty($jsonDecodeData->beneficiary_name) ? $jsonDecodeData->beneficiary_name : '' ?>">
                                    <span id="beneficiary_name_error" class="form_error"></span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_account_no">Beneficiary IBAN/Account Number : <span class="require">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" id="beneficiary_account_no" class="form-control" name="beneficiary_account_no" maxlength="25" value="<?php echo !empty($jsonDecodeData->beneficiary_account_no) ? $jsonDecodeData->beneficiary_account_no : '' ?>">
                                    <span id="beneficiary_account_no_error" class="form_error"></span>
                                </div>
                              </div> 
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_country">Beneficiary Branch Country : <span class="require">*</span></label>
                                <div class="col-md-4">
                                    <select id="beneficiary_country" class="form-control" name="beneficiary_country">
                                        <option value="<?php echo !empty($jsonDecodeData->beneficiary_country) ? $jsonDecodeData->beneficiary_country : '' ?>"><?php echo !empty($jsonDecodeData->beneficiary_country) ? $jsonDecodeData->beneficiary_country : '' ?></option>
                                            <?php foreach ($countryObject as $country) { ?>
                                                <option value="<?php echo $country->name; ?>"><?php echo $country->name; ?></option>
                                            <?php } ?>
                                    </select>
                                    <span id="beneficiary_country_error" class="form_error"></span>
                                </div>
                              </div> 
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_city">Beneficiary Branch City : <span class="require">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" id="beneficiary_city" class="form-control" name="beneficiary_city" value="<?php echo !empty($jsonDecodeData->beneficiary_city) ? $jsonDecodeData->beneficiary_city : '' ?>">
                                    <span id="beneficiary_city_error" class="form_error"></span>
                                </div>
                              </div> 
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_bank_name">Beneficiary Bank Name : <span class="require">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" id="beneficiary_bank_name" class="form-control" name="beneficiary_bank_name" value="<?php echo !empty($jsonDecodeData->beneficiary_bank_name) ? $jsonDecodeData->beneficiary_bank_name : '' ?>">
                                    <span id="beneficiary_bank_name_error" class="form_error"></span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_bank_branch">Beneficiary Bank Branch : <span class="require">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" id="beneficiary_bank_branch" class="form-control" name="beneficiary_bank_branch" value="<?php echo !empty($jsonDecodeData->beneficiary_bank_branch) ? $jsonDecodeData->beneficiary_bank_branch : '' ?>">
                                    <span id="beneficiary_bank_branch_error" class="form_error"></span>
                                </div>
                              </div> 
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_bank_no">Beneficiary Bank SWIFT/BIC : <span class="require">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" id="beneficiary_bank_no" class="form-control" name="beneficiary_bank_no" maxlength="25" value="<?php echo !empty($jsonDecodeData->beneficiary_bank_no) ? $jsonDecodeData->beneficiary_bank_no : '' ?>">
                                    <span id="beneficiary_bank_no_error" class="form_error"></span>
                                </div>
                              </div> 
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_routing_code">Routing Code : <span class="require">*</span></label>
                                <div class="col-md-4">
                                    <input type="text" id="beneficiary_routing_code" class="form-control" name="beneficiary_routing_code" maxlength="25" value="<?php echo !empty($jsonDecodeData->beneficiary_routing_code) ? $jsonDecodeData->beneficiary_routing_code : '' ?>">
                                    <span id="beneficiary_routing_code_error" class="form_error"></span>
                                </div>
                              </div> 
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_email">Beneficiary E-mail address : </label>
                                <div class="col-md-4">
                                    <input type="text" id="beneficiary_email" class="form-control" name="beneficiary_email" value="<?php echo !empty($jsonDecodeData->beneficiary_email) ? $jsonDecodeData->beneficiary_email : '' ?>">
                                    <span id="beneficiary_email_error" class="form_error"></span>
                                </div>
                              </div> 
                              <h4 class="text-center">Beneficiary Correspondent Bank Details</h4>
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_cor_bank">Correspondent Bank : </label>
                                <div class="col-md-4">
                                    <input type="text" id="beneficiary_cor_bank" class="form-control" name="beneficiary_cor_bank" value="<?php echo !empty($jsonDecodeData->beneficiary_cor_bank) ? $jsonDecodeData->beneficiary_cor_bank : '' ?>">
                                    <span id="beneficiary_cor_bank_error" class="form_error"></span>
                                </div>
                              </div>
                              <div class="form-group">
                                <label class="col-md-4 control-label" for="beneficiary_cor_swift_adrs">SWIFT Address : </label>
                                <div class="col-md-4">
                                    <input type="text" id="beneficiary_cor_swift_adrs" class="form-control" name="beneficiary_cor_swift_adrs" value="<?php echo !empty($jsonDecodeData->beneficiary_cor_swift_adrs) ? $jsonDecodeData->beneficiary_cor_swift_adrs : '' ?>">
                                    <span id="beneficiary_cor_swift_adrs_error" class="form_error"></span>
                                </div>
                              </div> 
                            </div>
                            
                    </div>
                </fieldset>
                <div class="form-actions right"> 
                    <input type="submit" name="submit" value="Submit Request" class="btn mav-blue-btn">
                    <a class="btn red" href="<?php echo (isset($_GET['s']) && $_GET['s'] != 0) ? 'payment' : 'request'; ?>">Cancel</a>
                </div>
                
            </form>
        </div>
    </div>
</div>