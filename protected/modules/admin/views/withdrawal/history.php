<?php 
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Withdrawal' => '/admin/withdrawal/request',
    'History'
);
if(isset($success) && $success != '' ){
echo "<p class='success-2'><i class='fa fa-check-circle icon-success'></i><span class='span-success-2'><span class='span-success'>".$success."</span></p>";}
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	

        <div class="expiration order-list-div order-list-div-table margin-topDefault padding-bottom-10 filter-toggle confirmMenu">
            <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="">
                <div class="col-md-4 col-sm-6 ">
                    <div class="input-group input-large date-picker input-daterange" style="z-index:9;" >
                        <input type="text" name="from" placeholder="From Date" class="datepicker form-control to_date" value="<?php echo (!empty($_GET['from']) != '') ? $_GET['from'] : ""; ?>">
                        <span class="input-group-addon">
                            to </span>
                        <input type="text" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control from_date" value="<?php echo (!empty($_GET['to'])) ? $_GET['to'] : ""; ?>">
                    </div>
                </div>
                <?php $statusId =  "" ;
                if(isset($_GET['res_filter'])){ $statusId = $_GET['res_filter']; } ?>
                <div class="col-md-3 col-sm-6 form-inline">
                    <select class="customeSelect howDidYou form-control select2me confirmBtn" id="ui-id-5" name="res_filter">
                        <option value="" selected="selected">All</option>
                        <option value="0" <?php if($statusId == "0"){ echo "selected"; } ?> >Pending Approval</option>
                        <option value="1" <?php if($statusId == "1"){ echo "selected"; } ?> >Success</option>
                        <option value="2" <?php if($statusId == "2"){ echo "selected"; } ?> >Reject</option>
                        <option value="3" <?php if($statusId == "3"){ echo "selected"; } ?> >Cancelled</option>
                        <option value="4" <?php if($statusId == "4"){ echo "selected"; } ?> >Pending Payment</option>
                    </select>           
                    <input type="submit" class="btn btn-success confirmOk" value="OK" name="submit" id="submit"/>
                </div>
            </form>
        </div>
<div class="blue-table noarrow ">
        <?php 
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'city-grid',
                'dataProvider' => $dataProvider,
                'enableSorting' => 'true',
                'ajaxUpdate' => false,
                'template' => "{pager}\n{items}\n{summary}\n{pager}", //THIS DOES WHAT YOU WANT
                'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
                'pager' => array(
                    'header' => false,
                    'firstPageLabel' => "<<",
                    'prevPageLabel' => "<",
                    'nextPageLabel' => ">",
                    'lastPageLabel' => ">>",
                ),
                'columns' => array(
                    array(
                        'class' => 'IndexColumn',
                        'header' => '<span>No.</span>',
                    ),
                    array(
                        'name' => 'username',
                        'header' => '<span>User Name</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->user()->name ? $data->user()->name : ""',
                    ),
                    array(
                        'name' => 'wallet_type',
                        'header' => '<span>Wallet Type</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->WalletType()->name',
                    ),
                    array(
                        'name' => 'amount',
                        'header' => '<span>Requested Ecurrency</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->gateway()->name',
                    ),
                    array(
                        'name' => 'account_number',
                        'header' => '<span >Account Number</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->account_number',
                    ),
                    array(
                        'name' => 'paid',
                        'header' => '<span>Total Paid</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->paid',
                    ),
                    array(
                        'name' => 'received_amount',
                        'header' => '<span>Received Amount</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => ' number_format($data->received_amount ,2);',
                    ),
                    array(
                        'name' => 'admin_charge',
                        'header' => '<span>Admin Charge</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => ' number_format($data->admin_charge ,2)',
                    ),
                    array(
                        'name' => 'bank_wire_details',
                        'header' => '<span>Beneficiary Details</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value'=>array($this,'GetBankWireDetails'),
                    ),
                    array(
                        'name' => 'withdrawstatus',
                        'header' => '<span >Status</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value'=>array($this,'GetWithdrawalStatus'),
                    ),
                    array(
                        'name' => 'created_at',
                        'header' => '<span>Requested Date</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->created_at',
                    ),
                    array(
                        'name' => 'comment',
                        'header' => '<span style="white-space: nowrap;">Comment</span>',
                        'value' => '$data->comment',
                    ),
                ),
            ));
            ?>
</div>
        </form>
  <script>
        function confirmCancel() {
            var answer=confirm("Are you sure you want to cancel this request ?");
            if (answer==true){
                return true;
            }else{
                return false;
            }
        }
    function selectAll(source) {
        checkboxes = document.getElementsByClassName('allcheckbox');
        for(var i in checkboxes)
            checkboxes[i].checked = source.checked;
    }
</script>