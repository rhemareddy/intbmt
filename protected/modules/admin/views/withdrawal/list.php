<?php 
/* @var $this UserController */
/* @var $model User */
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Withdrawal' => '/admin/withdrawal/request',
    $label,
);
$curAction = @Yii::app()->getController()->getAction()->controller->action->id;
$curActionLower = strtolower($curAction);
if (Yii::app()->user->hasFlash('requestMsg')): 
    echo "<p class='alert alert-success'>".Yii::app()->user->getFlash('requestMsg').'</p>';
endif; 
?>

<div class="row">
    <form name=myform action="" method=post>
    <div class="col-md-12 scroll-row">
        <div class="expiration margin-topDefault confirmMenu row">
            <div class="col-md-2">
                <div id="search_length" class="dataTables_length">
                    <?php if($curActionLower == 'payment'){                         
                        echo '<input type="submit" name="success" value="Paid" class="btn btn-success confirmOk"> ';
                    }else{
                        echo '<input type="submit" name="proceed" value="Approve for payment" class="btn btn-success confirmOk"> ';
                    } ?>
                    
                </div>

            </div> 
            <div class="col-md-2 col-sm-6">
                <input type="submit" name="reject" value="Reject" class="btn btn-success confirmOk">        
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="scroll-row blue-table noarrow">
        <?php 
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'city-grid',
                'dataProvider' => $dataProvider,
//                'enableSorting' => 'true',
                'ajaxUpdate' => false,
                'template' => "{pager}\n{items}\n{summary}\n{pager}", 
                'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
                'pager' => array(
                    'header' => false,
                    'firstPageLabel' => "<<",
                    'prevPageLabel' => "<",
                    'nextPageLabel' => ">",
                    'lastPageLabel' => ">>",
                ),
                'columns' => array(
                    array(
                        'name' => '',
                        'header' => '<span><input type="checkbox" onclick="selectAll(this)" id="selectall"></span>',
                        'value'=>array($this,'GetWithdrawalCheckbox'),
                    ),
                    array(
                        'class' => 'IndexColumn',
                        'header' => '<span>No.</span>',
                    ),
                    array(
                        'name' => 'user_id',
                        'header' => '<span>User</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->user()->name ? $data->user()->name : ""',
                    ),
                    array(
                        'name' => 'country',
                        'header' => '<span style="white-space: nowrap;">Country</span>',
                        'value' => array($this,'userCountry'),
                    ),
                    array(
                        'name' => 'is_restricted',
                        'header' => '<span style="white-space: nowrap;">Is Restricted</span>',
                        'value' => array($this,'checkRestricted'),
                    ),
                    array(
                        'name' => 'amount_type',
                        'header' => '<span>Wallet</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->WalletType()->name',
                    ),
                    array(
                        'name' => 'amount',
                        'header' => '<span>Ecurrency</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->gateway()->name',
                    ),
                    array(
                        'name' => 'account_number',
                        'header' => '<span >User Account</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->account_number',
                    ),
                    array(
                        'name' => 'received_amount',
                        'header' => '<span>To Pay</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->received_amount',
                    ),
                    array(
                        'name' => 'admin_charge',
                        'header' => '<span>Admin Charges</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->admin_charge',
                    ),
                    array(
                        'name' => 'bank_wire_details',
                        'header' => '<span>Beneficiary Details</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value'=>array($this,'GetBankWireDetails'),
                    ),
                    array(
                        'name' => 'status',
                        'header' => '<span>Status</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value'=>array($this,'GetWithdrawalStatus'),
                    ),
                    array(
                        'name' => 'amount',
                        'header' => '<span>Date</span><i class="fa fa-sort  withdrow-i"></i>',
                        'value' => '$data->created_at',
                    ),
                    array(
                        'name' => 'status',
                        'header' => '<span>Comment</span>',
                        'value'=>array($this,'GetWithdrawalComment'),
                    ),
                    array(
                        'class' => 'CButtonColumn',
                        'header' => '<span>Action</span><i class="fa fa-sort  withdrow-i"></i>',
                        'template' => '{Edit}{Mail}',
                        'htmlOptions' => array('width' => '10%'),
                        'buttons' => array(
                             'Edit' => array(
                                'label' => '<i class="fa fa-edit"></i>',
                                'options' => array('class' => 'action-icons', 'title' => 'Edit'),
                                'url' => 'Yii::app()->createUrl("admin/withdrawal/edit", array("id"=>BaseClass::mgEncrypt($data->id),"s"=>$data->status ))',
                                 
                            ),
                            'Mail' => array(
                                'label' => '<i class="fa fa-envelope"></i>', 
                                'url' => 'BaseClass::mgEncrypt($data->id)',
                                'options' => array('class' => 'action-icons', 'title' => 'Send Mail', 'onclick' => 'js:sendMailPopUp($(this).attr("href")); return false;'),
                            ),
                        ),
                    ),
                ),
            ));
            ?>
    </div>
    </div>
        </form>
</div>
<!-- For fancy box -->
<script type="text/javascript" src="/js/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="/js/fancybox/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="/js/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<script type="text/javascript" src="/js/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

<script>
function confirmCancel() {
    var answer=confirm("Are you sure you want to cancel this request ?");
    if (answer==true){
        return true;
    }else{
        return false;
    }
}

function sendMailPopUp(id) {
    $.fancybox({
        width: "60%",
        href: "sendmail?id=" + id,
        type: 'ajax'
    });
}
</script>