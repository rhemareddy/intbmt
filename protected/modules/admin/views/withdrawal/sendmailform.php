<div style="width: 570px">
    <div class="portlet box orange   ">
        <div class="portlet-title">
            <div class="caption">
                Send Mail
            </div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" id="form_admin_reservation" enctype="multipart/form-data" action="/admin/withdrawal/sendmail" method="post" onsubmit="return validateEmailForm()">
                <input type="hidden" value="<?php echo (!empty($_GET['id'])) ? $_GET['id'] : ""; ?>" name="id">
                <input type="hidden" name="status" id="status" value="<?php echo !empty($withdrawalObject->status) ? $withdrawalObject->status : ''; ?>" />
                <div class="mainCompose mainInner">
                    <div class="col-md-12 form-group">
                        <label class="col-md-4">Department </label>
                        <div class="col-md-6">
                            <span>Billing</span>
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-4">To </label>
                        <div class="col-md-6">
                            <span><?php echo !empty($withdrawalObject->user->name) ? $withdrawalObject->user->name : 'NA'; ?></span>
                            <input type="hidden" class="form-control" name="user_id" id="user_id" size="60" maxlength="75" class="textBox" value="<?php echo !empty($withdrawalObject->user_id) ? $withdrawalObject->user_id : ''; ?>" />
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-4">Subject </label>
                        <div class="col-md-6">
                            <span><?php echo 'Withdrawal of transaction id : ' . $withdrawalObject->transaction->transaction_id; ?></span>
                             <input type="hidden" name="email_subject" id="email_subject" value="<?php echo 'Withdrawal of transaction id : ' . $withdrawalObject->transaction->transaction_id; ?>" />
                        </div>
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="col-md-4">Message *</label>
                        <div class="col-md-8">
                            <textarea class="form-control" name="email_body" id="email_body" rows="10" cols="50" maxlength="1000"></textarea>
                            <div>Maximum Size 1000 Letters</div>
                            <span class="color-red" id="email_body_error"></span>
                        </div>
                    </div>
                    
                    <div class="col-md-12 form-group">
                        <label class="col-md-4"></label>
                        <div class="col-md-6">
                             <div class="padding-bottom-20 history-button">                     
                                    <button type="submit" value="Update" class="btn btn-success confirmOk">SUBMIT</button>
                                </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function validateEmailForm(){
        $("#email_body_error").html("");
        if ($("#email_body").val() == "" || $.trim($("#email_body").val()) == "") {
            $("#email_body_error").html("Please Enter Message..!!");
            $("#email_body").focus();
            return false;
        }
    }
</script>