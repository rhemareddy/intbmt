<?php 
$this->breadcrumbs = array(
    'CMS',
    'Home Banner',
);
if (isset($_GET['id'])) {
    $classAddEdit = "active"; //Add tab making as EDit
    $type = "EDIT";
    $actionUrl = "/admin/cms/homebanner?id=" . $_GET['id'];
} else {
    $classList = "active"; 
    $actionUrl = "/admin/cms/homebanner";
}
$bulkaction = "";
$statusId = "";
if (isset($_POST['bulkaction'])) { $bulkaction = $_POST['bulkaction']; }
if (isset($_GET['res_filter'])) { $statusId = $_GET['res_filter']; }

if (Yii::app()->user->hasFlash('error')){ 
    echo '<p class="error-2" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">'.Yii::app()->user->getFlash('error').'</span></p>';
} 
if (Yii::app()->user->hasFlash('success')){ 
    echo '<p class="success-2" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">'.Yii::app()->user->getFlash('success').'</span></p>';
} 
?>
    <div class="row">
        <div class="expiration margin-topDefault confirmMenu">
    <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/cms/homebanner">
         <div class="col-md-2 margin-topDefault form-inline no-pad">
            <select class=" howDidYou form-control input-medium select2me confirmBtn select-custom" id="ui-id-5" name="res_filter">
                <option value="">Select</option>
                <option value="0" <?php if($statusId == "0"){ echo "selected"; } ?> >Inactive</option>
                <option value="1" <?php if($statusId == "1"){ echo "selected"; } ?> >Active</option>                        
            </select>
        </div>
        <div class="col-md-5 col-sm-6 margin-topDefault form-inline">      
            <input type="submit" class="btn btn-success confirmOk" value="OK" name="submit" id="submit"/>
        </div>
    </form>
</div>
</div>
<div class="row">
    <div class="col-md-12 margin-top-20">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="<?php echo isset($classList) ? $classList : ""; ?>"><a href="#list" id="link-banner-list" aria-controls="profile" role="tab" data-toggle="tab">LIST</a></li>
            <li role="presentation" class="<?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>"><a href="#addedit" id="link-banner-addedit" aria-controls="home" role="tab" data-toggle="tab"><?php echo isset($type) ? $type : "ADD"; ?></a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>" id="addedit">
            <div class="col-md-7 col-sm-7">
                <form action="<?php echo $actionUrl; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return validateHomeBanner();">
                    <fieldset>
                        <legend> <?php echo isset($type) ? $type : "ADD"; ?> Banner </legend>
                        <div class="form-group" >
                            <label for="page_id" class="col-lg-4 control-label">Name<span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input type="text" name="banner_name" id="banner_name" class="form-control" value="<?php echo isset($homeBannerObject->banner_name) ? $homeBannerObject->banner_name : ""; ?>"/>
                                <div id="banner_name_error" class="form_error"></div>
                            </div>
                        </div>
                         <?php
                            if (isset($_GET['id'])) {
                                ?>
                                <div class="form-group" >
                                    <label for="page_id" class="col-lg-4 control-label">Banner Image</label>
                                    <div class="col-lg-8">
                                        <img src="<?php echo isset($homeBannerObject->banner_img) ? Yii::app()->params['cms'] . $homeBannerObject->banner_img : ""; ?>" style="max-height: 200px;max-width: 200px;" />                   
                                    </div>
                                </div> 
                                <?php
                            }
                            ?>
                        <div class="form-group" >
                            <label for="page_id" class="col-lg-4 control-label"><?php echo isset($_GET['id']) ? "Change Banner Image" : "Banner Image"; ?><span class="require">*</span></label>
                            <div class="col-lg-8">
                                <input type="file" name="banner_img" id="banner_img" class="form-control" />
                                Min image size should be 1366*567
                                <div id="banner_img_error" class="form_error"></div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="row">
                        <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                            <input type="submit" name="addedit_submit" id="addedit_submit" value="Submit" class="btn red">
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?php echo isset($classList) ? $classList : ""; ?>" id="list">
        <form action="<?php echo $actionUrl; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
        <div class="user-status">        
           <div class="col-md-1  bulk-label col-sm-6 margin-topDefault form-inline"><label> Bulk Action</label></div>
           <div class="col-md-3 col-sm-6 margin-topDefault form-inline">
               <select class="customeSelect howDidYou form-control input-medium select2me confirmBtn" id="bulkaction" name="bulkaction">
                   <option value="NA">Select</option>
                   <option value="Active" <?php if($bulkaction == "Active"){ echo "selected"; } ?>>Active</option>
                   <option value="Inactive" <?php if($bulkaction == "Inactive"){ echo "selected"; } ?>>Inactive</option>
                   <option value="Remove" <?php if($bulkaction == "Remove"){ echo "selected"; } ?>>Remove</option>
               </select>
           </div>
           <div class="col-md-2 col-sm-6 margin-topDefault form-inline">
               <input type="submit" class="btn btn-success" value="OK" name="bulkaction_submit" id="bulkaction_submit"/>
           </div>        
       </div>
       <div class="clearfix"></div>
        <?php 
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'template' => "{pager}\n{items}\n{summary}\n{pager}",
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;">No.</span>',
                ),
                array(
                   'name' => '',
                   'header' => '<span style="white-space: nowrap;"><input type="checkbox" onclick="selectAll(this)" id="selectall"></span>',
                   'value'=> array($this,'GetHomeBannerCheckbox'),
                   ),
                array(
                    'name' => 'banner_name',
                    'header' => '<span style="white-space: nowrap;">Banner Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->banner_name',
                ),
                array(
                    'name' => 'banner_img',
                    'header' => '<span style="white-space: nowrap;">Banner Image &nbsp; &nbsp; &nbsp;</span>',
                    'value' => function($data) {
                        $image = '<img src="'.Yii::app()->params['cms'].$data->banner_img.'" style="width: 80px;height: 50px;">';
                        echo $image;
                    },
                ),
                array(
                    'name' => 'status',
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                ),
                array(
                        'name' => '',
                        'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                        'value'=>array($this,'GetHomeBannerAction'),
                    ),
            ),
        ));
        ?>
        </form>
        </div>
    </div>
    </div>
</div>
<script type="text/javascript">
            CKEDITOR.replace('tbl_heading_text', {
                removeButtons: 'Source,Copy,Save,Preview,Print,Templates,NewPage,Find,Replace,SelectAll,Cut,Paste,Undo,Redo,Smiley,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,Table,Iframe,Link,Unlink,CreateDiv,PasteFromWord,PasteText,RemoveFormat,BidiLtr,BidiRtl,HorizontalRule,SpecialChar,PageBreak,Maximize,ShowBlocks,About,Scayt,language',
                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                height: '150px', //setting ckeditor height
            });
            CKFinder.setupCKEditor(editor, '../');
</script> 
<script type="text/javascript">
            CKEDITOR.replace('tbl_footer_text', {
                removeButtons: 'Source,Copy,Save,Preview,Print,Templates,NewPage,Find,Replace,SelectAll,Cut,Paste,Undo,Redo,Smiley,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,Table,Iframe,Link,Unlink,CreateDiv,PasteFromWord,PasteText,RemoveFormat,BidiLtr,BidiRtl,HorizontalRule,SpecialChar,PageBreak,Maximize,ShowBlocks,About,Scayt,language',
                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                height: '150px', //setting ckeditor height
            });
            CKFinder.setupCKEditor(editor, '../');
</script> 
<script type="text/javascript">
            CKEDITOR.replace('button_text', {
                removeButtons: 'Source,Copy,Save,Preview,Print,Templates,NewPage,Find,Replace,SelectAll,Cut,Paste,Undo,Redo,Smiley,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Flash,Table,Iframe,Link,Unlink,CreateDiv,PasteFromWord,PasteText,RemoveFormat,BidiLtr,BidiRtl,HorizontalRule,SpecialChar,PageBreak,Maximize,ShowBlocks,About,Scayt,language',
                filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl: '/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl: '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
                height: '100px', //setting ckeditor height
            });
            CKFinder.setupCKEditor(editor, '../');
</script> 