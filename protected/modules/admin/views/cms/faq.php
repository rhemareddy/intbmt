<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'CMS' => '/admin/cms/news',
    'FAQ'
);
Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');
?>
<link href="/css/buttons.dataTables.min.css" rel="stylesheet">
<script src="/js/datatables.min.js"></script>
<div class="padding0 faq-active">
    <?php if (Yii::app()->user->hasFlash('success')) { 
        echo '<p class="alert alert-success" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">'.Yii::app()->user->getFlash('success').'</span></p>';
    }
    if (Yii::app()->user->hasFlash('error')) { 
        echo '<p class="alert alert-danger" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">'.Yii::app()->user->getFlash('error').'</span></p>';
    } ?>
    <div class="padding0 col-md-1 col-sm-3 col-xs-3 marginbottom20 ">
        <select class="select-bor howDidYou form-control" id="statusSearch" name="res_filter">
            <option value="Active" >Active</option>  
            <option value="Inactive">Inactive</option>
        </select>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3 margintop3">
        <a class="btn mav-btn" href="/admin/cms/addfaq">Add FAQ</a> 
    </div>
</div>
<table id="faq" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
    <thead>
        <tr>
<!--            <th>No</th>-->
            <th>Sl No</th>
			<th>Topic</th>
            <th>Question</th>
            <th>Answer</th>
            <th>Status</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
<script src="/js/dataTables.buttons.min.js"></script>
<script src="/js/jszip.min.js"></script>
<script src="/js/buttons.html5.min.js"></script>
<script src="/js/buttons.print.min.js"></script>
<script>
    $(document).ready(function () {
		var i=1;
        oTable = $('#faq').DataTable({
            "responsive": true,
            "iDisplayLength": 50,
            "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
            "processing": true,
            "serverSide": true,
            "paging": true,
            "bSort": true,
            "pagingType": "full_numbers",
            "order": [[5, 'desc']],
            "sDom": 'B<"top"flp>rt<"bottom"p>i<"clear">',
            "buttons": [
                'copy', 'csv', 'excel', 'print'
            ],
            "ajax": {
                "url": "faqlist",
                "type": "POST"
            },
            "columns": [
//                {
//                   "fnRowCallback" : function(nRow, aData, iDisplayIndex){      
//                          var oSettings = oTable.fnSettings();
//                           $("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
//                           return nRow;
//                }
//                },
				{
    "data": "id",
    render: function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
    }
},
                {"data": "name"},
                {"data": "title"},
                {"data": "answer"},
                {
                    "data": "status",
                    "searchable": true,
                    render: function (data, type, row) {
                        if (data == 1) {
                            return "Active";
                        } else {
                            return "Inactive";
                        }
                    }
                },
                {"data": "created_at"},
                {
                    "data": "id",
                    "searchable": false,
                    "orderable": false,
                    render: function (data, type, row) {
                        return '<span id="payoutmanual' + data + '"></span><a title="Edit" onclick="editResult(' + data + ')" class="fa fa-success btn default green delete" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a title="changestatus" class="fa fa-success btn default black delete" href="/admin/cms/changestatus?id='+data+'"><i class="fa fa-retweet"></i></a>';
                    },
                    className: "dt-body-center"
                }
            ],
            language: {
                searchPlaceholder: "By Topic or Question"
            }
        });
    });

    $("#statusSearch").change(function () {
        oTable.columns(3).search($("#statusSearch").val().trim());
        oTable.draw();
    });

    function editResult(data)
    {
        var succeed = false;
        $.ajax({
            type: "POST",
            url: "/admin/cms/getencrypid",
            async: false,
            data: {id: data},
            success: function (msg) {
                window.location.href = "/admin/cms/addfaq?eid=" + msg;
            }
        });
        return succeed;
    }

</script>