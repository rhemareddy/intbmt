<?php
$this->breadcrumbs = array(
    'CMS',
    'Pop Up',
);
if (isset($_GET['id'])) {
  $classAddEdit = "active";
  $type = "EDIT";
  $actionUrl = "/admin/cms/popup?id=" . $_GET['id'];
} else {
  $classList = "active";
  $actionUrl = "/admin/cms/popup";
}
$statusId = "";
if (isset($_GET['res_filter'])) {
  $statusId = $_GET['res_filter'];
}
if (Yii::app()->user->hasFlash('error')) {
  echo '<p class="error-2"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">' . Yii::app()->user->getFlash('error') . '</span></p>';
}
if (Yii::app()->user->hasFlash('success')) {
  echo "<p class='alert alert-success'>" . Yii::app()->user->getFlash('success') . '</span></p>';
}
?>
<div class="row">
  <div class="expiration margin-topDefault confirmMenu popUp">
    <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="/admin/cms/popup">
      <div class="col-md-2 form-inline no-pad">
        <select class="howDidYou form-control input-medium select2me confirmBtn select-custom" id="ui-id-5" name="res_filter">
          <option value="">Select</option>
                    <option value="0" <?php if ($statusId == "0") { echo "selected";} ?> >Inactive</option>
                    <option value="1" <?php if ($statusId == "1") { echo "selected";} ?> >Active</option>
        </select>
      </div>
      <div class="col-md-5 margin-left-15 form-inline">      
        <input type="submit" class="btn btn-success confirmOk" value="OK" name="submit" id="submit"/>
      </div>
    </form>
  </div>
</div>
<div class="row">
  <div class="col-md-12 margin-top-20">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="<?php echo isset($classList) ? $classList : ""; ?>"><a href="#list" id="link-banner-list" aria-controls="profile" role="tab" data-toggle="tab" onclick="list()">LIST</a></li>
      <li role="presentation" class="<?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>"><a href="#addedit" id="link-banner-addedit" aria-controls="home" role="tab" data-toggle="tab"><?php echo isset($type) ? $type : "ADD"; ?></a></li>
    </ul>
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane <?php echo isset($classAddEdit) ? $classAddEdit : ""; ?>" id="addedit">
        <div class="col-md-6">
          <form action="<?php echo $actionUrl; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="return validatePopup();">
            <fieldset>
              <legend> <?php echo isset($type) ? $type : "ADD"; ?> Pop Up </legend>
              <div class="form-group" >
                <label for="page_id" class="col-md-4 control-label">Name<span class="require">*</span></label>
                <div class="col-md-8">
                  <input type="text" name="banner_name" id="banner_name" class="form-control" value="<?php echo isset($popUpObject->name) ? $popUpObject->name : ""; ?>" maxlength="25"/>
                  <div id="banner_name_error" class="form_error"></div>
                </div>
              </div>
<?php if (isset($_GET['id'])) { ?>
                <div class="form-group" >
                  <label for="page_id" class="col-md-4 control-label">Desktop Image</label>
                  <div class="col-md-8">
                    <img src="<?php echo isset($popUpObject->desktop_image) ? Yii::app()->params['cms'] . $popUpObject->desktop_image : ""; ?>" style="max-height: 200px;max-width: 200px;" />                   
                  </div>                                    
                </div> 
<?php } ?>
              <div class="form-group" >
                <label for="page_id" class="col-md-4 control-label"><?php echo isset($_GET['id']) ? "Change Desktop Image" : "Desktop Image"; ?><span class="require">*</span></label>
                <div class="col-md-8">
                  <input type="file" name="banner_img" id="banner_img" class="form-control" />
                  <div id="banner_img_error" class="form_error"></div>
                  <span class="img-info">(Image size should be 600x394 )</span>
                </div>
              </div>
<?php if (isset($_GET['id'])) { ?>
                <div class="form-group" >
                  <label for="page_id" class="col-md-4 control-label">Mobile Image</label>
                  <div class="col-md-8">
                    <img src="<?php echo isset($popUpObject->mobile_image) ? Yii::app()->params['cms'] . $popUpObject->mobile_image : ""; ?>" style="max-height: 200px;max-width: 200px;" />                   
                  </div>
                </div> 
<?php } ?>
              <div class="form-group" >
                <label for="page_id" class="col-md-4 control-label"><?php echo isset($_GET['id']) ? "Change Mobile Image" : "Mobile Image"; ?><span class="require">*</span></label>
                <div class="col-md-8">
                  <input type="file" name="mobile_img" id="mobile_img" class="form-control" />
                  <div id="mobile_img_error" class="form_error"></div>
                  <span class="img-info">(Image size should be 200x100 )</span>
                </div>
              </div>
              <div class="form-group" >
                <label for="page_id" class="col-md-4 control-label">Status <span class="require">*</span></label>
                <div class="col-md-8">
                  <select class="form-control input-medium" id="ui-id-5" name="status">
                    <option value="1" <?php echo (!empty($popUpObject) && $popUpObject->status == 1) ? "selected" : ""; ?>>Active</option>                        
                    <option value="0" <?php echo (!empty($popUpObject) && $popUpObject->status == 0) ? "selected" : ""; ?>>Inactive</option>
                  </select>
                </div>
              </div>
            </fieldset>
            <div class="row">
              <div class="col-md-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="addedit_submit" id="addedit_submit" value="Submit" class="btn red">
              </div>
            </div>
          </form>
        </div>
      </div>
      <div role="tabpanel" class="tab-pane <?php echo isset($classList) ? $classList : ""; ?>" id="list">
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'template' => "{pager}\n{items}\n{summary}\n{pager}",
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            'pager' => array(
              'header' => false,
              'firstPageLabel' => "<<",
              'prevPageLabel' => "<",
              'nextPageLabel' => ">",
              'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                  'class' => 'IndexColumn',
                  'header' => '<span style="white-space: nowrap;">No.</span>',
                ),
                array(
                  'name' => 'name',
                  'header' => '<span style="white-space: nowrap;">Popup Name &nbsp; &nbsp; &nbsp;</span>',
                  'value' => '$data->name',
                ),
                array(
                  'name' => 'big_image',
                  'header' => '<span style="white-space: nowrap;">Desktop Image &nbsp; &nbsp; &nbsp;</span>',
                  'value' => function($data) {
                    $image = '<a target="_bank" href="' . Yii::app()->params['cms'] . $data->desktop_image . '" title="Preview"><img src="' . Yii::app()->params['cms'] . $data->desktop_image . '" style="width: 80px;height: 50px;"></a>';
                    echo $image;
                  },
                ),
                array(
                  'name' => 'small_image',
                  'header' => '<span style="white-space: nowrap;">Mobile Image &nbsp; &nbsp; &nbsp;</span>',
                  'value' => function($data) {
                    $image = '<a target="_bank" href="' . Yii::app()->params['cms'] . $data->mobile_image . '" title="Preview"><img src="' . Yii::app()->params['cms'] . $data->mobile_image . '" style="width: 80px;height: 50px;"></a>';
                    echo $image;
                  },
                ),
                array(
                  'name' => 'popstatus',
                  'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                ),
                array(
                  'name' => '',
                  'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                  'value' => array($this, 'GetPopUpAction'),
                ),
            ),
        ));
        ?>
      </div>
    </div>
  </div>
</div>
<script>
    function list(){
        window.location = "/admin/cms/popup";
    }
</script>

