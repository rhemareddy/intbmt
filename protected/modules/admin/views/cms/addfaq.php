<?php 
$this->breadcrumbs = array(
    'Admin' => array('/admin/default/dashboard'),
    'CMS' => array('/admin/cms/faq'),
    'FAQ'
);
$actionUrl = "/admin/cms/addfaq";
$topicUrl = "/admin/cms/addfaqtopic";
$type = "";
if (isset($_GET['eid']) && !empty($_GET['eid'])) {
	$editId = $_GET['eid'];
	$actionUrl = "/admin/cms/addfaq?eid=$editId";
	$type = "EDIT";
}
?>
<div class="col-md-6 col-sm-6" id="faq">
    <?php if (Yii::app()->user->hasFlash('success')){ ?>
    <p class="alert alert-success" id="error_msg_2"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </span></p>
<?php } ?>
<?php if (Yii::app()->user->hasFlash('error')){ ?>
    <p class="alert alert-danger" id="error_msg_1"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">
        <?php echo Yii::app()->user->getFlash('error'); ?>
    </span></p>
<?php } ?>
    <form enctype="multipart/form-data" action="<?php echo $actionUrl; ?>" method="post" onsubmit="return faqValidation();">
        <div class="portlet box toe-blue">
            <div class="portlet-title">
                <div class="caption">
                    <h4><?php echo isset($type) ? $type : "Add"; ?> FAQ</h4>
                </div>
            </div>
            <div class="portlet-body form padding15">
                <div class="row form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Topic *</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="topic_id" id="topic_id">
                            <option value="">Select Topic</option>
                            <?php 
                            if(isset($topicObject) && !empty($topicObject)){
                                foreach ($topicObject as $topicList){
                            ?>
                            <option value="<?php echo $topicList->id; ?>"
                                    <?php 
                                    if (isset($faqObject->topic_id)){
                                        if($faqObject->topic_id == $topicList->id){
                                            echo 'selected';
                                        }
                                    }
                                    ?>
                                    ><?php echo isset($topicList->name)?$topicList->name:""; ?></option> 
                            <?php } } ?>
                        </select>
                        <span id="error_faq_topic" style="color:red;"></span>
                    </div>
                    <?php if(isset($_GET['eid'])){ ?>
                    <?php }else{ ?>
                    <div class="col-sm-4">
                        <a onclick="addTopic()">Add New Topic</a>
                    </div>
                    <?php } ?>
                </div>
                <div class="row form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Title *</label>
                    <div class="col-sm-8">
                        <input type="text" value="<?php echo isset($faqObject->title)?$faqObject->title:""; ?>" class="form-control" name="title" id="faqtitle" placeholder="FAQ Question" value="<?php echo isset($newsObject->title) ? $newsObject->title : ""; ?>">
                        <span id="error_faq_title" style="color:red;"></span>
                    </div>
                </div>
                <div class="row form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Answer *</label>
                    <div class="col-sm-8">
                        <textarea name="answer" id="summernote_1" class="news_message"><?php echo isset($faqObject->answer) ? $faqObject->answer : ""; ?></textarea>
                        <span id="error_faq_answer" style="color:red;"></span>
                    </div>
                </div>
                <input type="submit" name="addedit" id="addedit" value="submit" class="btn green">
                <a href="/admin/cms/faq" class="btn orange-btn">Cancel</a>
            </div>
        </div>
    </form>
</div>
<div class="col-md-6 col-sm-6" id="faqtopic" style="display: none;">
    <form enctype="multipart/form-data" action="<?php echo $topicUrl; ?>" method="post" onsubmit="return topicValidation();">
        <div class="portlet box toe-blue">
            <div class="portlet-title">
                <div class="caption">
                    <h4><i class="icon-list"></i><?php echo isset($type) ? $type : "Add"; ?> FAQ Topic</h4>
                </div>
            </div>
            <div class="portlet-body form padding15">
                <div class="row form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label text-right">Topic Name *</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" id="topicname" placeholder="New Topic" value="<?php echo isset($newsObject->title) ? $newsObject->title : ""; ?>">
                        <span id="error_topic_name" style="color:red;"></span>
                    </div>
                </div>
                <input type="submit" name="addedit" id="addedit" value="Submit" class="btn btn-success">
                <a class="btn btn-success" onclick="showFaq();">Cancel</a>
            </div>
        </div>
    </form>
</div>
<script>
   function addTopic(){
       $("#faqtopic").show();
   } 
   
   function showFaq(){
       $("#faqtopic").hide();
       $("#faq").show();
   } 
</script>