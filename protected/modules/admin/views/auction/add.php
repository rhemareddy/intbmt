<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Auction' => '/admin/auction/add',
    'Add',
);
?>
<div class="col-md-7 col-sm-7">
    <?php 
        if ($error) { echo '<div class="error" id="error_msg">'.$error.'</div>'; } 
        if ($success) { echo '<div class="success" id="error_msg">'.$success.'</div>'; }
    ?>
    <div class="portlet box toe-blue auctionAdd   ">
        <div class="portlet-title">
            <div class="caption">
                Add Auction
            </div>
        </div>
        <div class="portlet-body form product-formbody">
            <form action="/admin/auction/add" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validationAuction();">
                <fieldset>
                    <div class="form-body">
                        <div class="form-group">
                            <label for="country" class="col-lg-4 control-label">Product<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <select name="product_id" id="product_id" class="form-control product">
                                    <option value="">Select Product</option>
                                    <?php foreach ($productObject as $product) { ?>
                                        <option value="<?php echo $product->id; ?>"> <?php echo $product->name; ?> </option>";         
                                    <?php } ?>
                                </select>
                                <span style="color:red;" id="product_id_error"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Start Date & Time<span class="require">*</span></label>
                            <div class="col-lg-4">
                                <input id="start" class="datetimepicker form-control" type="text" name="sdate">
                                <span style="color:red;" id="start_error"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Close Date & Time<span class="require">*</span></label>
                            <div class="col-lg-4">
                                <input id="close"  class="datetimepicker form-control"  onchange="isWeekDay(this.value)"  type="text" name="cdate">
                                <span style="color:red;" id="close_error"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Entry fee<span class="require">*</span></label>
                            <div class="col-lg-4">
                                <input type="text" id="entry_fees" class="form-control" onkeypress='return validateQty(this,event);'  name="entry_fees">
                                <span style="color:red;" id="entry_fees_error"></span>
                            </div>
                        </div>

                        <input type="hidden" id="lowest_bid_price" class="form-control" name="lowest_bid_price" value="1">

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Is Premium</label>
                            <div class="col-lg-7">
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="is_premium" value="1" checked="checked">Yes
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="is_premium" value="0" >No
                                </label>
                            </div>
                        </div>
                        <input type="hidden" class="pro-radio" name="is_publish" value="1">

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Is Auto Series</label>
                            <div class="col-lg-7">
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="is_autoserious" value="1" checked="checked">Yes
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="is_autoserious" value="0" >No
                                </label>                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Is AutoBid</label>
                            <div class="col-lg-7">
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="is_autobid" value="1" checked="checked">On
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="is_autobid" value="0" >Off
                                </label>                                
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-actions right">                     
                    <input type="submit" name="submit" value="Submit" class="btn mav-blue-btn">
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    function isWeekDay(date){
        $("body").css('pointer-events', 'auto');
        var dataString = 'today=' + date;
        $.ajax({
            type: "POST",
            url: "/bid/isweekday",
            data: dataString,
            cache: false,
            beforeSend: function () {
                fedIn();
            },
            success: function (respoonseData) {
               fedOut();
                if(respoonseData==""){
                    $("#close_error").html("Auction can not end in Weekend!!!.");
                }
            }
        });
        fedOut();
        $("body").css('pointer-events', 'auto');
    }
</script>
<script type="text/javascript" src="/metronic/assets/plugins/select2/select2.min.js?ver=1471423608"></script> 
<link rel="stylesheet" href="/metronic/assets/plugins/select2/select2.css" type="text/css" media="all">   
<script>
$(document).ready(function(){
    $('.product').select2();
    
});
</script>