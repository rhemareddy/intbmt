<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Auction' => '/admin/auction/add',
    'Declare Winner'
);

Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');

?>
<link href="/css/buttons.dataTables.min.css" rel="stylesheet">

<!--<span class="success" id="bidSuccessMsg" style="display: none">Bid Created Successfully.</span>
<span class="error" id="bidErrorMsg" style="display: none">Failed to create bid.</span>-->

<script src="/js/datatables.min.js"></script>
<!--<script src="/js/dataTables.select.min.js"></script>-->
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
<div class="user-status col-md-12 filter-toggle">
    <form id="profitability_filter_frm" name="profitability_filter_frm" method="post" action="/admin/report/profitability" >
        <div class="col-md-4">    
            <span id="date_error" style="color:red"></span>
            <div class="input-group input-large date-picker input-daterange pull-left">
                <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
                <span class="input-group-addon"> to </span>
                <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
            </div>
        </div>
        <div class="col-md-1 col-sm-6 form-inline">
            <input type="button" class="btn btn-success margintop3 margin-left-15" value="Filter" name="profitability" id="profitability"/>
        </div>
    </form>
</div>
    <div class="row">
        <div class="col-md-12 responsiveTable">
            <?php
            if (Yii::app()->user->hasFlash('error')):
                echo '<div class="alert alert-danger">' . Yii::app()->user->getFlash('error') . '</div>';
            endif;
            if (Yii::app()->user->hasFlash('success')):
                echo '<div class="alert alert-success">' . Yii::app()->user->getFlash('success') . '</div>';
            endif;
            ?>
      
            
            
            <table id="profitabilityreport" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Auction Id</th>
                        <th>Product Name</th>
                        <th>End Date</th>
                        <th>Amount Collected</th>
                        <th>Declare Winner</th>
                        <th>Auction Status</th>
                    </tr>
                </thead>
            </table>

            <script src="/js/dataTables.buttons.min.js"></script>
            <script src="/js/jszip.min.js"></script>
            <script src="/js/buttons.html5.min.js"></script>
            <script src="/js/buttons.print.min.js"></script>
            <input type="hidden" value="0" id="setDeclareWinner" name="setDeclareWinner"/>
            <script>
                $(document).ready(function () {
                    oTable = $('#profitabilityreport').DataTable({
                        
                        "responsive": true,
                        "iDisplayLength": 50,
                        "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        "processing": true,
                        "serverSide": true,
                        "paging": true,
                        "bSort": true,
                        "pagingType": "full_numbers",
                        "order": [[2, "asc"]],
                        "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                        "buttons": [ {
                                extend: 'csv',
                                footer: true,
                                exportOptions: {
                                     columns: [0,1,2,3,5]
                                 }
                            }],
                        
                        "ajax": {
                            "url": "closingauctiondata",
                            "type": "POST"
                        },
                        "columns": [
                            {"data": "auctionId"},
                            {"data": "productName"},
                            {"data": "endDate"},
                            {"data": "amountCollected"},
                            {"data": "winnerName" ,
                                render: function (data, type, row) { 
                                    if (row.winnerName != 'Not_Declared') {
                                        return data;
                                    } else {
                                        return '<div id="update_winner_search' + row.auctionId + '"><a href="#" data-toggle="modal" data-target="#submit_comment' + row.auctionId + '">Declare </a></div><div class="modal fade" id="submit_comment' + row.auctionId + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel' + row.auctionId + '"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span  aria-hidden="true">&times;</span></button><h4 class="modal-title news-h4 order-history-comment" id="myModalLabel' + row.auctionId + '">Search Winner Name</h4><span id ="success_msg_comment' + row.auctionId + '" class="alert-success"> </span><span id ="error_msg_comment' + row.auctionId + '" class="alert-danger"> </span></div><div class="modal-body"><p class="text-justify order-history-comment"><input type="hidden" placeholder="User Name" class="form-control" name="winner_search_auction_id" id="winner_search_auction_id' + row.auctionId + '" value="' + row.auctionId + '"/><input type="text" placeholder="User Name" class="form-control" onchange="getBidUser(' + row.auctionId + ')" name="winner_search" id="winner_search' + row.auctionId + '" ><span class="alert-danger" id ="error_winner_search' + row.auctionId + '" class="error-msg"> </span></p></div><div class="modal-footer"><a href="javascript:void(0);" type="button" class="btn modal-submit green" onclick="letsDeclareWinner(' + row.auctionId + ')";>Submit</a></div></div></div></div>';
                                    }
                                }
                            },
                            {"data": "auctionStatus"},
                        ],
                  });
                });
                
                $("#profitability").click(function() {
                    $("#date_error").html("");
                    if($("#toDate").val()>$("#fromDate").val()){
                        $("#date_error").html("To Date should be greater then From Date!!!");
                        $("#fromDate").focus();
                        return false;
                    }
                oTable.columns(2).search($("#toDate").val().trim(),$("#fromDate").val().trim());
                
                oTable.draw();
                });
                   
                function getBidUser(auctionId){ 
                    $("#setDeclareWinner").val(0);
                    var userName = $("#winner_search"+auctionId).val();
                       var dataString = 'auctionId=' + auctionId +'&userName='+userName;
                        $.ajax({
                            type: "POST",
                            url: "/bid/readbiduser",
                            data: dataString,
                            cache: false,
//
                            success: function (respoonseData) { 
                                $("#error_winner_search"+auctionId).html('');
                                $("#success_msg_comment"+auctionId).html('');
                                if(respoonseData==1){
                                    $("#error_winner_search"+auctionId).html("User is not existed!.");
                                    return false;
                                } else if(respoonseData==2){
                                    $("#success_msg_comment"+auctionId).html("User Existed.")
                                    $("#success_msg_comment"+auctionId).show();
                                    $("#setDeclareWinner").val(1);
                                    return true;
                                } else if(respoonseData==3){
                                    $("#error_winner_search"+auctionId).html("There are no active bids for give user.");
                                    return false;
                                }

                            }
            });
            }
            
            function letsDeclareWinner(acutionId){
            var userName = $("#winner_search"+acutionId).val();    
            if(userName==""){ 
                $("#error_winner_search"+acutionId).html('Enter Winner Name.');
                return false;
            }
            var isValidUser = $("#setDeclareWinner").val();    
                if(isValidUser==1){
                    var dataString = 'auctionId=' + acutionId +'&userName='+userName;
                    $.ajax({
                        type: "POST",
                        url: "/bid/declarewinner",
                        data: dataString,
                        cache: false,
                        success: function (respoonseData) {
                            location.reload();
                        }
                    });
                }
            }
            </script>
        </div>
    </div>