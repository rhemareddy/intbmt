<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Auction' => '/admin/auction/add',
    'Auction Edit',
);
?>
<div class="col-md-7 col-sm-7 portlet box toe-blue top10">
    <?php 
        if ($error) { echo '<div class="error" id="error_msg">'.$error.'</div>'; } 
        if ($success) { echo '<div class="success" id="error_msg">'.$success.'</div>'; }
    ?>
    <div class="portlet-title">
        <div class="caption">
         Auction Edit
        </div>
    </div>
        <div class="portlet-body form padding15 ">
            <form action="/admin/auction/edit?id=<?php echo BaseClass::mgEncrypt($auctionObject->id); ?>" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validationAuction();">
        <fieldset>
            <div class="form-body">
                <div class="form-group">
                    <label for="country" class="col-lg-4 control-label">Package<span class="require">*</span></label>
                    <div class="col-lg-7">
                        <select name="product_id" id="product_id" class="form-control" disabled="disabled">
                            <option value="">Select Package</option>
                            <?php foreach ($productObject as $product) { ?>
                                <option value="<?php echo $product->id; ?>" <?php if (!empty($auctionObject->product_id) && $auctionObject->product_id == $product->id) { ?> selected="selected" <?php } ?>> <?php echo $product->name; ?> </option>";         
                            <?php } ?>
                        </select>
                        <span style="color:red;" id="product_id_error"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Start Date & Time<span class="require">*</span></label>
                    <div class="col-lg-4">
                        <input type="text" id="start" class="datetimepicker form-control" name="sdate" value="<?php echo (!empty($auctionObject)) ? $auctionObject->start_date : ""; ?>">&nbsp;
                        <span style="color:red;" id="start_error"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Close Date & Time<span class="require">*</span></label>
                    <div class="col-lg-4">
                        <input type="text" id="close" class="datetimepicker form-control" name="cdate" value="<?php echo (!empty($auctionObject)) ? $auctionObject->close_date : ""; ?>">&nbsp;
                        <span style="color:red;" id="close_error"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Entry fee<span class="require">*</span></label>
                    <div class="col-lg-4">
                        <input type="text" id="entry_fees" class="form-control" onkeypress='return validateQty(this,event);' name="entry_fees" value="<?php echo (!empty($auctionObject)) ? number_format($auctionObject->entry_fees, 2) : ""; ?>">
                        <span style="color:red;" id="entry_fees_error"></span>
                    </div>
                </div>

                <input type="hidden" id="lowest_bid_price" class="form-control" name="lowest_bid_price" value="1">
                
                <div class="form-group">
                    <label class="col-lg-4 control-label top7" for="lastname">Is Premium</label>
                    
                    <div class="col-lg-7">
                        <label class="checkbox-inline"><input type="radio" class="purchase-radio" name="is_premium" value="1" <?php echo ($auctionObject->is_premium == 1) ? "checked" : ""; ?>>Yes </label>
                        <label class="checkbox-inline"><input type="radio" class="purchase-radio" name="is_premium" value="0" <?php echo ($auctionObject->is_premium == 0) ? "checked" : ""; ?>>No </label>
                    </div>
                </div>
                <input type="hidden" class="pro-radio" name="is_publish" value="1">
                <div class="form-group">
                    <label class="col-lg-4 control-label top7" for="lastname">Is Auto serious</label>
                    
                    <div class="col-lg-7">
                        <label class="checkbox-inline"><input type="radio" class="purchase-radio" name="is_autoserious" value="1" <?php echo ($auctionObject->is_autoserious == 1) ? "checked" : ""; ?>>Yes </label>
                        <label class="checkbox-inline"><input type="radio" class="purchase-radio" name="is_autoserious" value="0" <?php echo ($auctionObject->is_autoserious == 0) ? "checked" : ""; ?>>No </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label top7" for="lastname">Is AutoBid</label>
                    
                    <div class="col-lg-7">
                        <label class="checkbox-inline"><input type="radio" class="purchase-radio" name="is_autobid" value="1" <?php echo ($auctionObject->is_autobid == 1) ? "checked" : ""; ?>>On </label>
                        <label class="checkbox-inline"><input type="radio" class="purchase-radio" name="is_autobid" value="0" <?php echo ($auctionObject->is_autobid == 0) ? "checked" : ""; ?>>Off </label>
                    </div>
                </div>
            </div>
        </fieldset>

        <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Update" class="btn mav-blue-btn">
                <a href="/admin/auction/list" class="btn orange-btn">Cancel</a>
            </div>
        </div>
    </form>
</div>
</div>