<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Auction' => '/admin/auction/add',
    'List'
);
$bidListUrl = "/admin/auction/list";
    $statusId = "";
    if (isset($_REQUEST['res_filter'])) {
        $statusId = $_REQUEST['res_filter'];
    }
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
<div class="col-md-12 user-status filter-toggle">
    <div class="expiration margin-topDefault confirmMenu">
     <form id="regervation_filter_frm" name="regervation_filter_frm" method="get" action="<?php $bidListUrl; ?>" class="form-inline">
        <div class="form-group">   
            <input type="text" class="form-control" name="name" id="name" placeholder="Product Name" value="<?php if($name) echo $name; ?>"/>                                    
        </div>
        <div class="form-group">                 
             <input type="text" name="to" id="to" placeholder="Start Date" class="onlydate form-control" value="<?php if($to) echo $to; ?>">                                   
        </div>
        <div class="form-group">                 
             <input type="text" name="from" id="from" placeholder="End Date" class="onlydate form-control" value="<?php if($from) echo $from; ?>">                                   
        </div>
        <div class="form-group">
            <div class="col-md-3 mobile-padding-0">
        <select class="customeSelect howDidYou form-control " id="ui-id-5" name="res_filter" style="margin-right: 10px;">
            <option value="">Select Status</option>
            <option value="1"  <?php if(isset($status) && $status==1) echo "selected"; else echo ""; ?>>Open</option>
            <option value="0" <?php if(isset($status) && $status==0 && $status!="") echo "selected"; else echo ""; ?>>Close</option>
        </select>
  </div> 
        </div> 
        <div class="form-group">
               <div class="col-md-2 mobile-padding-0">
            <select name="perpage" aria-controls="perpage" class="customeSelect howDidYou form-control ">
                <option value="50"  <?php echo (isset($_GET['perpage']) && ($_GET['perpage'] == 50))?"selected":""; ?> >50</option>
                <option value="100" <?php echo (isset($_GET['perpage']) && ($_GET['perpage'] == 100))?"selected":""; ?>>100</option>
                <option value="150" <?php echo (isset($_GET['perpage']) && ($_GET['perpage'] == 150))?"selected":""; ?>>150</option>
                <option value="200"  <?php echo (isset($_GET['perpage']) && ($_GET['perpage'] == 200))?"selected":""; ?>>200</option>
            </select>
                      </div>
        </div>
        <input type="submit" class="btn btn-primary mobile-left-15" value="OK" name="submit" id="submit"/>
      
        </form>
    </div>
    
</div>
<div class="row">
    <div class="col-md-12 blue-table scroll-row">
        <?php
        if (Yii::app()->user->hasFlash('success')): 
            echo '<div class="alert alert-success">'.Yii::app()->user->getFlash('success') .'</div>';
        endif;
        
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{pager}{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            //'rowCssClassExpression'=>'fa fa-success btn default black delete',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
               array(
                'class' => 'IndexColumn',
                'header' => '<span style="white-space: nowrap;color:#01b7f2">No</span>',
                ),
                array(
                    'name' => 'productname',
                    'header' => '<span style="white-space: nowrap;">Product ID &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->product->id;',
                ),
                array(
                    'name' => 'productname',
                    'header' => '<span style="white-space: nowrap;">Product Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->product->name;',
                ),
                array(
                    'name' => 'start_date',
                    'header' => '<span style="white-space: nowrap;">StartDate&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->start_date',
                ),
                     array(
                    'name' => 'close_date',
                    'header' => '<span style="white-space: nowrap;">CloseDate&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->close_date',
                ),

                 array(
                    'name' => 'entry_fees',
                    'header' => '<span style="white-space: nowrap;">Fees &nbsp; &nbsp; &nbsp;</span>',
                    'value' => 'number_format($data->entry_fees,2)',
                ),
                array(
                    'name' => 'winner_user_id',
                    'header' => '<span style="white-space: nowrap;">Winner &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '(isset($data->user)) ? $data->user()->name : "---"',
                ),
                array(
                    'name' => 'status',
                    'header' => '<span style="white-space: nowrap;">Status &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->status == 1) ? Yii::t(\'status\', \'Open\') : Yii::t(\'status\', \'Close\')',
                ),
                array(
                    'name' => 'is_premium',
                     'header' => '<span style="white-space: nowrap;">Premium &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->is_premium == 1) ? "Yes" : "No"',
                ),
                 array(
                    'name' => 'is_autobid',
                     'header' => '<span style="white-space: nowrap;">Autobid &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->is_autobid == 1) ? "On" : "Off"',
                ),
                array(
                    'name' => 'is_autoserious',
                     'header' => '<span style="white-space: nowrap;">Auto Series &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->is_autoserious == 1) ? "On" : "Off"',
                ),
                  array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Created&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
                array(
                    'class' => 'CButtonColumn',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'template' => '{Change}{Edit}{Delete}',
                    'htmlOptions' => array('width' => '30%'),
                    'buttons' => array(
                        'Change' => array(
                             'label' => '<i class="fa fa-retweet"></i>',
                            'options' => array('class' => 'btn btn-xs','title' => 'Change Status'),
                            'url' => 'Yii::app()->createUrl("admin/auction/changestatus", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ),
                        'Edit' => array(
                                'label' => '<i class="fa fa-pencil-square-o"></i>',
                            'options' => array('class' => 'btn btn-xs','title' => 'Edit'),
                            'url' => 'Yii::app()->createUrl("admin/auction/edit", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ),
                        'Delete' => array(
                             'label' => '<i class="fa fa-times"></i>',
                            'options' => array('class' => 'btn btn-xs','title' => 'Delete', 'onclick' => "js:alert('If you delete this product than auction will be pending .So do you want to delete this auction?')"),
                            'url' => 'Yii::app()->createUrl("admin/auction/deleteauction", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ) 
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>