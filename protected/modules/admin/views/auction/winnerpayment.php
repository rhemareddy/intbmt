<?php
/* @var $this Auction */
/* @var $model Auction */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Winner Payment Process'
);

Yii::app()->clientScript->registerCssFile('/css/datatables.min.css');
Yii::app()->clientScript->registerCssFile('/css/buttons.dataTables.min.css');
Yii::app()->clientScript->registerCssFile('/css/datatables.bootstrap.min.css');

if (Yii::app()->user->hasFlash('success')): 
     echo '<div class="alert alert-success">'.Yii::app()->user->getFlash('success') .'</div>';
endif;
if (Yii::app()->user->hasFlash('error')):
    echo '<div class="alert alert-danger">' . Yii::app()->user->getFlash('error') . '</div>';
endif;

?>
<script src="/js/datatables.min.js"></script>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
        <div class="user-status col-md-12 filter-toggle margin-bottom-10">
            <form id="winner_frm" name="winner_frm" method="post" action="/admin/auction/winnerpayment" >
                <div class="col-md-4"> 
                    <span id="date_error" style="color:red"></span>
                    <div class="input-group input-large date-picker input-daterange pull-left">
                        <input type="text" id="toDate" name="from" data-provide="datepicker" placeholder="From Date" class="datepicker form-control" value="">
                        <span class="input-group-addon"> to </span>
                        <input type="text" id="fromDate" name="to" data-provide="datepicker" placeholder="To Date" class="datepicker form-control" value="">
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 margin-topDefault">Payment Status</label>
                            <div class="col-md-7 col-sm-7">
                                <select class="customeSelect howDidYou form-control " id="winner_status" name="winner_status">
                                    <option value="all" >All</option>
                                    <option value="payment" selected="selected">Awaiting Payment</option>
                                    <option value="accdetails">Awaiting Account Details</option>
                                    <option value="paid">Paid</option>
                                </select>   
                           </div>
                    </div>
                </div>
                <div class="col-md-1 col-sm-6 form-inline">
                    <input type="button" class="btn btn-success margintop3 mobile-left-15" value="Filter" name="winnerPaymentFilter" id="winnerPaymentFilter"/>
                </div>
            </form>
        </div>
        
<form id="bulkwinnerpaid" name="bulkwinnerpaid" method="post" action="/admin/auction/bulkwinnerpaid" >

    <div class="row ">
        <div class="col-md-12">
            <input type="submit" id="submit" name="submit" value="Mark As Paid" class="btn btn-warning pull-right ">
        </div>
    </div>
    <div class="row ">
        <div class="col-md-12 responsiveTable margin-top-10">
            <table id="winnerPaymentList" class="table table-bordered no-footer adb-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><input type="checkbox" id="auctionIds" class="editor-active"></th>
                        <th>Auction Id</th>
                        <th>Winner Name</th>
                        <th>Product Name</th>
                        <th>Amount</th>
                        <th>Closed Date</th>
                        <th>Account Details</th>
                        <th>Status</th>
                    </tr>
                </thead>
            </table>

            <script src="/js/dataTables.buttons.min.js"></script>
            <script src="/js/jszip.min.js"></script>
            <script src="/js/buttons.html5.min.js"></script>
            <script src="/js/buttons.print.min.js"></script>

            <script>
                $(document).ready(function () {
                    oTable = $('#winnerPaymentList').DataTable({
                        "responsive": true,
                        "iDisplayLength": 50,
                        "aLengthMenu": [[50, 100, 150, -1], [50, 100, 150, "All"]],
                        "processing": true,
                        "serverSide": true,
                        "paging": true,
                        "bSort": true,
                        "pagingType": "full_numbers",
                        "order": [[5, "desc"]],
                        "aoColumnDefs": [{
                                'bSortable': false,
                                'aTargets': [0]
                            }],
                        "sDom": 'B<"top"lpf>rt<"bottom"p>i<"clear">',
                        "buttons": [
                            {
                                extend: 'csv',
                                exportOptions: {
                                    columns: [1, 2, 3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'copyHtml5',
                                exportOptions: {
                                    columns: [1, 2, 3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'excelHtml5',
                                exportOptions: {
                                    columns: [1, 2, 3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'print',
                                exportOptions: {
                                    columns: [1, 2, 3, 4, 5, 6, 7]
                                }
                            },
                        ],
                        "ajax": {
                            "url": "/admin/auction/winnerpaymentdata",
                            "type": "POST"
                        },
                        "columns": [
                            {
                                render: function (data, type, row) {
                                            if (row.winnerStatus == 1) {
                                                return '<input type="checkbox" class="editor-active" name="requestids[' + row.auctionId + ']" value="' + row.auctionId + '">';
                                            } else {
                                                return "";
                                            }
                                        },
                            },
                            {"data": "auctionId"},
                            {"data": "winnerName"},
                            {"data": "productName"},
                            {"data": "productPrice"},
                            {"data": "closeDate"},
                            {"data": "accountDetails"},
                            {"data": "winnerStatusLabel"},
                        ],
                    });

                });

                $("#auctionIds").click(function () {
                    $('#winnerPaymentList tbody input[type="checkbox"]').prop('checked', this.checked);
                });

                $("#winnerPaymentFilter").click(function () {
                    $("#date_error").html("");
                    if ($("#toDate").val() > $("#fromDate").val()) {
                        $("#date_error").html("To Date should be greater then From Date!!!");
                        $("#fromDate").focus();
                        return false;
                    }
                    oTable.columns(7).search($("#winner_status").val().trim());
                    oTable.columns(5).search($("#toDate").val().trim(), $("#fromDate").val().trim());
                    oTable.draw();
                });
            </script>
        </div>
    </div> 
</form>