<?php
$breadcrum =  (isset($_GET['copy']) && $_GET['copy'] == 'yes')?'Copy':'Edit'; 
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Product' => '/admin/product/add',
     $breadcrum
);
?>
<div class="col-md-9 col-sm-9">
    <?php if ($error) { ?><div class="error" id="error_msg"><?php echo $error; ?></div><?php } ?>
<div class="portlet box toe-blue   ">
        <div class="portlet-title">
            <div class="caption">
                Edit Product
            </div>
        </div>
    <div class="portlet-body form"  id="product_form">
    <form action="/admin/product/edit?id=<?php echo BaseClass::mgEncrypt($productObject->id); ?>" method="post" class="form-horizontal" onsubmit='return validationProduct();' enctype="multipart/form-data">
        <input type="hidden" name="is_copy" id="is_copy" value="<?php echo (isset($_GET['copy']) && $_GET['copy'] == 'yes')?1:""; ?>"
        <fieldset>
            <div class="form-body">
                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Product Type<span class="require">*</span></label>
                    <div class="col-lg-7 padding-left0">
                        <label class="checkbox-inline"><input type="radio" id="purchase_status" class="purchase-radio" name="purchase_type" value="1" <?php echo ($productObject->purchase_type == 1) ? "checked" : ""; ?>>Buy</label>
                        <label class="checkbox-inline"><input type="radio" id="purchase_status" class="purchase-radio" name="purchase_type" value="2" <?php echo ($productObject->purchase_type == 2) ? "checked" : ""; ?>>Bid</label>
                        <label class="checkbox-inline"><input type="radio" id="purchase_status" class="purchase-radio" name="purchase_type" value="3" <?php echo ($productObject->purchase_type == 3) ? "checked" : ""; ?>>Buy and Bid </label>
                        <span style="color:red;" id="purchase_status_error"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Product Name<span class="require">*</span></label>
                    <div class="col-lg-7">
                        <input type="text" id="product_name" class="form-control" name="name" value="<?php echo (!empty($productObject)) ? $productObject->name : ""; ?>" onblur="return getProductDetails(this.value,'edit',<?php echo $productObject->id; ?>);">
                        <span id="search_product">&nbsp;</span>
                        <span style="color:red;" id="product_name_error"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Price<span class="require">*</span></label>
                    <div class="col-lg-7">
                        <input type="text" id="product_price" class="form-control" name="price" value="<?php echo (!empty($productObject)) ? $productObject->price : ""; ?>">
                        <span style="color:red;" id="product_price_error"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Description</label>
                    <div class="col-lg-7">
                        <textarea id="description" class="form-control" name="description"><?php echo (!empty($productObject)) ? strip_tags($productObject->description) : ""; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="country" class="col-lg-4 control-label">Product Image</label>
                    <div class="col-lg-7">
                        <input type="file" id="image" class="form-control" name="image" multiple="">
                        <img src="/upload/product/<?php echo $productObject->image; ?>" height="100" width="200">
                        <span style="color:red;" id="image_error"></span>
                    </div>

                </div>
                
                <div class="form-group">
                    <label for="country" class="col-lg-4 control-label top7">Is Cover</label>
                    <div class="col-lg-7">
                        
                        <label class="checkbox-inline "><input type="radio" class="purchase-radio" name="is_cover" value="1" <?php echo ($productObject->is_cover == 1) ? "checked" : ""; ?>>Yes</label>
                        <label class="checkbox-inline"><input type="radio" class="purchase-radio" name="is_cover" value="0" <?php echo ($productObject->is_cover == 0) ? "checked" : ""; ?>>No</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="country" class="col-lg-4 control-label top7">Status</label>
                    <div class="col-lg-7">
                        <label class="checkbox-inline"><input type="radio" class="purchase-radio" name="status" value="1" <?php echo ($productObject->status == 1) ? "checked" : ""; ?>>Yes </label>
                        <label class="checkbox-inline"><input type="radio" class="purchase-radio" name="status" value="0" <?php echo ($productObject->status == 0) ? "checked" : ""; ?>>No </label>
                    </div>
                </div>

            </div>
        </fieldset>

        <div class="form-actions right">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Update" class="btn red">
                <a href="/admin/product/list" class="btn mav-blue-btn">Cancel</a>
                <input type="hidden" name="productAvailability" id="productAvailability" value=""/>
            </div>
        </div>
    </form>
 </div>

    </div>
</div>