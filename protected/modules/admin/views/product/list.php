<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Product' => '/admin/product/add',
    'List'
);

$statusId = 1;
if (isset($_REQUEST['res_filter'])) {
    $statusId = $_REQUEST['res_filter'];
}
?>
 <input type="button" class="btn btn-primary pull-right margin-bottom-10 filter-btn " value="Filter" name="submit">	
<div class="col-md-12 user-status filter-toggle">

    <div class="expiration margin-topDefault confirmMenu">

        <form name="regervation_filter_frm" method="get" action="" class="form-inline">
            <div class="form-group mobile-left-15">                 
                <input type="text" class="form-control" name="name" id="name" placeholder="Product Name" value="<?php if ($name) echo $name; ?>"/>                                    
                
            </div>

            <div class="form-group">
                <div class="col-md-3 ">
                <select class="customeSelect howDidYou form-control " id="res_filter" name="res_filter">
                    <option value="all" <?php if ($statusId == "all") echo "selected"; else echo ""; ?>>Select Status</option>
                    <option value="1" <?php if ($statusId == "1") echo "selected"; else echo ""; ?> >Active</option>
                    <option value="0" <?php if ($statusId == "0") echo "selected"; else echo ""; ?>>In Active</option>
                </select>
               </div>
            </div>
            <input type="submit" class="btn btn-primary mobile-left-15" value="OK" name="submit" id="submit"/>
        </form>
    </div>

</form>

</div>
<div class="row">
    <div class="col-md-12 blue-table scroll-row">
        <?php
        if (Yii::app()->user->hasFlash('success')): 
            echo '<div class="alert alert-success">'.Yii::app()->user->getFlash('success') .'</div>';
       endif;
        
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{pager}{items}{summary}{pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            //'rowCssClassExpression'=>'fa fa-success btn default black delete',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                    'class' => 'IndexColumn',
                    'header' => '<span style="white-space: nowrap;color:#01b7f2">No</span>',
                ),
                array(
                    'name' => 'name',
                    'header' => '<span style="white-space: nowrap;">Name&nbsp;</span>',
                    'value' => '$data->name',
                ),
                array(
                    'name' => 'price',
                    'header' => '<span style="white-space: nowrap;">Price&nbsp;</span>',
                    'value' => '$data->price',
                ),
                
                array(
                    'name' => 'Description',
                    'header' => '<span style="white-space: nowrap;">Description &nbsp; &nbsp; &nbsp;</span>',
                    //'value' => '$data->description',
                    'value' => function($data) {
                        $desc = strip_tags($data->description);
                        echo $desc;
                    },
                    'htmlOptions' => array('width' => '30%'),
                ),
                array(
                    'name' => 'purchase_type',
                    'header' => '<span style="white-space: nowrap;">Purchase Option &nbsp; &nbsp; &nbsp;</span>',
                    'value' => array($this, 'getPurchaseStatus'),
                ),
                array(
                    'name' => 'status',
                    'header' => '<span style="white-space: nowrap;">Status &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                ),
                array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Created Date&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
                array(
                    'class' => 'CButtonColumn',
                    'id' => 'button-column',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'template' => '{Change}{Edit}{Delete}{Copy}',
                        'htmlOptions' => array('width' => '8%'),
                      //  'htmlOptions'=>array('width'=>'75'),
                        // 'headerHtmlOptions'=>array('width'=>'75'),
                    'buttons' => array(
                        'Change' => array(
                               'label' => '<i class="fa fa-retweet"></i>',
                            'options' => array('class' => 'btn btn-xs','title' => 'Change Status'),
                            'url' => 'Yii::app()->createUrl("admin/product/changestatus", array("id"=>BaseClass::mgEncrypt($data->id),"createdby"=>$data->created_by))',
                        ),
                        'Edit' => array(
                          'label' => '<i class="fa fa-pencil-square-o"></i>', 
                            'options' => array('class' => ' btn btn-xs ','title' => 'Edit'),
                            'url' => 'Yii::app()->createUrl("admin/product/edit", array("id"=>BaseClass::mgEncrypt($data->id)))',
                        ),
                        'Delete' => array(
                          'label' => '<i class="fa fa-times"></i>', 
                            'options' => array('class' => 'btn btn-xs','title' => 'Delete', 'onclick' => "return confirm('If you will delete this product than this product auction will be delete. So you really want to delete this product?')"),
                            'url' => 'Yii::app()->createUrl("admin/product/deleteproduct", array("id"=>BaseClass::mgEncrypt($data->id),"createdby"=>$data->created_by))',
                        ),
                        'Copy' => array(
                          'label' => '<i class="fa fa-copy"></i>', 
                            'options' => array('class' => ' btn btn-xs ','title' => 'Copy'),
                            'url' => 'Yii::app()->createUrl("admin/product/edit", array("id"=>BaseClass::mgEncrypt($data->id),"copy"=>"yes"))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>