<?php
$this->breadcrumbs = array(
    'Product' => array('product/list'),
    'Product Edit',
);
?>
<div class="col-md-7 col-sm-7">
    <?php if ($error) { ?><div class="error" id="error_msg"><?php echo $error; ?></div><?php } ?>
    <?php if ($success) { ?><div class="success" id="error_msg"><?php echo $success; ?></div><?php } ?>

    <form action="/admin/product/edit?id=<?php echo $productObject->id; ?>" method="post" class="form-horizontal" onsubmit='return validationProduct();' enctype="multipart/form-data">
        <fieldset>
            <div class="form-body">
                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Purchase Type<span class="require">*</span></label>
                    <div class="col-lg-7">
                        <input type="radio" id="purchase_status" class="form-control" name="purchase_type" value="1" <?php echo ($productObject->purchase_type == 1) ? "checked" : ""; ?>>Buy
                        <input type="radio" id="purchase_status" class="form-control" name="purchase_type" value="2" <?php echo ($productObject->purchase_type == 2) ? "checked" : ""; ?>>Bid
                        <input type="radio" id="purchase_status" class="form-control" name="purchase_type" value="3" <?php echo ($productObject->purchase_type == 3) ? "checked" : ""; ?>>Buy and Bid
                        <span style="color:red;" id="purchase_status_error"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Product Name<span class="require">*</span></label>
                    <div class="col-lg-7">
                        <input type="text" id="name" class="form-control" name="name" value="<?php echo (!empty($productObject)) ? $productObject->name : ""; ?>">
                        <span style="color:red;" id="name_error"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Price<span class="require">*</span></label>
                    <div class="col-lg-7">
                        <input type="text" id="price" class="form-control" name="price" value="<?php echo (!empty($productObject)) ? $productObject->price : ""; ?>">
                        <span style="color:red;" id="price_error"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-4 control-label" for="lastname">Description<span class="require">*</span></label>
                    <div class="col-lg-7">
                        <textarea id="description" class="form-control" name="description"><?php echo (!empty($productObject)) ? strip_tags($productObject->description) : ""; ?></textarea>
                        <span style="color:red;" id="description_error"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="country" class="col-lg-4 control-label">Product Image</label>
                    <div class="col-lg-7">
                        <input type="file" id="image" class="form-control" name="image" multiple="">
                        <img src="/upload/product/<?php echo $productObject->image; ?>" height="100" width="200">
                        <span style="color:red;" id="image_error"></span>
                    </div>

                </div>
                
                <div class="form-group">
                    <label for="country" class="col-lg-4 control-label">Is Cover</label>
                    <div class="col-lg-7">
                        <input type="radio" class="form-control" name="is_cover" value="0" <?php echo ($productObject->is_cover == 0) ? "checked" : ""; ?>>No
                        <input type="radio" class="form-control" name="is_cover" value="1" <?php echo ($productObject->is_cover == 1) ? "checked" : ""; ?>>Yes
                    </div>
                </div>

                <div class="form-group">
                    <label for="country" class="col-lg-4 control-label">Status</label>
                    <div class="col-lg-7">
                        <input type="radio" class="form-control" name="status" value="1" <?php echo ($productObject->status == 1) ? "checked" : ""; ?>>Yes
                        <input type="radio" class="form-control" name="status" value="0" <?php echo ($productObject->status == 0) ? "checked" : ""; ?>>No
                    </div>
                </div>

                
            </div>
        </fieldset>

        <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Update" class="btn red">
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

    function validationProduct()
    {
        var regex = /^\d*(.\d{2})?$/;
        var amount = document.getElementById('price');

        $("#purchase_status_error").html("");
        if (!$('input[name=purchase_type]:checked').val()) {
            $("#purchase_status_error").html("Choose Any Purchase Option");
            $("#purchase_status").focus();
            return false;
        }

        $("#name_error").html("");
        if ($("#name").val() == "") {
            $("#name_error").html("Enter Package Name");
            $("#name").focus();
            return false;
        }
        $("#price_error").html("");
        if ($("#price").val() == "") {
            $("#price_error").html("Enter Package Price");
            $("#price").focus();
            return false;
        } else {
            if (isNaN($("#price").val())) {
                $("#price_error").html("Enter valid Package price.");
                $("#price").focus();
                return false;
            }
        }

        if (!regex.test(amount.value)) {
            $("#price_error").html("Enter Valid Package Price");
            $("#price").focus();
            return false;
        }

    }
</script>


