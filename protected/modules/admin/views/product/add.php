<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Product' => '/admin/product/add',
    'Add',
);
?>
<div class="col-md-9 col-sm-9">
    <?php
    if ($error) {
        echo '<div class="error" id="error_msg">' . $error . '</div>';
    }
    if ($success) {
        echo '<div class="success" id="error_msg">' . $success . '</div>';
    }
    ?>
    <div class="portlet box toe-blue   ">
        <div class="portlet-title">
            <div class="caption">
                Add Product
            </div>
        </div>

        <div class="portlet-body form product-formbody"  id="product_form">
            <form action="" method="post" class="form-horizontal" enctype="multipart/form-data" onsubmit="return validationProduct();">

                <fieldset>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Product Type<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="purchase_type" value="1" checked="checked">Buy
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="purchase_type" value="2" >Bid
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio"  class="pro-radio" name="purchase_type" value="3" >Buy and Bid
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Product Name<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" id="product_name" class="form-control" name="name" onblur="return getProductDetails(this.value,'add');" >
                                <span id="search_product">&nbsp;</span>
                                <span style="color:red;" id="product_name_error"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Price<span class="require">*</span></label>
                            <div class="col-lg-7">
                                <input type="text" id="product_price" class="form-control" name="price">
                                <span style="color:red;" id="product_price_error"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="lastname">Description</label>
                            <div class="col-lg-7">
                                <textarea id="product_description" class="form-control" name="description"></textarea>
                                <span style="color:red;" id="product_description_error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="country" class="col-lg-4 control-label">Product Image</label>
                            <div class="col-lg-7">
                                <input type="file" id="image" class="form-control" name="image">
                                <span style="color:red;" id="image_error"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="country" class="col-lg-4 control-label">Is Cover</label>
                            <div class="col-lg-7">
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="is_cover" value="1" checked="checked">Yes
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="is_cover" value="0" >No
                                </label>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-lg-4 control-label">Status</label>
                            <div class="col-lg-7">
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="status" value="1" checked="checked">Yes
                                </label>
                                <label class="checkbox-inline">
                                    <input type="radio" class="pro-radio" name="status" value="0" >No
                                </label>
                            </div>
                        </div>

                    </div>
                </fieldset>
                <div class="form-actions right">  
                    <input type="submit" name="submitProduct" value="Submit" class="btn mav-blue-btn">
                    <input type="hidden" name="productAvailability" id="productAvailability" value=""/>
                </div>

            </form>
        </div>

    </div>
</div>