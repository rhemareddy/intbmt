<?php $this->renderPartial('../mailTemplate/header'); ?>
<tr>
  <td valign="" bgcolor="#fcb81a" height="55" align="left" style="line-height:0px; font-size:16px">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	  <tbody>
		<tr>
		  <td width="5%" valign="middle" align="center" style="line-height:22px; color: #ffffff; font-size:16px; font-family:'Nunito'"> </td>
		  <td width="90%" valign="middle" align="left" style="line-height:22px; color: #ffffff; font-size:16px; font-family:'Nunito'"> Hello <?php echo ucfirst($model->full_name); ?></td>
		  <td width="5%" valign="middle" align="center" style="line-height:22px; color: #ffffff; font-size:16px; font-family:'Nunito'"> </td>
		</tr>
	  </tbody>
	</table>
  </td>
</tr>
<!-- text description -->
<tr>
  <td bgcolor="#fafafa" height="" align="left" style="line-height:0px; font-size:16px">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	  <tbody>
		<tr>
		  <td width="5%" valign="middle" align="center" style="line-height:22px; color: #828282; font-size:16px; font-family:'Nunito'"> </td>
		  <td width="95%" valign="middle" align="left" style="line-height:22px; color: #828282; font-size:16px; font-family:'Nunito'"> 
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			  <tbody>
				<tr>
				  <td height="20"></td>
				</tr>
				<tr>
				  <td valign="middle" align="left" style="line-height:22px; padding-left: 30px; color: #00aeef; font-size:16px; font-family:'Nunito'">
					Thank you for signing up with us
				  </td>
				</tr>
				<tr>
				  <td height="20"></td>
				</tr>
				<tr>
				  <td valign="middle" align="left" style="line-height:22px; padding-left: 30px; color: #6b6b6b; font-size:16px; font-family:'Nunito'">
					Your email address <a href="" style="color:#f15c2b; display:inline"><?php echo $model->email; ?></a>, has been set as the Registrant contact on <a href="http://mavwealth.com/" style="color:#f15c2b; display:inline">mavwealth.com </a>for UserName :<strong> <?php echo $model->name; ?> </strong> . To verify your email address, please click on the following link.
				  </td>
				</tr>
				<tr>
				  <td height="20" bgcolor="" style=""></td>
				</tr>
				<tr>
				  <td valign="middle" align="left" style="line-height:22px; padding-left: 30px; color: #00aeef; font-size:16px; font-family:'Nunito'; text-align: center;">
					<a href="<?php echo Yii::app()->getBaseUrl(true); ?>/user/confirm?activation_key=<?php echo $rand; ?>" style="text-decoration:none; color:#FFFFFF; background:#01B7F2; padding: 5px;">Verify</a>
				  </td>
				</tr>
				<tr>
				  <td height="20"></td>
				</tr>
				<tr>
				  <td valign="middle" align="left" style="line-height:22px; padding-left: 30px; color: #6b6b6b; font-size:16px; font-family:'Nunito'">
					Please note that failure to verify the Registrant contact email address will lead to deactivation of the respective user, if not completed within 3 days from the date of that action. Once deactivated, the member user will not function until the email address is verified.
				  </td>
				</tr>
				<tr>
				  <td height="20"></td>
				</tr>
				<?php $this->renderPartial('../mailTemplate/footer'); ?>