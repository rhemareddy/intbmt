<!-- address -->
<tr>
  <td style="">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	  <tr>
		<td width="350" valign="top" align="left" style="line-height: 18px;padding-left: 30px;vertical-align: top;padding-top: 5px;color: #828282;font-size: 12px;"> 
		  <strong>Off:</strong> 27, Old Gloucester Street, <br> LONDON, WC1N 3AX, <br> UNITED KINGDOM
		</td>
                 <td valign="middle" align="left" style="line-height: 18px ; width: 250px ; float: left ; color: #828282 ; font-size: 12px">
	<table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding-left:35px">
	  <tbody>
		<tr>
		  <td width="100%" height="20" valign="middle" align="left" style="line-height:18px; color: #828282; font-size:12px; font-family:'Nunito'"><strong>Call Us: </strong><?php echo Yii::app()->params['phone']; ?></td>
		</tr>
		<tr>
		  <td width="100%" height="20" valign="middle" align="left" style="line-height:18px; color: #828282; font-size:12px; font-family:'Nunito'"><strong>Mail us:</strong><a href="mailto:<?php echo Yii::app()->params['mail']; ?>"><?php echo Yii::app()->params['mail']; ?></a></td>
		</tr>
		<tr>
		  <td width="100%" height="20" valign="middle" align="left" style="line-height:18px; color: #828282; font-size:12px; font-family:'Nunito'"><strong>Visit Us On:</strong> www.<p style="color:#f15c2b; display:inline">mavwealth</p>.com</td>
		</tr>
	  </tbody>
	</table>
  </td>
	  </tr>
	</table>
  </td>
 
</tr>
<tr>
  <td bgcolor="#fcfcfc" height="70" style="line-height:0; padding-left: 30px; border-top:1px solid #dfdfdf;">
	<table width="600" cellspacing="0" cellpadding="0" border="0">
	  <tbody>
		<tr>
		  <td width="300">
			<table width="100%">
			  <tr>
				<td>
				  <a href="javascript:void(0)"> <img width="" border="0" src="<?php echo Yii::app()->getBaseUrl(true); ?>/email-images/skype.png" style="float:left; margin-right:5px">
					<p style="width:60px; float:left; color:#19bcf1">help.mavwealth</p>
				  </a>
				</td>
			  </tr>
			</table>
		  </td>
		  <td width="165" valign="middle" style="line-height:0px;color: #828282;font-size:12px; font-family:'Nunito'">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
			  <tbody>
				<tr>
				  <td width="15%" valign="middle" align="left"><a href="https://www.facebook.com/MavWealthbid/?ref=bookmarks"><img width="" border="0" src="<?php echo Yii::app()->getBaseUrl(true); ?>/email-images/facebook-icon.png"></a></td>
				  <td width="15%" valign="middle" align="left"><a href="https://plus.google.com/+Mavwealth"><img width="" border="0" src="<?php echo Yii::app()->getBaseUrl(true); ?>/email-images/googleplus.png"></a></td>
				  <td width="15%" valign="middle" align="left"><a href="https://www.youtube.com/channel/UC4trNlCCl_bgfUTYFAHMPAA"><img width="" border="0" src="<?php echo Yii::app()->getBaseUrl(true); ?>/email-images/youtube-icon.png"></a></td>
				  <td width="40%" valign="middle" align="left"></td>
				</tr>
			  </tbody>
			</table>
		  </td>
		</tr>
	  </tbody>
	</table></td>
</tr>
<tr>
  <td valign="" bgcolor="#fafafa" height="30" style="line-height:12px; border-top:1px solid #dfdfdf;font-size: 14px;color: #cccccc">
	<table width="100%" cellspacing="0" cellpadding="0" border="0">
	  <tbody>
		<tr>
		  <td width="100%" valign="middle" align="center" style="line-height:12px; font-size:12px; font-family:'Nunito'"> Please do not reply to this email. Emails sent to this address will not be answered. </td>
		</tr>
	  </tbody>
	</table>
  </td>
</tr>