<?php
$this->breadcrumbs = array(
    'Site' => array('/admin/site/index'),
    'Create'
);
?>
<?php if(!empty($error)){?><p class="failed"><span><?php echo $error;?></span></p><?php }?>
<?php if(!empty($successMsg)){?><p class="success"><span><?php echo $successMsg;?></span></p><?php }?>
   <script src="/ckeditor/ckeditor.js"></script>
   <script type="text/javascript" src="/ckfinder/ckfinder.js"></script>

<form class="form-horizontal" role="form" id="form_admin_reservation" enctype="multipart/form-data" action="/admin/site/edit?id=<?php echo $staticPageObject->id;?>" method="post" onsubmit="return validateForm()"> 
    <div class="col-md-12 form-group">
        <label class="col-md-2">Title *</label>
        <div class="col-md-6">
<!--<input type="text" class="form-control dvalid" name="title" id="title" size="60" maxlength="75" class="textBox" value="<?php //echo (!empty($staticPageObject))?$staticPageObject->title : "";?>" />-->
             <select name="title" id="title" class="form-control dvalid">
                <option value="">Select Title</option>
                <option value="howtoplay" <?php if(!empty($staticPageObject) && $staticPageObject->title == 'howtoplay') { echo 'selected="selected"'; } ?>>How to Play</option>
                <option value="tips" <?php if(!empty($staticPageObject) && $staticPageObject->title == 'tips') { echo 'selected="selected"'; } ?>>Tips N Tricks</option>
                <option value="faq" <?php if(!empty($staticPageObject) && $staticPageObject->title == 'faq') { echo 'selected="selected"'; } ?>>FAQ</option>
            </select>
            <span class="clrred" style="color:red" id="title_error"></span>
        </div>
    </div>
    <div class="col-md-12 form-group">
        <label class="col-md-2">Content *</label>
        <div class="col-md-8">
            <textarea class="form-control dvalid" name="description" id="description" rows="10" cols="50" maxlength="250">
                 <?php echo (!empty($staticPageObject))?$staticPageObject->description : "";?>
            </textarea>
            <span class="clrred" style="color:red"  id="description_error"></span>
        </div>
    </div>
<!--    <div class="col-md-12 form-group">
        <label class="col-md-2">Attachement &nbsp;</label>
    <div class="col-md-6">
        <input type="hidden" name="attachment1" id="" value=""/>
        <input type="file" name="attachment" id="attachement"/>
      
        <span class="clrred" style="color:red"  id="email_address_error"></span>
    </div>
   </div>-->
    <div class="col-md-12 form-group">
        <label class="col-md-2"></label>
        <div class="col-md-6">
            <input type="submit" class="btn green" name="submit" id="submit" size="60" maxlength="75" class="textBox" value="Submit" />
        </div>
    </div> 
</form>

<script language = "Javascript">
    function validateForm() {
        $("#title_error").html("");
        if ($("#title").val() == "") {
            $("#title_error").html("Enter the Title of Page");
            $("#title").focus();
            return false;
        } 
        var page_content = CKEDITOR.instances['description'].getData();
        $("#description_error").html("");
        if (page_content == "") {
            $("#description_error").html("Enter the Page Content!!!");
            $("#description").focus();
            return false;
        }
    }
</script>
 <script type="text/javascript">
    CKEDITOR.replace( 'description' , {
    filebrowserBrowseUrl : '/ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl : '/ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl : '/ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
});
CKFinder.setupCKEditor( editor, '../' );
</script>