<?php
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Settings' => '/admin/package/packagesettings',
    'System Setting',
);
?>

<div class="alert alert-success" id="success_msg" style="display:none;"><strong>Information Updated Successfully.</strong></div>
<div class="alert alert-danger" id="error_msg" style="display:none;"><strong>Failed To Update Data.</strong></div>

<div class="portlet-body blue-table">
    <table class="table table-bordered no-footer adb-table" id="sample_editable_1">
        <thead>
            <tr>
                <th style="display:none;">id</th>
                <th>Name</th>
                <th>Value</th>
                <th>Comment</th>
                <th>Edit</th>
                <th>Cancel</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($systemSettingObjectList as $systemSettingObject) { ?>
                <tr >
                    <td style="display:none;"><?php echo $systemSettingObject->id; ?></td>
                    <td>
                        <p id="system_name_<?php echo $systemSettingObject->id; ?>"><?php echo $systemSettingObject->name; ?></p>
                        <p id="input_system_name_<?php echo $systemSettingObject->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $systemSettingObject->name; ?>" id="system_name_value_<?php echo $systemSettingObject->id; ?>" name="wallet_name_<?php echo $systemSettingObject->id; ?>" />
                        </p>
                    </td>
                    
                    <td>
                        <p id="system_cashback_<?php echo $systemSettingObject->id; ?>"><?php echo $systemSettingObject->value; ?></p>
                        <p id="input_system_cashback_<?php echo $systemSettingObject->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $systemSettingObject->value; ?>" name="system_cashback_<?php echo $systemSettingObject->id; ?>" id="system_cashback_value_<?php echo $systemSettingObject->id; ?>" />
                        </p>
                    </td>
                    <td>
                        <p id="system_cashback_<?php echo $systemSettingObject->comment; ?>"><?php echo $systemSettingObject->comment; ?></p>
                    </td>
                    
                    <td>
                        <a class="edit" href="javascript:;" id="editSystem_<?php echo $systemSettingObject->id; ?>" onclick="editWalletData(<?php echo $systemSettingObject->id; ?>);">Edit</a>
                        <a class="save" style="display:none;" id="saveSystem_<?php echo $systemSettingObject->id; ?>" href="javascript:;" onclick="saveWalletData(<?php echo $systemSettingObject->id; ?>);">Save</a>
                    </td>
                    <td><a class="cancel" href="javascript:;" style="display:none;" id="cancelSystem_<?php echo $systemSettingObject->id; ?>" onclick="restoreSystemData(<?php echo $systemSettingObject->id; ?>);">Cancel</a></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<script>
function editWalletData(id) {
    $("#editSystem_" + id).hide();
    $("#saveSystem_" + id).show();
    $("#cancelSystem_" + id).show();

    $("#system_cashback_" + id).hide();
    $("#input_system_cashback_" + id).show();
    
}

function restoreSystemData(id) {
    $("#editSystem_" + id).show();
    $("#saveSystem_" + id).hide();
    $("#cancelSystem_" + id).hide();
    
    $("#system_cashback_" + id).show();
    $("#input_system_cashback_" + id).hide();
    
}

function saveWalletData(id) {
    $.ajax({
        type: "post",
        url: "systemlist",
        data: {id: id,
            system_cashback: $("#system_cashback_value_" + id).val(),
        },
        success: function (data) {
            if (data == 'success') {
                $("#success_msg").show();
            } else {
                $("#error_msg").show(); 
            }
            $("#system_cashback_" + id).show();
            $("#system_cashback_" + id).html($("#system_cashback_value_" + id).val());
            $("#input_system_cashback_" + id).hide();
            $("#editSystem_" + id).show();
            $("#saveSystem_" + id).hide();
            $("#cancelSystem_" + id).hide();
        }
    })
}
</script>