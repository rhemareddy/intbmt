<?php
$this->breadcrumbs = array(
    'Package' => array('package/list'),
    'Package Edit',
);
$packageImagePath = Yii::app()->params['imagePath']['package'];
$thumbImagePath = Yii::app()->params['thumbImage'];
?>
<div class="col-md-7 col-sm-7">
    <?php if(!empty($_GET['error'])){?><div class="error" id="error_msg"><?php echo $_GET['error'];?></div><?php }?>
    <?php if(!empty($_GET['success'])){?><div class="success" id="error_msg"><?php echo $_GET['success'];?></div><?php }?>
   
    <form action="/admin/package/deleteimages?id=<?php echo $id;?>" method="post" class="form-horizontal" enctype="multipart/form-data"> 
     
         <fieldset>
      
<div class="form-body">
    <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname">Package Images</label>
                
    </div>
     <?php $i=1; foreach ($packageHasImageObject as $object) { ?>
            <div class="form-group">
                <label class="col-lg-4 control-label" for="lastname"><?php echo "No.".$i.":"; ?></label>
                <div class="col-lg-7">
                    <?php 
                    $thumbImage = '/' . $packageImagePath . $object->package_id . '/' . $thumbImagePath . '/' . $object->image()->image; 
                    ?>
                   <img src="<?php echo $thumbImage; ?>" alt="" />
                   <input type="radio" id="deleteImg" name="deleteImg" value="<?php echo $object->id; ?>">
                </div>
            </div>
     <?php $i++; }?>
    
</div>
        </fieldset>

    <div class="row">
            <div class="col-lg-8 col-md-offset-4 padding-left-0 padding-top-20">                        
                <input type="submit" name="submit" value="Delete" class="btn red">
            </div>
        </div>
    </form>
</div>





<script type="text/javascript">
    function validation()
    {
       var regex = /^\d*(.\d{2})?$/;
        var amount = document.getElementById('price');
        
        $("#name_error").html("");
        if ($("#name").val() == "") {
            $("#name_error").html("Enter Package Name");
            $("#name").focus();
            return false;
        }
        $("#price_error").html("");
        if ($("#price").val() == "") {
            $("#price_error").html("Enter Package Price");
            $("#price").focus();
            return false;
        } else {
            if (isNaN($("#price").val())) {
                $("#price_error").html("Enter valid Package price.");
                $("#price").focus();
                return false;
            }
        }

        if (!regex.test(amount.value)) {
            $("#price_error").html("Enter Valid Package Price");
            $("#price").focus();
            return false;
        }
        
        $("#category_id_error").html("");
        if ($("#category_id").val() == "") {
            $("#category_id_error").html("Enter select package type");
            $("#category_id").focus();
            return false;
        }
        
        $("#city_error").html("");
        if ($("#city").val() == "") {
            $("#city_error").html("Enter city");
            $("#city").focus();
            return false;
        }
        
        $("#source_error").html("");
        if ($("#source").val() == "") {
            $("#source_error").html("Enter source place");
            $("#source").focus();
            return false;
        }
        
        $("#destination_error").html("");
        if ($("#destination").val() == "") {
            $("#destination_error").html("Enter destination place");
            $("#destination").focus();
            return false;
        }
        
        $("#description_error").html("");
        if ($("#description").val() == "") {
            $("#description_error").html("Enter Package Description");
            $("#description").focus();
            return false;
        }
        
        $("#no_persons_error").html("");
        if ($("#no_persons").val() == "") {
            $("#no_persons_error").html("Enter number of persons");
            $("#no_persons").focus();
            return false;
        }

        if ($("#no_persons").val() != "") {
            if (isNaN($("#no_persons").val())) {
                $("#no_persons_error").html("Enter valid number.");
                $("#no_persons_error").focus();
                return false;
            } else {
                $("#no_persons_error").html("");
            }

        }
       
        $("#no_days_error").html("");
        if ($("#no_days").val() == "") {
            $("#no_days_error").html("Enter number of days");
            $("#no_days").focus();
            return false;
        }
        
        if ($("#no_days").val() != "") {
            $("#no_days_error").html("");
            if (isNaN($("#no_days").val())) {
                $("#no_days_error").html("Enter valid number.");
                $("#no_days_error").focus();
                return false;
            }

        }
        
         $("#no_nights_error").html("");
        if ($("#no_nights").val() == "") {
            $("#no_nights_error").html("Enter number of nights");
            $("#no_nights").focus();
            return false;
        }

        if ($("#no_nights").val() != "") {
            $("#no_nights_error").html("");
            if (isNaN($("#no_nights").val())) {
                $("#no_nights_error").html("Enter valid number.");
                $("#no_nights_error").focus();
                return false;
            }
        }
//        $("#image_error").html("");
//        if ($("#image").val() == "") {
//            $("#image_error").html("Please upload package image");
//            $("#image").focus();
//            return false;
//        }

        
    }
    </script>
     
     
