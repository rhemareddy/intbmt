<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Package List'
);

    $statusId = "";
    if (isset($_REQUEST['res_filter'])) {
        $statusId = $_REQUEST['res_filter'];
    }
?>

<style>
    .confirmBtn{left: 333px;
                position: absolute;
                top: 0;}

    .confirmOk{left: 610px;
               position: absolute;
               top: 8px;}
    .confirmMenu{position: relative;}
</style>
<div class="col-md-12">

    <div class="expiration margin-topDefault confirmMenu">

        <form id="regervation_filter_frm" name="regervation_filter_frm" method="post" action="/admin/package/vendorlist" class="form-inline">
            <div class="form-group">                 
                <input type="text" class="form-control" name="name" id="name" placeholder="Package Name" value="<?php if($name) echo $name; ?>"/>                                    
            </div>
            <div class="form-group">                 
                 <input type="text" name="to" id="to" placeholder="Start Date" class="datepicker form-control" value="<?php if($to) echo $to; ?>">                                   
            </div>
            <div class="form-group">                 
                 <input type="text" name="from" id="from" placeholder="End Date" class="datepicker form-control" value="<?php if($from) echo $from; ?>">                                   
            </div>
            <div class="form-group">
            <select class="customeSelect howDidYou form-control input-medium" id="ui-id-5" name="res_filter" style="margin-right: 10px;">
                <option value="">Select Status</option>
                <option value="1" <?php if(!empty($statusId) && $statusId==1) echo "selected"; else echo ""; ?>>Active</option>
                <option value="0" <?php if(!empty($statusId) && $statusId==0) echo "selected"; else echo ""; ?>>In Active</option>
            </select>
               
            </div>
           <input type="submit" class="btn btn-primary " value="OK" name="submit" id="submit"/>
        </form>
    </div>
    
</form>

</div>
<div class="row">
    <div class="col-md-12">
        <?php if (!empty($_GET['msg']) && $_GET['msg'] == '1') { ?> <div class="success"><?php echo "Record Deleted Succesfully." ?></div> <?php } ?>
        <?php if (!empty($_GET['msg']) && $_GET['msg'] == '2') { ?> <div class="success"><?php echo "Status Changed Succesfully." ?></div> <?php } ?>
        <?php if (!empty($_GET['success'])) { ?> <div class="success"><?php echo $_GET['success'];?></div> <?php } ?>
         <?php if (!empty($_GET['error'])) { ?> <div class="error"><?php $_GET['error'];?></div> <?php } ?>

        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'state-grid',
            'dataProvider' => $dataProvider,
            'enableSorting' => 'true',
            'ajaxUpdate' => true,
            'summaryText' => 'Showing {start} to {end} of {count} entries',
            'template' => '{items} {summary} {pager}',
            'itemsCssClass' => 'table table-striped table-bordered table-hover table-full-width',
            //'rowCssClassExpression'=>'fa fa-success btn default black delete',
            'pager' => array(
                'header' => false,
                'firstPageLabel' => "<<",
                'prevPageLabel' => "<",
                'nextPageLabel' => ">",
                'lastPageLabel' => ">>",
            ),
            'columns' => array(
                array(
                    'name' => 'id',
                    'header' => '<span style="white-space: nowrap;">Sl. No &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$row+1',
                ),
                array(
                    'name' => 'name',
                    'header' => '<span style="white-space: nowrap;">Name &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->name',
                ),
                array(
                    'name' => 'price',
                    'header' => '<span style="white-space: nowrap;">Price &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->price',
                ),
                 array(
                    'name' => 'city',
                    'header' => '<span style="white-space: nowrap;">City &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->city',
                ),
                 array(
                    'name' => 'source',
                    'header' => '<span style="white-space: nowrap;">Source&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->source',
                ),
                 array(
                    'name' => 'destination',
                    'header' => '<span style="white-space: nowrap;">Destination&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->destination',
                ),
                array(
                    'name' => 'no_persons',
                    'header' => '<span style="white-space: nowrap;">No Persons&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->no_persons',
                ),
                array(
                    'name' => 'no_days',
                    'header' => '<span style="white-space: nowrap;">No Days&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->no_days',
                ),
                array(
                    'name' => 'no_nights',
                    'header' => '<span style="white-space: nowrap;">No Nights&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->no_nights',
                ),
              
//                array(
//                    'name' => 'update_at',
//                    'header' => '<span style="white-space: nowrap;">Updated Time&nbsp; &nbsp; &nbsp;</span>',
//                    'value' => array($this, 'getPackageUpdatedTime')
//                ),
                array(
                    'name' => 'Description',
                    'header' => '<span style="white-space: nowrap;">Description &nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->description',
                ),
                array(
                    'name' => 'status',
                    'value' => '($data->status == 1) ? Yii::t(\'translation\', \'Active\') : Yii::t(\'translation\', \'Inactive\')',
                ),
                  array(
                    'name' => 'created_at',
                    'header' => '<span style="white-space: nowrap;">Created Date&nbsp; &nbsp; &nbsp;</span>',
                    'value' => '$data->created_at',
                ),
                array(
                    'class' => 'CButtonColumn',
                    'header' => '<span style="white-space: nowrap;">Action &nbsp; &nbsp; &nbsp;</span>',
                    'template' => '{Change}{Edit}{Delete}',
                    'htmlOptions' => array('width' => '30%'),
                    'buttons' => array(
                        'Change' => array(
                            'label' => Yii::t('translation', 'Change Status'),
                            'options' => array('class' => 'fa fa-success btn default black delete'),
                            'url' => 'Yii::app()->createUrl("admin/package/changestatus", array("id"=>$data->id,"createdby"=>$data->created_by))',
                        ),
                        'Edit' => array(
                            'label' => 'Edit',
                            'options' => array('class' => 'fa fa-success btn default black delete blue'),
                            'url' => 'Yii::app()->createUrl("admin/package/edit", array("id"=>$data->id))',
                        ),
                        'Delete' => array(
                            'label' => Yii::t('translation', 'Delete'),
                            'options' => array('class' => 'fa fa-success btn default black delete red', 'onclick' => "js:alert('Do u want to delete this package?')"),
                            'url' => 'Yii::app()->createUrl("admin/package/deletepackage", array("id"=>$data->id,"createdby"=>$data->created_by))',
                        ),
                    ),
                ),
            ),
        ));
        ?>
    </div>
</div>