<?php 
$this->breadcrumbs = array(
    'Dashboard' => '/admin/default/dashboard',
    'Settings' => '/admin/package/packagesettings',
    'Package Settings',
);
?>
<p class="success" id="success_msg" style="display:none;"><i class="fa fa-check-circle icon-success"></i><span class="span-success-2">Information Updated Successfully.</span></p>
<p class="error" id="error_msg" style="display:none;"><i class="fa fa-times-circle icon-error"></i><span class="span-error-2">Failed To Update Data</span></p>

<div class="portlet-body packageSetting blue-table">
    <table class="table table-bordered no-footer adb-table" id="sample_editable_1">
        <thead>
            <tr>
                <th style="display:none;">id</th>
                <th>Package Name</th>
                <th>Min Amount</th>
                <th>Max Amount</th>
                <th>Validity</th>
                <th>Daily CashBack (%)</th>
                <th>Direct (%)</th>
                <th>Binary Capping (%)</th>
                <th>Edit</th>
                <th>Cancel</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($packageObject as $package) { ?>
                <tr >
                    <td style="display:none;"><?php echo $package->id; ?></td>
                    <td>
                        <p id="package_name_<?php echo $package->id; ?>"><?php echo $package->name; ?></p>
                        <p id="input_package_name_<?php echo $package->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $package->name; ?>" id="package_name_value_<?php echo $package->id; ?>" name="package_name_<?php echo $package->id; ?>" />
                        </p>
                    </td>
                    <td>
                        <p id="min_amount_<?php echo $package->id; ?>"><?php echo $package->min_amount; ?></p>
                        <p id="input_min_amount_<?php echo $package->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $package->min_amount; ?>" name="min_amount_<?php echo $package->id; ?>" id="min_amount_value_<?php echo $package->id; ?>" />
                        </p>
                    </td>

                    <td>
                        <p id="max_amount_<?php echo $package->id; ?>"><?php echo $package->max_amount; ?></p>
                        <p id="input_max_amount_<?php echo $package->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $package->max_amount; ?>" name="max_amount_<?php echo $package->id; ?>" id="max_amount_value_<?php echo $package->id; ?>" />
                        </p>
                    </td>

                    <td>
                        <p id="validity_<?php echo $package->id; ?>"><?php echo $package->validity; ?></p>
                        <p id="input_validity_<?php echo $package->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $package->validity; ?>" name="validity_<?php echo $package->id; ?>" id="validity_value_<?php echo $package->id; ?>" />
                        </p>
                    </td>

                    <td>
                        <p id="daily_returns_<?php echo $package->id; ?>"><?php echo $package->daily_returns; ?></p>
                        <p id="input_daily_returns_<?php echo $package->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $package->daily_returns; ?>" name="daily_returns_<?php echo $package->id; ?>" id="daily_returns_value_<?php echo $package->id; ?>" />
                        </p>
                    </td>

                    <td>
                        <p id="direct_<?php echo $package->id; ?>"><?php echo $package->direct; ?></p>
                        <p id="input_direct_<?php echo $package->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $package->direct; ?>" name="direct_<?php echo $package->id; ?>" id="direct_value_<?php echo $package->id; ?>" />
                        </p>
                    </td>

                    <td>
                        <p id="binary_capping_<?php echo $package->id; ?>"><?php echo $package->binary_capping; ?></p>
                        <p id="input_binary_capping_<?php echo $package->id; ?>" style="display:none;">
                            <input type="text" value="<?php echo $package->binary_capping; ?>" name="binary_capping_<?php echo $package->id; ?>" id="binary_capping_value_<?php echo $package->id; ?>" />
                        </p>
                    </td>

                    <td>
                        <a class="edit" href="javascript:;" id="editPackage_<?php echo $package->id; ?>" onclick="editPackageData(<?php echo $package->id; ?>);">Edit</a>
                        <a class="save" style="display:none;" id="savePackage_<?php echo $package->id; ?>" href="javascript:;" onclick="savePackageData(<?php echo $package->id; ?>);">Save</a>
                    </td>
                    <td><a class="cancel" href="javascript:;" onclick="restorePackageData(<?php echo $package->id; ?>);">Cancel</a></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<link href="/metronic/custom/custom.css" rel="stylesheet" rtype="text/css" />
<script src="/js/packagesettings.js" type="text/javascript"></script>