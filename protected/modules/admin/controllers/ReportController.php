<?php

class ReportController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'address', 'wallet','creditwallet', 'package', 'companysponsor', 
                    'verification','socialaccount', 'contact', 'referral','deposit','trackreferral', 'vendor', 
                    'loginactivities','transaction','requestpaymentrefid','transactioncsv', 'binaryreport','binaryreportcsv',
                    'downlinereport','requestpayment', 'requestpaymentcsv', 'requestpaymentrefid','orderlist','orderlistcsv',
                    'supporttransaction','mavtrans','readtransdata','transactionreaddata', 'profitability', 'profitabilitydata',
                    'bulkaction','supportdata','orderlistdata','agentcontact', 'agentcontactdata','contactdata','agentrequest',
                    'agentrequestdata','transactioncsv'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionIndex() {
        $model = new User();
        $pageSize = Yii::app()->params['defaultPageSize'];
        $todayDate = Yii::app()->params['startDate'];
        $fromDate = date('Y-m-d');
        $status = 1;
        $condition = "";
        
        if (!empty($_REQUEST['from']) && !empty($_REQUEST['to'])) {
            $todayDate = $_REQUEST['from'];
            $fromDate = $_REQUEST['to'];
            
            $condition = ' AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"';
        }
        
        if (isset($_REQUEST['res_filter'])) {
            $status = $_REQUEST['res_filter'];
        }

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => (' status = "' . $status . '"' .$condition), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));

        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAddress() {
        $model = new UserProfile();
        $pageSize = Yii::app()->params['defaultPageSize'];
        $todayDate = Yii::app()->params['startDate'];
        $fromDate = date('Y-m-d');
         
        if (!empty($_REQUEST)) {
            $todayDate = $_REQUEST['from'];
            $fromDate = $_REQUEST['to'];
        }

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ('address !="" AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"' ), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));
        
        $this->render('address', array(
            'dataProvider' => $dataProvider,
        ));
        
    }
    
    public function actionDeposit() {
        $loggedInuserName = User::model()->findByPk(Yii::app()->session['userid']);
        $model = User::model()->findAll(array('condition' => 'sponsor_id = "' . $loggedInuserName->name . '"'));
        //$connection = Yii::app()->db;
        $userid = "";
        $userID = 0;
        if ($model) {
            foreach ($model as $user) {
                $userid .= "'" . $user->id . "',";
            }
            $userID = rtrim($userid, ',');
            $condition = 'user_id IN(' . $userID . ') AND ';
        } else {
            $condition = "user_id IN('0') AND ";
        }
        $pageSize = 100;
          
        
        // Date filter.
        if (!empty($_POST)) {
            $todayDate = $_POST['from'];
            $fromDate = $_POST['to'];
        }else{
            $todayDate = '0000-00-00';
            $fromDate = '0000-00-00';
        }   
            $dataProvider = new CActiveDataProvider('Order', array(
            'criteria' => array(
                'condition' => ($condition.'created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '" AND status=1' ), 'order' => 'created_at DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));
        
         
        $totalAmount = "";
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT package.price FROM `package`,`order` where order.user_id in (".$userID.") AND order.package_id = package.id AND order.created_at >= '" . $todayDate . "' AND order.created_at <= '" . $fromDate . "' AND order.status='1'");
        $row = $command->queryAll();
        
        foreach ($row as $amount) {
            $totalAmount += $amount['price'] * 5 / 100;
         }

        $this->render('admin_referral_income', array(
            'dataProvider' => $dataProvider,
            'totalAmount' => $totalAmount,
        ));
        
    }
    
    public function actionTrackReferral() {
       $error = "";
        $success = "";
        $todayDate = date('Y-m-d');
        $fromDate = date('Y-m-d');
        //$loggedInUserId = Yii::app()->session['userid'];
        //$userObject = User::model()->findByPK($loggedInUserId);
        $pageSize = Yii::app()->params['defaultPageSize'];
        if (!empty($_REQUEST)) {
            $todayDate = $_REQUEST['from'];
            $fromDate = $_REQUEST['to'];
            $dataProvider = new CActiveDataProvider('User', array(
                'criteria' => array(
                    'condition' => ('social != "" AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"'), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize)));
        } else {
            $dataProvider = new CActiveDataProvider('User', array(
                'criteria' => array(
                    'condition' => ('social != "" AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"'), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize)));
        }
        $this->render('/report/admintrack_referral', array(
            'error' => $error, 'success' => $success, 'dataProvider' => $dataProvider
        ));  
    }

    public function actionVerification() {
        $model = new UserProfile();
        $pageSize = Yii::app()->params['defaultPageSize'];
        $todayDate = Yii::app()->params['startDate'];
        $fromDate = date('Y-m-d');
        //By default Pending.
        $status = 0;

        $condition = 'address_proff !="" AND id_proof !="" AND status = ' . $status.' AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"';
        if (!empty($_REQUEST)) {
            // Add status.
            $condition = ' address_proff !="" AND id_proof !="" ';
            if (!empty($_REQUEST['from'])) {
                $todayDate = $_REQUEST['from'];
                $condition .= ' AND created_at >= "' . $todayDate . '"';
            }
            if (!empty($_REQUEST['to'])) {
                $fromDate = $_REQUEST['to'];
                $condition .= ' AND created_at <= "' . $fromDate . '"';
            }
            if (isset($status)) {
                 if(!empty($_REQUEST['res_filter'])){
                 $status = $_REQUEST['res_filter'];
                 }
                 $condition .= ' AND document_status = ' . $status;
            }
        }
	//echo $condition;exit;
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => $condition, //('created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '" AND status = "' . $status . '"' ), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));
        $this->render('verification', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionCompanySponsor() {
        $model = new User();
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $adminSpnId = Yii::app()->params['adminSpnId']; 
        $socialSpnId = Yii::app()->params['socialSpnId']; 
        $sponserList = $adminSpnId.",".$socialSpnId;
        $contactStatus = AdminUserContacted::$_CONTACTSTATUS_PENDING;
        $fromToDate = '';
        $status = 1;
        
        if(!empty($_GET['submit'])) { //For search
            if (!empty($_GET['from'])) {  
                $todayDate = date("Y-m-d",strtotime($_GET['from']));
                $fromDate = date("Y-m-d",strtotime($_GET['to']));
                $fromToDate = 'AND u.created_at >= "' . $todayDate . '" AND u.created_at <= "' . $fromDate . '"';
            }
            $status = $_GET['res_filter'];
            $sponsorGetList = "'".$_GET['sponsor_list']."'";
            $sponserList = !empty($_GET['sponsor_list']) ? $sponsorGetList : $sponserList;
            $contactStatus = $_GET['contact_status'];
        }
        
        if (!empty($_POST['bulkaction']) || !empty($_POST['requestids'])) { //For bulk actions
        $queryString = $_SERVER['QUERY_STRING'];
            if (!empty($_POST['bulkaction']) && !empty($_POST['requestids'])) {
                $arraySelectedId = $_POST['requestids'];
                $arrayComment = $_POST['comment'];
                $aUniques = array_intersect_key($arrayComment,$arraySelectedId);
                $this->companySponsorBulkActions($aUniques,$queryString);
               
            } else {
                Yii::app()->user->setFlash('error', 'Please select user');
                $this->redirect(array('/admin/report/companysponsor?'.$queryString)); 
            }  
        }
        
        $count = Yii::app()->db->createCommand()
            ->select('count(*)')
            ->from('user u')
            ->where('role_id = 1 AND sponsor_id IN (' . $sponserList . ') AND ifnull(contact_status,"'.AdminUserContacted::$_CONTACTSTATUS_PENDING.'") = "'.$contactStatus.'" AND status = "' . $status . '"' .$fromToDate)
            ->leftjoin('admin_user_contacted p', 'p.user_id=u.id')->queryScalar();
        
        $user = Yii::app()->db->createCommand()
            ->select('u.*, p.*, u.created_at as uCreatedDate, u.id as uid')
            ->from('user u')
            ->where('role_id = 1 AND sponsor_id IN (' . $sponserList . ') AND ifnull(contact_status,"'.AdminUserContacted::$_CONTACTSTATUS_PENDING.'") = "'.$contactStatus.'" AND status = "' . $status . '"' .$fromToDate)
            ->leftjoin('admin_user_contacted p', 'p.user_id=u.id');
                
        $dataProvider=new CSqlDataProvider($user, array(
            'totalItemCount' => $count,
            'sort' => [
                'defaultOrder'=>'uid DESC',
                'attributes' => [
                    'name', 'full_name', 'sponsor_id', 'email', 'uCreatedDate', 'unique_id', 'status', 
                ],
            ],
            'pagination' => array(
                'pageSize' => $pageSize,
            ),
        ));
        
        
        if(!empty($_GET['csv']) && ($_GET['csv'] == 1)) {
        $this->companySponsorCsvReport($dataProvider);
        }
        
        $this->render('companysponsor', array(
            'dataProvider' => $dataProvider,'pageSize' => $pageSize,
        ));
    }
    
    public function companySponsorBulkActions($aUniques, $queryString){
            if($_POST['bulkaction'] == 1) {
               foreach ($aUniques as $key => $comment) {
                   $contactResult = AdminUserContacted::model()->ubdateContactStatus($key, $comment, "CONTACTED");
               }
               Yii::app()->user->setFlash('success', 'Contact Status has been updated successfully.');
               $this->redirect(array('/admin/report/companysponsor?'.$queryString));
           }

           if($_POST['bulkaction'] == 2) {
               foreach ($aUniques as $key => $comment) {
                   $contactResult = AdminUserContacted::model()->ubdateContactStatus($key, $comment, "PENDING");                  
               }
               Yii::app()->user->setFlash('success', 'Pending Status has been updated successfully.');
               $this->redirect(array('/admin/report/companysponsor?'.$queryString));
           }

           if($_POST['bulkaction'] == 3) {
               foreach ($aUniques as $key => $comment) {
                   $contactResult = AdminUserContacted::model()->ubdateContactStatus($key, $comment, "");                  
               }
               Yii::app()->user->setFlash('success', 'Comment has been updated successfully.');
               $this->redirect(array('/admin/report/companysponsor?'.$queryString));
           }
    }
    
    public function companySponsorCsvReport($dataProvider){
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="sponsorlist.csv"');        
        $dataAdminReport = "User Name, Full Name, Sponsor, Email, Registered Date, Registered Via, Unique Id, Status, Contact Status, Comment\n";
        if(($dataProvider->getData()) > 0) {
            foreach($dataProvider->getData() as $data)
            {
                $sponsor = $data['sponsor_id'] == 1 ? "Admin" : 'Social';
                $social = $data['social'] != '' ? $data['social'] : 'Website';
                $status = $data['status'] == 1 ? 'Active' : 'Inactive';
                //$phone = $data['phone'] != 0 ? " +" . $data['country_code'] . "- ".$data['phone'] : 'NA'; 
                $uniqueId = $data['unique_id'] != '' ? $data['unique_id'] : 'NA';
                $contactStatus = ($data["contact_status"]) ? $data["contact_status"]:"PENDING";
                $dataAdminReport .= $data['name'] . "," . $data['full_name'] . "," . $sponsor ."," . $data['email'] .",". $data['uCreatedDate'] .",". $social.",". $uniqueId .",". $status .",". $contactStatus .",". preg_replace('/\s+/', ' ', $data['comment'])."\n";
            }
        }
        echo $dataAdminReport;
        exit();
    }

    public function GetComSpnsrCheckbox($data){
            echo "<input type='checkbox' class='allcheckbox' name='requestids[".$data['uid']."]' value='".$data['uid']."'>" ;
    } 

    public function getComSpnsrComment($data) {
        if(!empty($data)) {
            echo "<textarea rows='4' class='form-control' cols='50'  name='comment[".$data['uid']."]'>".$data['comment']."</textarea>";
        } else {
            echo "";
        }
    }
    
    public function actionProduct() {
        $model = new Product();
        $pageSize = Yii::app()->params['defaultPageSize'];
        $todayDate = Yii::app()->params['startDate'];
        $fromDate = date('Y-m-d');
        $status = 1;
        if (!empty($_POST)) {
            $todayDate = $_POST['from'];
            $fromDate = $_POST['to'];
            $status = $_POST['res_filter'];
        }

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ('created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '" AND status = "' . $status . '"' ), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));

        $this->render('package', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionSocialAccount() {
        $model = new UserProfile();
        $pageSize = Yii::app()->params['defaultPageSize'];
        $todayDate = Yii::app()->params['startDate'];
        $fromDate = date('Y-m-d');
        $status = 1;
        if (!empty($_POST)) {
            $todayDate = $_POST['from'];
            $fromDate = $_POST['to'];
            $status = $_POST['res_filter'];
        }

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ('facebook_id != "" and twitter_id != ""  AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '" AND status = "' . $status . '"' ), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));

        $this->render('socialaccount', array(
            'dataProvider' => $dataProvider,
        ));
    }

    
    
    protected function gridAddressImagePopup($data, $row) {
        $bigImagefolder = Yii::app()->params->imagePath['verificationDoc']; // folder with uploaded files
        echo "<a data-toggle='modal' href='#zoom_$data->id'>$data->address_proff</a>" . '<div class="modal fade" id="zoom_' . $data->id . '" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog" style="width:500px;">
                            <div class="modal-content">
                                <div class="modal-body" style="width: 500px;overflow: auto;height: 500px;padding: 0;">
                                    <img src="' . $bigImagefolder . $data->address_proff . '">
                                </div>
                            </div>
                        </div>
                </div>';
    }

    protected function gridIdImagePopup($data, $row) {
        $bigImagefolder = Yii::app()->params->imagePath['verificationDoc']; // folder with uploaded files
        echo "<a data-toggle='modal' href='#zoom1_$data->id'>$data->id_proof</a>" . '<div class="modal fade" id="zoom1_' . $data->id . '" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog" style="width:500px;">
                        <div class="modal-content">
                            <div class="modal-body" style="width: 500px;overflow: auto;height: 500px;padding: 0;">
                                <img src="' . $bigImagefolder . $data->id_proof . '">
                            </div>
                            </div>
                        </div>
                </div>';
    }
     public function getAddress($data, $row) {
        $addressObject =  UserProfile::model()->findByAttributes(array('user_id'=>$data->id));
        if(!empty($addressObject)){
            echo $addressObject->address;
        }
    }
    public function actionVendor() {
        $model = new User();
        $pageSize = Yii::app()->params['defaultPageSize'];
        $todayDate = Yii::app()->params['startDate'];
        $fromDate = date('Y-m-d');
        $status = 1;
        if (!empty($_POST)) {
            $todayDate = $_POST['from'];
            $fromDate = $_POST['to'];
            $status = $_POST['res_filter'];
        }

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ('created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '" AND status = "' . $status . '" AND role_id = 3' ), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));

        $this->render('vendor', array(
            'dataProvider' => $dataProvider,
        ));
    }
    
    public function actionBinaryReport() {
        $error = "";  
        $order = "date asc";
        $toDate = date('Y-m-d');
        $model = new BinaryGenerationReport();
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $fromDate = date('Y-m-d', strtotime('-1 day', strtotime($toDate)));
        
        if (!empty($_GET['from'])) {
            $fromDate = $_GET['from'];
            $toDate = $_GET['to'];
        }
        
        $condition = 'date >= "' . $fromDate . '" AND date <= "' . $toDate . '"' ;
        
        if (isset($_GET['search']) && $_GET['search'] != '') {
            $userObject = User::model()->findByAttributes(array('name' => $_GET['search']));
            if(!empty($userObject)) {
                $condition = 'date >= "' . $fromDate . '" AND date <= "' . $toDate . '"' . " AND user_id = " . $userObject->id ;
            }else{
                $error = "User doesn't exists";
                $condition = " user_id = 000 ";
            }
        }
        
        if (!empty($_GET['BinaryGenerationReport_sort'])) { $order = "'".$_GET['BinaryGenerationReport_sort']."' DESC ";}
        
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ($condition), 'order' => $order,
            ), 'pagination' => array('pageSize' => $pageSize),));
        
        $this->render('binaryreport', array(
            'dataProvider' => $dataProvider,
            'pageSize' => $pageSize,
            'error'=>$error
        ));
        
    }
    
    public function actionLoginActivities() {
        $model = new UserActivityLog();
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $order = "";
        $condition = "";
        $searchFlag = 3; // All
        
        if (isset($_GET['success_failed']) && ($_GET['success_failed'] == 0 || $_GET['success_failed'] == 1)) {
            $searchFlag = $_GET['success_failed'];
            $condition = " status = " . $searchFlag; 
        } else {
            $condition = " status IN (1,0)";  
        }       
                       
        if (!empty($_GET['from']) && !empty($_GET['to'])) {
            $fromDate = date("Y-m-d",strtotime($_GET['from']));
            $toDate = date("Y-m-d",strtotime($_GET['to']));
            $condition .= ' AND created_at >= "' . $fromDate . '" AND created_at <= "' . $toDate . '"';
        }
        
        if (isset($_GET['search']) && $_GET['search'] != '') {            
            if ($_GET['sort_by'] == "name") {
                $userObject = User::model()->findByAttributes(array('name' => $_GET['search']));
                if (!empty($userObject)) {
                    $condition .= " AND user_id =" . $userObject->id;
                } else {
                    $condition .= " AND user_id = 0";
                }
            } else {
                $condition .= " AND ip ='" . $_GET['search'] . "'";
            }
        }
        
        if (empty($_GET['UserActivityLog_sort'])) { $order = 'id DESC'; }
       
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ($condition), 'order' => $order,
            ), 'pagination' => array('pageSize' => $pageSize)));

        $this->render('user_login_activities', array(
            'dataProvider' => $dataProvider, 'pageSize' => $pageSize, 'searchFlag' => $searchFlag,
        ));
    }
    
    public function gridPhone($data) {
        BaseClass::gridProfilePhone($data);
    }

    public function gridOrderPhone($data) {
        $phone = "NA";
        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data->user_id));
        if ($userProfileObject && $userProfileObject->phone != 0 ) {
            $phone = "+" .$userProfileObject->country_code . "- ". $userProfileObject->phone;
        }
        echo $phone;
    }
    
    public function gridProfilePhoneNumber($data) {
        BaseClass::gridProfilePhone($data);
    }
    
    /* For the multicheckbox admin comment  */
    public function GetVerifyStatusCheckbox($data){
        if($data->admin_mark_status == 'NOT VERIFIED'){
            echo "<input type='checkbox' class='allcheckbox' name='requestids[".$data->id."]' value='".$data->id."'>" ;
        }    
    }
    
    public function GetEcurrancyStatusWithResponse($data) {
        $status = "";
        $myclass = Yii::app()->controller->action->id; 
        $gateway = 0;
        if($myclass == "transaction" || $myclass == "supporttransaction") {
            $gateway = $data->gateway_id ;
            $gatewayName = ($data->gateway())?$data->gateway()->name:"" ;
            $jsonResponse = $data->json_response ;            
        }else{
            if($data->transaction){
                $gateway = ($data->transaction)?$data->transaction->gateway_id:"" ;
                $gatewayName = ($data->transaction)?$data->transaction->gateway()->name:"" ; 
                $jsonResponse = $data->transaction->json_response ; 
            }    
        }
        
        if($gateway == 0 || $gateway == 8){
            $status = "N/A";
        }else{
            if(!empty($gateway)){
               $status ='<a data-toggle="modal" data-target="'.'#'.$data->transaction_id .'">'.$gatewayName.'</a>
                <!-- Modal -->
                    <div id="'.$data->transaction_id.'" class="modal fade" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Response Form Payment Gateway</h4>
                          </div>
                          <div class="modal-body">
                            <p>'.  $jsonResponse  .'</p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>

                      </div>
                    </div>';
               
               
            }else{
                $status = $gatewayName ;
            }    
        }
        echo $status ;        
    }
    
    public function CheckPaymentRefId($data) {
        if($data->status == 1) {
            if(!empty($data->payment_transaction_id)) {
                echo $data->payment_transaction_id;
            } else {
                if($data->mode == Transaction::$_MODE_ADDFUND || $data->mode == Transaction::$_MODE_TRANSFER || $data->mode == Transaction::$_MODE_ADMIN_ADDFUND) {
                    $queryStr =  "'".$_SERVER["QUERY_STRING"]."'" ;
                    echo '<a class="fancybox margin-right-20" onclick="transRefIdPopUp('.$data->transaction_id.','.$queryStr.')">Insert Payment Ref Id</a>';
                }
            }
        } else {
            echo '';
        }
    }
    
    public function userWalletAmount($data){
        echo $data->used_rp;
    }
    
    public function getFromUserName($data){ 
        $moneyTransferListObject = $data->moneytransfer();
        if(count($moneyTransferListObject)>0){
            echo isset($moneyTransferListObject[0]->fromuser->name)?$moneyTransferListObject[0]->fromuser->name:"";
        }
        else{
            echo $data->user()->name;
        }
    }
    
    public function getToUserName($data){ 
        $moneyTransferListObject = $data->moneytransfer();
        if(count($moneyTransferListObject)>0){
            echo isset($moneyTransferListObject[0]->touser->name)?$moneyTransferListObject[0]->touser->name:"";
        } else {
            echo "(Mavwealth)";
        }
    }
    
     public function transferType($data) {
        switch ($data->mode) {  
            case 'transfer':
               $transactionType =  'Fund Transfer'; 
               break;           
            default:
               $transactionType =  $data->mode; 
            break ;                               
       }    
       return $transactionType;    
    }
        
    public function moneyTransferComment($data){ 
        $moneyTransferListObject = $data->moneytransfer();
        if(count($moneyTransferListObject) > 0){
            echo $moneyTransferListObject['0']->comment;
        }else{
           echo ucfirst($data->mode);
        }
    }
    
    public function toUserBalance($data){
        $moneyTransferListObject = $data->moneytransfer();
        if(count($moneyTransferListObject)>0){
            echo $moneyTransferListObject['0']->to_user_balance;
        }
    }
    
    public function fromUserBalance($data){
        $moneyTransferListObject = $data->moneytransfer();
        if(count($moneyTransferListObject)>0){
            echo $moneyTransferListObject['0']->from_user_balance;
        }
    }
    
    public function GetWalletInfo($data){
        
        $moneyTransferListObject = array();
        $moneyTransferListObject = $data->moneytransfer();
        
         if(count($moneyTransferListObject) > 0){   
            $useWallet = Purchase::_getWalletUsed($moneyTransferListObject);
         } else {
             $useWallet = "N/A";
         }
        echo $useWallet;
    }
    
    public function getTrasactionStatus($data) {
        if($data->status == 1) {
            echo 'Completed';
        } else if($data->status == 0) {
            echo 'Pending';
        } else if($data->status == 3) {
            echo 'Processing';
        } else {
            echo 'Cancelled';
        }
    }
    
    public function GetVerifyStatusComment($data){ 
        $comment = $data->admin_comment ? $data->admin_comment:'' ;
        if($data->admin_mark_status == "VERIFIED"){
            echo $comment ;            
        }else{
            echo "<textarea rows='4' class='form-control' cols='50'  name='comment[".$data->id."]'>".$comment."</textarea>";
        }                    
    }
    
    
    
    public function actionBinaryReportCSV() {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="binary.csv"');        
        $binaryCenter = "Date,User,Left Purchase,Right Purchase,Left Carry,Right Carry,Previous Left Carry,Previous Right Carry,flush,Binary Commision,Created At \n";
        
        $toDate = date('Y-m-d');
        $fromDate = date('Y-m-d', strtotime('-1 day', strtotime($toDate)));
        $cond ="";
        $order = "id DESC"; 
        if (!empty($_GET['from'])) {
            $fromDate = $_GET['from'];
            $toDate = $_GET['to'];
        }
        if (isset($_GET['search']) && $_GET['search'] != '') {
            $userObject = User::model()->findByAttributes(array('name' => $_GET['search']));
            if(!empty($userObject)){
                $cond = " AND user_id =" . $userObject->id;
            }
        }
        if (empty($_GET['BinaryGenerationReport_sort'])) { $order = 'id DESC'; }
        
        $criteria = new CDbCriteria;
        $criteria->condition = ('date >= "' . $fromDate  . '" AND date <= "' . $toDate . '"' . $cond .' ORDER BY '. $order);
        $binaryObject = BinaryGenerationReport::model()->findAll($criteria);
        if (count($binaryObject) > 0) {
            foreach ($binaryObject as $binaryObjectList) {                                
                $binaryCenter .=                    
                    ($binaryObjectList->date).",". 
                    ((!empty($binaryObjectList->user)) ? $binaryObjectList->user->name : 'NA').",". 
                    ($binaryObjectList->left_purchase).",". 
                    ($binaryObjectList->right_purchase).",". 
                    ($binaryObjectList->left_carry).",". 
                    ($binaryObjectList->right_carry).",". 
                    ($binaryObjectList->previous_left_carry).",". 
                    ($binaryObjectList->previous_right_carry).",". 
                    ($binaryObjectList->flush).",". 
                    ($binaryObjectList->binary_commision).",". 
                    ($binaryObjectList->created_at).","."\n";
            }
        }
         echo $binaryCenter;
        exit();                
    }
    
    public function actionDownlineReport() {
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $success = '';
        $dateQuery = "";
        $condition = "";
        $emptyArray = array();
        $model = new Order();

        if (!empty($_GET['search'])) {
            $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
            /* fetch userObject */
            $userObject = User::model()->findByAttributes(array('name' => trim($_GET['search'])));

            if (empty($userObject)) {
                Yii::app()->user->setFlash('error', 'It seems user does not exists');
                $this->redirect('downlinereport');
            }

            $UserId = $userObject->id;
            $genealogyObject = Genealogy::model()->findByAttributes(array('user_id' => $UserId));
            
            if (empty($genealogyObject->right_users) && empty($genealogyObject->left_users)) {
                Yii::app()->user->setFlash('error', 'It seems user does not have downline users.');
                $this->redirect('downlinereport');
            } else {
                $prefix = "";
                if (!empty($genealogyObject->left_users) && !empty($genealogyObject->right_users)) {
                    $prefix = ",";
                }
                $userList = $genealogyObject->left_users . $prefix . $genealogyObject->right_users;
                $condition = ' AND t.user_id IN (' . $userList . ') AND t.type = "AddCash" and t.status = 1 ';
            }


            if (!empty($_GET['from'])) {
                $fromDate = $_GET['from'];
                $toDate = $_GET['to'];
                $dateQuery = 't.created_at >= "' . $fromDate . '" AND t.created_at <= "' . $toDate . '" AND ';
            }

            $order = "";
            if (empty($_GET['Order_sort'])) {
                $order = 't.id DESC';
            }

            $dataProvider = new CActiveDataProvider($model, array(
                'criteria' => array(
                    'with' => array('user', 'transaction'),
                    'condition' => ($dateQuery . ' t.status = 1' . $condition), 'order' => $order,
                ),
                'sort' => array(
                    'attributes' => array(
                        'user_name' => array('asc' => 'user.name', 'desc' => 'user.name DESC',),
                        'full_name' => array('asc' => 'user.full_name', 'desc' => 'user.full_name DESC',),
                        'paid_amt' => array('asc' => 'transaction.paid_amount', 'desc' => 'transaction.paid_amount DESC',),
                        '*',
                    ),
                ),
                'pagination' => array('pageSize' => $pageSize),));
        } else {
            $dataProvider = new CArrayDataProvider($emptyArray, array(
            'pagination' => array('pageSize' => $pageSize)));
        }
        $this->render('downlinereport', array(
            'dataProvider' => $dataProvider,
            'pageSize' => $pageSize,
        ));
    }

    public function actionRequestPayment() {
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $model = new OfflinePayment();
        $gatewayObject = Gateway::model()->findAll(array('condition'=> 'mode = "OFFLINE" '));
        $condition = "";
        $order = "";
        
        if(empty($_GET['OfflinePayment_sort'])) {
            $order = "created_at DESC";
        }
       
        if (!empty($_GET['from']) && !empty($_GET['to'])) {
            $fromDate = date('Y-m-d', strtotime($_GET['from']))." 00:00:01";
            $toDate = date('Y-m-d', strtotime($_GET['to']))." 23:59:59";
            $condition .= 'created_at >= "' . $fromDate . '" AND created_at <= "' . $toDate . '"';
        }
        
        if(!empty($_GET['res_filter'])){
                $status = $_GET['res_filter'];
                if(!empty($condition)){
                    $condition .= ' AND ';  
                }
                $condition .= 'gateway_id = "' . $status . '"';            
        }
        
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array( 
                        'condition' => ($condition), 'order' => $order,
            ), 'pagination' => array('pageSize' => $pageSize)));
        
        $this->render('requestpayment', array('dataProvider' => $dataProvider, 
            'pageSize' => $pageSize, 'gatewayObject' => $gatewayObject));
    }
    
    public function actionRequestPaymentCSV() {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="requestpayment.csv"');        
        $dataOfflinePayment = "Name,Email,Gateway,Country,Phone,Amount,Req.Ip,Req.Country,Created At \n";
        $country = "NA";
        $condition = "";
        if (!empty($_GET['from']) && !empty($_GET['to'])) {
            $fromDate = date('Y-m-d', strtotime($_GET['from']))." 00:00:01";
            $toDate = date('Y-m-d', strtotime($_GET['to']))." 23:59:59";
            $condition .= 'created_at >= "' . $fromDate . '" AND created_at <= "' . $toDate . '"';
        }
        
        if(!empty($_GET['res_filter'])){
                if((!empty($_GET['from']) && !empty($_GET['to'])) && !empty($_GET['res_filter'])  ){
                    $condition .= ' AND ';  
                }
                $condition .= ' gateway_id = ' . $_GET['res_filter'] ;            
        }   
                
        $criteria = new CDbCriteria;
        $criteria->condition = ($condition);
        $criteria->order =  "created_at DESC"; 
        $offlinePaymentObject = OfflinePayment::model()->findAll($criteria);
        if (count($offlinePaymentObject) > 0) {
            foreach ($offlinePaymentObject as $offlinePaymentList) { 
                $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $offlinePaymentList->user_id));
                $phone = "";
                if(!empty($userProfileObject->phone)){
                    $phone .= isset($userProfileObject->country_code)?"+".$userProfileObject->country_code:"";
                    $phone .= " ".$userProfileObject->phone;
                    
                }
                if(!empty($userProfileObject)){
                    $country = isset($userProfileObject->country->name)?$userProfileObject->country->name:"NA";
                }
                $dataOfflinePayment .=                    
                    ($offlinePaymentList->user->name).",". 
                    ($offlinePaymentList->user->email ).",". 
                    ($offlinePaymentList->gateway_id ? $offlinePaymentList->gateway->name : 'NA').",". 
                    ($country).",".
                    ($phone)."," .
                    ($offlinePaymentList->amount).",". 
                    ($offlinePaymentList->actual_ip ? $offlinePaymentList->actual_ip : '').",".
                    ($offlinePaymentList->actual_country ? $offlinePaymentList->actual_country : '').",".
                    ($offlinePaymentList->created_at).",". 
                    "\n";
                }
        }
         echo $dataOfflinePayment;
        exit;        
    }
    
    public function isCashWalletUsed($data){
        $moneyTransferListObject = $data->moneytransfer(); //has many money transfer
        $useWallet = 0;
         if(count($moneyTransferListObject) > 0){  
            foreach ($moneyTransferListObject as $moneyTransferObject){
                $walleType = $moneyTransferObject->to_fund_type ;
                if($walleType == 1){
                    $useWallet = $moneyTransferObject->fund;
                }
            }
         }  
        echo $useWallet;
    }
    
    public function getContactStatus($data) {
        $adminUserContactObject = AdminUserContacted::model()->findByAttributes(array('user_id' => $data->id));
        if(!empty($adminUserContactObject)) {
            echo $adminUserContactObject->status;
        } else {
            echo 'PENDING';
        }
    }
    
    public function getComment($data) {
        if(!empty($data)) {
            echo "<textarea rows='4' class='form-control' cols='50'  name='comment[".$data['uid']."]'>".$data['comment']."</textarea>";
        } else {
            echo "";
        }
    }
    
    public function actionOrderList() {
        $this->render('orderlist');
    }   
    
    public function actionOrderListData(){
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
        $searchValue = "";
        if(!empty($_POST['search']['value'])){
            $searchValue = 'transaction_id = '.$_POST['search']['value'] . ' AND ';
        }
        
        $statusCondition = 'o.status = 1';
         if($_POST['columns'][7]['search']['value'] == 2) {
              $statusCondition = 'o.status = 0';
         }
         if($_POST['columns'][7]['search']['value'] == 'all') {
              $statusCondition = 'o.status IN (1,0)';
         }
        $dataQuery = Yii::app()->db->createCommand()
            ->select('o.id as oid,o.package_price,u.id,t.transaction_id as transactionId,u.name as userName,o.start_date,o.end_date,o.created_at,p.name as packageName, '
                    . '(case when o.status = 0 then "Pending" ELSE "Completed" END ) as orderStatus')
            ->from('order o')
            ->join('user u', 'u.id=o.user_id')
            ->join('package p', 'p.id=o.package_id')
            ->join('transaction t', 't.id=o.transaction_id');
            $dataQuery->where($statusCondition);
            if(!empty($_POST['search']['value'])){
                $searchData = $_POST['search']['value'];
                $dataQuery->andWhere('u.name LIKE :username', array(':username' => '%'.$searchData.'%'));
                $dataQuery->orWhere('t.transaction_id LIKE :transactionId', array(':transactionId' => '%'.$searchData.'%'));
                $dataQuery->orWhere('p.name LIKE :packageName', array(':packageName' => '%'.$searchData.'%'));
                $dataQuery->orWhere('o.start_date LIKE :startDate', array(':startDate' => '%'.$searchData.'%'));
                $dataQuery->orWhere('o.end_date LIKE :endDate', array(':endDate' => '%'.$searchData.'%'));
            }
            if(!empty($_POST['columns']['6']['search']['value']) && !empty($_POST['columns']['6']['search']['regex'])){
                $todayDate = $_POST['columns']['6']['search']['value'];
                $fromDate = $_POST['columns']['6']['search']['regex'];
                $dataQuery->andWhere('o.created_at <= :todate AND o.created_at >= :fromdate', array(':todate' => $todayDate, ':fromdate' => $fromDate));
            }
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $userListObject = $dataQuery->queryAll();
        //Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->from('order o')
            ->join('user u', 'u.id=o.user_id')
            ->join('package p', 'p.id=o.package_id')
            ->join('transaction t', 't.id=o.transaction_id');
            $countQuery->where($statusCondition);
            if(!empty($_POST['search']['value'])){
                $searchData = $_POST['search']['value'];
                $countQuery->andWhere('u.name LIKE :username', array(':username' => '%'.$searchData.'%'));
                $countQuery->orWhere('t.transaction_id LIKE :transactionId', array(':transactionId' => '%'.$searchData.'%'));
                $countQuery->orWhere('p.name LIKE :packageName', array(':packageName' => '%'.$searchData.'%'));
                $countQuery->orWhere('o.start_date LIKE :startDate', array(':startDate' => '%'.$searchData.'%'));
                $countQuery->orWhere('o.end_date LIKE :endDate', array(':endDate' => '%'.$searchData.'%'));
            }
            if(!empty($_POST['columns']['6']['search']['value']) && !empty($_POST['columns']['6']['search']['regex'])){
                $todayDate = $_POST['columns']['6']['search']['value'];
                $fromDate = $_POST['columns']['6']['search']['regex'];
                $countQuery->andWhere('o.created_at <= :todate AND o.created_at >= :fromdate', array(':todate' => $todayDate, ':fromdate' => $fromDate));
            }
            $totalRecordsArray = $countQuery->queryRow();
            
        $userObjectCount = $totalRecordsArray['totalRecords'];

        $userJSONData = CJSON::encode($userListObject);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $userObjectCount . ',
                    "recordsFiltered": ' . $userObjectCount . ',
                    "data":' . $userJSONData . '
        }';
        exit;
    }

        public function actionOrderListCSV() {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="orderlist.csv"');        
        $dataMediaCenter = "User,Package Name ,Purchased Date,Ecurrency Amt, Wallet Amt , Wallet Type , Payment , Ecurrency Details \n";
         
        $fromDate = Yii::app()->params['startDateLastSevenDay']." 00:00:01"; 
        $toDate = date('Y-m-d')." 23:59:59"; 
        
        $condition = "";
       
        if (!empty($_GET['submit'])) {
            $fromDate = date('Y-m-d', strtotime($_GET['from']))." 00:00:01";
            $toDate = date('Y-m-d', strtotime($_GET['to']))." 23:59:59";
            if($_GET['res_filter'] != ''){
                $condition .= ' AND status = "' . $_GET['res_filter'] . '"';            
            }
        }
                
        $order = "created_at DESC"; 
        $criteria = new CDbCriteria;
        $criteria->condition = ('created_at >= "' . $fromDate . '" AND created_at <= "' . $toDate . '"'.$condition. ' ORDER BY '. $order);
        $orderObject = Order::model()->findAll($criteria);
        if (count($orderObject) > 0) {
            foreach ($orderObject as $orderObjectList) {  
                $status = ($orderObjectList->status == 0) ? "Pending" : (($orderObjectList->status == 1) ? "Completed" : "Cancelled") ;
                $moneyTransferObject = MoneyTransfer::model()->findByAttributes(array('transaction_id' => $orderObjectList->transaction_id)); 
                if(count($moneyTransferObject) > 0  && ($orderObjectList->transaction->paid_amount != $orderObjectList->transaction->used_rp) ){                     
                    $walleType = $moneyTransferObject->to_fund_type ;
                    switch ($walleType) {                
                        case 1:
                            $walleType = 'Bid';
                            break;
                        case 2:
                            $walleType = 'CashBack';
                            break;               
                        default:
                            $walleType = 'NA';
                    } 
                }else
                $walleType = "NA";                
                $dataMediaCenter .=                    
                    ($orderObjectList->user->name ).",". 
                    ($orderObjectList->package->name ? $orderObjectList->package->name : 'NA').",".
                    ($orderObjectList->created_at).",". 
                    ($orderObjectList->transaction->paid_amount).",". 
                    ($orderObjectList->transaction->used_rp).",". 
                    ($walleType).","  .
                    ($status).",". 
                    (($orderObjectList->transaction()->gateway_id == 0 ) ? "NA" : $orderObjectList->transaction()->gateway()->name).",". "\n";
                }
        }
         echo $dataMediaCenter;
        exit;        
    }
    
    public function GetPaymentStatus($data) {
        $paymentStatus = "NA";
        $orderObject = Order::model()->findByAttributes(array('id' => $data->id));
        if(count($orderObject) > 0) {
            if($orderObject->status == 0) {
                $paymentStatus = 'Pending';
            } else if($orderObject->status == 1) {
                $paymentStatus = 'Completed';
            } else {
                $paymentStatus = 'Cancelled';
            }
        }
        echo $paymentStatus;
    }
    
    public function actionSupportTransaction() {
        $this->render('support_transaction');
    }

    public function actionSupportData() {
        
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        $fromDate = date('Y-m-d'); 
        $todayDate = Yii::app()->params['startDateLastSevenDay']; 
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
                
        $transactionObject = array();

        if(!empty($_POST['columns']['1']['search']['value']) && !empty($_POST['columns']['1']['search']['regex']) && $_POST['columns']['1']['search']['value'] != 'admin'  && strlen(trim($_POST['columns']['1']['search']['value'])) != 0){ 
            
            $dataQuery = Yii::app()->db->createCommand()                       
            ->select('t.id,t.mode,t.transaction_id ,t.created_at , t.actual_amount ,t.paid_amount ,t.used_rp ,t.payment_transaction_id ,t.admin_mark_status,t.admin_comment,'
                    . 'COALESCE(touser.name,u.name) as toname,COALESCE(fromuser.name,u.name) as fromname ,g.name as gatewayName , w.name as walletName ,'
                    .'(case when t.status = 2 then "Cancelled" when t.status = 1 then "COMPLETED" ELSE "PENDING" END ) as txnStatus')
            ->from('transaction t')
            ->leftjoin('money_transfer m', ' m.transaction_id = t.id')
            ->leftjoin('user u', 'u.id= t.user_id and u.role_id != 3')
            ->leftjoin('user touser', 'm.to_user_id = touser.id')
            ->leftjoin('user fromuser', 'm.from_user_id = fromuser.id')
            ->leftjoin('gateway g', 'g.id = t.gateway_id')
            ->leftjoin('wallet_type w', 'w.id = m.from_fund_type');
             
            if(!empty($_POST['search']['value'])){
                $searchData = $_POST['search']['value'];
                $dataQuery->where('t.transaction_id LIKE :transactionId', array(':transactionId' => '%'.$searchData.'%'));
                $dataQuery->orWhere('t.payment_transaction_id LIKE :paymenttransactionId', array(':paymenttransactionId' => '%'.$searchData.'%'));
                $dataQuery->orWhere('touser.name LIKE :toUser', array(':toUser' => '%'.$searchData.'%'));
                $dataQuery->orWhere('fromuser.name LIKE :fromUser', array(':fromUser' => '%'.$searchData.'%'));
            }
             
            $searchValue = $_POST['columns']['1']['search']['value']; 
            $searchBy = $_POST['columns']['2']['search']['value'];
            if($searchBy == 'name'){  
                $dataQuery->andWhere('fromuser.name = :name', array(':name' => $searchValue));
                $dataQuery->orWhere('touser.name = :name', array(':name' => $searchValue));
            }elseif($searchBy == 'transaction_id'){
                $dataQuery->andWhere('t.transaction_id = :transactionId', array(':transactionId' => $searchValue)); 
            }elseif ($searchBy == 'payment_transaction_id'){
                $dataQuery->andWhere('t.payment_transaction_id = :paymentTransactionId', array(':paymentTransactionId' => $searchValue));
            } 
            $dataQuery->order($orderBy)->limit($limit)->offset($offset);
            $transactionObject = $dataQuery->queryAll();
        }
        
        $totalRecordsArray = array();
        $supportTransactionCount = 0;
        
        if(!empty($_POST['columns']['1']['search']['value']) && !empty($_POST['columns']['1']['search']['regex']) && $_POST['columns']['1']['search']['value'] != 'admin'  && strlen(trim($_POST['columns']['1']['search']['value'])) != 0 ){
            
            //Get total record in the datablse with condition
            $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->from('transaction t')
            ->leftjoin('money_transfer m', ' m.transaction_id = t.id')
            ->leftjoin('user u', 'u.id= t.user_id and u.role_id != 3')
            ->leftjoin('user touser', 'm.to_user_id = touser.id')
            ->leftjoin('user fromuser', 'm.from_user_id = fromuser.id')
            ->leftjoin('gateway g', 'g.id = t.gateway_id')
            ->leftjoin('wallet_type w', 'w.id = m.from_wallet_id');
            
             if(!empty($_POST['search']['value'])){
                $searchData = $_POST['search']['value'];
                $countQuery->where('t.transaction_id LIKE :transactionId', array(':transactionId' => '%'.$searchData.'%'));
                $countQuery->orWhere('t.payment_transaction_id LIKE :paymenttransactionId', array(':paymenttransactionId' => '%'.$searchData.'%'));
                $countQuery->orWhere('touser.name LIKE :toUser', array(':toUser' => '%'.$searchData.'%'));
                $countQuery->orWhere('fromuser.name LIKE :fromUser', array(':fromUser' => '%'.$searchData.'%'));
            }
            
            $searchValue = $_POST['columns']['1']['search']['value']; 
            $searchBy = $_POST['columns']['2']['search']['value'];
            if($searchBy == 'name'){  
		$countQuery->andWhere('fromuser.name = :name', array(':name' => $searchValue));
                $countQuery->orWhere('touser.name = :name', array(':name' => $searchValue));
            }elseif($searchBy == 'transaction_id'){
                $countQuery->andWhere('t.transaction_id = :transactionId', array(':transactionId' => $searchValue)); 
            }elseif ($searchBy == 'payment_transaction_id'){
                $countQuery->andWhere('t.payment_transaction_id = :paymentTransactionId', array(':paymentTransactionId' => $searchValue));
            } 
            $totalRecordsArray = $countQuery->queryRow();
            $supportTransactionCount = $totalRecordsArray['totalRecords'];
        }

        $supportTransactionJSONData = CJSON::encode($transactionObject);
        echo '{"draw": '.$draw.',
                "recordsTotal": ' . $supportTransactionCount . ',
                "recordsFiltered": ' . $supportTransactionCount . ',
                "data":' . $supportTransactionJSONData . '
        }';
        exit;
        
    }

    public function actionTransaction() {
        
        $model = new Transaction();
        $gatewayObject = Gateway::model()->findAll(array('condition' => 'payment_status = 1'));
        $error = '';
        $condition = '';
        
        $success = '';  
        if(!empty($_POST['requestids'])){                
            $arrayRequestedId = $_POST['requestids'];
            $arrayComment = $_POST['comment'];
            $aUniques = array_intersect_key($arrayComment,$arrayRequestedId);
            // for the Trasation request update.
            BaseClass::updateTransationStatus($aUniques);
            $success = "Transation has been verified successfully.";
        }
        
        $order = "";
        if(empty($_GET['Transaction_sort'])){ $order = 't.id DESC'; }
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $todayDate = Yii::app()->params['startDate'];
        $fromDate = date('Y-m-d');
        $status = 1;         
        
        if (!empty($_GET['from'])) { 
            $todayDate = $_GET['from'];
            $fromDate = $_GET['to'];
            if(isset($_GET['res_filter'])) {
                $status = $_GET['res_filter'];
            }
            if(!empty($_GET['type'])) {
                $condition .= ' mode = "'.$_GET['type'].'" AND';
            }
            
            if(!empty($_GET['ecurrency'])) {
                $condition .= ' gateway_id = '.$_GET['ecurrency'].' AND';
            }
            if(!empty($_GET['isverified'])) {
                $condition .= ' admin_mark_status = "'.$_GET['isverified'].'" AND';
            }             
        }
        $condition .= ' t.status = ' . $status .' AND t.actual_amount > 0 AND t.created_at >= "' . $todayDate . '" AND t.created_at <= "' . $fromDate . '"'; 
        
        $criteria = new CDbCriteria();
        $criteria->distinct=true;
        if(isset($_GET['search'])) {
            
            $userObject = User::model()->findByAttributes(array('name' => $_GET['searchvalue']));
            if(!empty($_GET['searchvalue'])) {
                if (isset($_GET['searchby']) && $_GET['searchby'] == 'name') {
                    if (!empty($userObject)) {
                        $criteria->join = 'LEFT JOIN money_transfer m  ON t.id = m.transaction_id ' ; 
                        $condition = " ( m.from_user_id =" . $userObject->id . " OR m.to_user_id =" . $userObject->id . " OR t.user_id =" . $userObject->id . ")";
                    }else{
                        $condition = "t.status = 123465";
                    }
                } else if (isset($_GET['searchby']) && $_GET['searchby']) {
                    if(!empty($_GET['searchvalue'])) { 
                        $condition = " t.".$_GET['searchby']." = '" . $_GET['searchvalue'] . "'";  
                    } else{
                        $condition = "t.status = 123465";
                    }
                }  
            } 
        }   
             
        $criteria->join .= 'LEFT JOIN user u ON u.id = t.user_id ' ;         
        $criteria->addCondition($condition);
        $criteria->order = $order;
        
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => $criteria,
            'sort'=>array(
                'attributes'=>array( 
                    'user_name'=>array('asc'=>'u.name','desc'=>'u.name DESC',),
                    '*',
                ),
            ),
            'pagination' => array('pageSize' => $pageSize),));
        
        $this->render('transaction', array(
            'dataProvider' => $dataProvider,
            'gatewayObject' => $gatewayObject,
            'pageSize' => $pageSize,
            'error' => $error,
            'success' => $success,
        ));
    }
    
    public function actionTransactionCSV(){
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="tranction.csv"');        
        $dataTransfer = "Transaction Id,Date,From User, To User, Amount,Paid Amt,Wallet Amt,Ecurrency,Payment Ref Id,Status,Comment,Transfer Type,Is Verified, Verification Comment\n";  
        
        $model = new Transaction();
        $gatewayObject = Gateway::model()->findAll(array('condition' => 'payment_status = 1'));
        $error = '';
        $condition = '';
        $success = '';  
        $status = 1; 
        $order = "";
        if(empty($_GET['Transaction_sort'])){ $order = 't.id DESC'; }
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $todayDate = Yii::app()->params['startDate'];
        $fromDate = date('Y-m-d');
        
       if (!empty($_GET['from'])) { 
            $todayDate = $_GET['from'];
            $fromDate = $_GET['to'];
            if(isset($_GET['res_filter'])) {
                $status = $_GET['res_filter'];
            }
            if(!empty($_GET['type'])) {
                $condition .= ' mode = "'.$_GET['type'].'" AND';
            }
            
            if(!empty($_GET['ecurrency'])) {
                $condition .= ' gateway_id = '.$_GET['ecurrency'].' AND';
            }
            if(!empty($_GET['isverified'])) {
                $condition .= ' admin_mark_status = "'.$_GET['isverified'].'" AND';
            }             
        }
        $condition .= ' t.status = ' . $status .' AND t.actual_amount > 0 AND t.created_at >= "' . $todayDate . '" AND t.created_at <= "' . $fromDate . '"'; 
        
        $criteria = new CDbCriteria();
        $criteria->distinct=true;
        if(isset($_GET['search'])) {
            $userObject = User::model()->findByAttributes(array('name' => $_GET['searchvalue']));
            if(!empty($_GET['searchvalue'])) {
                if (isset($_GET['searchby']) && $_GET['searchby'] == 'name') {
                    if (!empty($userObject)) {
                        $criteria->join = 'LEFT JOIN money_transfer m  ON t.id = m.transaction_id ' ; 
                        $condition = " ( m.from_user_id =" . $userObject->id . " OR m.to_user_id =" . $userObject->id . " OR t.user_id =" . $userObject->id . ")";
                    } 
                } else if (isset($_GET['searchby']) && $_GET['searchby']) {
                    if(!empty($_GET['searchvalue'])) {    
                        $condition = " t.".$_GET['searchby']." = '" . $_GET['searchvalue'] . "'";  
                    } 
                }  
            } 
        }   
             
        $criteria->join .= 'LEFT JOIN user u ON u.id = t.user_id ' ;         
        $criteria->addCondition($condition);
        $criteria->order = $order;
        $transactionObjects = Transaction::model()->findAll($criteria);
        if (count($transactionObjects) > 0) {
            foreach ($transactionObjects as $transactionObjectsList) {
                $moneyTransferObject = MoneyTransfer::model()->findByAttributes(array('transaction_id' => $transactionObjectsList->id));
                if(!empty($moneyTransferObject)) {
                    $fromUser = $moneyTransferObject->fromuser()->name;
                    $toUser = $moneyTransferObject->touser()->name;
                    $comment = $moneyTransferObject->comment;
                } else {
                    $fromUser = $transactionObjectsList->user()->name;
                    $toUser = '(Mavwealth)';
                    $comment = $transactionObjectsList->mode;
                }
                if($transactionObjectsList->status == 1) { $transactionStatus = 'Completed'; }
                else if($transactionObjectsList->status == 0) { $transactionStatus = 'Pending'; }
                else { $transactionStatus = 'Cancelled'; }
                        
                $transactionType = $transactionObjectsList->mode ;
                
                $dataTransfer .=
                    ($transactionObjectsList->transaction_id). ",".
                    ($transactionObjectsList->created_at).",".
                    ($fromUser).",".
                    ($toUser).",".
                    ($transactionObjectsList->actual_amount).",".                    
                    ($transactionObjectsList->paid_amount) . ",".
                    ($transactionObjectsList->used_rp ? $transactionObjectsList->used_rp : 0). ",".
                    ($transactionObjectsList->gateway->name ? $transactionObjectsList->gateway->name : 'NA').",".    
                    ($transactionObjectsList->payment_transaction_id ? $transactionObjectsList->payment_transaction_id : 'NA').",".    
                    ($transactionStatus).",".
                    ($comment).",".
                    ($transactionObjectsList->mode ?  $transactionObjectsList->mode : 'NA').",".
                    ($transactionObjectsList->admin_mark_status ?  $transactionObjectsList->admin_mark_status : 'NA').",".
                    ($transactionObjectsList->admin_comment ?  $transactionObjectsList->admin_comment : 'NA')."\n";
            }
        }
        echo $dataTransfer;
        exit();        
    }
    
    public function actionBulkAction(){
                
        if (isset($_POST['requestids']) && !empty($_POST['requestids'])) {
            
            $arrayRequestedId = $_POST['requestids'];
            $arrayComment = $_POST['comment'];
            $aUniques = array_intersect_key($arrayComment,$arrayRequestedId);
            $updateStatus = BaseClass::updateTransationStatus($aUniques);
            echo 1;exit;
        }
        echo 0;exit;
    }
    
    public function actionRequestPaymentRefId(){ 
        
        if(isset($_POST['transaction_id'])) {  
            $queryStr = $_REQUEST['queryStr'];
            if (strpos($queryStr, 'transaction_id='.$_POST['transaction_id'].'&') !== false) {
                $queryStr =  str_replace("transaction_id=9532919082516&","",$queryStr);
            }
           $transactionObject = Transaction::model()->findByAttributes(array('transaction_id' => $_POST['transaction_id']));
           if(!empty($transactionObject)){ 
                $criteria = new CDbCriteria();
                $criteria->condition = 'payment_transaction_id = "'.$_POST['payment_ref_id'].'"';
                $transactionPaymentObject = Transaction::model()->count($criteria);
                unset($_POST['transaction_id']);
                
                if(empty($transactionPaymentObject)){
                    $transactionObject->payment_transaction_id = $_POST['payment_ref_id'];
                    if ($transactionObject->save(false)) {
                        Yii::app()->user->setFlash('success', "Payment Reference Id Updated Successfully.");
                        $this->redirect(array('transaction?'.$queryStr));
                    }
                } else {
                    Yii::app()->user->setFlash('error', "Duplicate Payment Ref Id Found.");
                    $this->redirect(array('transaction?'.$queryStr));
                } 
            }
        }
        $this->renderPartial('requestpaymentrefid');
    }
    
    public function actionProfitability(){
        $this->render('profitability');
    }
    
    public function actionProfitabilityData() {

        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
 
        $dataQuery = Yii::app()->db->createCommand()
            ->select('a.id as auctionId, p.name as productName, a.close_date as endDate, ifnull(u.name,"Not Declared") as winnerName,
            (case when u.role_id = 1 then "Regular" when u.role_id = 3 then "Preferential" ELSE "--" END) as winnerUserType,
            (case when a.status = 1 then "Open" ELSE "Close" END) as auctionStatus,
            (select (count(ab.auction_id) * a.entry_fees) from auction_bids ab  join user U1 on ab.user_id = U1.id and U1.role_id = 1  where ab.auction_id  = a.id ) as amountCollected')
            ->from('auction a')
            ->join('product p', 'p.id = a.product_id')
            ->leftjoin('user u', 'u.id = a.winner_user_id');
        
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $dataQuery->andWhere('p.name LIKE :productName OR u.name LIKE :userName', array(':productName' => '%'.$searchData.'%', ':userName' => '%'.$searchData.'%'));
        }
        
        $auctionStatus = 0;
        if($_POST['columns']['6']['search']['value']=='active') {
            $auctionStatus = 1;
        }
        $dataQuery->andWhere('a.status = :status', array(':status' => $auctionStatus));
        
        if(!empty($_POST['columns']['2']['search']['value'])){ 
            $todayDate = $_POST['columns']['2']['search']['value']." 00:00:01";
            $fromDate = $_POST['columns']['2']['search']['regex']." 23:59:59";
            $dataQuery->andWhere('a.close_date >= :todate', array(':todate' => $todayDate));
            $dataQuery->andWhere('a.close_date <= :fromdate', array(':fromdate' => $fromDate));
        }
        
        
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $profitabilityObject = $dataQuery->queryAll();
        
//        print_r($dataQuery); die;
        
        $profitabilityCount = count($profitabilityObject);
        
        $profitabilityJSONData = CJSON::encode($profitabilityObject);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $profitabilityCount . ',
                    "recordsFiltered": ' . $profitabilityCount . ',
                    "data":' . $profitabilityJSONData . '
        }';
        exit;
    }
    
    public function actionContact() {  
        
        $criteria= new CDbCriteria;
        $criteria->distinct = true ;
        $criteria->select = 'distinct(type)' ;
        $contactObject = Contact::model()->findAll($criteria); 
        $this->render('contact', array('contactObject' => $contactObject));
    }
    
    public function actionContactData() {
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        $fromDate = date('Y-m-d'); 
        $todayDate = Yii::app()->params['startDateLastSevenDay'];
         
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString; 
        }
        
        if(!empty($_POST['columns']['2']['search']['value']) && !empty($_POST['columns']['2']['search']['regex'])){
            $todayDate = $_POST['columns']['2']['search']['value'];
            $fromDate = $_POST['columns']['2']['search']['regex'];
        }
       
        //Get total record in the datablse with condition for grid view
        $dataQuery = Yii::app()->db->createCommand()                       
        ->select('a.*,c.name as cname,CONCAT("+",a.country_code," ",a.phone) as phoneNuber')
        ->from('contact a')
        ->leftjoin('country c', 'c.id = a.country_id');        
        
        $dataQuery->where('a.created_at >= :todate', array(':todate' => $todayDate));
        $dataQuery->andWhere('a.created_at <= :fromdate', array(':fromdate' => $fromDate));
        
        if(!empty($_POST['search']['value'])){ 
            $searchData = $_POST['search']['value'];
            $dataQuery->andWhere('a.name LIKE :name', array(':name' => '%'.$searchData.'%'));
            $dataQuery->andWhere('a.email LIKE :email', array(':email' => '%'.$searchData.'%'));
        }
                        
        if(!empty($_POST['columns']['1']['search']['value']) &&  $_POST['columns']['1']['search']['value']){ 
            $type =  $_POST['columns']['1']['search']['value']; 
            $dataQuery->andWhere('a.type = :type', array(':type' => $type));
        }
       
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $userListObject = $dataQuery->queryAll();
        
        //Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->from('contact a')
            ->leftjoin('country c', 'c.id = a.country_id'); 

        $countQuery->where('a.created_at >= :todate', array(':todate' => $todayDate));
        $countQuery->andWhere('a.created_at <= :fromdate', array(':fromdate' => $fromDate));
        
        if( !empty($_POST['columns']['1']['search']['value']) &&  $_POST['columns']['1']['search']['value']){ 
            $type =  $_POST['columns']['1']['search']['value']; 
            $countQuery->andWhere('a.type = :type', array(':type' => $type));
        }
        
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $countQuery->andWhere('a.name LIKE :name', array(':name' => '%'.$searchData.'%'));
            $countQuery->andWhere('a.email LIKE :email', array(':email' => '%'.$searchData.'%'));
        }
        
        $totalRecordsArray = $countQuery->queryRow();
        $userObjectCount = $totalRecordsArray['totalRecords'];

        $userJSONData = CJSON::encode($userListObject);
        echo '{"draw": '.$draw.',
                "recordsTotal": ' . $userObjectCount . ',
                "recordsFiltered": ' . $userObjectCount . ',
                "data":' . $userJSONData . '
        }';
        exit;
    }
    
    public function actionAgentcontact(){
        $this->render('agent_contact');
    }
    
    public function actionAgentcontactData() {
        $uid = Yii::app()->session['userid'] ;
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        $fromDate = date('Y-m-d'); 
        $todayDate = Yii::app()->params['startDateLastSevenDay'];
        
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString; 
        }
        
        if(!empty($_POST['columns']['2']['search']['value']) && !empty($_POST['columns']['2']['search']['regex'])){
            $todayDate = $_POST['columns']['2']['search']['value'];
            $fromDate = $_POST['columns']['2']['search']['regex'];
        }
       
        //Get total record in the datablse with condition for grid view
        $dataQuery = Yii::app()->db->createCommand()                       
        ->select('a.name , a.email, u.full_name as fname, a.message,a.country_code,a.phone,a.created_at,a.updated_at,a.actual_ip,a.actual_country,c.name as cname,CONCAT("+",a.country_code," ",a.phone) as phoneNuber')
        ->from('agent_contact_list a')
        ->leftjoin('country c', 'c.id = a.country_id')        
        ->leftjoin('user u', 'u.id = a.contact_agent_id');        
        
        if(!empty($_POST['search']['value'])){ 
            $searchData = $_POST['search']['value'];
            $dataQuery->andWhere('a.name LIKE :name', array(':name' => '%'.$searchData.'%'));
            $dataQuery->orWhere('a.email LIKE :email', array(':email' => '%'.$searchData.'%'));
            $dataQuery->orWhere('u.full_name LIKE :fname', array(':fname' => '%'.$searchData.'%'));
        }
        
        $dataQuery->andWhere('a.created_at >= :todate', array(':todate' => $todayDate));
        $dataQuery->andWhere('a.created_at <= :fromdate', array(':fromdate' => $fromDate));
       
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $userListObject = $dataQuery->queryAll();
        
        //Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->from('agent_contact_list a')
            ->leftjoin('country c', 'c.id = a.country_id')   
	    ->leftjoin('user u', 'u.id = a.contact_agent_id');    
	 
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $countQuery->andWhere('a.name LIKE :name', array(':name' => '%'.$searchData.'%'));
            $countQuery->orWhere('a.email LIKE :email', array(':email' => '%'.$searchData.'%'));
            $countQuery->orWhere('u.full_name LIKE :fname', array(':fname' => '%'.$searchData.'%'));
        }
	
	$countQuery->andWhere('a.created_at >= :todate', array(':todate' => $todayDate));
        $countQuery->andWhere('a.created_at <= :fromdate', array(':fromdate' => $fromDate));
        
        $totalRecordsArray = $countQuery->queryRow();
        $userObjectCount = $totalRecordsArray['totalRecords'];

        $userJSONData = CJSON::encode($userListObject);
        echo '{"draw": '.$draw.',
                "recordsTotal": ' . $userObjectCount . ',
                "recordsFiltered": ' . $userObjectCount . ',
                "data":' . $userJSONData . '
        }';
        exit;
    }
    
    public function actionAgentRequest() {  
        $this->render('agent_request');
    }
    
    public function actionAgentRequestData() {
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        $fromDate = date('Y-m-d'); 
        $todayDate = Yii::app()->params['startDateLastSevenDay'];
         
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString; 
        }
        
        if(!empty($_POST['columns']['2']['search']['value']) && !empty($_POST['columns']['2']['search']['regex'])){
            $todayDate = $_POST['columns']['2']['search']['value'];
            $fromDate = $_POST['columns']['2']['search']['regex'];
        }
       
        //Get total record in the datablse with condition for grid view
        $dataQuery = Yii::app()->db->createCommand()                       
        ->select('a.*,c.name as cname,CONCAT("+",a.country_code," ",a.phone) as phoneNuber')
        ->from('contact a')
        ->leftjoin('country c', 'c.id = a.country_id')
        ->where('a.type="agent"') ;        
        
        $dataQuery->andWhere('a.created_at >= :todate', array(':todate' => $todayDate));
        $dataQuery->andWhere('a.created_at <= :fromdate', array(':fromdate' => $fromDate));
        
        if(!empty($_POST['search']['value'])){ 
            $searchData = $_POST['search']['value'];
            $dataQuery->andWhere('a.name LIKE :name', array(':name' => '%'.$searchData.'%'));
            $dataQuery->orWhere('a.email LIKE :email', array(':email' => '%'.$searchData.'%'));
            $dataQuery->AndWhere('a.type = :type', array(':type' => "agent"));
        }
       
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $userListObject = $dataQuery->queryAll();
        
        //Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->from('contact a')
            ->leftjoin('country c', 'c.id = a.country_id')
            ->where('a.type="agent"') ;  

        $countQuery->andWhere('a.created_at >= :todate', array(':todate' => $todayDate));
        $countQuery->andWhere('a.created_at <= :fromdate', array(':fromdate' => $fromDate));

        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $countQuery->andWhere('a.name LIKE :name', array(':name' => '%'.$searchData.'%'));
            $countQuery->orWhere('a.email LIKE :email', array(':email' => '%'.$searchData.'%'));
            $countQuery->AndWhere('a.type = :type', array(':type' => "agent"));
        }
        
        $totalRecordsArray = $countQuery->queryRow();
        $userObjectCount = $totalRecordsArray['totalRecords'];

        $userJSONData = CJSON::encode($userListObject);
        echo '{"draw": '.$draw.',
                "recordsTotal": ' . $userObjectCount . ',
                "recordsFiltered": ' . $userObjectCount . ',
                "data":' . $userJSONData . '
        }';
        exit;
    }
    
    public function userPhoneNumber($data, $row) {
        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data['uid']));
        $phone = "";
        if ($userProfileObject->country_code != 0) {
            $phone .= "+" . $userProfileObject->country_code . "- ";
        }if ($userProfileObject->phone != 0) {
            $phone .= $userProfileObject->phone;
        }if ($userProfileObject->country_code == 0 && $userProfileObject->phone == 0) {
            $phone .= "NA";
        }
        echo $phone;
    }
    
    public function userCountry($data, $row) {
        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data['uid']));
        $country = "NA";
        if ($userProfileObject->country_code != 0) {
            $country =  $userProfileObject->country()->name ;
        }
        echo $country;
    }
    
}
