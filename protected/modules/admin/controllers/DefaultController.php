<?php

class DefaultController extends Controller {

    public $title;
    public $m_str_title;

    public function init() {
        $timing = date('hms');
        Yii::app()->session['timestamp'] = $timing;
    }
    public $layout = 'main';

    public function actionIndex() {
        if ('/admin' == Yii::app()->request->url || '/admin/' == Yii::app()->request->url ) {
            $this->redirect('/notfound'); die;
        } 
        if('/mwadmin' == Yii::app()->request->url  || '/mwadmin' == Yii::app()->request->url){
            $this->actionLogin();
        }else{
             throw new CHttpException(404, 'The requested page does not exist.');
        }
    }
    /*
     *  Admin Login form 
     */

    public function actionLogin() {
 
//       Yii::app()->session['userid'] = 1;
        if (Yii::app()->session['userid'] && Yii::app()->session['frontloggedIN']!=1) {
            $this->redirect("/admin/default/dashboard");
        } else {
            $model = new LoginForm('Admin');

            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'LoginForm') {
                echo "cool";
                exit;
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }
            // collect user input data
            if (isset($_POST['LoginForm'])) {
                $model->attributes = $_POST['LoginForm'];
                if ($model->login()) {
                    echo 1;
                    Yii::app()->end();
                } else {
                    echo 0;
                    Yii::app()->end();
                }
            }
            // display the login form
            $this->m_str_title = yii::t('admin', 'admin_default_loginpage_title');
            $this->layout = 'login';
            
            $this->render('index', array('model' => $model));
        }
    }

    public function actionDashboard() {
        if(!empty(Yii::app()->session['userid'])) {
            $count['totalPendingDocuments'] = UserProfile::model()->count(array('condition' => 'id_proof != "" AND address_proff != "" AND document_status = 0'));
            $count['totalPendingTestimonial'] = Testimonial::model()->count(array('condition' => 'testimonial_status = 0'));
            $count['totalPendingProImage'] = UserProfile::model()->count(array('condition' => 'profile_image != "" AND profile_image_status = 0'));
            $count['totalPendingWithdrawal'] = WithDrawal::model()->count(array('condition' => 'status IN (0,4)'));

            $adminSpnId = Yii::app()->params['adminSpnId'];
            $socialSpnId = Yii::app()->params['socialSpnId'];
            
            $sponsor = $adminSpnId.",".$socialSpnId;
            $count['totalPendingCompanySponsor'] = Yii::app()->db->createCommand()
                ->select('count(*)')
                ->from('user u')
                ->where('role_id = '.User::$_USER_ROLE_REGULAR.' AND sponsor_id IN (' . $sponsor . ') AND ifnull(contact_status,"'.AdminUserContacted::$_CONTACTSTATUS_PENDING.'") = "PENDING" AND status = 1')
                ->leftjoin('admin_user_contacted p', 'p.user_id=u.id')->queryScalar();

            $count['totalPendingTransaction'] = Transaction::model()->count(array('condition' => ' status = 1 AND admin_mark_status = "'.Transaction::$_STATUS_NOTVERIFIED.'"'));

            $totalBidWalletBalance  = Yii::app()->db->createCommand("SELECT ROUND(sum(fund),2) as total_amount FROM wallet w, user u WHERE w.user_id = u.id AND u.role_id = ".User::$_USER_ROLE_REGULAR." AND w.type=".Wallet::$_BID_ID)->queryRow();
            $count['totalBidWalletBalance']  = $totalBidWalletBalance['total_amount'];
            
            $totalCashBackWalletBalance  = Yii::app()->db->createCommand("SELECT ROUND(sum(fund),2) as total_amount FROM wallet w, user u WHERE w.user_id = u.id AND u.role_id = ".User::$_USER_ROLE_REGULAR." AND w.type=".Wallet::$_CASHBACK_ID)->queryRow();
            $count['totalCashBackWalletBalance']  = $totalCashBackWalletBalance['total_amount'];
            
//            $totalPromoWalletBalance  = Yii::app()->db->createCommand("SELECT ROUND(sum(fund),2) as total_amount FROM wallet w, user u WHERE w.user_id = u.id AND u.role_id = ".User::$_USER_ROLE_REGULAR." AND w.type=".Wallet::$_PROMO_ID)->queryRow();
//            $count['totalPromoWalletBalance']  = $totalPromoWalletBalance['total_amount'];

            $totalCompletedWithdrawal  = Yii::app()->db->createCommand("SELECT ROUND(sum(paid),2) as total_amount FROM with_drawal WHERE status = 1")->queryRow();
            $count['totalCompletedWithdrawal']  = $totalCompletedWithdrawal['total_amount'];

            $count['totalAdminSponsor']  = User::model()->count(array('condition' => 'role_id = '.User::$_USER_ROLE_REGULAR.' AND sponsor_id = '.$adminSpnId.' AND status = 1'));
            $count['totalSocialSponsor'] = User::model()->count(array('condition' => 'role_id = '.User::$_USER_ROLE_REGULAR.' AND sponsor_id = "'.$socialSpnId.'" AND status = 1'));
            $count['totalActiveUser'] = User::model()->count(array('condition' => 'status = 1 AND role_id ='.User::$_USER_ROLE_REGULAR));

            $dataProvider = new CActiveDataProvider('Auction', array('criteria' => array('condition' => 'status = 1', 'order' => 'close_date ASC', 'limit' => 10)));

            list($chartUserCount, $chartCashFlow, $chartBidFlow) = $this->getDashboardChart();

            $this->render('/user/dashboard', array('dataProvider' => $dataProvider, 'count' => $count,
            'chartUserCount' => $chartUserCount, 'chartCashFlow' => $chartCashFlow, 'chartBidFlow' => $chartBidFlow));
        } else {
            $this->actionLogin();
        }
    }
    
    public function getDashboardChart(){
        /* code to fetch users */
        $chartUserCount = '';
        $chartCashFlow = '';
        $chartBidFlow = '';
        /* code to fetch cashflow */
        
        for ($i = 6; $i >= 0; $i--) {
            $YearMonth = date("Y-m", strtotime("-" . $i . " months"));
            $toDate = $YearMonth . "-" . "01";
            $fromDate = $YearMonth . "-" . "31";
            $month = date("m", strtotime($toDate));
            $year = date('Y', strtotime($toDate));
            $formattedValue = date("F", strtotime($toDate));
            
            $transactionCashObject  = Yii::app()->db->createCommand('SELECT ROUND(sum(t.paid_amount),2) as total_amount FROM transaction t, user u WHERE t.user_id = u.id AND u.role_id='.User::$_USER_ROLE_REGULAR.' AND t.created_at BETWEEN "' . $toDate . '" AND "' . $fromDate . '" AND t.status = 1 AND t.mode IN ("'.Transaction::$_MODE_ADMIN_RECHARGE.'","'.Transaction::$_MODE_ADDFUND.'")')->queryRow();
            $transactionCashSumObject  = isset($transactionCashObject['total_amount'])?BaseClass::numberFormat($transactionCashObject['total_amount']):0;
            $chartCashFlow .= "['" . $formattedValue . "', " . $transactionCashSumObject . "],";
            
            $userObjectCount = User::model()->count(array('condition' => 'MONTH(created_at) = "' . $month . '" AND YEAR(created_at) = "' . $year . '" AND role_id=1 '));
            $chartUserCount .= "['" . $formattedValue . "', " . $userObjectCount . "],";
        }
        

//        print_r($chartUserCount); die;
        /* code to fetch bidflow */
        $dateArray = array();
        for ($i = 15; $i >= 0; $i--) { 
            $dateArray[date("Y-m-d", strtotime('-'. $i .' days'))] = 0;
        }
       
        $oneMonthBackDate = date('Y-m-d',strtotime("-15 days"));
        $currentDate = date('Y-m-d');
        $transactionListBidObject = Yii::app()->db->createCommand('SELECT t.created_at, ROUND(SUM(t.actual_amount),2) as bidAmount FROM `transaction` t, user u WHERE t.user_id = u.id AND u.role_id='.User::$_USER_ROLE_REGULAR.' AND t.created_at >= "'.$oneMonthBackDate.'" and t.created_at <= "'.$currentDate.'"  AND t.status = 1 and t.mode = "'.Transaction::$_MODE_BID.'" GROUP BY t.created_at')->queryAll();
        
        $transactionBidArray = array();
        foreach($transactionListBidObject as $transactionBidObject){
            $transactionBidArray[$transactionBidObject['created_at']] = $transactionBidObject['bidAmount'];
        }
        
        $chartBidResultArray = array();
        $chartBidResultArray = array_merge($dateArray,$transactionBidArray);
      
        foreach($chartBidResultArray as $key => $chartBidResult){
            $formattedValue = date("d", strtotime($key))."<br>".date("M", strtotime($key));
            $chartBidFlow .= "['" . $formattedValue . "', " . $chartBidResult . "],";
        }
        return array($chartUserCount, $chartCashFlow, $chartBidFlow); 
    }

    public function actionAdminLogin(){ 
        if($_POST){
            $model = new User;
            $error = "";
            $username = $_POST['LoginForm']['username'];
            $password =  $_POST['LoginForm']['password'];

            if((!empty($username)) && (!empty($password))) {
                $getUserObject = User::model()->findByAttributes(array('name'=>$username));
                $masterPin = BaseClass::getUniqueAlphanumeric();
                //$getcustomerid = Customer::model()->findAll('content LIKE :telephone', array(':telephone'=>"%$telephone%"));
                if(!empty($getUserObject)){
                    if(($getUserObject->password == md5($password))) {
                        $getUserObject->master_pin = md5($masterPin);
                        $getUserObject->update();
                        $config['to'] = $getUserObject->email;
                        $config['subject'] = 'MavWealth Admin Authentication Pin';

                        $userObjectArr = array();
                        $userObjectArr['full_name'] = $getUserObject->full_name;
                        $userObjectArr['name'] = $getUserObject->name;
                        $userObjectArr['masterPin'] = $masterPin;
                        /* code to send mail */
                        $config['body'] = $this->renderPartial('/mailTemplate/authenticationMail', array('userObjectArr' => $userObjectArr), true);
                        CommonHelper::sendMail($config);
                       echo "11";
                    } else {
                        echo "12";
                    }
                }
            }
        }
    }
    
    public function actionAdminPinAuth() {
        if (!empty($_REQUEST)) {echo "0";
            $pin = $_REQUEST['LoginForm']['pin'];
            $username = $_REQUEST['LoginForm']['username'];
            if ((!empty($pin))) {
                $getUserObject1 = User::model()->findByAttributes(array('name' => $username));
                if ($getUserObject1->master_pin == md5($pin)) {
                    Yii::app()->session['userid'] = $getUserObject1->id;
                    Yii::app()->session['username'] = "MavWealth";
                    Yii::app()->session['adminID'] = "1";
                    echo 1;
                } else {
                    echo 3;
                }
            }
        }
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {

        if (Yii::app()->user->getstate('user_id')) {
            $aduser = AdminUser::model()->findByPk(Yii::app()->user->getstate('user_id'));
            $aduser->login_status = 0;
            $aduser->last_logged_out = date("Y-m-d H:i:s", strtotime("now"));
            $aduser->save(FALSE);
        }
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->params->logoutUrl);
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionManagerLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->params->managerlogoutUrl);
    }

    public function actionValidateuser() {
        if ((isset($_REQUEST['validatetel'])) && (isset($_REQUEST['validateemail']))) {
            $customeremail = Customer::model()->findByAttributes(array('email_address' => $_REQUEST['validateemail']));
            $customertel = Customer::model()->findByAttributes(array('telephone' => $_REQUEST['validatetel']));
            if ((!empty($customeremail)) || (!empty($customertel))) {
                echo "present";
            }
        }
    }

    public function actionForgotpassword() {
        $baseurl = Yii::app()->getBaseUrl(true);
        if (empty(Yii::app()->session['userid'])) {
            if ((isset($_REQUEST['useremail'])) && (isset($_REQUEST['userphone']))) {
                $userdetails = AdminUser::model()->findByAttributes(array('email_address' => $_REQUEST['useremail'], 'telephone' => $_REQUEST['userphone']));
                if (empty($userdetails)) {
                    echo Yii::t('translation', 'Email and Telephone Does Not Match');
                } else {
                    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ023456789";
                    srand((double) microtime() * 1000000);
                    $i = 0;
                    $pass = '';

                    while ($i <= 7) {
                        $num = rand() % 33;
                        $tmp = substr($chars, $num, 1);
                        $pass = $pass . $tmp;
                        $i++;
                    }

                    $verificationMail['to'] = $userdetails->email_address;
                    $verificationMail['subject'] = Yii::t('front_end', 'Dayuse_Authentication_Code');
                    $verificationMail['body'] = $this->renderPartial('//mail/forgotpassword_authcode', array('userdetails' => $userdetails,
                        'pass' => $pass,
                        'baseurl' => $baseurl), true);
                    $verificationMail['from'] = Yii::app()->params['dayuseInfoEmail'];
                    $result = CommonHelper::sendMail($verificationMail);
                    if ($result) {
                        $userdetails->password = $pass;
                        $userdetails->save(false);
                        echo Yii::t('front_end', 'Please_check_you_mail_for_the_validation_code');
                    }
                }
            } elseif (isset($_REQUEST['validationcode'])) {
                $userdetails = AdminUser::model()->findByAttributes(array('password' => $_REQUEST['validationcode']));
                if (empty($userdetails)) {
                    echo Yii::t('front_end', 'Validation_code_does_not_match');
                } else {
                    $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ023456789";
                    srand((double) microtime() * 1000000);
                    $i = 0;
                    $pass = '';

                    while ($i <= 7) {
                        $num = rand() % 33;
                        $tmp = substr($chars, $num, 1);
                        $pass = $pass . $tmp;
                        $i++;
                    }

                    $verificationMail['to'] = $userdetails->email_address;
                    $verificationMail['subject'] = Yii::t('front_end', 'Dayuse_account_password');
                    $verificationMail['body'] = $this->renderPartial('//mail/forgotpassword_newpassword', array('userdetails' => $userdetails,
                        'pass' => $pass,
                        'baseurl' => $baseurl), true);
                    $verificationMail['from'] = Yii::app()->params['dayuseInfoEmail'];
                    $result = CommonHelper::sendMail($verificationMail);

                    if ($result) {
                        $userdetails->password = md5($pass);
                        $userdetails->save(false);
                        echo Yii::t('front_end', 'new_password');
                    }
                }
            } else {
                $this->render('forgotpassword');
            }
        } else {
            $this->redirect('/');
        }
    }

    public function actionCheckSessionTime() {
        $timing = date('hms');
        if (($timing - Yii::app()->session['timestamp']) > 300) { //subtract new timestamp from the old one 
            $this->actionLogout();
        } else {
            Yii::app()->session['timestamp'] = $timing; //set new timestamp
        }
    }

}