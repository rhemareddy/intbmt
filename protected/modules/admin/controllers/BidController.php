<?php

class BidController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('add', 'edit', 'list', 'listmore', 'autocompletebypid', 'bidsummary', 'listopen', 'listclose', 'changestatus', 'changepublish', 'deletebid', 'biddetailreport', 'matchthisbid'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Adding bid.
     */
    public function actionAdd() {
        $error = "";
        $success = "";
        $msg = 1;
        $start_date = "";
        $close_date = "";
        $auctionObject = new Auction;
        $productObject = Product::model()->findAll();
        if (!empty($_POST)) {
            $start_date = $_POST['sdate'] . " " . $_POST['stime'];
            $close_date = $_POST['cdate'] . " " . $_POST['ctime'];
            //echo '<pre>';print_r($start_date);exit;
            if ($_POST['product_id'] == '' && $_POST['sdate'] == '' && $_POST['cdate'] == '') {
                $error .= "Please fill required(*) marked fields.";
            } else {

                $auctionObject->attributes = $_POST;
                $auctionObject->start_date = $start_date;
                $auctionObject->close_date = $close_date;
                $auctionObject->created_at = new CDbExpression('NOW()');
                $auctionObject->updated_at = new CDbExpression('NOW()');
                $auctionObject->save(false);
                $success .= "Auction Successfully Added";
            }
        }

        $this->render('bid_add', array('success' => $success, 'error' => $error, 'auctionObject' => $auctionObject, 'productObject' => $productObject));
    }

    /**
     * Editing Bid.
     */
    public function actionEdit() {
        $error = "";
        $success = "";
        $msg = 1;
        $start_date = "";
        $close_date = "";
        //$auctionObject = new Auction;
        $auctionObject = Auction::model()->findByPK(array('id' => $_GET['id']));
        $productObject = Product::model()->findAll();
        if (!empty($_POST)) {
            $start_date = $_POST['sdate'] . " " . $_POST['stime'];
            $close_date = $_POST['cdate'] . " " . $_POST['ctime'];
            //echo '<pre>';print_r($start_date);exit;
            if ($_POST['product_id'] == '' && $_POST['sdate'] == '' && $_POST['cdate'] == '') {
                $error .= "Please fill required(*) marked fields.";
            } else {

                $auctionObject->attributes = $_POST;
                $auctionObject->start_date = $start_date;
                $auctionObject->close_date = $close_date;
                $auctionObject->created_at = new CDbExpression('NOW()');
                $auctionObject->updated_at = new CDbExpression('NOW()');
                $auctionObject->update();
                $success .= "Auction Successfully Updated";
                $this->redirect('list?success=' . $success);
            }
        }

        $this->render('bid_edit', array('success' => $success, 'error' => $error, 'auctionObject' => $auctionObject, 'productObject' => $productObject));
    }

    /**
     * Get bid list.
     */
    public function actionList() {
        $this->render('bid_list');
    }
    
    public function actionBidSummary() {
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
 
        $dataQuery = Yii::app()->db->createCommand()
            ->select('ab.auction_id as auctionId, p.name as productName, a.start_date as startDate, a.close_date as endDate, '
                    . 'a.entry_fees as bidFee, (select bid_value from auction_bids where status = 3 and auction_id = ab.auction_id) as winningBid, ifnull(u.name,"Not Declared") as winnerName, '
                    . '(case when u.role_id = 1 then "Regular" when u.role_id = 3 then "Preferential" ELSE "--" END) as winnerUserType, '
                    . '(case when a.status = 1 then "Active" ELSE "InActive" END) as auctionStatus, '
                    . '(select count(ab.auction_id) from auction_bids ab  join user U1 on ab.user_id = U1.id and U1.role_id = 1  where ab.auction_id  = a.id ) as userBids, '
                    . 'ab.updated_at as updatedDate')
            ->from('auction_bids ab')
            ->join('auction a', 'a.id = ab.auction_id')
            ->join('product p', 'p.id = a.product_id')
            ->leftjoin('user u', 'u.id = a.winner_user_id')
            ->group('ab.auction_id');
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $usertypeData = '';
            if($searchData == 'regular') {
                $usertypeData = 1;
            } else if($searchData == 'preferential') {
                $usertypeData = 3;
            }
            $dataQuery->andWhere('p.name LIKE :productName', array(':productName' => '%'.$searchData.'%'));
            $dataQuery->orWhere('u.name LIKE :userName', array(':userName' => '%'.$searchData.'%'));
            $dataQuery->orWhere('u.role_id = :roleId', array(':roleId' => $usertypeData));
        }
        $auctionStatus = 1;
        if($_POST['columns']['8']['search']['value']=='inactive') {
            $auctionStatus = 0;
        }
        $dataQuery->andWhere('a.status = :status', array(':status' => $auctionStatus));
        
        if(!empty($_POST['columns']['9']['search']['value'])){
            $todayDate = $_POST['columns']['9']['search']['value'];
            $fromDate = $_POST['columns']['9']['search']['regex'];
            $dataQuery->andWhere('ab.created_at >= :todate', array(':todate' => $todayDate));
            $dataQuery->andWhere('ab.created_at <= :fromdate', array(':fromdate' => $fromDate));
        }
        
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $bidSummaryObject = $dataQuery->queryAll();
        
        $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->from('auction_bids ab')
            ->join('auction a', 'a.id = ab.auction_id')
            ->join('product p', 'p.id = a.product_id')
            ->leftjoin('user u', 'u.id = a.winner_user_id')
            ->group('ab.auction_id');
        
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $usertypeData = '';
            if($searchData == 'regular') {
                $usertypeData = 1;
            } else if($searchData == 'preferential') {
                $usertypeData = 3;
            }
            $countQuery->andWhere('p.name LIKE :productName', array(':productName' => '%'.$searchData.'%'));
            $countQuery->orWhere('u.name LIKE :userName', array(':userName' => '%'.$searchData.'%'));
            $countQuery->orWhere('u.role_id = :roleId', array(':roleId' => $usertypeData));
        }
        
        $countQuery->andWhere('a.status = :status', array(':status' => $auctionStatus));
        
        if(!empty($_POST['columns']['9']['search']['value'])){
            $todayDate = $_POST['columns']['9']['search']['value'];
            $fromDate = $_POST['columns']['9']['search']['regex'];
            $countQuery->andWhere('ab.created_at >= :todate', array(':todate' => $todayDate));
            $countQuery->andWhere('ab.created_at <= :fromdate', array(':fromdate' => $fromDate));
        }
        $bidSummaryObjectCount = count($countQuery->queryAll());

        //$bidSummaryObjectCount = $totalRecordsArray['totalRecords'];
        
        $auctionBidsJSONData = CJSON::encode($bidSummaryObject);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $bidSummaryObjectCount . ',
                    "recordsFiltered": ' . $bidSummaryObjectCount . ',
                    "data":' . $auctionBidsJSONData . '
        }';
        exit;
    }
    
    public function actionListMore() {        
       
        $this->render('bid_list_more');
        
    }
    
    public function actionBidDetailReport() {
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
 
        $dataQuery = Yii::app()->db->createCommand()
            ->select('ab.auction_id as auctionId, ab.user_id as userId, a.close_date as endDate, '
                    . 'p.name as productName, u.name as userName, '
                    . 'ab.bid_value as bidAmt, '
                    . '(case when u.role_id = 1 then "Regular" when u.role_id = 3 then "Preferential" ELSE "--" END) as userType, '
                    . '(case when ab.status = 1 then "Not Unique" when ab.status = 3 then "Bid Considered" ELSE "Bid Considered" END) as bidStatus, '
                    . 'ab.updated_at as updatedDate, (case when a.status = 1 then "Open" ELSE "Close" END) as auctionStatus')
            ->join('auction a', 'a.id = ab.auction_id')
            ->join('product p', 'p.id = a.product_id')
            ->join('user u', 'u.id = ab.user_id')
            ->from('auction_bids ab');

        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $usertypeData = '';
            if($searchData == 'regular') {
                $usertypeData = 1;
            } else if($searchData == 'preferential') {
                $usertypeData = 3;
            }
            $dataQuery->andWhere('p.name LIKE :productName', array(':productName' => '%'.$searchData.'%'));
            $dataQuery->orWhere('u.name LIKE :userName', array(':userName' => '%'.$searchData.'%'));
            $dataQuery->orWhere('u.role_id = :roleId', array(':roleId' => $usertypeData));
        }
        
        $auctionStatus = 1;
        if($_POST['columns']['8']['search']['value']=='close') {
            $auctionStatus = 0;
        }
        if($_POST['columns']['5']['search']['value'] == 'ul') { /* Cond. for unique and Lowest */
            $dataQuery->andWhere('ab.status IN ("0","3")');
        } else if($_POST['columns']['5']['search']['value'] == "nu") { /* Cond. for not unique */
            $dataQuery->andWhere('ab.status = :bidStatus', array(':bidStatus' => 1));
        } else { /* Cond. for all */
            $dataQuery->andWhere('ab.status IN ("0","1","3")');
        }
        $dataQuery->andWhere('a.status = :auctionStatus', array(':auctionStatus' => $auctionStatus));
        

        if(!empty($_POST['columns']['7']['search']['value'])){
            $todayDate = $_POST['columns']['7']['search']['value'];
            $fromDate = $_POST['columns']['7']['search']['regex'];
            $dataQuery->andWhere('ab.created_at >= :todate', array(':todate' => $todayDate));
            $dataQuery->andWhere('ab.created_at <= :fromdate', array(':fromdate' => $fromDate));
        }
        
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        
        $bidDetailSummaryObject = $dataQuery->queryAll();
        
        $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->join('auction a', 'a.id = ab.auction_id')
            ->join('product p', 'p.id = a.product_id')
            ->join('user u', 'u.id = ab.user_id')
            ->from('auction_bids ab');
        
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $usertypeData = '';
            if($searchData == 'regular') {
                $usertypeData = 1;
            } else if($searchData == 'preferential') {
                $usertypeData = 3;
            }
            $countQuery->andWhere('p.name LIKE :productName', array(':productName' => '%'.$searchData.'%'));
            $countQuery->orWhere('u.name LIKE :name', array(':name' => '%'.$searchData.'%'));
            $countQuery->orWhere('u.role_id = :roleId', array(':roleId' => $usertypeData));
        }
        
        if($_POST['columns']['5']['search']['value'] == 'ul') { /* Cond. for unique and Lowest */
            $countQuery->andWhere('ab.status IN ("0","3")');
        } else if($_POST['columns']['5']['search']['value'] == "nu") { /* Cond. for not unique */
            $countQuery->andWhere('ab.status = :bidStatus', array(':bidStatus' => 1));
        } else { /* Cond. for all */
            $countQuery->andWhere('ab.status IN ("0","1","3")');
        }
        $countQuery->andWhere('a.status = :auctionStatus', array(':auctionStatus' => $auctionStatus));

        if(!empty($_POST['columns']['7']['search']['value'])){
            $todayDate = $_POST['columns']['7']['search']['value'];
            $fromDate = $_POST['columns']['7']['search']['regex'];
            $countQuery->andWhere('ab.created_at >= :todate', array(':todate' => $todayDate));
            $countQuery->andWhere('ab.created_at <= :fromdate', array(':fromdate' => $fromDate));
        }
        $bidDetailsCount = $countQuery->queryAll();
        $bidSummaryObjectCount = $bidDetailsCount[0]['totalRecords'];
        //$bidSummaryObjectCount = $totalRecordsArray['totalRecords'];

        $auctionBidsJSONData = CJSON::encode($bidDetailSummaryObject);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $bidSummaryObjectCount . ',
                    "recordsFiltered": ' . $bidSummaryObjectCount . ',
                    "data":' . $auctionBidsJSONData . '
        }';
        exit;
    }

    /**
     * Get Winner in bid.
     */
    protected function getWinner($data, $row) {
        $userId = $data->auction->winner_user_id;
        $userObject = User::model()->findByPk($userId);
        if (!empty($userObject)) {
            return $userObject->name;
        } else {
            return "Not Declared";
        }
    }
    
     /**
     * Get Winner Role.
     */
    protected function GetWinnerRole($data, $row) {
        $userId = $data->auction->winner_user_id;
        $userObject = User::model()->findByPk($userId);
        if (!empty($userObject)) {
            if (!empty($userObject->role_id) && $userObject->role_id == 1) {
            return "Regular User";
            }
            if (!empty($userObject->role_id) && $userObject->role_id == 2) {
            return "Admin";
            } 
            if (!empty($userObject->role_id) && $userObject->role_id == 3) {
            return "Preferential User";
            }  
        } else {
            return "NA";
        }
    }

    /**
     * Get bid value.
     */
    protected function getBidValue($data, $row) {
        $auctionId = $data->id;
        $auctionObject = AuctionBids::model()->findByAttributes(array('auction_id' => $auctionId, 'status' => 1));
        if (!empty($auctionObject)) {
            return $auctionObject->bid_value;
        }
    }

    /**
     * Get bid status.
     */
    protected function getBidStatus($auctionId) {
        $auctionObject = Auction::model()->findByAttributes(array('id' => $auctionId, 'status' => 1));

        if (!empty($auctionObject)) {
            return "Open";
        } else {
            return "Closed";
        }
    }



    /**
     * Get bid open list.
     */
    public function actionListOpen() {
        $pageSize = Yii::app()->params['defaultPageSize'];
        //$auctionBidsListObject = Auction::model()->findAllByAttributes(array('status' => 1)); 
        // Default  Condition.
        $condition = "" . 'start_date <= "' . date('Y-m-d H:i:s') . '" AND close_date > "' . date('Y-m-d H:i:s') . '"' . "";
        $condition .= ' AND ds.status = 1';
        $dataProvider = new CActiveDataProvider('AuctionBids', array(
            'criteria' => array(
                'condition' => ($condition),
                'select' => 't.auction_id, ds.*',
                'order' => 'ds.id DESC',
                'group' => 't.auction_id',
                'distinct' => true,
                'join' => 'LEFT JOIN auction ds on ds.id = t.auction_id',
            ),
            'pagination' => array('pageSize' => $pageSize),
        ));
//         $dataProvider = new CActiveDataProvider('Auction', array(
//                'criteria' => array(
//                   'condition' => ($condition), 'order' => 'id DESC',
//                ), 'pagination' => array('pageSize' => $pageSize),
//            ));
        $this->render('bid_list_open', array('dataProvider' => $dataProvider));
    }

    /**
     * Get bid close list.
     */
    public function actionListClose() {
        $pageSize = Yii::app()->params['defaultPageSize'];
        //$auctionBidsListObject = Auction::model()->findAllByAttributes(array('status' => 0));

        $condition = 'ds.status = 0';
        $dataProvider = new CActiveDataProvider('AuctionBids', array(
            'criteria' => array(
                'condition' => ($condition),
                'select' => 't.auction_id, ds.*',
                'order' => 'ds.id DESC',
                'group' => 't.auction_id',
                'distinct' => true,
                'join' => 'LEFT JOIN auction ds on ds.id = t.auction_id',
            ),
            'pagination' => array('pageSize' => $pageSize),
        ));
//         $dataProvider = new CActiveDataProvider('Auction', array(
//                'criteria' => array(
//                   'condition' => ($condition), 'order' => 'id DESC',
//                ), 'pagination' => array('pageSize' => $pageSize),
//            ));
        $this->render('bid_list_close', array('dataProvider' => $dataProvider));
    }

    /**
     * Get autocomplete product id.
     */
    public function actionAutoCompleteByPid($term) {
        $match = $term;
        $projectObject = Product::model()->findAll(
                'name LIKE :match', array(':match' => "%$match%")
        );
        $list = array();
        foreach ($projectObject as $q) {
            $data['value'] = $q['id'];
            $data['label'] = $q['name'];
            $list[] = $data;
            unset($data);
        }
        echo json_encode($list);
    }

    /**
     * Changing bid status.
     */
    public function actionChangeStatus() {

        if ($_REQUEST['id']) {
            $auctionObject = Auction::model()->findByPK($_REQUEST['id']);
            if ($auctionObject->status == 1) {
                $auctionObject->status = 0;
            } else {
                $auctionObject->status = 1;
            }
            $auctionObject->save(false);
            $this->redirect(array('/admin/bid/list', 'msg' => 2));
        }
    }

    /**
     * Changing bid publish status.
     */
    public function actionChangePublish() {

        if ($_REQUEST['id']) {
            $auctionObject = Auction::model()->findByPK($_REQUEST['id']);
            if ($auctionObject->is_publish == 1) {
                $auctionObject->is_publish = 0;
            } else {
                $auctionObject->is_publish = 1;
            }
            $auctionObject->save(false);
            $this->redirect(array('/admin/bid/list', 'msg' => 3));
        }
    }

    /**
     * Delete Bid.
     */
    public function actionDeleteBid() {
        if ($_REQUEST['id']) {
            $auctionObject = Auction::model()->findByPK($_REQUEST['id']);
            $auctionObject->delete();
            $this->redirect(array('/admin/bid/list', 'msg' => 1));
        }
    }
    
    /**
     * Total bids for the auction.
     */
    public function getTotalBids($data) {        
        return (AuctionBids::Model()->count("auction_id=:value", array(":value" => $data->auction_id)));
    }
    
    /**
     * Generate Preferential user bids.
     */
    public function actionMatchThisBid() {    
        
        $generateBid = '';
        $auctionId = $_POST['auctionId'];
        $bidAmt = $_POST['bidAmt'];
        $userId = $_POST['userId'];
        
        $preferentialUserId = Bid::getPreferentialUser();
        
        $generateRandomBid = AuctionBids::model()->create($preferentialUserId, Wallet::$_BID_ID, $bidAmt, $auctionId,  AuctionBids::$_BID_NOT_UNIQUE);
        
        $uniqueLowBidObject = AuctionBids::model()->find(array('condition' => 'auction_id = ' . $auctionId . ' AND user_id=' . $userId . ' AND status IN ("'.AuctionBids::$_BID_UNIQUE_AND_LOWEST.'" , "'.AuctionBids::$_BID_UNIQUE.'")'));
        
        if(!empty($uniqueLowBidObject)) {
            
            $uniqueLowBidObject->status = AuctionBids::$_BID_NOT_UNIQUE;
            $uniqueLowBidObject->update();

            $auctionBidsListObject = Bid::updateBidStatus($preferentialUserId, $auctionId);

            if($auctionBidsListObject) {
                echo 1;
            } else {
                echo 0;
            }
        } else {
            echo 2;
        }
    }

}
