<?php

class OfferController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'add', 'edit', 'changestatus'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Offer Index(List) page.
     */
    public function actionIndex() {
        $pageSize = Yii::app()->params['defaultPageSize'];
        $fromDate = date("Y-m-d");
        $todayDate = date("Y-m-d");
        $criteria = new CDbCriteria;
        $criteria->order = ('bid_points DESC');

        if (isset($_POST) && !empty($_POST)) {
            $fromDate = date($_POST['from']);
            $todayDate = date($_POST['to']);
            $criteria->addBetweenCondition('created_at', $fromDate, $todayDate);
        }

        if ($_GET) {
            //Sort Implatation
            /* if (isset($_GET['OfferPage_sort'])) {
              $criteria->order = ($_GET['LandingPage_sort']);
              } */
        }

        $dataProvider = new CActiveDataProvider('Offer', array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => $pageSize)));

        $this->render('index', array('dataProvider' => $dataProvider, 'fromDate' => $fromDate, 'toDate' => $todayDate));
    }

    /**
     * Offer Add.
     */
    public function actionAdd() {
        $maxUploadSizeProImg = Yii::app()->params['MaxUploadSizeProfileImg'];
        $error = NULL;
        $success = NULL;
        if (isset($_POST) && !empty($_POST)) {
            $offerImage = $_FILES['offer_image']["size"];
            if ($offerImage < $maxUploadSizeProImg) {
                $postArray = $_POST;
                $offerImgName = time() . $_FILES['offer_image']['name'];
                $path = Yii::getPathOfAlias('webroot') . "/upload/offer-image/";
                BaseClass::uploadFile($_FILES['offer_image']['tmp_name'], $path, $offerImgName);
                $postArray['offer_image'] = $offerImgName;

                $Object = Offer::model()->create($postArray);
                if ($Object) {
                    $success = "Offer Page Updated Successfully.";
                } else {
                    $error = 'Something went wrong. Please try later.';
                }
            } else {
                $error = 'Maximum file upload size is 1MB.';
            }
        }
        $this->render('offer_add', array('error' => $error, 'success' => $success));
    }

    /**
     * Offer Edit.
     */
    public function actionEdit() {
        $maxUploadSizeProImg = Yii::app()->params['MaxUploadSizeProfileImg'];
        $error = NULL;
        $success = NULL;

        if (isset($_GET) && !empty($_GET)) {
            $offerObject = Offer::model()->findByPk($_GET['id']);
        }

        if (isset($_POST) && !empty($_POST)) {
            $offerObject = Offer::model()->findByPk($_POST['offer_id']);
            $postArray = $_POST;
            
            if (isset($_FILES['offer_image']["size"]) && !empty($_FILES['offer_image']["size"])) {
                $offerImage = $_FILES['offer_image']["size"];
                if ($offerImage < $maxUploadSizeProImg) {                    
                    $offerImgName = time() . $_FILES['offer_image']['name'];
                    $path = Yii::getPathOfAlias('webroot') . "/upload/offer-image/";
                    BaseClass::uploadFile($_FILES['offer_image']['tmp_name'], $path, $offerImgName);
                    $postArray['offer_image'] = $offerImgName;                    
                } else {
                    $error = 'Maximum file upload size is 1MB.';
                }
            }

            if (!$error) {
                $Object = Offer::model()->updateRecord($postArray, $offerObject);
                if ($Object) {
                    $success = "Offer Page Updated Successfully.";
                } else {
                    $error = 'Something went wrong. Please try later.';
                }
            }
        }

        $this->render('offer_edit', array('error' => $error, 'success' => $success, 'offerObject' => $offerObject));
    }

    /**
     * Show image in pop up.
     * @param type $data
     * @param type $row
     */
    protected function offerImagePopup($data, $row) {
        $imagefolder = "/upload/offer-image/"; // folder with uploaded files
        echo "<a data-toggle='modal' href='#zoom_$data->id'>Click to open</a>" . '<div class="modal fade" id="zoom_' . $data->id . '" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog" style="auto;">
                        <div class="modal-content">
                                <div class="modal-body" style="width: auto;overflow: auto;height: auto;padding: 0;">
                                         <img src="' . $imagefolder . $data->offer_image . '">
                                                         </div>
                            </div>
                        </div>
                </div>';
    }

    /**
     * 
     */
    public function actionChangeStatus() {
        $msg = "";
        if (isset($_GET) && !empty($_GET)) {
            $offerObject = Offer::model()->findByPk($_GET['id']);
            if ($offerObject->status) {
                $offerObject->status = 0;
                $msg = "Offer status changed to Inactive.";
            } else {
                $offerObject->status = 1;
                $msg = "Offer status changed to Active.";
            }

            $offerObject->save(false);
            Yii::app()->user->setFlash('success', $msg);
            $this->redirect('/admin/offer');
        }
    }

}
