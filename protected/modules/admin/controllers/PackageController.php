<?php

class PackageController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'add', 'edit', 'editimages', 'deleteimages', 'list', 'vendorlist',
                    'changestatus', 'deletepackage', 'getPackageUpdatedTime','thumbupload', 'packagesettings'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Package');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

     /*
     * Function to Update package images
     */

    public function actionEditImages() {
        $error = "";
        $success = "";
        $iscover = "";
        $packageHasImageObject = PackageHasImage::model()->findAllByAttributes(array('package_id' => $_GET['id']));
        if ($_POST) {
                $iscover = $_POST['is_cover'];
               
                if(!empty($packageHasImageObject)){
                    $existCoverImageObject = PackageHasImage::model()->findByAttributes(array('package_id' => $_GET['id'], 'is_cover' => 1));
                    if(!empty($existCoverImageObject)){
                        $existCoverImageObject -> is_cover = 0;
                        $existCoverImageObject->save(false); 
                    }
                    $newCoverImageObject = PackageHasImage::model()->findByPK($iscover);
                    if(!empty($newCoverImageObject)){
                        $newCoverImageObject->is_cover = 1;
                        $newCoverImageObject->save(false); 
                    } 
                }
                    
                $success .= "Cover Image Changed Successfully";
                $this->redirect('editimages?success='.$success."&id=".$_GET['id']);
            }
     
        $this->render('package_edit_images', array('success' => $success, 'error' => $error, 'packageHasImageObject' => $packageHasImageObject, 'id' => $_GET['id']));
    }
    
     /*
     * Function to Delete package images
     */

    public function actionDeleteImages() {
        $error = "";
        $success = "";
        $packageHasImageObject = PackageHasImage::model()->findAllByAttributes(array('package_id' => $_GET['id']));
            if (!empty($_REQUEST['deleteImg'])) {
            $packageHasImageObject = PackageHasImage::model()->findByPK($_REQUEST['deleteImg']);
            //echo '<pre>';print_r($packageHasImageObject);exit;
            $packageHasImageObject->delete();
          
                 $error.= "Cover Image Deleted Successfully";
                 $this->redirect(array('/admin/package/deleteimages', 'error' => $error, 'id' => $_GET['id']));
          
        }
         $this->render('package_delete_images', array('success' => $success, 'error' => $error, 'packageHasImageObject' => $packageHasImageObject, 'id' => $_GET['id']));
     
    }
    
    public function actionPackageSettings() {
        if($_POST) {
            
            $packageObject = Package::model()->findByAttributes(array('id' => $_POST['id']));
            
            if(!empty($packageObject)){
                $packageObject->name = $_POST['name'];
                $packageObject->min_amount = $_POST['min_amount'];
                $packageObject->max_amount = $_POST['max_amount'];
                $packageObject->validity = $_POST['validity'];
                $packageObject->daily_returns = $_POST['daily_returns'];
                $packageObject->direct = $_POST['direct'];
                $packageObject->binary_capping = $_POST['binary_capping'];
                if($packageObject->update(false)) {
                    echo 'success';exit;
                } else {
                    echo 'failed';exit;
                }  
                
            }
        }
        $packageObject = Package::model()->findAll(array('condition' => 'status= 1'));
        $this->render('packagesettings', array('packageObject' => $packageObject));
        
    }

}
