<?php

class LandingpageController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'add', 'edit', 'changestatus'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Landingpage Index(List) page.
     */
    public function actionIndex() {

        $pageSize = Yii::app()->params['defaultPageSize'];
        $fromDate = date("Y-m-d");
        $todayDate = date("Y-m-d");
        $criteria = new CDbCriteria;
        $criteria->order = ('id DESC');

        if (isset($_POST) && !empty($_POST)) {
            $fromDate = date($_POST['from']);
            $todayDate = date($_POST['to']);
            $criteria->addBetweenCondition('created_at', $fromDate, $todayDate);
        }
        
        if ($_GET) {
            //Sort Implatation
            /*if (isset($_GET['LandingPage_sort'])) {
                $criteria->order = ($_GET['LandingPage_sort']);
            }*/
        }

        $dataProvider = new CActiveDataProvider('LandingPage', array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => $pageSize)));

        $this->render('index', array('dataProvider' => $dataProvider, 'fromDate' => $fromDate, 'toDate' => $todayDate));
    }

    /**
     * Landingpage Add.
     */
    public function actionAdd() {
        $error = NULL;
        $success = NULL;
        $path = Yii::getPathOfAlias('webroot') . "/landingpage/";
        $allowed = array('zip', 'ZIP');

        $dirCount = CommonHelper::countDirectories($path);
        $dirName = ($dirCount + 1); // +1 Next Directory.            
        $targetdir = $path . $dirName;
        $pathCssJsFiles = "/landingpage/" . $dirName . "/";

        if (isset($_POST) && !empty($_POST)) {
            $filename = $_FILES["upload"]["name"];
            $source = $_FILES["upload"]["tmp_name"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);

            if (in_array($ext, $allowed)) {

                if (CommonHelper::extractZipFiles($source, $targetdir, $filename)) {
                    $postArray = $_POST;
                    $postArray['upload'] = $pathCssJsFiles;
                    $Object = LandingPage::model()->create($postArray);

                    if ($Object) {
                        $success = "Landing Page Updated Successfully.";
                    } else {
                        $error = 'Something went wrong. Please try later.';
                    }
                } else {
                    $error = 'There was some problem with file upload. Please try later.';
                }
            } else {
                $error = 'Only zip files are allowed. Entry Will be recordred.';
            }
        }

        $this->render('landpage_add', array('error' => $error, 'success' => $success, 'filePath' => $pathCssJsFiles));
    }

    /**
     * Landingpage Edit.
     */
    public function actionEdit() {
        $error = NULL;
        $success = NULL;
        $path = Yii::getPathOfAlias('webroot') . "/landingpage/";
        $allowed = array('zip', 'ZIP');

        $dirCount = CommonHelper::countDirectories($path);
        $dirName = ($dirCount + 1); // +1 Next Directory.            
        $targetdir = $path . $dirName;
        $pathCssJsFiles = "/landingpage/" . $dirName . "/";

        if (isset($_GET) && !empty($_GET)) {
            $lpageObject = LandingPage::model()->findByPk($_GET['id']);
        }
        
        if (isset($_POST) && !empty($_POST)) {
            $filename = $_FILES["upload"]["name"];
            $source = $_FILES["upload"]["tmp_name"];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (in_array($ext, $allowed)) {
                if (CommonHelper::extractZipFiles($source, $targetdir, $filename)) {
                    $lpageObject = Landingpage::model()->findByPk($_POST['lpage_id']);
                    $postArray = $_POST;
                    $postArray['upload'] = $pathCssJsFiles;
                    $Object = LandingPage::model()->updateRecord($postArray, $lpageObject);

                    if ($Object) {
                        $success = "Landing Page Updated Successfully.";
                    } else {
                        $error = 'Something went wrong. Please try later.';
                    }
                } else {
                    $error = 'There was some problem with file upload. Please try later.';
                }
            } else {
                $error = 'Only zip files are allowed. Entry Will be recordred.';
            }
        }

        $this->render('landpage_edit', array('error' => $error, 'success' => $success, 'lpageObject' => $lpageObject, 'filePath' => $pathCssJsFiles));
    }

    /**
     * Show image in pop up.
     * @param type $data
     * @param type $row
     */
    protected function getLandingPageScreenShot($data, $row) {

        $imagefolder = $data->upload; // folder with uploaded files

        echo "<a data-toggle='modal' href='#zoom_$data->id'>Click to open</a>" . '<div class="modal fade" id="zoom_' . $data->id . '" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog" style="auto;">
                        <div class="modal-content">
                        <div class="modal-header border-bottom-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
                                <div class="modal-body" style="width: auto;overflow: auto;height: 450px;padding: 0;">
                                         <img src="' . $imagefolder . 'screenshot.png">
                                                         </div>
                            </div>
                        </div>
                </div>';
    }

    /**
     * 
     */
    public function actionChangeStatus() {
        $msg = "";
        if (isset($_GET) && !empty($_GET)) {
            $lpageObject = LandingPage::model()->findByPk($_GET['id']);
            if ($lpageObject->status) {
                $lpageObject->status = 0;
                $msg = "LandingPage status changed to Inactive.";
            } else {
                $lpageObject->status = 1;
                $msg = "LandingPage status changed to Active.";
            }

            $lpageObject->save(false);
            Yii::app()->user->setFlash('success', $msg);
            $this->redirect('/admin/landingpage');
        }
    }

    /**
     * 
     * Remove files in a folder recursively.
     * @param type $dir
     */
    public function rmdir_recursive($dir) {
        foreach (scandir($dir) as $file) {
            if ('.' === $file || '..' === $file)
                continue;
            if (is_dir("$dir/$file"))
                $this->rmdir_recursive("$dir/$file");
            else
                unlink("$dir/$file");
        }

        rmdir($dir);
    }

}
