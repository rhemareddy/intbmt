<?php
class UserController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
//        BaseClass::isCountryValid();
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request Select Payment Gateway 
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions

                'actions' => array('index', 'view', 'changestatus', 'wallet', 'creditwallet', 'list', 'debitwallet', 'genealogy',
                    'add', 'deleteuser', 'edit', 'verificationapproval', 'testimonialapproval', 'changeapprovalstatus',
                    'testimonialapprovalstatus', 'profileimageapproval', 'binarycalculation',  'resetpassword', 'binarymail', 
                    'isuserexisted', 'profileimageapprovalstatus', 'broadcast', 'documentverification', 'updatepassword',
                    'updatemasterpin', 'changetoreject', 'viewverificationprofile', 'getusername', 'testimonialrejectstatus', 'changepassword', 
                    'getdata','readuserdata', 'powerline', 'powerlinereport', 'readinactiveuserdata','manageagent',
                    'manageagentdata','changeagentstatus','checkingagent', 'agentadd', 'uplinedownline', 'usergroup',
                    'trackusergroup', 'profileimagerejectstatus', 'changeemail'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new User;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionChangeStatus() {
        if ($_REQUEST['id']) {
            $userObject = User::model()->findByPk($_REQUEST['id']);
            if ($userObject->status == 1) {
                $userObject->status = 0;
                $userObject->save(false);
                Yii::app()->user->setFlash('success', "User status changed to Inactive!.");
            } else {
                $masterPin = BaseClass::getUniqInt(5);
                $password = BaseClass::getPassword();
                $userObject->password = md5($password);
                $userObject->master_pin = md5($masterPin);
                $userObject->status = 1;
                $userObject->save(false);

                /* array created for mailing values */
                $userObjectArr = array();
                $userObjectArr['name'] = $userObject->name;
                $userObjectArr['full_name'] = $userObject->full_name;
                $userObjectArr['password'] = $password;
                $userObjectArr['masterPin'] = $masterPin;
                /* code to send mail */

                $config['to'] = $userObject->email;
                $config['subject'] = 'Login Details';
                $config['body'] = $this->renderPartial('/mailTemplate/login-details', array('userObjectArr' => $userObjectArr), true);
                CommonHelper::sendMail($config);

                Yii::app()->user->setFlash('success', "User status changed to Active!.");
            }
            $this->redirect('/admin/user');
        }
    }

    public function actionBinaryCalculation() {

        $date = date('Y-m-d');
        $startDate = date('Y-m-d', strtotime('-1 day', strtotime($date)));
        $endDate = date('Y-m-d', strtotime('-1 day', strtotime($date)));
        $error = "";
        $msg = "";
        $currentUserId = "";
        $order = "";
        $pageSize = Yii::app()->params['defaultPageSizeBinary'];

        if (empty($_GET['BinaryRecord_sort'])) {
            $order = 'id DESC';
        }

        $binaryRecordObjectNew = BinaryRecord::model()->find(array('condition' => 'date = "' . $endDate . '"'));

        $dataProvider = new CActiveDataProvider('BinaryRecord', array(
            'criteria' => array(
                'order' => $order,
            ), 'pagination' => array('pageSize' => $pageSize),));

        $binaryRecordObjectLast = BinaryRecord::model()->find(array('condition' => 'date != ""', 'order' => 'date DESC'));

        if (!empty($binaryRecordObjectLast)) {
            $startDate = date('Y-m-d', strtotime('+1 day', strtotime($binaryRecordObjectLast->date)));
            $endDate = date('Y-m-d', strtotime('-1 day', strtotime($date)));
        }
        $dateArr = BaseClass::BetweenDates($startDate, $endDate);
        if (!empty($_POST)) {

            if ($_POST['cancelled'] == '1') {
                $error = "Binary generation cancelled";
            } else if ($startDate == $date) {
                $error = "Sorry you can't generate binary";
            } else if (count($binaryRecordObjectNew) > 0) {
                $error = "Binary already generated";
            } else {
                if ($dateArr) {
                    if (count($dateArr) > 0) { 
                        foreach ($dateArr as $date1) {
                            try {  
                                $binaryObject = BinaryRecord::model()->createRecord($date1);
                                if (empty($binaryObject)) {
                                    Yii::trace('ERROR in binary generation of date ' . $date1);
                                    exit;
                                }
                                 
                            } catch (Exception $ex) {
                                $exception = $ex->getMessage();
//                                $binaryObject = BinaryRecord::model()->createLog($exception, $startDate);
                                exit;
                            }
                            
                            $parentObject = BaseClass::newBinaryFlow($date1);

                            $binaryRecordObjectNew = BinaryRecord::model()->find(array('condition' => 'date = "' . $date1 . '"'));
                            try {
                                $binaryRecordObjectNew->status = 1;
                                $binaryRecordObjectNew->created_at = date("Y-m-d");
                                $binaryRecordObjectNew->update();
                            } catch (Exception $ex) {

                                $exception = $ex->getMessage();
                                //$binaryObject = BinaryRecord::model()->createLog($exception, $fromDate);
                                exit;
                            }
                        }

                        $msg = "Binary Calculaton Generated Successfully.";
                    }
                }
            }
        }
        $this->render('binary', array(
            'dataProvider' => $dataProvider,
            'error' => $error,
            'msg' => $msg,
            'binaryRecordObjectNew' => $binaryRecordObjectNew,
            'dateArr' => $dateArr
        ));
    }

    public static function binaryMail($parentObject) {
        $userObject = User::model()->findByPk($parentObject->user_id);
        $userObjectArr = array();
        $userObjectArr['to_name'] = $userObject->full_name;
        $userObjectArr['user_name'] = $userObject->name;
        $config['to'] = $userObject->email;
        $config['subject'] = 'Binary Income Credited';
        $config['body'] = Yii::app()->controller->renderPartial('//mailTemp/binary_commission', array('userObjectArr' => $userObjectArr), true);
        CommonHelper::sendMail($config);
        return 1;
    }

    public function actionGenealogy() {
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/application/custom_validation.js', CClientScript::POS_END);
        
        $genealogyLeftListLevalTwo = "";
        $genealogyRightListLevalTwo = "";
        $genealogyRightLeftListLevalTwo = "";
        $genealogyRightRightListLevalTwo = "";
        
        $sponsorObject = User::model()->findAll(array('condition' => 'sponsor_id = ' . Yii::app()->params["adminSpnId"]));
        $currentUserId = !empty($_GET['id']) ? $_GET['id'] : BaseClass::mgEncrypt(Yii::app()->params["adminId"]);
        $currentDecriptId = BaseClass::mgDecrypt($currentUserId);
        $userObject = User::model()->findByAttributes(array('id' => BaseClass::mgDecrypt($currentUserId)));
        
        /* 1st level  */
        $genealogyLeftListObject = BaseClass::getGenoalogyTreeChild($currentDecriptId, "left");
        $genealogyRightListObject = BaseClass::getGenoalogyTreeChild($currentDecriptId, "right");

        /* 2nd level left */
        if($genealogyLeftListObject){
            $genealogyLeftListLevalTwo = BaseClass::getGenoalogyTreeChild($genealogyLeftListObject->user_id, "left");  
            $genealogyRightListLevalTwo = BaseClass::getGenoalogyTreeChild($genealogyLeftListObject->user_id, "right");  
        }
        
        /* 2nd level right */
        if($genealogyRightListObject){
            $genealogyRightLeftListLevalTwo = BaseClass::getGenoalogyTreeChild($genealogyRightListObject->user_id, "left");  
            $genealogyRightRightListLevalTwo= BaseClass::getGenoalogyTreeChild($genealogyRightListObject->user_id, "right");  
        }
        
        $this->render('viewGenealogy', array(
            'genealogyLeftListObject' => $genealogyLeftListObject,
            'genealogyRightListObject' => $genealogyRightListObject,
            'currentUserId' => $currentUserId,
            'userObject' => $userObject,
            'genealogyLeftListLevalTwo' => $genealogyLeftListLevalTwo,
            'genealogyRightListLevalTwo' => $genealogyRightListLevalTwo,
            'genealogyRightLeftListLevalTwo' => $genealogyRightLeftListLevalTwo,
            'genealogyRightRightListLevalTwo' => $genealogyRightRightListLevalTwo,
        ));
    }
    
    /**
     * Lists all models.
     */
    public function actionIndex() {
        $successMsg = "";
        if (!empty($_POST['user_bulkaction_submit'])) {
        $queryString = $_SERVER['QUERY_STRING'];
            if (!empty($_POST['bulkaction']) && !empty($_POST['requestids'])) {
                $filterArray = $_POST['requestids'];
                $this->userBulkActions($filterArray, $queryString);
             } else{
                 Yii::app()->user->setFlash('error', 'Please select user');
                 $this->redirect($_SERVER['HTTP_REFERER']); 
             }
        }
        
        $this->render('index', array( 'successMsg' => $successMsg));
    }
    
    public function actionReadUserData(){
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
        $searchValue = "";
        if(!empty($_POST['search']['value'])){
            $searchValue = 'transaction_id = '.$_POST['search']['value'] . ' AND ';
        }
        
        $statusCondition = 'u.status = 1';
         if($_POST['columns'][10]['search']['value'] == 2) {
              $statusCondition = 'u.status = 0';
         }
         if($_POST['columns'][10]['search']['value'] == 'all') {
              $statusCondition = '';
         }
        $dataQuery = Yii::app()->db->createCommand()
            ->select('u.id,u.name,u.full_name,u.position,u.email,u.created_at,CONCAT("+",country_code," ",phone) as phoneNuber,p.phone,spn.name as sponsorName, '
                    . '(case when p.is_mobno_varified = 0 then " X " ELSE "Verified" END) as mobileStatus , '
                    . '(case when p.document_status = 1 then "Verified" when p.document_status = 2 then "Rejected" ELSE " X " END) as documentStatus , '
                    . '(case when u.status = 0 then "InActive" ELSE "ACTIVE" END ) as userStatus')
            ->from('user u')
            ->join('user_profile p', 'u.id=p.user_id')
            ->join('user spn', 'u.sponsor_id=spn.id');
            $dataQuery->where('u.role_id IN (1,3)');
            if(!empty($_POST['search']['value'])){
                $searchData = $_POST['search']['value'];
                $dataQuery->andWhere('u.name LIKE :username', array(':username' => '%'.$searchData.'%'));
                $dataQuery->orWhere('u.full_name LIKE :fullName', array(':fullName' => '%'.$searchData.'%'));
                $dataQuery->orWhere('u.email LIKE :email', array(':email' => '%'.$searchData.'%'));
                $dataQuery->orWhere('spn.name LIKE :spnName', array(':spnName' => '%'.$searchData.'%'));
            }
            if(!empty($_POST['columns']['9']['search']['value']) && !empty($_POST['columns']['9']['search']['regex'])){
                $fromDate = $_POST['columns']['9']['search']['value'];
                $todayDate = $_POST['columns']['9']['search']['regex'];
                $dataQuery->andWhere('u.created_at >= :todate', array(':todate' => $todayDate));
                $dataQuery->andWhere('u.created_at <= :fromdate', array(':fromdate' => $fromDate));
            }
            if(!empty($statusCondition)){
                $dataQuery->andWhere($statusCondition);
            }
            $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $userListObject = $dataQuery->queryAll();
        //Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->from('user u')
            ->join('user_profile p', 'u.id=p.user_id')
            ->join('user spn', 'u.sponsor_id=spn.id');
            $countQuery->where('u.role_id IN (1,3)');
            if(!empty($_POST['search']['value'])){
                $searchData = $_POST['search']['value'];
                $countQuery->andWhere('u.name LIKE :username', array(':username' => '%'.$searchData.'%'));
                $countQuery->orWhere('u.full_name LIKE :fullName', array(':fullName' => '%'.$searchData.'%'));
                $countQuery->orWhere('u.email LIKE :email', array(':email' => '%'.$searchData.'%'));
                $countQuery->orWhere('spn.name LIKE :spnName', array(':spnName' => '%'.$searchData.'%'));
            }
            if(!empty($_POST['columns']['9']['search']['value']) && !empty($_POST['columns']['9']['search']['regex'])){
                $countQuery->andWhere('u.created_at >= :todate', array(':todate' => $todayDate));
                $countQuery->andWhere('u.created_at <= :fromdate', array(':fromdate' => $fromDate));
            }
            if(!empty($statusCondition)){
                $countQuery->andWhere($statusCondition);
            }
            $totalRecordsArray = $countQuery->queryRow();
            
        $userObjectCount = $totalRecordsArray['totalRecords'];

        $userJSONData = CJSON::encode($userListObject);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $userObjectCount . ',
                    "recordsFiltered": ' . $userObjectCount . ',
                    "data":' . $userJSONData . '
        }';
        exit;
    }
    public function actionReadInActiveUserData(){
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
        $searchValue = "";
        if(!empty($_POST['search']['value'])){
            $searchValue = 'transaction_id = '.$_POST['search']['value'] . ' AND ';
        }
        
 
        $dataQuery = Yii::app()->db->createCommand()
            ->select('u.id,u.name,u.full_name,u.position,u.email,u.created_at,CONCAT("+",country_code," ",phone) as phoneNuber,p.phone,spn.name as sponsorName, '
                    . '(case when p.is_mobno_varified = 0 then " X " ELSE "Verified" END) as mobileStatus , '
                    . '(case when p.document_status = 1 then "Verified" when p.document_status = 2 then "Rejected" ELSE " X " END) as documentStatus , '
                    . '(case when u.status = 0 then "InActive" ELSE "ACTIVE" END ) as userStatus')
            ->from('user u')
            ->join('user_profile p', 'u.id=p.user_id')
            ->join('user spn', 'u.sponsor_id=spn.id');
            $dataQuery->where('u.role_id IN (1,3)');
            if(!empty($_POST['search']['value'])){
                $searchData = $_POST['search']['value'];
                $dataQuery->andWhere('u.name LIKE :username', array(':username' => '%'.$searchData.'%'));
                $dataQuery->orWhere('u.full_name LIKE :fullName', array(':fullName' => '%'.$searchData.'%'));
                $dataQuery->orWhere('u.email LIKE :email', array(':email' => '%'.$searchData.'%'));
                $dataQuery->orWhere('spn.name LIKE :spnName', array(':spnName' => '%'.$searchData.'%'));
            }
            if(!empty($_POST['columns']['9']['search']['value']) && !empty($_POST['columns']['9']['search']['regex'])){
                $fromDate = $_POST['columns']['9']['search']['value']; 
                $todayDate = $_POST['columns']['9']['search']['regex'];
                $dataQuery->andWhere('u.created_at >= :todate', array(':todate' => $todayDate));
                $dataQuery->andWhere('u.created_at <= :fromdate', array(':fromdate' => $fromDate));
            }
            $dataQuery->andWhere('u.status = 0');
            $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $userListObject = $dataQuery->queryAll();
        //Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->from('user u')
            ->join('user_profile p', 'u.id=p.user_id')
            ->join('user spn', 'u.sponsor_id=spn.id');
            $countQuery->where('u.role_id IN (1,3)');
            if(!empty($_POST['search']['value'])){
                $searchData = $_POST['search']['value'];
                $countQuery->andWhere('u.name LIKE :username', array(':username' => '%'.$searchData.'%'));
                $countQuery->orWhere('u.full_name LIKE :fullName', array(':fullName' => '%'.$searchData.'%'));
                $countQuery->orWhere('u.email LIKE :email', array(':email' => '%'.$searchData.'%'));
                $countQuery->orWhere('spn.name LIKE :spnName', array(':spnName' => '%'.$searchData.'%'));
            }
            if(!empty($_POST['columns']['9']['search']['value']) && !empty($_POST['columns']['9']['search']['regex'])){
                $countQuery->andWhere('u.created_at >= :todate', array(':todate' => $todayDate));
                $countQuery->andWhere('u.created_at <= :fromdate', array(':fromdate' => $fromDate));
            }
            $countQuery->andWhere('u.status = 0');
            $totalRecordsArray = $countQuery->queryRow();
            
        $userObjectCount = $totalRecordsArray['totalRecords'];

        $userJSONData = CJSON::encode($userListObject);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $userObjectCount . ',
                    "recordsFiltered": ' . $userObjectCount . ',
                    "data":' . $userJSONData . '
        }';
        exit;
    }
    
    public function userBulkActions($filterArray, $queryString){
               if ($_POST['bulkaction'] == 1) { //For sending activation mail
                $result = $this->ResendActivation($filterArray);
                if (!empty($result)) {
                     Yii::app()->user->setFlash('success', 'Resend Activation Sent Successfully');
                } else{
                     Yii::app()->user->setFlash('error', 'Resend Activation Failed');
                }
                $this->redirect($_SERVER['HTTP_REFERER']);
            } else if ($_POST['bulkaction'] == 2) { //For sending sms 
                $result = $this->SendSMSRemainder($filterArray);
                if (!empty($result)) {
                     Yii::app()->user->setFlash('success', 'SMS Remainder Sent Successfully');
                } else {
                     Yii::app()->user->setFlash('error', 'SMS Remainder Sent Failed');
                }
                $this->redirect($_SERVER['HTTP_REFERER']);
            } else if ($_POST['bulkaction'] == 3) { //For sending mail to sponsor
                $result = $this->SendSponsorEmail($filterArray);
                if (!empty($result)) {
                    Yii::app()->user->setFlash('success', 'Email Sent Successfully');
                } else {
                    Yii::app()->user->setFlash('error', 'Email Sent Failed');
                }
                $this->redirect($_SERVER['HTTP_REFERER']);
            } else if ($_POST['bulkaction'] == 4 || $_POST['bulkaction'] == 5) { //For changing mobile verification status
                $result = $this->updateMobile($filterArray, $_POST['bulkaction']);
                if (!empty($result)) {
                     Yii::app()->user->setFlash('success', 'Mobile Status Changed Successfully');
                } else {
                     Yii::app()->user->setFlash('error', 'Mobile Status Update Failed');
                }
                $this->redirect(array('/admin/user'));
            }
            else if ($_POST['bulkaction'] == 6) { //For sending internal mail to sponsor
                $result = $this->SendSponsorInternalMail($filterArray);
                if (!empty($result)) {
                    Yii::app()->user->setFlash('success', 'Internal Mail Sent Successfully');
                } else {
                     Yii::app()->user->setFlash('error', 'Internal Mail Sent Failed');
                }
            $this->redirect($_SERVER['HTTP_REFERER']);
            }
    }
    
     public function userCsvExport($condition){
        $filename = "userlist" . date('d-m-Y') . '.csv';
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="'.$filename.'"');
        $criteria = new CDbCriteria;
        $criteria->condition = $condition;
        $criteria->order = ('id DESC');
        $userObject = User::model()->findAll($criteria);
        
        $data = "Sponsor, Name, Full Name, Email, Phone, Status(1:Active & 0:Inactive), Date\n";
        if (count($userObject) > 0) {
            foreach ($userObject as $user) {
                $status = "";
                if($user->status == 1){
                    $status = "Active"; 
                } else {
                    $status = "Inactive";  
                }
                
                $phone = "";
                $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $user->id));
                if (isset($userProfileObject) && !empty($userProfileObject)) {
                 if ($userProfileObject->country_code != 0 && $userProfileObject->phone != 0) {
                     $phone = "+" . $data->userprofile->country_code . "- ". $data->userprofile->phone;
                 }else{
                     $phone = "NA";
                 }
                }
                
                $data .= $user->sponsor()->full_name . "," . $user->name.",".$user->full_name.",".$user->email.",".$phone.",".$status.",".$user->created_at."\n";
            }
        }
        echo $data;
        exit();
     }

    public function actionList() {
        $model = new User;
        $pageSize = 10;

        $dataProvider = new CActiveDataProvider('User', array(
            'pagination' => array('pageSize' => $pageSize),
        ));
        if (!empty($_POST['search'])) {
            $dataProvider = CommonHelper::search(isset($_REQUEST['search']) ? $_REQUEST['search'] : "", $model, array('full_name', 'email', '	phone', 'sponsor_id'), array(), isset($_REQUEST['selected']) ? $_REQUEST['selected'] : "");
        }
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionWallet() {
       $model = new Wallet;
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $walletType = "";
        $error = "";
        $order = "";
        
        if(!empty($_POST['requestids'])){   
            
            $allRequestIds = '';
            $arrayRequestedId = $_POST['requestids'];
            foreach($arrayRequestedId as $requestedId) {
                $allRequestIds .= $requestedId . ",";
            }

            $allRequestIds = trim($allRequestIds, ",");
            $allRequestIds = "'" . str_replace(array("'", ","), array("\\'", "','"), $allRequestIds) . "'";
            
            if($_POST['actiontype'] == 'enable') {
                $updateQuery = "UPDATE wallet SET wallet_transaction_status = 'YES' 
                        WHERE id IN (".$allRequestIds.")";
                Yii::app()->user->setFlash('success', "Wallet Transaction Enabled successfully.");
             } else if($_POST['actiontype'] == 'disable') {
                $updateQuery = "UPDATE wallet SET wallet_transaction_status = 'NO'
                        WHERE id IN (".$allRequestIds.")";
                Yii::app()->user->setFlash('error', "Wallet Transaction Disabled successfully.");
             }

            $command = Yii::app()->db->createCommand($updateQuery)->execute();
            $this->redirect('wallet');
        }
        
        if (empty($_GET['Wallet_sort'])) {
            $order = 'user_id desc';
        }
        //Cash wallet
        if (!empty($_GET['walletType'])) {
            $walletType = $_GET['walletType'];
            $condition = "  type = '" . $walletType . "' AND status = 1";
        } else {
            $condition = "  type IN (1,2,3,4) AND status = 1";
        }

        if (isset($_GET['search']) && $_GET['search'] != '') {
            $userIdString = '';
            $userSearchString = "'" . str_replace(array("'", ","), array("\\'", "','"), $_GET['search']) . "'";
            $userObjectArray = User::model()->findAll(array('condition' => 'name IN ('.$userSearchString.')'));
            
            foreach ($userObjectArray as $userObject) {
                $userIdString .= $userObject->id . ',';
            }
            
            $userIdString = trim($userIdString, ',');
            $userIdString = "'" . str_replace(array("'", ","), array("\\'", "','"), $userIdString) . "'";
            if (!empty($userObjectArray)) {
                $condition .= ' AND user_id IN (' . $userIdString . ') AND status = 1';
            } else {
                $error = "It seems user does not exists with us.";
            }
        }

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ($condition), 'order' => $order,
            ), 'pagination' => array('pageSize' => $pageSize),));

        $this->render('walletList', array(
            'dataProvider' => $dataProvider,
            'walletType' => $walletType,
            'error' => $error,
            'pageSize' => $pageSize
        ));
    }

    public function actionCreditWallet() {
        $error = "";
        $userId = "";
        $walletObj = "";
        $userObject = NULL;
        if ($_POST) {
            if ($_POST['cancelled'] == '1') {
                $error = "Transaction has been cancelled.";
            } else {
                $walletType = $_POST['towallettype'];
                $fundAmount = $_POST['paid_amount'];
                if ($_POST['comment'] == '') {
                    $_POST['comment'] = 'Fund recharge by admin';
                }
                $comment = $_POST['comment'];
                $postDataArray = $_POST;
                $postDataArray['paid_amount'] = 0 ;
                $postDataArray['used_rp'] = $fundAmount;
                $postDataArray['comment'] = "";
                $postDataArray['mode'] = Transaction::$_MODE_ADMIN_RECHARGE;
                $userId = $_POST['userId'];
                $loggedinUser = Yii::app()->session['userid'];
                
                $toUserObject = User::model()->findByPk($userId);
                
                //Getting admin or sub-admin wallet amount 
                $fromWalletObject = Wallet::model()->findByAttributes(array('user_id' => $loggedinUser, 'type' => $walletType));

                // check admin can't transfer fund more than admin wallet fund. 
                if ($fromWalletObject->fund < $fundAmount && $loggedinUser != 1) {
                    Yii::trace(Yii::app()->session['userid'] . ' This admin don"t have sufficient fund in his/her wallet');
                    throw new CHttpException(404, 'The requested page does not exist.');
                }
                
                if ($userId != 0 && ($loggedinUser != $userId)){
                    //Create Transaction
                    $postDataArray['user_id'] = $userId ;
                    $postDataArray['paid_amount'] = $_POST['towallettype'] == 1 ? $fundAmount : 0 ;
                    $postDataArray['used_rp'] = $_POST['towallettype'] != 1 ? $fundAmount : 0 ;
                    if($_POST['gateway_id'] == 8) {
                        $postDataArray['paid_amount'] = 0;
                        $postDataArray['used_rp'] = $fundAmount ;
                    }
                    
                    $postDataArray['gatewayId'] = ( $_POST['towallettype'] == 4 && $toUserObject->role_id != 2 ) ? $_POST['gateway_id'] : 8;
                    $postDataArray['payment_transaction_id'] = isset($_POST['payment_transaction']) ? $_POST['payment_transaction']:"";
                    $postDataArray['status'] = 1 ;           
                    $transactionObject = Transaction::model()->createAddFundCreation($postDataArray);
                    $transactionId = $transactionObject->id;
                    
                    //Create Money Transfer
                    $postDataArray['fromwallettype'] = $walletType; // Bid wallet
                    $postDataArray['towallettype'] = $walletType; // Bid wallet
                    $postDataArray['toUserId'] = $userId;
                    $postDataArray['fromUserId'] = 1 ; // admin id
                    $postDataArray['walletId'] = $walletType; //Admin Bid wallet
                    $postDataArray['comment'] = $comment;
                    $postDataArray['status'] = 1;
                    if ($transactionObject) {  
                        $moneyTransferObject = MoneyTransfer::model()->createCompleteMoneyTransfer($postDataArray, $transactionObject, $userId , Yii::app()->params['adminId'] ,$fundAmount );
                    
                        if( $walletType == 5 && $toUserObject->role_id == 1  ){ 
                            $todayDate = date("Y-m-d");
                            //Create Order
                            $packageType = Package::getPackage($fundAmount);
                            $orderObject = new Order();
                            $orderArray['transaction_id'] = $transactionId;
                            $orderArray['package_price'] = $fundAmount;
                            $orderArray['user_id'] = $userId;
                            $orderArray['type'] = Order::$_TYPE_ADDCASH;
                            $orderArray['start_date'] = $todayDate;
                            if(Purchase::isWeekday($todayDate)) {
                                $orderArray['end_date'] = Purchase::addWeekdays($todayDate,($packageType->validity-1)); 
                            } else {
                                $orderArray['end_date'] = Purchase::addWeekdays($todayDate,($packageType->validity)); 
                            }
                            $orderArray['package_id'] = $packageType->id; //TODO: 7: free backage
                            $orderArray['status'] = 1; //TODO: Success
                            $orderObject = Order::model()->addEdit($orderObject, $orderArray);
                        
                            $packageObject = $orderObject->package();
                            Purchase::_updateMemebrshipType($packageObject, $toUserObject);
                            self::creatDirectCommission($orderObject, $toUserObject);
                        }
                        
                        /* Create Money Transation of the perticular Order or fund */
                        $toUserObjectMail = User::model()->findByPK($moneyTransferObject->to_user_id);
                        $userObjectArr['to_name'] = $toUserObjectMail->name;
                        $userObjectArr['full_name'] = $toUserObjectMail->full_name;
                        $userObjectArr['from_name'] = ucwords(Yii::app()->params['adminSpnName']);
                        $userObjectArr['date'] = date('Y-m-d');
                        $userObjectArr['fund'] = $transactionObject->actual_amount;
                        $userObjectArr['transactionId'] = $transactionObject->transaction_id;
                        $userObjectArr['from_wallet'] = isset($moneyTransferObject->from_fund_type)?$moneyTransferObject->formwallettype()->name:"";
                        $userObjectArr['to_wallet'] = isset($moneyTransferObject->to_fund_type)?$moneyTransferObject->towallettype()->name:"";
                        /* mail to user */
                        $config['to'] = $toUserObjectMail->email;
                        $config['subject'] = $moneyTransferObject->towallettype()->name.' Wallet Recharged By Admin';
                        
                        $config['body'] = $this->renderPartial('../mailTemplate/fund_transfer', array('userObjectArr' => $userObjectArr), true);
                        CommonHelper  ::sendMail($config);
                        // Send internal mail.
                        BaseClass::sendInternalMail($toUserObjectMail, $config['subject'], 'Your '.$moneyTransferObject->towallettype()->name.' wallet recharged successfully.');
                        Yii::app()->user->setFlash('success', "User wallet recharged successfully.");
                        if(!empty($_REQUEST['mode']) && $_REQUEST['mode'] == Transaction::$_MODE_ADDFUND) {
                            $this->redirect(array('/admin/user/creditwallet?mode=AddFund'));
                        }
                        $this->redirect(array('user/wallet'));
                    }                    
                }
                else{
                    if ($userId == 0) {
                        $error = "User does not exist.";
                    } else if ($userId == 1 && $loggedinUser == 1) { // For admin recharge self
                        $adminWalletObject = Wallet::model()->findByAttributes(array('user_id' => $loggedinUser, 'type' => $walletType));
                        $fundAmount = ($fundAmount + $adminWalletObject->fund);
                        $adminWalletObject->fund = $fundAmount;
                        $adminWalletObject->updated_at = new CDbExpression('NOW()');
                        $adminWalletObject->update();
                    } else if ($userId != 1 && ($loggedinUser == $userId)) {
                        $error = "Cannot add wallet for self contact super admin.";
                    }
                }
                
                
            }
        }
        $userType = 1;
        $userId = 0;
        if (!empty($_GET['id'])) {
            $userId = BaseClass::mgDecrypt($_GET['id']);
            if (!empty($_GET['type'])) {
                $userType = BaseClass::mgDecrypt($_GET['type']); 
            }
        }
        
        $condition = " (payment_status = 1 or id = 8) " ;
        $gatewayObject = Gateway::model()->findAll(array('condition' => $condition ));
        
        $walletObj = Wallet::model()->findByAttributes(array('user_id' => Yii::app()->session['userid'], 'type' => $userType));
        if (!isset($userObject)) {
            $userObject = User::model()->findByPk($userId);
        }

        $this->render('creditwallet', array('userObject' => $userObject, 'walletObj' => $walletObj, 'gatewayObject' =>$gatewayObject ,'error' => $error));
    }

    public function actionDebitWallet() {
        $error = "";
        $walletObj = "";
        if ($_POST) {
            if ($_POST['cancelled'] == '1') {
                $error = "Transaction has been cancelled.";
            } else {
                $userId = $_POST['userId'];
                $postDataArray = $_POST;
                $toUserObject = User::model()->findByPk($userId);
                $walletType = $_POST['towallettype'];
                $fundAmount = $_POST['paid_amount'];
                $postDataArray['paid_amount'] = 0 ;
                $postDataArray['used_rp'] = $fundAmount;
                if ($_POST['comment'] == '') {
                    $_POST['comment'] = 'Fund deducted by admin';
                }
                
                $postDataArray['mode'] = Transaction::$_MODE_ADMIN_DEDUCTFUND;
                //In deducting the wallet money transfer is inward for admin
                // hence the special override of session
                $comment = $_POST['comment'];
                $postDataArray['toUserId'] = Yii::app()->session['userid'];
                $postDataArray['comment'] = $comment;
                $postDataArray['fromUserId'] = $userId;
                $postDataArray['towallettype'] = $walletType;
                $postDataArray['fromwallettype'] = $walletType;
                //Getting user wallet amount information
                $toWalletObject = Wallet::model()->findByAttributes(array('user_id' => $userId, 'type' => $walletType));
                
                // check admin can't transfer fund more than user wallet fund. 
                if ($toWalletObject->fund < $fundAmount) {
                    Yii::trace(Yii::app()->session['userid'] . ' user don"t have sufficient fund in his/her wallet');
                    throw new CHttpException(404, 'The requested page does not exist.');
                }
                
                $transactionObject = Transaction::model()->createTransaction($postDataArray, $toUserObject, 'admin');

                /* Create Money Transation of the perticular Order or fund */
                $moneyTransferObject = MoneyTransfer::model()->createCompleteMoneyTransfer($postDataArray, $transactionObject,Yii::app()->session['userid'], $userId ,$fundAmount ,1 , NULL , $toWalletObject);
                
                $userObjectArr = array();
                $fromUserObjectMail = User::model()->findByPK($moneyTransferObject->from_user_id);
                $userObjectArr['to_name'] = ucwords(Yii::app()->params['adminSpnName']);
                $userObjectArr['full_name'] = $fromUserObjectMail->full_name;
                $userObjectArr['from_name'] = $fromUserObjectMail->name;
                $userObjectArr['date'] = date('Y-m-d');
                $userObjectArr['fund'] = $transactionObject->actual_amount;
                $userObjectArr['transactionId'] = $transactionObject->transaction_id;
                $userObjectArr['from_wallet'] = isset($moneyTransferObject->from_fund_type)?$moneyTransferObject->formwallettype()->name:"";
                $userObjectArr['to_wallet'] = isset($moneyTransferObject->to_fund_type)?$moneyTransferObject->towallettype()->name:"";
                /* mail to user */
                $config['to'] = $fromUserObjectMail->email;
                $config['subject'] = $moneyTransferObject->formwallettype()->name.' Wallet Amount Deducted By Admin';
                $config['body'] = $this->renderPartial('../mailTemplate/fund_transfer', array('userObjectArr' => $userObjectArr), true);
                CommonHelper::sendMail($config);

                // Send internal mail.
                BaseClass::sendInternalMail($fromUserObjectMail, $config['subject'], 'Fund Deducted from your '.$moneyTransferObject->formwallettype()->name.' wallet.');
                Yii::app()->user->setFlash('success', "User wallet deducted successfully.");
                $this->redirect(array('user/wallet'));
            }
        }
        $userId = BaseClass::mgDecrypt($_GET['id']);
        if (!empty($_GET['type'])) {
            $userType = BaseClass::mgDecrypt($_GET['type']);
            $walletObj = Wallet::model()->findByAttributes(array('user_id' => $userId, 'type' => $userType));
        }
        $userObject = User::model()->findByPk($userId);
        $this->render('debitwallet', array('userObject' => $userObject, 'walletObj' => $walletObj, 'error' => $error));
    }
    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new User('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /*
     * Function to add multiple admin by superadmin
     */

    public function actionAdd1() {
        $success = "";
        $error = "";
        $countryObject = Country::model()->findAll();

        $this->render('user_add', array(
            'countryObject' => $countryObject, 'error' => $error, 'success' => $success
        ));
    }

    /*
     * Function to Delete Users from list
     */

    public function actionDeleteUser() {
        if ($_REQUEST['id']) {
            $userObject = User::model()->findByPK($_REQUEST['id']);
            $userprofileObject = UserProfile::model()->findByAttributes(array('user_id' => $_REQUEST['id']));
            $userObject->delete();
            if ($userprofileObject) {
                $userprofileObject->delete();
            }
            $this->redirect(array('/admin/user/index', 'successMsg' => 2));
        }
    }

    /*
     * Function to fetch verification document
     */

    public function actionVerificationApproval() {
        $model = new UserProfile();
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['defaultPageSize'];
        $condition = 'id_proof != "" AND address_proff != ""'; //Default condition
        $status = 0;
    
        if(!empty($_REQUEST['from']) && !empty($_REQUEST['to'])){
            $todayDate = date("Y-m-d",strtotime($_REQUEST['from']));
            $fromDate = date("Y-m-d",strtotime($_REQUEST['to']));
            $condition .= ' AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"';
        }

        if(isset($_REQUEST['res_filter']) && !empty($_REQUEST['res_filter'])){
            $status = $_REQUEST['res_filter'];
            if ($status != 'all') {
                $condition .= ' AND document_status = ' . $status ;
            } else {
                $condition .= ' AND document_status IN (1,0,2)';
            }
        } else {
           $condition .= ' AND document_status = ' . $status ; 
        }
       
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ($condition), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),
        ));
        
        $this->render('verification_approval', array(
            'dataProvider' => $dataProvider, 'pageSize' => $pageSize
        ));
    }

    public function actionChangeApprovalStatus() {
        if ($_REQUEST['id']) {
            $userprofileObject = UserProfile::model()->findByPk($_REQUEST['id']);
            $userObject = User::model()->findByPk($userprofileObject->user_id);
            if ($_REQUEST['status'] == 0) {
                $userprofileObject->document_status = 1;

                // Send internal mail.
                BaseClass::sendInternalMail($userObject, 'Document Verification Status Changed.', 'Document Verification Status is Approved.');
            }
            if ($_REQUEST['status'] == 1) {
                $userprofileObject->document_status = 0;

                // Send internal mail.
                BaseClass::sendInternalMail($userObject, 'Document Verification Status Changed.', 'Document Verification Status is Pending.');
            }

            $userprofileObject->save(false);
            $this->redirect(array('/admin/user/verificationapproval', 'successMsg' => 1));
        }
    }
    
    public function actionChangeToReject() {
        if ($_REQUEST['id']) {
            $userprofileObject = UserProfile::model()->findByPk($_REQUEST['id']);
            $userObject = User::model()->findByPk($userprofileObject->user_id);
            $userprofileObject->document_status = 2; // 2. Reject.
            $userprofileObject->save(false);

            // Send internal mail.
            BaseClass::sendInternalMail($userObject, 'Document Verification Status Changed.', 'Document Verification Status is Rejected.');
            $this->redirect(array('/admin/user/verificationapproval', 'successMsg' => 1));
        }
    }
    
        public function actionViewVerificationProfile() {
        if ($_GET['id']) {
            $userId = BaseClass::mgDecrypt($_GET['id']);
            $userProfileObject = UserProfile::model()->findByPK($userId);
            if (!$userProfileObject) {
                throw new CHttpException(404, 'The specified user cannot be found.');
            }
            $this->render('view_verification_profile', array(
                'userProfileObject' => $userProfileObject,
            ));
        } else {
            throw new CHttpException(404, 'The specified user cannot be found.');
        }
    }

    /*
     * Function to fetch verification document
     */

    public function actionTestimonialApproval() {
        $model = new Testimonial();
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['defaultPageSize'];
        $status = 0;
        $condition = "";
        
        $order = "id desc";
        if(!empty($_GET['Testimonial_sort'])){ $order = '"'.$_GET['Testimonial_sort'].'" DESC'; }
        
        if(isset($_REQUEST['res_filter']) && !empty($_REQUEST['res_filter'])){
            $status = $_REQUEST['res_filter'];
            if($status != "all"){
                $condition .= ' testimonial_status = "' . $status . '"';
            } else {
                $condition .= ' testimonial_status IN (1,0,2)';
            }
        } else {
           $condition .= ' testimonial_status = "' . $status . '"';
        }
        
        if(!empty($_REQUEST['from']) && !empty($_REQUEST['to'])){
            $todayDate = date("Y-m-d",strtotime($_REQUEST['from']));
            $fromDate = date("Y-m-d",strtotime($_REQUEST['to']));
            $condition .= ' AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"';
        }

            $dataProvider = new CActiveDataProvider($model, array(
                'criteria' => array('condition' => ($condition), 'order' => $order),
                'pagination' => array('pageSize' => $pageSize),
            ));
        
        $this->render('testimonial_approval', array(
            'dataProvider' => $dataProvider, 'pageSize' => $pageSize
        ));
    }
    
    protected function GetTestimonialAction($data, $row) {

        if ($data->testimonial_status == 0 || $data->testimonial_status == 1) {
            $testimonialAction = '<a href="/admin/user/testimonialrejectstatus?id=' . $data->id . '" class="action-icons" title="Reject"><i class="fa fa-ban"></i></a><a href="/admin/user/testimonialapprovalstatus?id=' . $data->id . '" class="action-icons" title="Change Status"><i class="fa fa-retweet"></i></a>';
        }  else {
            $testimonialAction = '';
        }

        echo $testimonialAction;
    }
    
       public function actionTestimonialRejectStatus() {
        if ($_REQUEST['id']) {
            $testimonialObject = Testimonial::model()->findByPk($_REQUEST['id']);
            $userObject = User::model()->findByPk($testimonialObject->user_id);
            $testimonialObject->testimonial_status = 2;
            $testimonialObject->save(false);
            // Send internal mail.
            BaseClass::sendInternalMail($userObject, 'Testimonial Status Changed', 'Your Testimonial status is rejected.');

            $this->redirect(array('/admin/user/testimonialapproval', 'successMsg' => 1));
        }
    }

    protected function GetTestimonialVideo($data, $row) {
        $testimonial = "";
            if ($data->testimonial_type == 1) {    
                $testimonial = $data->testimonial;
            } else if($data->testimonial_type == 2){
                $testimonial = '<a href="'.$data->testimonial.'" target="_blank">'.$data->testimonial.'</a>';
            } else {
                $testimonial = '';
            } 
   
        echo $testimonial;
    }
    
    public function actionTestimonialApprovalStatus() {
        if ($_REQUEST['id']) {
            $testimonialObject = Testimonial::model()->findByPk($_REQUEST['id']);
            if ($testimonialObject->testimonial_status == 1) {
                $testimonialObject->testimonial_status = 0;
            } else {
                $testimonialObject->testimonial_status = 1;
            }
            $testimonialObject->save(false);

            $this->redirect(array('/admin/user/testimonialapproval', 'successMsg' => 1));
        }
    }
    
    /*
     * Function to fetch verification profile image
     */

    public function actionProfileImageApproval() {
        $model = new UserProfile();
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['defaultPageSize'];
        $status = "0";
        $condition = 'profile_image != "" ';
        
        $order = "id desc";
        if(!empty($_GET['UserProfile_sort'])){ $order = '"'.$_GET['UserProfile_sort'].'" DESC'; }
        
        if(isset($_GET['res_filter']) && !empty($_GET['res_filter'])){
            $status = $_GET['res_filter'];
            if ($status != 'all') { $condition .= ' AND profile_image_status = ' . $status; }
            else { $condition .= ' AND profile_image_status IN (0,1,2)'; }
            
        } else {
            $condition .= ' AND profile_image_status = ' . $status;
        }
        
        if(!empty($_GET['from']) && !empty($_GET['to'])){
            $todayDate = date("Y-m-d",strtotime($_GET['from']));
            $fromDate = date("Y-m-d",strtotime($_GET['to']));
            $condition .= ' AND created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"';
        }

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ($condition), 'order' => $order,
            ),
            'pagination' => array('pageSize' => $pageSize),
        ));
    
        $this->render('profile_image_approval', array(
            'dataProvider' => $dataProvider, 'pageSize' => $pageSize
        ));
    }
    
     protected function gridProfileImagePopup($data, $row) {
        $imagefolder = Yii::app()->params->imagePath['profileImage']; // folder with uploaded files
        echo "<a data-toggle='modal' href='#zoom_$data->id'>Click to open</a>" . '<div class="modal fade" id="zoom_' . $data->id . '" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog profile-dialog" style="width:350px;">
                        <div class="modal-content">
                         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center">Profile Image</h4>
      </div>
                                <div class="modal-body text-center" style="width: auto;overflow: auto;height: auto;">
                                
                                         <img src="' . $imagefolder . $data->profile_image . '">
                                                         </div>
                            </div>
                        </div>
                </div>';
    }
    
    protected function GetProfileImageAction($data, $row) {
    $profileImageApproval = "";
        if ($data->profile_image_status == 0 || $data->profile_image_status == 1) {
            $profileImageApproval = '<a href="/admin/user/profileimagerejectstatus?id=' . $data->id . '" class="action-icons" title="Reject"><i class="fa fa-ban"></i></a><a href="/admin/user/profileimageapprovalstatus?id=' . $data->id . '" class="action-icons" title="Change Status"><i class="fa fa-retweet"></i></a>';
        }
        echo $profileImageApproval;
    }
    
     public function actionProfileImageApprovalStatus() {
        if ($_REQUEST['id']) {
            $userprofileObject = UserProfile::model()->findByPk($_REQUEST['id']);
            if ($userprofileObject->profile_image_status == 1) {
                $userprofileObject->profile_image_status = 0;
            } else {
                $userprofileObject->profile_image_status = 1;
            }
            $userprofileObject->save(false);

            $this->redirect(array('/admin/user/profileimageapproval', 'successMsg' => 1));
        }
    }
    
    public function actionProfileImageRejectStatus() {
        if ($_REQUEST['id']) {
            $userprofileObject = UserProfile::model()->findByPk($_REQUEST['id']);
            if(isset($userprofileObject) && !empty($userprofileObject)){
                $userprofileObject->profile_image_status = 2;
                $userprofileObject->save(false);
                $this->redirect(array('/admin/user/profileimageapproval', 'successMsg' => 2));
            }
        }
    }

    /*
     * Function to update user records
     */

    public function actionEdit() {
        $error = "";
        $success = "";
        $departmentObject = Department::model()->findAll(array('condition' => 'status =1'));
        if ($_GET['id']) {
            $userId = BaseClass::mgDecrypt($_REQUEST['id']);
            $userObject = User::model()->findByPK($userId);
            $profileObject = UserProfile::model()->findByAttributes(array('user_id' => $userId));
            if ($_REQUEST['id'] && $_POST) {
                
                if ($_POST['UserProfile']['name'] != '' && $_POST['UserProfile']['full_name'] != '' && $_POST['UserProfile']['email'] != '') {
                    /* Updating User info */
                    $userEmailObject = User::model()->findAll(array('condition' => 'email = "'.$_POST['UserProfile']['email'].'" AND id != '.$userObject->id));
                    
                    if(empty($userEmailObject)){
                    $postDataArray = $_POST['UserProfile'];
                    $userObject->attributes = $postDataArray;
                    $userObject->updated_at = new CDbExpression('NOW()');
                    $userObject->update();

                    if ($userObject->update()) {
                    // Profile Object set, Update it.
                    if (isset($profileObject) && !empty($profileObject)) {                       
                        $profileObject->attributes = $postDataArray;
                        $profileObject->updated_at = new CDbExpression('NOW()');
                        $profileObject->update();
                    } else {
                        // Create it.
                        $profileObject = new UserProfile();
                        $profileObject->attributes = $postDataArray;
                        $profileObject->user_id = $userObject->id;
                        $profileObject->created_at = new CDbExpression('NOW()');
                        $profileObject->updated_at = new CDbExpression('NOW()');
                        $profileObject->save(false);
                    }
                    
                    if(isset($_POST['department']) && !empty($_POST['department'])){
                    foreach($_POST['department'] as $department) {
                    $existDepartmentObject = DepartmentMapping::model()->findByAttributes(array('user_id' => $_REQUEST['id'], 'department_id' => $department));
                        if(empty($existDepartmentObject)){ //removing duplicates
                        $departmentObject = new DepartmentMapping();
                        $departmentObject->department_id = $department;
                        $departmentObject->user_id = $model->id;
                        $departmentObject->save();
                        }
                    } }
                        if($userObject->status == 1){
                        $this->redirect(array('/admin/user', 'successMsg' => 3));
                        } else{
                           $this->redirect(array('/admin/user','status' =>BaseClass::mgEncrypt($userObject->status), 'successMsg' => 3));  
                        }
                    }
                  } else{
                      $error .="Email Id already exist";
                  }
                } else {
                    $error .="Please fill all required(*) marked fields.";
                }
            }
        }

        $countryObject = Country::model()->findAll();

        $this->render('user_edit', array(
            'countryObject' => $countryObject, 'error' => $error, 'success' => $success, 
            'userObject' => $userObject, 'profileObject' => $profileObject, 
            'departmentObject' => $departmentObject,
        ));
    }
    
    public function actionUpdatePassword() {
        if($_REQUEST['id'] && $_POST) {
            $userId = BaseClass::mgDecrypt($_REQUEST['id']);
            $userObject = User::model()->findByPK($userId);
            if($_POST['update_type'] == 'encrypt') {
                $userObject->password = $_POST['UserProfile']['password'];
            } else if($_POST['update_type'] == 'decrypt') {
                $userObject->password = md5($_POST['UserProfile']['password']);
            }
            $userObject->updated_at = new CDbExpression('NOW()');
            if ($userObject->update()) {
                $this->redirect(array('/admin/user/index', 'successMsg' => 3));
            }
        }
    }
    
    public function actionUpdateMasterPin() {
        if($_REQUEST['id'] && $_POST) {
            $userId = BaseClass::mgDecrypt($_REQUEST['id']);
            $userObject = User::model()->findByPK($userId);
            if($_POST['update_type'] == 'encrypt') {
                $userObject->master_pin = $_POST['UserProfile']['master_pin'];
            } else if($_POST['update_type'] == 'decrypt') {
                $userObject->master_pin = md5($_POST['UserProfile']['master_pin']);
            }
            $userObject->updated_at = new CDbExpression('NOW()');
            if ($userObject->update()) {
                $this->redirect(array('/admin/user/index', 'successMsg' => 3));
            }
        }
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function getOnClickEvent($data, $row) {
        $fullName = "'" . $data->name . "'";
        echo '<a onclick="OpenChatBox(' . $fullName . ')">Click to chat</a>';
    }

    public function actionResetPassword() {
        $error = "";
        $success = "";
        $userObject = User::model()->findByPK(array('id' => Yii::app()->session['userid']));
        if (!empty($_POST)) {
            if ($_POST['UserProfile']['old_password'] != '' && $_POST['UserProfile']['new_password'] != '' && $_POST['UserProfile']['confirm_password'] != '') {

                if ($userObject->password != md5($_POST['UserProfile']['old_password'])) {
                    $error .= "Incorrect old password";
                } else {
                    $userObject->password = md5($_POST['UserProfile']['new_password']);
                    if ($userObject->update()) {
                        /* $userObjectArr = array();
                          $userObjectArr['full_name'] = $userObject->full_name;
                          $userObjectArr['name'] = $userObject->name;
                          $userObjectArr['ip'] = Yii::app()->params['ip'];
                          $userObjectArr['new_password'] = $_POST['UserProfile']['new_password'];
                          $success .= "Your password changed successfully";
                          $config['to'] = $userObject->email;
                          $config['subject'] = 'Mavwealth Password Changed';
                          $config['body'] =  $this->renderPartial('//mailTemp/change_password', array('userObjectArr'=>$userObjectArr),true);

                          //$config['body'] = 'Hey ' . $userObject->full_name . ',<br/>You recently changed your password. As a security precaution, this notification has been sent to your email addresses.';
                          CommonHelper::sendMail($config); */
                        $success .= "Your password changed successfully";
                    }
                }
            } else {
                $error .="Please fill all required(*) marked fields.";
            }
        }

        $this->render('resetpassword', array(
            'error' => $error, 'success' => $success,
        ));
    }

    protected function gridAddressImagePopup($data, $row) {
        $bigImagefolder = Yii::app()->params->imagePath['verificationDoc']; // folder with uploaded files
        echo "<a data-toggle='modal' href='#zoom_$data->id'>Click to open</a>" . '<div class="modal adminModel fade" id="zoom_' . $data->id . '" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog" style="width:500px;">
                        <div class="modal-content">
                            <div class="modal-body" style="width: 500px;overflow: auto;height: 500px;padding: 0;">
                                <span class="cancelBtn" data-dismiss="modal">X</span>
                                <img src="' . $bigImagefolder . $data->address_proff . '">
                            </div>
                        </div>
                    </div>
            </div>';
    }

    protected function gridIdImagePopup($data, $row) {
        $bigImagefolder = Yii::app()->params->imagePath['verificationDoc']; // folder with uploaded files
        echo "<a data-toggle='modal' href='#zoom1_$data->id'>Click to open</a>" . '<div class="modal adminModel fade" id="zoom1_' . $data->id . '" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog" style="width:500px;">
                        <div class="modal-content">
                            <div class="modal-body" style="width: 500px;overflow: auto;height: 500px;padding: 0;">
                                <span class="cancelBtn" data-dismiss="modal">X</span>
                                <img src="' . $bigImagefolder . $data->id_proof . '">
                            </div>
                        </div>
                    </div>
            </div>';
    }
    
    protected function gridPhotoPopup($data, $row) {
        $bigImagefolder = Yii::app()->params->imagePath['verificationDoc']; // folder with uploaded files
        echo "<a data-toggle='modal' href='#zoom2_$data->id'>Click to open</a>" . '<div class="modal adminModel fade" id="zoom2_' . $data->id . '" tabindex="-1" role="basic" aria-hidden="true">
                    <div class="modal-dialog" style="width:500px;">
                        <div class="modal-content">
                            <div class="modal-body" style="width: 500px;overflow: auto;height: 500px;padding: 0;">
                                <span class="cancelBtn" data-dismiss="modal">X</span>
                                <img src="' . $bigImagefolder . $data->photo_proof . '">
                            </div>
                        </div>
                    </div>
            </div>';
    }

    public function actionAdd() {
        $error = "";
        $success = "";
        $departmentObject = Department::model()->findAll(array('condition' => 'status =1'));
        if ($_POST) {
            /* Already Exits */
            $userObject = User::model()->findByAttributes(array('name' => $_POST['name']));
            if (count($userObject) == 0) {
                //$userObject = User::model()->findByAttributes(array('name' => $_POST['sponsor_id']));
                $masterPin = BaseClass::getUniqInt(5);
                $password = BaseClass::getUniqInt(5);
                
                $model = new User;
                $model->attributes = $_POST;
                $model->role_id = 2;

                $model->password = BaseClass::md5Encryption($password);
                if(!empty($_POST['sponsor_id'])){
                    $model->sponsor_id = $_POST['sponsor_id']; 
                }else {
                    $model->sponsor_id = "admin";
                }
               
                $model->master_pin = BaseClass::md5Encryption($masterPin);
                $model->created_at = date('Y-m-d');


                if (!$model->save(false)) {
                    echo "<pre>";
                    print_r($model->getErrors());
                    exit;
                }
                $modelUserProfile = new UserProfile();
                $modelUserProfile->user_id = $model->id;
                $modelUserProfile->created_at = date('Y-m-d');
                $modelUserProfile->save(false);

                $accessObject = new UserHasAccess;
                $accessObject->user_id = $model->id;
                $accessObject->access = "dashboard";
                $accessObject->created_at = date('Y-m-d');
                $accessObject->save();
                
               if(isset($_POST['department']) && !empty($_POST['department'])){
                foreach($_POST['department'] as $department) {
                $departmentObject = new DepartmentMapping();
                $departmentObject->department_id = $department;
                $departmentObject->user_id = $model->id;
                $departmentObject->save();
                } }
                
                Yii::app()->user->setFlash('success', 'User Created Successfully.');
                $this->redirect(array('/admin/user/add'));
            }
        }
        $spnId = "";
        if ($_GET) {
            if (!empty($arra)) {
                $spnId = $arra[0];
            } else {
                $spnId = $_GET['spid'];
            }
        }
        $countryObject = Country::model()->findAll();

        $this->render('user_add', array('countryObject' => $countryObject, 'spnId' => $spnId, 'error' => $error, 'success' => $success,
            'departmentObject' => $departmentObject));
    }
    
        public function actionIsUserExisted() {
        if ($_POST) {
            $userObject = User::model()->findByAttributes(array('name' => $_POST['username']));
            if (count($userObject) > 0) {
                echo "1";
                exit;
            } else {
                echo "0";
                exit;
            }
        }
    }
    
    public function getUserCheckbox($data) {
        echo "<input type='checkbox' class='allcheckbox' name='requestids[" . $data->id . "]' value='" . $data->id . "'>";
    }
    
    public function gridPhone($data) {
        BaseClass::gridFullPhone($data);
    }
    
    public function getMobileStatus($data) {
        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data->id));
        if ($userProfileObject) {
            echo $userProfileObject->is_mobno_varified == 1 ? "Verified" : "Not Verified";
        }
    }

    public function getDocumentStatus($data) {
        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data->id));
        if ($userProfileObject) {
            echo $userProfileObject->document_status == 1 ? "Verified" : "Not Verified";
        }
    }
    
        public function ResendActivation($filterArray) {
        $response = '';
        foreach ($filterArray as $key => $filter) {
            $userObject = User::model()->findByAttributes(array('id' => $key));
            $rand = $userObject->activation_key;
            $config['to'] = $userObject->email;
            $config['subject'] = 'Mavwealth Activation link';
            $config['body'] = $this->renderPartial('/mailTemplate/confirmation', array('model' => $userObject, 'rand' => $rand), true);
            $response = CommonHelper::sendMail($config);
        }
        if (!empty($response)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function SendSMSRemainder($filterArray) {
        $response = '';
        foreach ($filterArray as $key => $filter) {
            $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $key));
            $configMsg['to'] = $userProfileObject->country_code . $userProfileObject->phone;
            $configMsg['text'] = "We have sent you an email with activation link. Activate your account with Mavwealth and earn benefits";
            $response = BaseClass::sendSMS($configMsg);
        }
        if (!empty($response)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function SendSponsorEmail($filterArray) {
        $response = 0;
        $adminName = Yii::app()->params['adminSpnId'];
        $filterArrayValues = implode(",",$filterArray);
        $spnIdArray = array();
        $sponsorUserArray = array();
 
        $userListObject = Yii::app()->db->createCommand("SELECT a.*,b.full_name as spnFullName,b.email as spnEmail,b.name as spnName ,b.id as spnId FROM user AS a, user AS b WHERE a.id IN (".$filterArrayValues.") and a.`sponsor_id` = b.id and b.status=1")->queryAll();
        foreach ($userListObject as $userObject){
         if ($userObject['sponsor_id'] == $adminName || $userObject['sponsor_id'] == 2) {
             continue;
         } else {
            if(in_array($userObject['spnId'],$spnIdArray)){
                array_push($spnIdArray,$userObject['spnId']);
                $sponsorUserArray[$userObject['spnId']][] = $userObject;
            } else {
                array_push($spnIdArray,$userObject['spnId']);
                $sponsorUserArray[$userObject['spnId']][] = $userObject;
            }
          }   
        }

        if(isset($sponsorUserArray) && !empty($sponsorUserArray)){
            foreach($sponsorUserArray as $sponsorUser){
                   
                    $sponserName = $sponsorUser[0]['spnFullName'];
                    $config['to'] = $sponsorUser[0]['spnEmail'];
                    $config['subject'] = 'Encourage your downline to Activate';
                    $config['body'] = $this->renderPartial('/mailTemplate/sponsormail', array('sponserName' => $sponserName, 'userObjects' => $sponsorUser), true);
                    $response = CommonHelper::sendMail($config); 
                    $response == 1;
        } }
        
        return $response;
    }

    public function updateMobile($filterArray, $bulkaction) {
        $response = 0;
        $bulkaction == 4 ? $bulkaction = 1 : $bulkaction = 0;
        
        foreach ($filterArray as $key => $filter) {
            $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $key));
            if ($userProfileObject) {
                $userProfileObject->otp = "";
                $userProfileObject->is_mobno_varified = $bulkaction;
                if (!$userProfileObject->update()) {
                    echo "<pre>";
                    print_r($userProfileObject->getErrors());
                    exit;
                }
                $response = 1;
            }
        }

        return $response;
    }
    
    public function SendSponsorInternalMail($filterArray) {
        $response = 0;
        $adminName = Yii::app()->params['adminSpnId'];
         $filterArrayValues = implode(",",$filterArray);
        $spnIdArray = array();
        $sponsorUserArray = array();
 
        $userListObject = Yii::app()->db->createCommand("SELECT a.*,b.full_name as spnFullName,b.email as spnEmail,b.name as spnName ,b.id as spnId FROM user AS a, user AS b WHERE a.id IN (".$filterArrayValues.") and a.`sponsor_id` = b.id and b.status=1")->queryAll();
        foreach ($userListObject as $userObject){
         if ($userObject['sponsor_id'] == $adminName || $userObject['sponsor_id'] == 2) {
             continue;
         } else {
            if(in_array($userObject['spnId'],$spnIdArray)){
                array_push($spnIdArray,$userObject['spnId']);
                $sponsorUserArray[$userObject['spnId']][] = $userObject;
            } else {
                array_push($spnIdArray,$userObject['spnId']);
                $sponsorUserArray[$userObject['spnId']][] = $userObject;
            }
          }   
        }

        if(isset($sponsorUserArray) && !empty($sponsorUserArray)){
            foreach($sponsorUserArray as $sponsorUser){
            
            
                    $postArray['subject'] = 'Encourage your downline to Activate';
                    $newMailObject = Mail::model()->create($postArray);
                    
                    if (isset($newMailObject) && !empty($newMailObject)) {
                        $departmentId = 2; //Support Department
                        $postArray['mail_id'] = $newMailObject->id;
                        $postArray['from_user_id'] = Yii::app()->session['userid'];
                        $postArray['to_user_id'] = $sponsorUser[0]['spnId'];
                        $postArray['department_id'] = $departmentId;
                        $postArray['message'] = "We have noticed that few of your down line members on Mavwelth have not activated their accounts after registration. Please encourage them to activate their account.<br><br>"
                                . "Refer more and earn more with our unique Compensation and Reward plan designed especially for you.<br><br>"
                                . "Below is the list of your downline who are not yet active.<br><br>"
                                . " <table width='100%' cellspacing='0' cellpadding='0' border='1'><tbody>"
                                . "<tr><td><b>Username</b></td><td><b>Full Name</b></td><td><b>Email Id</b></td><td><b>Phone Number</b></td></tr>";
                        foreach($sponsorUser as $sponsorUser){
                           $sponsorUserPhone = "";
                           $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $sponsorUser['id'] ));
                           if((isset($userProfileObject->phone) && ($userProfileObject->phone != 0))){
                               $sponsorUserPhone = $userProfileObject->phone;
                           }
                           $postArray['message'] .= "<tr><td>".$sponsorUser['name'] ."</td>";
                           $postArray['message'] .= "<td>".$sponsorUser['full_name'] ."</td>";
                           $postArray['message'] .= "<td>".$sponsorUser['email'] ."</td>";
                           $postArray['message'] .= "<td>". $sponsorUserPhone ."</td></tr>";
                        }
                        $postArray['message'] .= "</tbody></table>";
                        $postArray['status'] = 0;

                        $newUserHasConversations = UserHasConversations::model()->create($postArray, 1);
                        $response = 1;
                        
                    }

        } }

        return $response;
    }

    public function actionBroadcast(){
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/metronic/assets/plugins/summernote/summernote.css');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/metronic/assets/plugins/summernote/summernote.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/metronic/assets/plugins/summernote/components-editors.min.js', CClientScript::POS_END);
        
        $model = new BroadcastMessage();
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $condition = "";

        if (isset($_GET['id'])) {
            $broadcastMessageId = BaseClass::mgDecrypt($_GET['id']);
            $broadcastMessageObject = BroadcastMessage::model()->findByPk($broadcastMessageId);
        } else {
            $broadcastMessageObject = new BroadcastMessage();
        }

        if (isset($_GET['res_filter'])) {
            if ($_GET['res_filter'] != "all") {
                $condition .= 'status =' . $_GET['res_filter'];
            } else {
                $condition .= 'status IN(1,0)';
            }
        } else {
            $condition .= 'status = 1';
        }

        if(isset($_POST['addedit_submit']) && !empty($_POST['addedit_submit'])){ //Home banner add or edit
            $newBroadcastMessageObject = BroadcastMessage::model()->create($_POST, $_FILES, $broadcastMessageObject);
            if(isset($newBroadcastMessageObject) && !empty($newBroadcastMessageObject)){
              Yii::app()->user->setFlash('success', 'Broadcast message succesfully updated');
              $this->redirect('/admin/user/broadcast');
            } else {
              Yii::app()->user->setFlash('error', 'Broadcast message submition failed');
              $this->redirect('/admin/user/broadcast');  
            }
        }

        if(isset($_POST['bulkaction']) && !empty($_POST['bulkaction'])){ //Home banner active, in active or delete
            $queryString = isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:"";
            $filterArray = "";
        if(isset($_POST['requestids']) && !empty($_POST['requestids'])){
            $filterArray = $_POST['requestids'];
        }
        //echo '<pre>';print_r($filterArray);exit;
        $this->broadcastBulkActions($filterArray, $queryString);
        } 

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ($condition), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));


        $this->render('broadcast', array('dataProvider' => $dataProvider, 'pageSize' => $pageSize,
            'broadcastMessageObject' => $broadcastMessageObject));
    }
    
    public function broadcastBulkActions($filterArray, $queryString){
        if($_POST['bulkaction'] == 'Active' || $_POST['bulkaction'] == 'Inactive') {
          $broadcastChangeStatus = $this->broadcastChangeStatus($filterArray, $_POST['bulkaction']); 
              if($broadcastChangeStatus){
                Yii::app()->user->setFlash('success', "Status has been changed successfully.");
                $this->redirect('/admin/user/broadcast?'.$queryString); 
              } else  {
                  Yii::app()->user->setFlash('error', "Please select any broadcast message from the list.");
                  $this->redirect('/admin/user/broadcast?'.$queryString); 
              }
        } else if($_POST['bulkaction'] == 'Remove'){
          $broadcastRemove = $this->removeBroadcast($filterArray);
              if($broadcastRemove){
                  Yii::app()->user->setFlash('success', "Records deleted successfully.");
                  $this->redirect('/admin/user/broadcast?'.$queryString); 
              } else  {
                 Yii::app()->user->setFlash('error', "Please select any broadcast message from the list.");
                  $this->redirect('/admin/user/broadcast?'.$queryString);  
              }
        } else { 
          Yii::app()->user->setFlash('error', "Please select any action.");
          $this->redirect('/admin/user/broadcast?'.$queryString); 
        }
    }

    public function GetBroadcastCheckbox($data) {        
            echo "<input type='checkbox' class='allcheckbox' name='requestids[".$data->id."]' value='".$data->id."'>" ;
    }
    
    public function broadcastChangeStatus($filterArray, $status){
        if (isset($filterArray) && !empty($filterArray)) {
            foreach ($filterArray as $key => $filter) {
            $broadcastMessageObject = BroadcastMessage::model()->findByPk($filter);
            //echo '<pre>';print_r($mediaCenterDocumentObject);
            if ($broadcastMessageObject) { 
                if($status == 'Active') { 
                   $broadcastMessageObject->status = 1; 
                } else {
                   $broadcastMessageObject->status = 0; 
                }
                $broadcastMessageObject->update();

                }  
             }
             return 1;  

        } else {
           return 0;

        }          
    }

    public function removeBroadcast($filterArray){
         if (isset($filterArray) && !empty($filterArray)) {
            foreach ($filterArray as $key => $filter) {
            $broadcastMessageObject = BroadcastMessage::model()->findByPk($filter);
            //echo '<pre>';print_r($mediaCenterDocumentObject);
            if ($broadcastMessageObject) { 
                $broadcastMessageObject->delete();
                }  
             }
             return 1;

        } else {
           return 0;

        }
    }

    protected function GetWalletButtonTitle($data, $row) {
        echo $title1 = '';
        if($data->wallet_transaction_status == 'YES') {
            echo $title1 = '<a href="/admin/user/creditwallet?id=' . BaseClass::mgEncrypt($data->user_id) . '&type=' . BaseClass::mgEncrypt($data->type) . '" title="Recharge" target="_blank" class="action-icons"><i class="fa fa-plus-circle"></i></a>';
        }
        
        if ($data->user_id != '1') {
            echo $title = '<a href="/admin/user/debitwallet?id=' . BaseClass::mgEncrypt($data->user_id) . '&type=' . BaseClass::mgEncrypt($data->type) . '" title="Deduct" target="_blank" class="action-icons"><i class="fa fa-minus-circle"></i</a>';
        }

    }
    
    public function actionGetUserName() {
        if ($_POST) {
            $allUserListArray = "";
            $validUserList = "";
            $inValidUserList = "";
            $validUserIdList = "";
            $userListArray = explode(',', $_POST['userName']);
            foreach ($userListArray as $userName) {
                $userObject = User::model()->findByAttributes(array('name' => $userName, 'role_id' => 1));
                if (is_object($userObject)) {
                    $validUserList .= $userObject->full_name . ", &nbsp;";
                    $validUserIdList .= $userObject->id . ",";
                } else {
                    $inValidUserList .= $userName . ", ";
                }
            } 
            $allUserListArray['validId'] = $validUserIdList;
            $allUserListArray['valid'] = $validUserList;
            $allUserListArray['invalid'] = $inValidUserList;
            echo CJSON::encode($allUserListArray);
            exit;
        } else {
            BaseClass::get404();
        }
    }
    
    public function GetBroadcastSpecifiedUsers($data, $row){
        if(isset($data->users) && !empty($data->users)){
           $userArray = explode(",",$data->users);
           foreach($userArray as $user){
               $userObject = User::model()->findByPK($user);
               echo isset($userObject->name)?$userObject->name."<br>":"";
           }
        } else{
            echo "NA"; 
        }
    }
    
    public function actionDocumentVerification() {

        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 10;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
        $searchValue = "";
        if(!empty($_POST['search']['value'])){
            $searchValue = 'transaction_id = '.$_POST['search']['value'] . ' AND ';
        }
        $condition = array('condition' => $searchValue.'id_proof != "" AND address_proff != "" AND document_status = 0','order' => $orderBy, 'limit' => $limit, 'offset' => $offset,);

        $userProfileObjectCount = UserProfile::model()->count($condition);

        $userProfileObject = UserProfile::model()->findAll($condition);
        $userprofileJSONData = CJSON::encode($userProfileObject);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $userProfileObjectCount . ',
                    "recordsFiltered": ' . $userProfileObjectCount . ',
                    "data":' . $userprofileJSONData . '
        }';
        exit;
    }
    
    public function actionChangePassword() {
        $successMsg = '';
        $error = '';
        if ($_POST) {
            $userObject = User::model()->findByAttributes(array('name' => $_POST['username'], 'status' => 1, 'role_id' => 1));
            if (!empty($userObject)) {

                if (isset($_POST['password'])) {
                    $userObject->password = md5($_POST['password']);
                }

                if (isset($_POST['master_pin'])) {
                    $userObject->master_pin = md5($_POST['master_pin']);
                }

                $userObject->update();
                Yii::app()->user->setFlash('success', 'User Password/Masterpin Updated Successfully');
            } else {
                Yii::app()->user->setFlash('error', 'Cannot update Password/Masterpin of Inactive User or Admin');
            }
        }

        $this->render('/user/changepassword');
    }
    
    public function actionChangeEmail() {
        $successMsg = '';
        $error = '';
        if ($_POST && isset($_POST['newemail']) && !empty($_POST['newemail'])) {
            $userObjectCount = User::model()->count(array('condition' => "email = '".$_POST['newemail']."'" ));

            if (!$userObjectCount) {
                $userObject = User::model()->findByAttributes(array('name' => $_POST['username']));
                if($userObject){
                    $userObject->email = $_POST['newemail'];
                    $userObject->update();
                    Yii::app()->user->setFlash('success', 'User email updated successfully.');
                }
            } else {
                Yii::app()->user->setFlash('error', 'Email already exists. Try with another one.');
            }
        }

        $this->render('/user/changeemail');
    }

    public function actiongetData() {
        if ($_POST) {
            $username = $_POST['name'];
            $userObject = User::model()->findByAttributes(array('name' => $username, 'role_id' => 1));
            if (!empty($userObject)) {
                $userObjectResponse = array(
                    'full_name' => $userObject->full_name,
                    'email' => $userObject->email,
                );
                echo CJSON::encode($userObjectResponse);
                exit;
            } else {
                echo 0;
                exit;
            }
        }
    }
    
    public function creatDirectCommission($orderObject, $userObject) {
        
        BaseClass::traceLog('Direct Commission ', $orderObject);
        $sponsorUserObject = User::model()->findByPk($userObject->sponsor_id);
        $adminId = Yii::app()->params['adminId'];
        if (!$sponsorUserObject || $adminId == $sponsorUserObject->id ) {
            return true; //If there is no sponser it will return true
        }

        $adminId = Yii::app()->params['adminId'];
        //5% comission on package amount 
        $fund = BaseClass::getPercentage($orderObject->package_price, $orderObject->package()->direct);

        $adminWalletObject = Wallet::model()->findByAttributes(array('user_id' => $adminId, 'type' => Wallet::$_CASHBACK_ID));

        /* code to add sponsor transaction */
        $commissionDataArray['userId'] = $adminId;
        $commissionDataArray['mode'] = Transaction::$_MODE_DIRECTCOMMISSION;
        $commissionDataArray['used_rp'] = $fund;
        $commissionDataArray['verified'] = Transaction::$_STATUS_NOTVERIFIED;
        $userSposorObject = User::model()->findByPk($sponsorUserObject->id);
        $transactionObjectSponsor = Transaction::model()->createTransaction($commissionDataArray, $userSposorObject, 1);

        BaseClass::traceLog('Direct Commission: Create Transaction ', $transactionObjectSponsor);
        $commissionDataArray['comment'] = 'Direct Commision Transferred';
//        $commissionDataArray['walletId'] = $adminWalletObject->id;        

        $commissionDataArray['fromwallettype'] = Wallet::$_CASHBACK_ID; //TODO: 1 : Cash Wallet  
        $commissionDataArray['towallettype'] = Wallet::$_CASHBACK_ID; //TODO: 1 : Cash Wallet  

        $moneyTransfertoObject = MoneyTransfer::model()->createCompleteMoneyTransfer($commissionDataArray, $transactionObjectSponsor, $sponsorUserObject->id, $adminId, $fund);

        BaseClass::sendInternalMail($sponsorUserObject, 'Direct Referral Income Credited', "Direct Referral Income Credited.");
        BaseClass::traceLog('Direct Commission: Create Money Transfer ', $moneyTransfertoObject);

        $userObjectArray = array();
        $userObjectArray['to_name'] = $sponsorUserObject->full_name;
        $userObjectArray['user_name'] = $userObject->name;
        $sentMailResponse = 1;
        if($sponsorUserObject->role_id != 2) {
            $directReferralCommissionEmailArray['to'] = $sponsorUserObject->email;
            $directReferralCommissionEmailArray['subject'] = 'Direct Referral Commission Credited';
            $directReferralCommissionEmailArray['body'] = $this->renderPartial('//mailTemp/direct_referral', array('userObjectArr' => $userObjectArray), true);
            $sentMailResponse = CommonHelper::sendMail($directReferralCommissionEmailArray);
        }
        return $sentMailResponse;
    }

    public function actionPowerline() {
        
        $packageObject = Package::model()->findAll(array('condition' => 'status = 1'));
        if ($_POST) {
            $msg = "";
            $packageObject = Package::model()->findByAttributes(array('id' => $_POST['package_id']));
            $postDataArray = Array();
            $userListArray = explode(',', $_POST['userIdList']);
            $userObject = "";
            if (count($userListArray) > 0) {

                foreach ($userListArray as $key=>$userId) {
                    if(empty($userId)){
                        continue;
                    }
                    $postDataArray['transactionId'] = BaseClass::getTransactionId();
                    $postDataArray['userId'] = $userId;
                    $postDataArray['mode'] = Transaction::$_MODE_POWERLINE;
                    $postDataArray['comments'] = 'Powerline Purchase';
                    $postDataArray['used_rp'] = $_POST['package_value'];
                    $transactionObject = Transaction::model()->createTransaction($postDataArray, $userObject, 1);
                    if (!$transactionObject) {
                        continue;
                    }
                    $todayDate = date("Y-m-d");
                    $orderArray['transaction_id'] = $transactionObject->id;
                    $orderArray['user_id'] = $userId;
                    $orderArray['package_id'] = $packageObject->id;
                    $orderArray['package_price'] = $_POST['package_value'];
                    $orderArray['type'] = Order::$_TYPE_ADDCASH;
                    $orderArray['start_date'] = $todayDate;
                    
                    if(Purchase::isWeekday($todayDate)) {
                        $orderArray['end_date'] = Purchase::addWeekdays($todayDate,($packageObject->validity-1)); 
                    } else {
                        $orderArray['end_date'] = Purchase::addWeekdays($todayDate,($packageObject->validity)); 
                    }
                    
                    $orderArray['status'] = 1; 
                    $orderObject = new Order;
                    $orderObject = Order::model()->addEdit($orderObject, $orderArray);
                    if(!$orderObject) {
                        continue;
                    }
                    
                    $userObject = User::model()->findByPk(array('id' => $userId));
                    
                    if (!empty($userObject) && $userObject->role_id == 1) {
                        if ($userObject->status == 1) {
                            $userObject->status = 0;
                            $userObject->save(false);
                        }
                    }
                    Purchase::_updateMemebrshipType($packageObject, $userObject);
                }
                $this->redirect(array('/admin/user/powerline', 'successMsg' => 1));
            }
        }
        $this->render('powerline', array('packageObject' => $packageObject));
    }
    
    public function actionPowerlineReport() {
        
        $model = new Transaction();
        $cond = '';
        
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $todayDate = Yii::app()->params['startDate'];
        $fromDate = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - 15, date("Y")));
        $status = 1;
        
        if (!empty($_GET['from'])) {
            $todayDate = $_GET['to'];
            $fromDate = $_GET['from'];
        }
                               
        $order = "id DESC";
        $cond = ' AND admin_comment = "Powerline Purchase"';
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ('created_at >= "' . $fromDate . '" AND created_at <= "' . $todayDate . '" AND status = "' . $status . '"' . $cond), 'order' => $order,
            ),
            'pagination' => array('pageSize' => $pageSize),));
        $this->render('powerline_report', array(
            'dataProvider' => $dataProvider,
            'pageSize' => $pageSize,
        ));
        
    } 

    public function actionManageAgent(){
        $countryObject = Country::model()->findAll();
        $this->render('manage_agent', array('countryObject' => $countryObject));
    }
    
    public function actionManageAgentData(){
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
        
        $dataQuery = Yii::app()->db->createCommand()
            ->select('al.id , al.user_id ,al.created_at ,u.id,u.name,u.full_name,u.email,CONCAT("+",country_code," ",phone) as phoneNuber,up.phone,c.name as countryName, '
                    . '(case when al.status = 0 then "InActive" ELSE "ACTIVE" END ) as userStatus')
            ->from('agent_list al')
            ->join('user u', 'al.user_id = u.id') 
            ->join('country c', 'al.country_id = c.id') 
            ->join('user_profile up', 'al.user_id = up.user_id') ;
        
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value']; 
            $dataQuery->where('u.name LIKE :name', array(':name' => '%'.$searchData.'%'));
            $dataQuery->orWhere('u.email LIKE :email', array(':email' => '%'.$searchData.'%'));
        }

        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $agentListObject = $dataQuery->queryAll();
        
        //Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
            ->select('count(*) as totalRecords')
            ->from('agent_list al')
            ->join('user u', 'al.user_id = u.id') 
            ->join('user_profile up', 'al.user_id = up.user_id') ;
        
        if(!empty($_POST['search']['value'])){
            $countQuery->where('name LIKE :name', array(':name' => '%'.$searchData.'%'));
            $countQuery->orWhere('email LIKE :email', array(':email' => '%'.$searchData.'%'));
        }
            
        $totalRecordsArray = $countQuery->queryRow();
        $agentObjectCount = $totalRecordsArray['totalRecords'];

        $userJSONData = CJSON::encode($agentListObject);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $agentObjectCount . ',
                    "recordsFiltered": ' . $agentObjectCount . ',
                    "data":' . $userJSONData . '
        }';
        exit;  
    }
    
    public function actionChangeAgentStatus() { 
        if ($_REQUEST['id']) {
            $agentListObject = AgentList::model()->findByAttributes(array('user_id' => $_REQUEST['id']));
            if ($agentListObject->status == 1) {
                $agentListObject->status = 0;                
                Yii::app()->user->setFlash('success', "User status changed to Inactive!.");
            } else {
                $agentListObject->status = 1;
                Yii::app()->user->setFlash('success', "User status changed to Active!.");
            }
            $agentListObject->save(false);
            $this->redirect('manageagent');
        }
    }
    
    public function actionCheckingAgent(){ 
        $inValid = "";
        $validUserId   = "";
        $alreadyInUse   = "";
        $userObject = User::model()->findByAttributes(array('name' => $_POST['userName']));
        if ($userObject){
            $agentListObject = AgentList::model()->findByAttributes(array('user_id' => $userObject->id , 'country_id' => $_POST['countryId'] ));
            if(count($agentListObject) == 0 ){
                $validUserId = $userObject->id;
            }else{
                $alreadyInUse = "Already In use";
            }
        } else {
             $inValid = "Invalid";
        }
        $allUserListArray['validId'] = $validUserId;        
        $allUserListArray['invalid'] = $inValid;
        $allUserListArray['already'] = $alreadyInUse;
        echo CJSON::encode($allUserListArray); exit;

    }
    
    public function actionAgentAdd(){ 
        if(!empty($_POST) && !empty($_POST['validUser'])){
            $agentListCheckObject = AgentList::model()->findByAttributes(array('user_id' => $_POST['validUser']));
            if(count($agentListCheckObject) >= 0){
                $agentListObject = new AgentList;
                $agentListObject->user_id = $_POST['validUser'];
                $agentListObject->country_id = $_POST['country_id'];
                $agentListObject->created_at = date('Y-m-d');
                $agentListObject->status = 1;
                if (!$agentListObject->save(false)) {
                   echo "<pre>"; 
                   print_r($agentListObject->getErrors());
                   exit;
                }
                echo 1; exit;
            }
        }
    }
    
    public function actionUplineDownline(){
       $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
       $row = array();
       $model = new Genealogy();
       $condition = "";
       $userObject = "";
       
       $dataProvider = new CArrayDataProvider($row, array(
            'pagination' => array('pageSize' => $pageSize)));
       
      if(!empty($_GET['search']) && !empty($_GET['searchMode'])) {  
       
        /*fetch userObject*/
        $userObject = User::model()->findByAttributes(array('name' => trim($_GET['search']), 'role_id' => 1));
        
        if(empty($userObject)){
            Yii::app()->user->setFlash('error', 'User does not exists');
            $this->redirect('uplinedownline');
        }
    
        if($_GET['searchMode'] == "Downline"){ //For downline search
            $genealogyObject = Genealogy::model()->findByAttributes(array('user_id' => $userObject->id));

            if (empty($genealogyObject->right_users) && empty($genealogyObject->left_users)) {
                Yii::app()->user->setFlash('error', 'No downline users.');

                $this->redirect(array('uplinedownline'));
               
            } else {
                $prefix = "";
                if (!empty($genealogyObject->left_users) && !empty($genealogyObject->right_users)) {
                    $prefix = ",";
                }
                $userList = $genealogyObject->left_users . $prefix . $genealogyObject->right_users;
                $condition = ' t.user_id IN (' . $userList . ')' ;
            }
        } else {  //For upline search
                $condition = '(( t.left_count> 0 AND (t.left_users = "'.$userObject->id.'" or t.left_users like "%,'.$userObject->id.',%" or t.left_users like "'.$userObject->id.',%" or t.left_users like "%,'.$userObject->id.'") ) OR 
( t.right_count > 0 and ( t.right_users = "'.$userObject->id.'" or t.right_users like "%,'.$userObject->id.',%" or t.right_users like "'.$userObject->id.',%" or t.right_users like "%,'.$userObject->id.'")))' ;
          
        }
        
         if(isset($_GET['listSearch']) && !empty($_GET['listSearch'])) {  
             $condition .= ' AND user.name="'.$_GET['listSearch'].'"';
         }
        
        if(!empty($_GET['csv'])){
          $this->uplineDownlineCsv($_GET, $condition, $userObject);
        }
        
        $order = "";
        if(empty($_GET['Genealogy_sort'])){ $order = 't.level DESC'; }

       
        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'with'=>array('user'),
                'condition' => ($condition), 'order' => $order,
            ),
            'sort'=>array(
                'attributes'=>array( 
                    'user_name'=>array('asc'=>'user.name','desc'=>'user.name DESC',),
                    '*',
                ),
            ),
            'pagination' => array('pageSize' => $pageSize)));
     }
        
        $this->render('upline_downline', array('dataProvider' => $dataProvider,'pageSize' => $pageSize,
            'userObject' => $userObject));
    }
    
    public function uplineDownlineCsv($getValues, $condition, $userObject){
        $filename = "uplinedownlinelist" . date('d-m-Y') . '.csv';
        header('Content-Type: application/csv');
        header('Content-Disposition: attachement; filename="'.$filename.'"');
        $criteria = new CDbCriteria;
        $criteria->condition = $condition;
        $criteria->join = ' JOIN user AS user ON t.user_id = user.id';
        $criteria->order = ('t.id DESC');
        $genealogyObject = Genealogy::model()->findAll($criteria);
       
        $data = "";
        //Main User deatils
        if(!empty($userObject)){
            $genealogyUserObject = Genealogy::model()->findByAttributes(array('user_id' => $userObject->id));    
            $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $userObject->id)); 
            $data .= " User Details \n";
            $data .= "Level, User Name, Sponsor Name, Kyc Verified, Country, Mobile No, Status(1:Active & 0:Inactive), Left Count, Right Count, Position\n";
       
            $userGenealogyLevel = isset($genealogyUserObject->level)?$genealogyUserObject->level:""; 
            $userSponsorName = isset($userObject->sponsor()->name)?$userObject->sponsor()->name:"";
            $userKeyVerified = (isset($userProfileObject) && $userProfileObject->document_status == 1)?"Verified":"Not Verified";  
            $userCountry = (isset($userProfileObject) && $userProfileObject->country_id != 0)?$userProfileObject->country()->name:"NA"; 
            $userPhone = (isset($userProfileObject) && $userProfileObject->phone != 0)?"+".$userProfileObject->country_code." - ".$userProfileObject->phone:"NA";
            $userStatus = (isset($userObject->status) && $userObject->status == 1)?"Active":"Inactive"; 
            $userGenealogyLeftCount = isset($genealogyUserObject->left_count)?$genealogyUserObject->left_count:""; 
            $userGenealogyRightCount = isset($genealogyUserObject->right_count)?$genealogyUserObject->right_count:"";
            $userPosition = isset($genealogyUserObject->position)?$genealogyUserObject->position:"";
         
            $data .= $userGenealogyLevel. "," . $userObject->name.",".$userSponsorName.",".$userKeyVerified.",".$userCountry.",".$userPhone.",".$userStatus.",".$userGenealogyLeftCount.",".$userGenealogyRightCount.",".$userPosition."\n";
        
        }
       
        //Search result
        if(!empty($getValues['searchMode'])){
         $data .= $getValues['searchMode']." Result \n";  
        }
        
        $data .= "Level, User Name, Sponsor Name, Kyc Verified, Country, Mobile No, Status(1:Active & 0:Inactive), Left Count, Right Count, Position \n";
       
        if (count($genealogyObject) > 0) { 
            foreach ($genealogyObject as $genealogy) {
                $profileObject = UserProfile::model()->findByAttributes(array('user_id' => $genealogy->user_id));
                
                $status = "";
                if($genealogy->user()->status == 1){
                    $status = "Active"; 
                } else {
                    $status = "Inactive";  
                }
                $userName = isset($genealogy->user()->name)? ucwords($genealogy->user()->name):"";
                $sponsorName = isset($genealogy->user()->sponsor()->name)? ucwords($genealogy->user()->sponsor()->name):"";
                
                 
                $isVerified = (isset($profileObject) && ($profileObject->document_status==1))?"Verified":"Not Verified";
                $country = (isset($profileObject) && ($profileObject->country_id != 0))?$profileObject->country()->name:"";
                $phone = "";
                if(isset($profileObject) && !empty($profileObject)){
                    if($profileObject->phone != 0){
                        if($profileObject->country_code != 0){
                            $phone .= "+".$profileObject->country_code." - "; 
                        }
                            $phone .= $profileObject->phone;
                    } else {
                        $phone = "NA";
                    }
                }

                $data .= $genealogy->level . "," . $userName.",".$sponsorName.",".$isVerified.",".$country.",".$phone.",".$status.",".$genealogy->left_count.",".$genealogy->right_count.",".$genealogy->position."\n";
            }
        }
        echo $data;
        exit();
     }
    
    public function isKycVerified($data, $row){
        $isVerified = "";
        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data->user_id)); 
        if(isset($userProfileObject) && !empty($userProfileObject)){
            if($userProfileObject->document_status == 1){
               $isVerified = "Verified";
            } else {
                $isVerified = "Not Verified";
            }
        }
        echo $isVerified;
    }
    
     public function getCountry($data, $row){
        $country = "";
        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data->user_id)); 
        if(isset($userProfileObject) && !empty($userProfileObject)){
            if($userProfileObject->country_id != 0){
               $country = $userProfileObject->country()->name;
            } else {
                $country = "NA";
            }
        }
        echo $country;
    }
    
    public function getPhone($data, $row){
        $phone = "";
        $userProfileObject = UserProfile::model()->findByAttributes(array('user_id' => $data->user_id)); 
        if(isset($userProfileObject) && !empty($userProfileObject)){
            if($userProfileObject->phone != 0){
                if($userProfileObject->country_code != 0){
                    $phone .= "+".$userProfileObject->country_code." - "; 
                }
                    $phone .= $userProfileObject->phone;
            } else {
                $phone = "NA";
            }
        }
        echo $phone;
    }
    
    public function actionUserGroup() {
       $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
       $model = new UserGroups();
       $condition = "status = 1";
       $userGroupsObject = "";
       
        $order = "";
        if(empty($_GET['UserGroups_sort'])){ $order = 'created_at DESC'; }
            
        $dataProvider = new CActiveDataProvider($model, array(
                'criteria' => array(
                    'condition' => ($condition), 'order' => $order,
                ), 'pagination' => array('pageSize' => $pageSize)));
        
        if (isset($_GET['id'])) { //For edit
                $userGroupId = BaseClass::mgDecrypt($_GET['id']);
                $userGroupsObject = UserGroups::model()->findByPk($userGroupId);
        } 
            
        if (isset($_POST['addedit']) && !empty($_POST['addedit'])) {
            if (isset($_GET['id'])) { //For edit
                
                $this->editUserGroup($userGroupsObject, $_POST, $_FILES);

            } else { //For add
                $this->addUserGroup($_POST, $_FILES);
            }
        }
        
      
        $this->render('user_groups', array('dataProvider' => $dataProvider,'pageSize' => $pageSize,
            'userGroupsObject' => $userGroupsObject));
       
    }
    
    public function addUserGroup($postData, $postFile){
            $userGroupsObject = new UserGroups();
            $existingUserGroup = UserGroups::model()->findByAttributes(array('name' => $postData['name']));
            if(isset($existingUserGroup) && !empty($existingUserGroup)){ //Group already existed
                Yii::app()->user->setFlash('error', 'Usergroup already existed');
                $this->redirect('/admin/user/usergroup');
            }
            
            $newUserGroupsObject = UserGroups::model()->create($userGroupsObject, $postData, $postFile);
            if (isset($newUserGroupsObject) && !empty($newUserGroupsObject)) {
               
                if (strpos($postData['usersIdList'], ',') !== false) {
                    $userIdsArray = explode(",",$postData['usersIdList']);
                    $userIdsArray = array_unique($userIdsArray);
                    foreach($userIdsArray as $userIds){
                        $userHasGroupsObject = new UserHasGroups();
                        $userHasGroupsObject->user_groups_id = $newUserGroupsObject->id; 
                        $userHasGroupsObject->user_id = $userIds;
                        $userHasGroupsObject->created_at = date('Y-m-d');
                        $userHasGroupsObject->save();
                       }
                } else {
                        $userHasGroupsObject = new UserHasGroups();
                        $userHasGroupsObject->user_groups_id = $newUserGroupsObject->id; 
                        $userHasGroupsObject->user_id = $postData['usersIdList'];
                        $userHasGroupsObject->created_at = date('Y-m-d');
                        $userHasGroupsObject->save();
                }
              
                Yii::app()->user->setFlash('success', 'Usergroup successfully added');
                $this->redirect('/admin/user/usergroup');
            } else {
                Yii::app()->user->setFlash('error', 'Usergroup submition failed');
                $this->redirect('/admin/user/usergroup');
            }
    }
    
    public function editUserGroup($userGroupsObject, $postData, $postFile){
        $updateUserGroupsObject = UserGroups::model()->create($userGroupsObject, $postData, $postFile);

        if(isset($updateUserGroupsObject) && !empty($updateUserGroupsObject)){
            $existUserHasGroupsObject = UserHasGroups::model()->findAll(array('condition'=>'user_groups_id='.$updateUserGroupsObject->id));
            if(isset($existUserHasGroupsObject) && !empty($existUserHasGroupsObject)){
                foreach($existUserHasGroupsObject as $existUserHasGroups){
                    $existUserHasGroups->delete();
                }
            }

            if (strpos($postData['usersIdList'], ',') !== false) {
                $userIdsArray = explode(",",$postData['usersIdList']);
                $userIdsArray = array_unique($userIdsArray);
                foreach($userIdsArray as $userIds){
                    $userHasGroupsObject = new UserHasGroups();
                    $userHasGroupsObject->user_groups_id = $updateUserGroupsObject->id; 
                    $userHasGroupsObject->user_id = $userIds;
                    $userHasGroupsObject->created_at = date('Y-m-d');
                    $userHasGroupsObject->save();
                   }
            } else {
                    $userHasGroupsObject = new UserHasGroups();
                    $userHasGroupsObject->user_groups_id = $updateUserGroupsObject->id; 
                    $userHasGroupsObject->user_id = $postData['usersIdList'];
                    $userHasGroupsObject->created_at = date('Y-m-d');
                    $userHasGroupsObject->save();
            }

            Yii::app()->user->setFlash('success', 'Usergroup successfully added');
            $this->redirect('/admin/user/usergroup');
        } else {
            Yii::app()->user->setFlash('error', 'Usergroup submition failed');
            $this->redirect('/admin/user/usergroup');
        }
    }
    
    public function getusers($data, $row){
         $userNameList = "";
            $userHasGroupsObject = UserHasGroups::model()->findAll(array('condition' => 'user_groups_id='.$data->id));
            if(isset($userHasGroupsObject) && !empty($userHasGroupsObject)){
                foreach ($userHasGroupsObject as $userHasGroups) {
                    $userObject = User::model()->findByPk($userHasGroups->user_id);
                    if (isset($userObject) && !empty($userObject)) {
                        $userNameList .= $userObject->name . ",";
                    }
                } 
            }
        echo rtrim($userNameList,",");
    }

     public function actionTrackUserGroup() {
       $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
       $row = array();
       $model = new UserHasGroups();
       $condition = "";
       $userGroupObject = "";
       
       if (isset($_GET['id'])) {
        $userGroupId = BaseClass::mgDecrypt($_GET['id']);
        $userGroupObject = UserGroups::model()->findByPk($userGroupId);
       }
      
       if(!empty($_GET['search_group'])){
        $userGroupObject = UserGroups::model()->findByAttributes(array('name' => $_GET['search_group']));
       }
       
        if(isset($userGroupObject) && !empty($userGroupObject)){
            $condition = 'user_groups_id ='.$userGroupObject->id;  

            $order = "";
            if(empty($_GET['UserHasGroups_sort'])){ $order = 'created_at DESC'; }

            $dataProvider = new CActiveDataProvider($model, array(
                'criteria' => array(
                    'condition' => ($condition), 'order' => $order,
                ), 'pagination' => array('pageSize' => $pageSize)));
        } else {
                $dataProvider = new CArrayDataProvider($row, array(
            'pagination' => array('pageSize' => $pageSize)));
                
            Yii::app()->user->setFlash('error', 'Group name is not exist');
        }
     
        $this->render('track_user_groups', array('dataProvider' => $dataProvider,'pageSize' => $pageSize,
        'userGroupObject' => $userGroupObject));
       
    }
    
    
    public function getDirectBusiness($data, $row){
        $directBusinessAmount = 0;
        $userIds = "";
        $from = !empty($_GET['from'])?$_GET['from']:date('Y-m-d', strtotime("-1 month"));
        $to = !empty($_GET['to'])?$_GET['to']:date('Y-m-d');
        
        $userHasGroupObject = UserHasGroups::model()->findAll(array('condition' => 'user_groups_id='.$data->user_groups_id));
        if(isset($userHasGroupObject) && !empty($userHasGroupObject)){
            foreach($userHasGroupObject as $userHasGroup){
            $userIds .= $userHasGroup->user_id.",";
            }
        }
        $userIds = rtrim($userIds,",");
      
        $directBusinessObject  = Yii::app()->db->createCommand('SELECT u.sponsor_id , sum(t.paid_amount) AS DirectBusiness FROM `transaction` t join user u on t.user_id = u.id and u.sponsor_id in ('.$userIds.') WHERE t.paid_amount > 0 and t.status = 1 and t.mode in ( "'.Transaction::$_MODE_ADMIN_RECHARGE.'", "'.Transaction::$_MODE_ADDFUND.'" ) AND t.created_at between "'.$from.'" AND "'.$to.'" GROUP BY u.sponsor_id')->queryAll();
        //echo '<pre>';print_r($directBusinessObject);
        if(isset($directBusinessObject) && !empty($directBusinessObject)){
            foreach($directBusinessObject as $directBusiness){
            if($directBusiness['sponsor_id'] == $data->user_id){
                $directBusinessAmount = $directBusiness['DirectBusiness'];
            } }
        }
       echo $directBusinessAmount;
    }
    
    public function getInDirectBusiness($data, $row){
        $inDirectBusinessAmount = 0;
        $from = !empty($_GET['from'])?$_GET['from']:date('Y-m-d', strtotime("-1 month"));
        $to = !empty($_GET['to'])?$_GET['to']:date('Y-m-d');
        $userList = 0;
        
        $genealogyObject = Yii::app()->db->createCommand(' select (CASE when (left_count > 0 && right_count > 0) then CONCAT(left_users,",",right_users) when (left_count = 0 && right_count > 0) then right_users when (left_count > 0 && right_count = 0) then left_users else 0 END ) as User_list from genealogy g where g.user_id = '.$data->user_id)->queryAll();
        
         if(isset($genealogyObject) && !empty($genealogyObject)){
            if($genealogyObject[0]['User_list'] != ""){
                $userList = trim($genealogyObject[0]['User_list'], ',');
         } }
        
        $inDirectBusinessObject  = Yii::app()->db->createCommand('SELECT sum(t.paid_amount) AS TotalBusiness FROM `transaction` t WHERE t.paid_amount > 0 and t.status = 1 and t.mode in ("'.Transaction::$_MODE_ADMIN_RECHARGE.'", "'.Transaction::$_MODE_ADDFUND.'") AND t.created_at between "'.$from.'" AND "'.$to.'" and t.user_id in ('.$userList.')')->queryAll();
        //echo '<pre>';print_r($inDirectBusinessObject);
        if(isset($inDirectBusinessObject) && !empty($inDirectBusinessObject)){
            if($inDirectBusinessObject[0]['TotalBusiness'] != ""){
                $inDirectBusinessAmount = $inDirectBusinessObject[0]['TotalBusiness'];
            } else {
                $inDirectBusinessAmount = 0; 
            }
        }
       echo $inDirectBusinessAmount;
    }
    
    public function getWithdrawal($data, $row){
        $withdrawalAmount = 0;
        $from = !empty($_GET['from'])?$_GET['from']:date('Y-m-d', strtotime("-1 month"));
        $to = !empty($_GET['to'])?$_GET['to']:date('Y-m-d');
        
        $withdrawalObject  = Yii::app()->db->createCommand('SELECT sum(`received_amount`) as withdrawal FROM `with_drawal` WHERE `user_id`='.$data->user_id.' AND created_at between "'.$from.'" AND "'.$to.'" AND `status` =1')->queryAll();
        //echo '<pre>';print_r($withdrawalObject);
        if(isset($withdrawalObject) && !empty($withdrawalObject)){
            if($withdrawalObject[0]['withdrawal'] != ""){
                $withdrawalAmount = $withdrawalObject[0]['withdrawal'];
            } else {
                $withdrawalAmount = 0; 
            }
        }
       echo $withdrawalAmount;
    }

}
