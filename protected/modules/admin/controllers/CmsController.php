<?php

class CmsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
        Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/metronic/assets/plugins/summernote/summernote.css');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/metronic/assets/plugins/summernote/summernote.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/metronic/assets/plugins/summernote/components-editors.min.js', CClientScript::POS_END);
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'news', 'faq', 'faqlist', 'addfaq', 'addfaqtopic', 'getencrypid', 'changestatus',
                    'popup', 'changestatuspopup', 'homebanner', 'getbannerid'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionNews() {
        $model = new News;
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $condition = "status = 1"; //Default condition


        if (!empty($_GET['from']) && !empty($_GET['to'])) {
            $todayDate = date("Y-m-d", strtotime($_GET['from']));
            $fromDate = date("Y-m-d", strtotime($_GET['to']));
        }

        if (!empty($_GET['from']) && !empty($_GET['to']) && isset($_GET['status'])) {
            $condition = ' created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '" AND status = ' . $_GET['status'];
        } else {
            if (!empty($_GET['from']) && !empty($_GET['to'])) {
                $condition = ' created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"';
            }

            if (isset($_GET['status'])) {
                $condition = "status = " . $_GET['status'];
            }
        }

        if (isset($_GET['id'])) {
            $newsId = BaseClass::mgDecrypt($_GET['id']);
            $newsObject = News::model()->findByPk($newsId);
        } else {
            $newsObject = new News();
        }

        if (isset($_POST['addedit']) && !empty($_POST['addedit'])) {
            $newNewsObject = News::model()->create($newsObject, $_POST, $_FILES);
            if (isset($newNewsObject) && !empty($newNewsObject)) {
                Yii::app()->user->setFlash('success', 'News successfully updated');
                $this->redirect('/admin/cms/news');
            } else {
                Yii::app()->user->setFlash('error', 'News submition failed');
                $this->redirect('/admin/cms/news');
            }
        }

        if (isset($_POST['bulkaction']) && !empty($_POST['bulkaction'])) { //News active, in active or delete
            $filterArray = "";
            if (isset($_POST['requestids']) && !empty($_POST['requestids'])) {
                $filterArray = $_POST['requestids'];
            }
            //echo '<pre>';print_r($filterArray);exit;
            if ($_POST['bulkaction'] == 'Active' || $_POST['bulkaction'] == 'Inactive') {
                $newsChangeStatus = $this->newsChangeStatus($filterArray, $_POST['bulkaction']);
                if ($newsChangeStatus) {
                    Yii::app()->user->setFlash('success', "Status has been changed successfully.");
                } else {
                    Yii::app()->user->setFlash('error', "Please select any news from the list.");
                }
                $this->redirect('/admin/cms/news');
            } else if ($_POST['bulkaction'] == 'Remove') {
                $newsRemove = $this->removeNews($filterArray);
                if ($newsRemove) {
                    Yii::app()->user->setFlash('success', "Records deleted successfully.");
                } else {
                    Yii::app()->user->setFlash('error', "Please select any news from the list.");
                }
                $this->redirect('/admin/cms/news');
            } else {
                Yii::app()->user->setFlash('error', "Please select any action.");
                $this->redirect('/admin/cms/news');
            }
        }

        $dataProvider = new CActiveDataProvider($model, array(
            'sort' => array(
                'attributes' => array(
                    'title' => array(
                        'asc' => 'title',
                        'desc' => 'title DESC',
                    ),
                    'news' => array(
                        'asc' => 'news',
                        'desc' => 'news DESC',
                    ),
                    'published_date' => array(
                        'asc' => 'published_date',
                        'desc' => 'published_date DESC',
                    ),
                    'created_at' => array(
                        'asc' => 'created_at',
                        'desc' => 'created_at DESC',
                    ),
                    'status' => array(
                        'asc' => 'status',
                        'desc' => 'status DESC',
                    ),
                    '*',
                ),
            ),
            'criteria' => array(
                'condition' => ($condition),
                'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),
        ));

        $this->render('news', array('dataProvider' => $dataProvider, 'newsObject' => $newsObject));
    }

    public function GetNewsCheckbox($data) {
        echo "<input type='checkbox' class='allcheckbox' name='requestids[" . $data->id . "]' value='" . $data->id . "'>";
    }

    public function newsChangeStatus($filterArray, $status) {
        if (isset($filterArray) && !empty($filterArray)) {
            foreach ($filterArray as $key => $filter) {
                $newsObject = News::model()->findByPk($filter);
                //echo '<pre>';print_r($mediaCenterDocumentObject);
                if ($newsObject) {
                    if ($status == 'Active') {
                        $newsObject->status = 1;
                    } else {
                        $newsObject->status = 0;
                    }
                    $newsObject->update();
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function removeNews($filterArray) {
        if (isset($filterArray) && !empty($filterArray)) {
            foreach ($filterArray as $key => $filter) {
                $newsObject = News::model()->findByPk($filter);
                //echo '<pre>';print_r($mediaCenterDocumentObject);
                if ($newsObject) {
                    $newsObject->delete();
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function actionFaq() {
        $this->render('faq');
    }

    public function actionFaqlist() {
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        $orderBy = "created_at desc";
        if ($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
        $dataQuery = Yii::app()->db->createCommand()
                ->select('f.id,f.topic_id,ft.name,f.created_at,f.status as status,ft.status as topicstatus,f.title,f.answer')
                ->from('faq f')
                ->join('faq_topic ft', 'ft.id=f.topic_id');
        if (!empty($_POST['columns']['3']['search']['value'])) {
            $status = $_POST['columns']['3']['search']['value'];
            if($status == "Inactive"){
              $status = 0;  
            }else{
              $status = 1;  
            }
            $dataQuery->where('f.status LIKE :status', array(':status' => '%' .$status. '%'));
        } else {
            $dataQuery->where('f.status = "1"');
        }
        if (!empty($_POST['search']['value'])) {
              $searchData = $_POST['search']['value'];
              $dataQuery->andWhere('ft.name LIKE :name', array(':name' => '%' . $searchData . '%'));
              $dataQuery->orWhere('f.title LIKE :title', array(':title' => '%' . $searchData . '%'));
        }

        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $userListObject = $dataQuery->queryAll();
        //Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
                ->select('count(*) as totalRecords')->from('faq f')
                ->join('faq_topic ft', 'ft.id=f.topic_id');
        if (!empty($_POST['columns']['3']['search']['value'])) {
            $status = $_POST['columns']['3']['search']['value'];
            if($status == "Inactive"){
              $status = 0;  
            }else{
              $status = 1;  
            }
            $countQuery->where('f.status LIKE :status', array(':status' => '%' .$status. '%'));
        } else {
            $countQuery->where('f.status = "1"');
        }
        if (!empty($_POST['search']['value'])) {
              $dataQuery->andWhere('ft.name LIKE :name', array(':name' => '%' . $searchData . '%'));
              $dataQuery->orWhere('f.title LIKE :title', array(':title' => '%' . $searchData . '%'));
        }
        $totalRecordsArray = $countQuery->queryRow();

        $userObjectCount = $totalRecordsArray['totalRecords'];

        $userJSONData = CJSON::encode($userListObject);
        echo '{"draw": ' . $draw . ',
                    "recordsTotal": ' . $userObjectCount . ',
                    "recordsFiltered": ' . $userObjectCount . ',
                    "data":' . $userJSONData . '
        }';
        exit;
    }

    public function actionAddFaq() {
        $topicObject = FaqTopic::model()->findAll(array("condition" => "status=1"));
        $faqObject = "";
        if(isset($_GET['eid']) && !empty($_GET['eid'])){
        $id = (int) BaseClass::mgDecrypt($_GET['eid']);
        $faqObject = Faq::model()->findByPk($id);   
        }
        if ($_POST) {
            $dataArray = $_POST;
            if (!empty($_POST['answer'])) {
                if(isset($_GET['eid']) && !empty($_GET['eid'])){
					$text = "Edited";
                $faqSaveObject = Faq::model()->updateFAQ($dataArray, $faqObject);  
                }else{
					$text = "Added";
                $faqSaveObject = Faq::model()->addFAQ($dataArray);
                }
                if ($faqSaveObject) {
                    Yii::app()->user->setFlash('success', "FAQ ".$text." Successfully.");
                } else {
                    Yii::app()->user->setFlash('error', "Something went Wrong.");
                }
                $this->redirect('/admin/cms/faq');
            } else {
                Yii::app()->user->setFlash('error', "Please enter message also.");
                $this->redirect('/admin/cms/addfaq');
            }
        }
        $this->render('addfaq', array('topicObject' => $topicObject, 'faqObject' => $faqObject));
    }

    public function actionAddFaqTopic() {
        if ($_POST) {
            $dataArray = $_POST;
            if (!empty($_POST['name'])) {
                $faqSaveObject = FaqTopic::model()->addTopic($dataArray);
                if ($faqSaveObject) {
                    Yii::app()->user->setFlash('success', "FAQ Topic Added Successfully.");
                    $this->redirect('/admin/cms/addfaq');
                } else {
                    Yii::app()->user->setFlash('error', "Something went Wrong.");
                    $this->redirect('/admin/cms/addfaq');
                }
            } else {
                Yii::app()->user->setFlash('error', "Please enter Name.");
                $this->redirect('/admin/cms/addfaq');
            }
        }
    }
    
     public function actionGetencrypid(){
        $id = $_POST['id'];
        $enId = BaseClass::mgEncrypt($id);
        echo $enId;
    }
    
        public function actionChangeStatus() {
        if(isset($_GET['id']) && !empty($_GET['id'])){
        $id = $_GET['id'];
        $faqObject = Faq::model()->findByPk($id);
        if($faqObject){
        $status = $faqObject->status;
        $faqSaveObject = Faq::model()->updateStatus($status, $faqObject);
        if ($faqSaveObject) {
                    Yii::app()->user->setFlash('success', "Status Updated Successfully.");
                } else {
                    Yii::app()->user->setFlash('error', "Something went Wrong.");
                }
        $this->redirect('/admin/cms/faq');
        }
        }
    }
    
    public function actionPopUp() {
        
        $popUpObject = new PopUp();   
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $condition = "";

        $order = '';
        if(empty($_GET['PopUp_sort'])) {
            $order = 'id DESC';
        }
        
        if(isset($_GET['id'])){
            $popUpId = BaseClass::mgDecrypt($_GET['id']);
            $popUpObject = PopUp::model()->findByPk($popUpId);
        }
        
        if (isset($_GET['submit']) && !empty($_GET['submit'])) {
           if($_GET['res_filter'] != ''){
            $condition = ' status =' . $_GET['res_filter']; 
           }
       }

        if(isset($_POST['addedit_submit']) && !empty($_POST['addedit_submit'])){ 
            $updatePopUpObject = PopUp::model()->create($_POST, $_FILES, $popUpObject);
            if(!empty($updatePopUpObject)) {
                $popupStatusObject = PopUp::model()->findAll(array('condition' => 'id != ' . $updatePopUpObject->id));
                foreach($popupStatusObject as $popupStatus) {
                    $popupStatus->status = 0;
                    if (!$popupStatus->save(false)) {
                        echo "<pre>"; 
                        print_r($popupStatus->getErrors());
                        exit;
                        throw new CHttpException(404,'Unable to find the code you requested.');
                    }
                }
            }
            if(isset($updatePopUpObject) && !empty($updatePopUpObject)){
              Yii::app()->user->setFlash('success', 'Pop Up succesfully updated');
              $this->redirect('/admin/Cms/popup');
            } else {
              Yii::app()->user->setFlash('error', 'Pop Up submition failed');
              $this->redirect('/admin/Cms/popup');  
            }
        }

        $dataProvider = new CActiveDataProvider($popUpObject, array(
            'criteria' => array(
                'condition' => ($condition), 'order' => $order,
            ), 'pagination' => array('pageSize' => $pageSize),));

        
        $this->render('popup', array(
            'dataProvider' => $dataProvider,
            'pageSize' => $pageSize,
            'popUpObject' => $popUpObject,
        ));
    }
    
    public function GetPopUpAction($data){
            echo $edit = '<a  href="/admin/cms/popup?id='.BaseClass::mgEncrypt($data->id).'" title="Edit" class="action-icons"><i class="fa fa-pencil-square-o"></i></a>';
            echo $status = '<a  href="/admin/cms/changestatuspopup?id='.BaseClass::mgEncrypt($data->id).'" title="Change Status" class="action-icons"><i class="fa fa-retweet"></i></a>';
        }
    
        public function actionChangeStatusPopup() {
        if ($_GET['id']) {
            $popUpId = BaseClass::mgDecrypt($_GET['id']);
            $popUpObject = PopUp::model()->findByPk($popUpId);
            if(!empty($popUpObject)) {
                if ($popUpObject->status == 1) {
                    $popUpObject->status = 0;
                    $popUpObject->save(false);
                } else if ($popUpObject->status == 0) {
                    $popUpObject->status = 1;
                    $popUpObject->save(false);
                    if(!empty($popUpObject)) {
                        $popupStatusObject = PopUp::model()->findAll(array('condition' => 'id != ' . $popUpObject->id));
                        foreach($popupStatusObject as $popupStatus) {
                            $popupStatus->status = 0;
                            if (!$popupStatus->save(false)) {
                                echo "<pre>"; 
                                print_r($popupStatus->getErrors());
                                exit;
                                throw new CHttpException(404,'Unable to find the code you requested.');
                            }
                        }
                    }
                }
                Yii::app()->user->setFlash('success', "Pop Up status changed Successfully.");
            }
        }
        $this->redirect('/admin/cms/popup');
    }
    
    public function actionHomeBanner(){
        $model = new HomeBanner();
        $pageSize = isset($_GET['per_page']) ? $_GET['per_page'] : Yii::app()->params['minPerPage'];
        $condition = "";

        if(isset($_GET['id'])){
         $homeBannerId = BaseClass::mgDecrypt($_GET['id']);
         $homeBannerObject = HomeBanner::model()->findByPk($homeBannerId);
        } else {
         $homeBannerObject = new HomeBanner();   
        }

        if (isset($_GET['submit']) && !empty($_GET['submit'])) {
           if($_GET['res_filter'] != ''){
            $condition = ' status =' . $_GET['res_filter']; 
           }
       }

        if(isset($_POST['addedit_submit']) && !empty($_POST['addedit_submit'])){ //Home banner add or edit
            $newHomeBannerObject = HomeBanner::model()->create($_POST, $_FILES, $homeBannerObject);
            if(isset($newHomeBannerObject) && !empty($newHomeBannerObject)){
              Yii::app()->user->setFlash('success', 'homebanner succesfully updated');
              $this->redirect('/admin/Cms/homebanner');
            } else {
              Yii::app()->user->setFlash('error', 'homebanner submition failed');
              $this->redirect('/admin/Cms/homebanner');  
            }
        }

        if(isset($_POST['bulkaction']) && !empty($_POST['bulkaction'])){ //Home banner active, in active or delete
        $filterArray = "";
        if(isset($_POST['requestids']) && !empty($_POST['requestids'])){
         $filterArray = $_POST['requestids'];
        }
        //echo '<pre>';print_r($filterArray);exit;
        if($_POST['bulkaction'] == 'Active') {
          $HomeBannerChangeStatus = $this->HomeBannerChangeStatus($filterArray, $_POST['bulkaction']); 
              if($HomeBannerChangeStatus){
                Yii::app()->user->setFlash('success', "Status has been changed successfully.");
                $this->redirect('/admin/Cms/homebanner'); 
              } else  {
                  Yii::app()->user->setFlash('error', "Please select any home banner from the list.");
                  $this->redirect('/admin/Cms/homebanner'); 
              }
        } else if($_POST['bulkaction'] == 'Inactive'){
          $HomeBannerChangeStatus = $this->HomeBannerChangeStatus($filterArray, $_POST['bulkaction']); 
              if($HomeBannerChangeStatus){
                Yii::app()->user->setFlash('success', "Status has been changed successfully.");
                 $this->redirect('/admin/Cms/homebanner'); 
              } else  {
                  Yii::app()->user->setFlash('error', "Please select any home banner from the list.");
                  $this->redirect('/admin/Cms/homebanner'); 
              }
        }else if($_POST['bulkaction'] == 'Remove'){
          $homeBannerRemove = $this->removeHomeBanner($filterArray);
              if($homeBannerRemove){
                  Yii::app()->user->setFlash('success', "Records deleted successfully.");
                  $this->redirect('/admin/Cms/homebanner'); 
              } else  {
                 Yii::app()->user->setFlash('error', "Please select any home banner from the list.");
                  $this->redirect('/admin/Cms/homebanner');  
              }
        } else { 
          Yii::app()->user->setFlash('error', "Please select any action.");
          $this->redirect('/admin/Cms/homebanner'); 
        }
      } 

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ($condition), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));


        $this->render('home_banner', array(
            'dataProvider' => $dataProvider,
            'pageSize' => $pageSize,
            'homeBannerObject' => $homeBannerObject,
        ));

    }

    public function GetHomeBannerCheckbox($data) {        
        echo "<input type='checkbox' class='allcheckbox' name='requestids[".$data->id."]' value='".$data->id."'>" ;
    }

    public function homeBannerChangeStatus($filterArray, $status){
        if (isset($filterArray) && !empty($filterArray)) {
            foreach ($filterArray as $key => $filter) {
            $homeBannerObject = HomeBanner::model()->findByPk($filter);
            //echo '<pre>';print_r($mediaCenterDocumentObject);
            if ($homeBannerObject) { 
                if($status == 'Active') { 
                   $homeBannerObject->status = 1; 
                } else {
                   $homeBannerObject->status = 0; 
                }
                $homeBannerObject->update();

                }  
             }
             return 1;  

        } else {
           return 0;

        }          
    }

    public function removeHomeBanner($filterArray){
         if (isset($filterArray) && !empty($filterArray)) {
            foreach ($filterArray as $key => $filter) {
            $homeBannerObject = HomeBanner::model()->findByPk($filter);
            //echo '<pre>';print_r($mediaCenterDocumentObject);
            if ($homeBannerObject) { 
                $homeBannerObject->delete();
                }  
             }
             return 1;

        } else {
           return 0;

        }           
    }

     public function GetHomeBannerAction($data){
        echo $preview = '<a target="_bank" href="'.Yii::app()->params['cms'].$data->banner_img.'" title="Preview" class="action-icons"><i class="fa fa-eye"></i></a>';   

        echo $edit = '<a  href="/admin/cms/homebanner?id='.BaseClass::mgEncrypt($data->id).'" title="Edit" class="action-icons"><i class="fa fa-pencil-square-o"></i></a>';

    }

    public function actionGetBannerId(){
       if($_POST){
           $homeBannerObject = HomeBanner::model()->findByAttributes(array('banner_order' => $_POST['id']));
           if(isset($homeBannerObject) && !empty($homeBannerObject)){
               echo 1;
           } else {
               echo 0;
           }
       }
    }

}
