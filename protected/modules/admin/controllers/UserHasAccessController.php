<?php

class UserHasAccessController extends Controller {

    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'memberaccess','members','changeapprovalstatus', 'add', 'edit'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
         
        $this->render('index');
    }
    
    public function actionMemberAccess() {
        $varString = "";
        $error = "";
        $success = "";
        $accessArr = array();
        $model = new UserHasAccess;
        if ((isset($_GET)) && $_GET['id'] != '') {
            $getAdminId = BaseClass::mgDecrypt($_GET['id']);
            $emailObject = User::model()->findBYPK($getAdminId);
            $UseraccessObject = UserHasAccess::model()->findByAttributes(array('user_id' => $getAdminId));
            if (!empty($UseraccessObject)) {
                if (!empty($_POST)) {
                    $postAdminId = BaseClass::mgDecrypt($_POST['admin_id']);
                    $accessObject = UserHasAccess::model()->findByAttributes(array('user_id' => $postAdminId));
                    if (empty($_POST['access']) || count($_POST['access']) == 0 || $_POST['admin_id'] == '') {
                        $error .= "Please check atleast one option.";
                    } else {
                        $access = $_POST['access'];


                        foreach ($access as $access1) {
                            $varString .= $access1 . ",";
                        }
                        $finalString = substr($varString, 0, -1);
                        if (count($accessObject) == 0) {
                            $model->user_id = $postAdminId;
                            $model->access = $finalString;
                            $model->created_at = new CDbExpression('NOW()');
                            $model->save(false);
                        } else {
                            $accessObject->user_id = $postAdminId;
                            $accessObject->access = $finalString;
                            $accessObject->created_at = new CDbExpression('NOW()');
                            $accessObject->save(false);
                        }
                        $success .= "User access permission saved successfully.";
                    }
                }
                $UseraccessObject = UserHasAccess::model()->findByAttributes(array('user_id' => $getAdminId));
                $accessArr = explode(',', $UseraccessObject->access);
            }
        } else {
                $error .= "Invalid request.";
            }

        $this->render('/user/member_access', array('emailObject' => $emailObject, 'error' => $error, 'success' => $success, 'accessArr' => $accessArr));
    }
    
    /*
     * function to fetch members
     */
    public function actionMembers() {
           $status = 1;
           $pageSize= 100;
           if (!empty($_POST)) {
           $status = $_POST['res_filter'];
           }
            $dataProvider = new CActiveDataProvider('User', array(
            'criteria' => array(
                'condition' => ('role_id = "2" AND status= "'.$status.'"'), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));

        $this->render('/user/members', array(
            'dataProvider' => $dataProvider,
        ));
    }
    
     public function actionChangeApprovalStatus() {
        if ($_REQUEST['id']) {
            $userprofileObject = User::model()->findByPk($_REQUEST['id']);
            if ($userprofileObject->status == 1) {
                $userprofileObject->status = 0;
            } else {
                $userprofileObject->status = 1;
            }
            $userprofileObject->save(false);
            $this->redirect(array('/admin/UserHasAccess/members', 'successMsg' => 1));
        }
    }
    
        public function actionAdd() {
        $departmentObject = Department::model()->findAll(array('condition' => 'status =1'));
        if ($_POST) {
            /* Already Exits */
            $userObject = User::model()->findByAttributes(array('name' => $_POST['name']));
            if (count($userObject) == 0) {
                $masterPin = BaseClass::getUniqInt(5);
                if(isset($_POST['password']) && !empty($_POST['password'])){
                  $password = $_POST['password'];    
                } else {
                  $password = BaseClass::getUniqInt(5);
                }
                $userObject = new User;
                $userObject->attributes = $_POST;
                $userObject->sponsor_id = 1;
                $userObject->full_name = $_POST['name'];
                $userObject->role_id = 2;
                $userObject->status = 1;
                $userObject->password = BaseClass::md5Encryption($password);
                $userObject->master_pin = BaseClass::md5Encryption($masterPin);
                $userObject->created_at = date('Y-m-d');
                $userObject->unique_id = "";

                if (!$userObject->save(false)) {
                    echo "<pre>";
                    print_r($userObject->getErrors());
                    exit;
                }
                $userObjectUserProfile = new UserProfile();
                $userObjectUserProfile->user_id = $userObject->id;
                $userObjectUserProfile->country_id = 0;
                $userObjectUserProfile->country_code = 0;
                $userObjectUserProfile->created_at = date('Y-m-d');
                $userObjectUserProfile->photo_proof = "";
                $userObjectUserProfile->otp_count = "";
                $userObjectUserProfile->status = 1;
                $userObjectUserProfile->save(false);

                $accessObject = new UserHasAccess;
                $accessObject->user_id = $userObject->id;
                $accessObject->access = "dashboard";
                $accessObject->created_at = date('Y-m-d');
                $accessObject->save();
                
                //create wallet for new user
                $walletObject = BaseClass::_createNewUserWallet($userObject->id);
                
                if(isset($_POST['department']) && !empty($_POST['department'])){
                    foreach($_POST['department'] as $department) {
                        $departmentObject = new DepartmentMapping();
                        $departmentObject->department_id = $department;
                        $departmentObject->user_id = $userObject->id;
                        $departmentObject->save();
                    }
                }
                
                $userObjectArr = array();
                $userObjectArr['name'] = trim($_POST['name']);
                $userObjectArr['password'] = $password;
               
                $config['to'] = trim($_POST['email']);
                $config['subject'] = 'Registration Confirmation';
                $config['body'] = $this->renderPartial('/mailTemplate/adminlogindetails', array('userObjectArr' => $userObjectArr), true);
                CommonHelper::sendMail($config);
                
                Yii::app()->user->setFlash('success', 'User Created Successfully.');
                $this->redirect(array('/admin/UserHasAccess/members'));
            } else {
                Yii::app()->user->setFlash('error', 'Name already exist');
                $this->redirect(array('/admin/UserHasAccess/add'));
            }
        }

        $this->render('/user/useraccessadd', array('departmentObject' => $departmentObject));
    }
    
        public function actionEdit() {
        $userId = BaseClass::mgDecrypt($_REQUEST['id']);
        $departmentObject = Department::model()->findAll(array('condition' => 'status =1'));
        $usersDepartmentObject = DepartmentMapping::model()->findAll(array('condition' => 'user_id ='. $userId.' AND status=1'));
        if ($_REQUEST['id']) {
            $userObject = User::model()->findByPK($userId);
            $profileObject = UserProfile::model()->findByAttributes(array('user_id' => $userId));
            if ($_REQUEST['id'] && $_POST) {
                if ($_POST['name'] != '' && $_POST['email'] != '') {
                    $existingUserObject = User::model()->findAll(array('condition' => '(name = "'.$_POST['name'].'" OR email ="'.$_POST['email'].'") AND id !='.$userId));                  

                    /* Updating User info */
                    if(count($existingUserObject) == 0){
                    $postDataArray = $_POST;
                    $userObject->name = $_POST['name'];
                    $userObject->email = $_POST['email'];
                    if(isset($_POST['password']) && !empty($_POST['password'])){
                      $userObject->password = BaseClass::md5Encryption($_POST['password']); 
                    }
                    $userObject->updated_at = new CDbExpression('NOW()');
                    $userObject->update();

                    // Profile Object set, Update it.
                    if (isset($profileObject) && !empty($profileObject)) {                       
                        $profileObject->attributes = $postDataArray;
                        $profileObject->updated_at = new CDbExpression('NOW()');
                        $profileObject->update();
                    } else {
                        // Create it.
                        $profileObject = new UserProfile();
                        $profileObject->attributes = $postDataArray;
                        $profileObject->user_id = $userObject->id;
                        $profileObject->created_at = new CDbExpression('NOW()');
                        $profileObject->updated_at = new CDbExpression('NOW()');
                        $profileObject->save(false);
                    }
                    
                    if(isset($_POST['department']) && !empty($_POST['department'])){
                    $existUserDepartmentObject = DepartmentMapping::model()->findAll(array('condition' => 'user_id ='.$userId));
                    if(isset($existUserDepartmentObject) && !empty($existUserDepartmentObject)){
                    foreach($existUserDepartmentObject as $existUserDepartment){
                         $existUserDepartment->status = 0;
                         $existUserDepartment->update();
                    } }
                    
                    foreach($_POST['department'] as $department) {
                    $specificUserepartmentObject = DepartmentMapping::model()->findByAttributes(array('user_id' => $userId, 'department_id' => $department));
                        if(empty($specificUserepartmentObject)){ //removing duplicates
                        $departmentObject = new DepartmentMapping();
                        $departmentObject->department_id = $department;
                        $departmentObject->user_id = $userId;
                        $departmentObject->save();
                        } else {
                           $specificUserepartmentObject->status=1;
                           $specificUserepartmentObject->update();
                        }
                    } }
                    
                    if ($userObject->update()) {
                        Yii::app()->user->setFlash('success', 'Details succesfully updated');
                        $this->redirect(array('/admin/UserHasAccess/members'));
                    } 
                 }else {
                    Yii::app()->user->setFlash('error', 'Name or email already exist');
                    $this->redirect(array('/admin/UserHasAccess/edit?id='.$_REQUEST['id']));
                 }
                } else {
                    Yii::app()->user->setFlash('error', 'Please fill all required(*) marked fields.');
                    $this->redirect(array('/admin/UserHasAccess/edit?id='.$_REQUEST['id']));
                }
            }
        }

        $this->render('/user/useraccessedit', array(
            'userObject' => $userObject, 'profileObject' => $profileObject, 
            'departmentObject' => $departmentObject, 'usersDepartmentObject' => $usersDepartmentObject
        ));
    }

}
