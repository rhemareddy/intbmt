<?php

class MailController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'reply', 'compose', 'sent', 'mark'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $pageSize = (isset($_GET['per_page']) && !empty($_GET['per_page'])) ? $_GET['per_page'] : Yii::app()->params['minPerPage']; 
        $successMsg = "";
        $dpt = "";
        $stringr = "";
        $mailStr = "";

        $emailObjectDpt = Department::model()->findAll(array('condition' => 'status=1'));

        $department = "";
        if(isset($_GET['department'])){
            $department = $_GET['department'];
        }
        $str = "";
        /* Code to Fetch all department */
        if (Yii::app()->session['userid']) {

            $userDepartment = DepartmentMapping::model()->findAll(array('condition' => 'user_id = ' . Yii::app()->session['userid'].' AND status = 1'));
            
            if (!empty($userDepartment)) {
                foreach ($userDepartment as $mailKey) {
                    $mailStr .= $mailKey->department_id . ",";
                }
                $mailString = substr($mailStr, 0, -1);
                $department = $mailKey->department_id;
                $userDepartments = DepartmentMapping::model()->findAll(array('condition' => 'department_id IN (' . $mailString . ') AND status = 1'));

                if ($userDepartments) {
                    foreach ($userDepartments as $keyuser) {
                        $stringr .= $keyuser['user_id'] . ",";
                    }
                    $userString = substr($stringr, 0, -1);
                }
                $condition = 'department_id IN (' . $mailString . ') AND (to_user_id IN ('.$userString.')) AND from_user_id != '.Yii::app()->session['userid'] ;
//                if( Yii::app()->session['userid']==1){
//                    $condition = 'department_id IN (' . $mailString . ')';
//                }
                
            } else {
                $condition = 'to_user_id IN (' . Yii::app()->session['userid'] . ')';
            }
        } 

        $order = "";
        if (empty($_GET['UserHasConversations_sort'])) {
            $order = 'updated_at DESC';
        }
        
        $searchCondition = "";
        $userIdArray = array();
        if (!empty($_GET['res_filter']) && !empty($_GET['search_user_name'])) {
            $userObject = User::model()->findByAttributes(array('name'=> $_GET['search_user_name']));
            if(!empty($userObject)){
            $searchCondition = 'department_id =' . $_GET['res_filter'].' AND (from_user_id IN (' . $userObject->id . ') OR to_user_id IN (' . $userObject->id . ')) AND from_user_id != '.Yii::app()->session['userid']; 
            } else {
             $searchCondition = 'department_id =' . $_GET['res_filter'].' AND (from_user_id IN ("NULL") OR to_user_id IN ("NULL")) AND from_user_id != '.Yii::app()->session['userid'];    
            }
            $dataProvider = new CActiveDataProvider('UserHasConversations', array(
                'criteria' => array('condition' => $searchCondition, 'order' => $order),
                'pagination' => array('pageSize' => $pageSize)));
            
            
        } else if(!empty($_GET['res_filter']) || !empty($_GET['search_user_name'])){ 
            
            if(!empty($_GET['res_filter'])){
                $searchCondition = 'department_id =' . $_GET['res_filter'] . ' AND (to_user_id IN ('.$userString.')) AND from_user_id != '.Yii::app()->session['userid'];  
            }
            if(!empty($_GET['search_user_name'])){
               
                if(empty($_GET['res_filter']) && !empty($mailString)){
                  $searchCondition .=  'department_id IN ('. $mailString.') AND ';
                }
                $userObject = User::model()->findByAttributes(array('name'=> $_GET['search_user_name']));
                if($userObject){
                    $searchCondition .= '(from_user_id IN (' . $userObject->id . ') OR to_user_id IN (' . $userObject->id . ')) AND from_user_id != '.Yii::app()->session['userid']; 
                }else{
                    $searchCondition .= "from_user_id = 'NULL' "; 
                }
                
            }
            $dataProvider = new CActiveDataProvider('UserHasConversations', array(
                'criteria' => array('condition' => $searchCondition, 'order' => $order),
                'pagination' => array('pageSize' => $pageSize)));
        } else { 
            $dataProvider = new CActiveDataProvider('UserHasConversations', array(
                'criteria' => array('condition' => $condition, 'order' => $order),
                'pagination' => array('pageSize' => $pageSize)));
        }
        
        $this->render('index', array(
            'dataProvider' => $dataProvider, 'emailObject' => $emailObjectDpt, 'userDepartment' => $userDepartment, 'successMsg' => $successMsg, 'pageSize' => $pageSize
        ));
    }

    /**
     * Lists all models.
     */
    public function actionSent() {
        $pageSize = 1000;
        $successMsg = "";
        $dpt = "";
        $str = "";
        $stringr = "";
        $mailStr = "";
        /* code to fetch department */


        /* Code to Fetch all department */
        if (Yii::app()->session['userid'] != '1') {
            $userDepartment = DepartmentMapping::model()->findAll(array('condition' => 'user_id = ' . Yii::app()->session['userid'].' AND status = 1'));

            if (!empty($userDepartment)) {
                foreach ($userDepartment as $mailKey) {
                    $mailStr .= $mailKey->department_id . ",";
                }
                $mailString = substr($mailStr, 0, -1);

                $department = $mailKey->department_id;

                $userDepartment = DepartmentMapping::model()->findAll(array('condition' => 'department_id IN (' . $mailString . ') AND status = 1'));

                if ($userDepartment) {
                    foreach ($userDepartment as $keyuser) {
                        $stringr .= $keyuser['user_id'] . ",";
                    }
                    $userString = "1," . substr($stringr, 0, -1);
                }
                $condition = 'department_id IN (' . $mailString . ') AND from_user_id IN (' . $userString . ') AND to_user_id != '.Yii::app()->session['userid'];
                //$condition = 'from_user_id = "' . $department . '" ';
            } else {
                $condition = 'from_user_id = "' . Yii::app()->session['userid'] . '" ';
            }
        } else {

            $emailObject = Department::model()->findAll(array('condition' => 'status=1'));

            foreach ($emailObject as $keys) {
                $dpt .= $keys['id'] . ",";
            }
            $department = substr($dpt, 0, -1);
            $emailObjectz = User::model()->findAll(array('condition' => 'role_id=2'));
            foreach ($emailObjectz as $keyz) {
                $str .= $keyz->id . ",";
            }
            $stringF = substr($str, 0, -1);
            $condition = "from_user_id IN (" . $stringF . ") AND to_user_id != ".Yii::app()->session['userid'];
        }


        $order = "";
        if (empty($_GET['UserHasConversations_sort'])) {
            $order = 'updated_at DESC';
        }
        /* $dataProvider = new CActiveDataProvider('Mail', array(
          'criteria' => array('condition' => 'from_user_id = ' . $string, 'order' => 'updated_at DESC'),
          'pagination' => array('pageSize' => $pageSize))); */

        $dataProvider = new CActiveDataProvider('UserHasConversations', array(
            'criteria' => array('condition' => $condition, 'order' => $order),
            'pagination' => array('pageSize' => $pageSize)));

        //print_r($dataProvider);exit;
        $this->render('sent', array(
            'dataProvider' => $dataProvider, 'successMsg' => $successMsg
        ));
    }

    public function actionCompose() {
        $error = "";
         /* Code to Fetch all department */
        $userDepartment = DepartmentMapping::model()->findAll(array('condition' => 'user_id = ' . Yii::app()->session['userid'].' AND status = 1'));
        if(Yii::app()->session['userid']==1){
            $userDepartment = Department::model()->findAll(array('condition' => 'status = 1' ));
        }
        
        if (isset($_POST) && !empty($_POST)) {
            $emailArray = $_POST['to_email'];
            foreach ($emailArray as $email) {
                $userObject = User::model()->findByAttributes(array('name' => $email));
                if (empty($userObject)) {
                    $this->render('compose', array('error' => 'User Does Not Exist','departmentObject' => $userDepartment));
                }

                if (!empty($_POST['id'])) {
                    $mailId = BaseClass::mgDecrypt($_POST['id']);
                    $mailObject = Mail::model()->findByPk($mailId);
                } else {
                    $mailObject = new Mail();
                }
             
                $department = "";
                if (!empty($_POST['department_id'])) {
                    if(is_numeric($_POST['department_id'])){
                      $decryptDepartmentId =  $_POST['department_id']; 
                    } else {
                      $decryptDepartmentId = BaseClass::mgDecrypt($_POST['department_id']);
                    }
                    $department = $decryptDepartmentId;
                }

                $to_email = $_POST['to_email'];
                $fname = "";
                //$attach = $_POST['attachment1'];
                $attach = NULL;

                if (!empty($_FILES['attachment']['name'])) {
                    //$fname = time() . $_FILES['attachment']['name'];
                    $fname = time();
                    $fileNameArray = explode(".", $_FILES['attachment']['name']);
                    $ext = end($fileNameArray);

                    $path = Yii::getPathOfAlias('webroot') . "/upload/attachement/";
                    $uploadSize = $_FILES['attachment']["size"];
                    if ($uploadSize > 2097152) {
                        $error .= "File can not be greater than 2MB";
                    } elseif ($ext != "jpg" && $ext != "png" && $ext != "pdf") {
                        $error .= "Incorrect File Type";
                    } else {
                        //$attach = substr($fname . "," . $_POST['attachment1'], 0, -1);
                        $attach = $fname.".".$ext;
                        BaseClass::uploadFile($_FILES['attachment']['tmp_name'], $path, $attach);
                        $mailObject->subject = $_POST['email_subject'];
                        $mailObject->created_at = new CDbExpression('NOW()');
                        $mailObject->updated_at = new CDbExpression('NOW()');
                        if ($mailObject->save(false)) {
                            $postArray['mail_id'] = $mailObject->id;
                            $postArray['from_user_id'] = Yii::app()->session['userid'];
                            $postArray['to_user_id'] = $userObject->id;
                            $postArray['department_id'] = $department;
                            $postArray['message'] = nl2br($_POST['email_body']);
                            $postArray['attachment'] = $attach;
                            $postArray['status'] = 0;
                            UserHasConversations::model()->create($postArray, 1);
                            $this->redirect(array('/admin/mail?successMsg=1'));
                        }
                    }

                } else {

                    $mailObject->subject = $_POST['email_subject'];
                    $mailObject->created_at = new CDbExpression('NOW()');
                    $mailObject->updated_at = new CDbExpression('NOW()');
                    if ($mailObject->save(false)) {
                        $postArray['mail_id'] = $mailObject->id;
                        $postArray['from_user_id'] = Yii::app()->session['userid'];
                        $postArray['to_user_id'] = $userObject->id;
                        $postArray['department_id'] = $department;
                        $postArray['message'] = nl2br($_POST['email_body']);
                        $postArray['attachment'] = $attach;
                        $postArray['status'] = 0;

                        UserHasConversations::model()->create($postArray, 1);
                        $this->redirect(array('/admin/mail?successMsg=1'));
                    }
                }
            }
            $this->redirect(array('/admin/mail?successMsg=1'));
        }
        $this->render('compose', array('error' => $error,'departmentObject' => $userDepartment));
    }

    public function actionReply() {
        if ($_GET) { 
            $mailId = BaseClass::mgDecrypt($_GET['id']);
            /* Code to Fetch all department */
                $userDepartment = DepartmentMapping::model()->findAll(array('condition' => 'user_id = ' . Yii::app()->session['userid'].' AND status = 1'));
                
                if(Yii::app()->session['userid']==1)
                {
                     
                    $userDepartment = Department::model()->findAll(array('condition' => 'status = 1' ));
                
                }
            //$mailObject = Mail::model()->findByPk($_REQUEST['id']);
            $UserHasConversationsObject = UserHasConversations::model()->findByAttributes(array('mail_id' => $mailId));
            
            $receiverName = $UserHasConversationsObject->fromuser()->name;            
            if (isset($UserHasConversationsObject)) {
                // If from user id is an admin, then consider to_userid as receiver.
                if ($UserHasConversationsObject->fromuser()->role_id == 2) $receiverName = $UserHasConversationsObject->touser()->name;
            }            
            
            //if (Yii::app()->session['userid'] != $UserHasConversationsObject->from_user_id) {
                $UserHasConversationsObject->status = 1;
            //}
            $UserHasConversationsObject->save(false);
            $this->render('compose', array('error' => '', 'mailObject' => $UserHasConversationsObject,'departmentObject' => $userDepartment, 'receiverName' => $receiverName));
        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        if ($id) {
            $userMailId = BaseClass::mgDecrypt($id);
            $userDepartment = DepartmentMapping::model()->find(array('condition' => 'user_id = ' . Yii::app()->session['userid']. ' AND status = 1'));
            $UserHasConversationObject = UserHasConversations::model()->findByPk($userMailId);
            $conversationId = isset($UserHasConversationObject->mail_id) ? $UserHasConversationObject->mail_id : '';
            $conversationMailObject = Mail::model()->findByPk($conversationId);
            $conversationObject = UserHasConversations::model()->findAllByAttributes(array('mail_id' => $conversationId));
            $UserHasConversationObject->status = 1;
            $UserHasConversationObject->save(false);
            
            $this->render('view', array(
                'mailObject' => $UserHasConversationObject,
                'conversationObject' => $conversationObject,
                'conversationMailObject' => $conversationMailObject
            ));
        }
    }
    
            public function actionMark(){
           
            if(isset($_GET)){
              $filter = isset($_GET['res_filter'])? $_GET['res_filter'] : '';
              $perpage = isset($_GET['per_page'])? $_GET['per_page'] : '';
              $submit = isset($_GET['submit'])? $_GET['submit'] : '';
            }
            $label = "change";
            $success ="";
            $model = new UserHasConversations;
            $pageSize = Yii::app()->params['defaultPageSize'];
            $userId = Yii::app()->session['userid'] ;            
           
            if(!empty($_POST['requestids'])){
                
                $filterdArray = array_filter($_POST['requestids']);
                foreach ($filterdArray as $key => $id){ 
                    $mailObject = UserHasConversations::model()->findByPk($key);
                    if($_POST['actiontype'] == 'unread'){
                           $mailObject->status = 0;
                           $mailObject->update(false);
                    } elseif($_POST['actiontype'] == 'read'){
                        $mailObject->status = 1;
                        $mailObject->update(false);
                    }
                }
            } 
           $this->redirect('/admin/mail?res_filter='.$filter.'&per_page='.$perpage.'&submit='.$submit);
        }
        
        public function GetMailCheckbox($data){            
            echo "<input type='checkbox' class='allcheckbox' name='requestids[".$data->id."]' value='".$data->id."'>" ;
        }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Mail;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Mail'])) {
            $model->attributes = $_POST['Mail'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Mail'])) {
            $model->attributes = $_POST['Mail'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Get the reservation status
     * 
     * @param type $data 
     * @param type $row
     */
    public function convertDate($data, $row) {
        echo date("d M Y g:i A", strtotime($data->updated_at));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Mail('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Mail']))
            $model->attributes = $_GET['Mail'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Mail the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Mail::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Mail $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'mail-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
