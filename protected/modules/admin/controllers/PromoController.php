<?php

class PromoController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function init() {
        BaseClass::isAdmin();
    } 

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'promolistdata', 'getpromoencryptid', 'checkpromoexist', 'getpromoencryptcode', 'usagelist', 'usagelistdata'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    
    public function actionIndex() {
        
        if (isset($_GET['id'])) {
            $promoId = BaseClass::mgDecrypt($_GET['id']);
            $promoObject = Promo::model()->findByPk($promoId);
        } else {
            $promoObject = new Promo();
        }
        
        if (isset($_POST['addedit_submit']) && !empty($_POST['addedit_submit'])) { 
            $addUpdatePromoObject = Promo::model()->addUpdate($_POST, $promoObject);
            if (isset($addUpdatePromoObject) && !empty($addUpdatePromoObject)) {
                Yii::app()->user->setFlash('success', 'Promo succesfully updated');
                $this->redirect('/admin/promo');
            } else {
                Yii::app()->user->setFlash('error', 'Promo updation failed');
                $this->redirect('/admin/promo');
            }
        }
        
        $this->render('index', array('promoObject' => $promoObject));
    }
    
    public function actionPromoListData(){
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
        
        $dataQuery = Yii::app()->db->createCommand()
            ->select('p.id as id, p.code as code, p.can_use as canUse, p.value as value, p.type as type, p.start_date as startDate, p.close_date as closeDate, '
                    . 'IFNULL(u.name, "NA") as userName, (case when p.status = 0 then "InActive" ELSE "ACTIVE" END ) as promoStatus, p.created_at as createdDate')
            ->from('promo p')
            ->leftjoin('user u', 'p.user_id = u.id');
        
        $dataQuery->andWhere('p.id != 1');
        
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $dataQuery->andWhere('p.code LIKE :code', array(':code' => '%'.$searchData.'%'));
            $dataQuery->orWhere('p.can_use LIKE :canUse', array(':canUse' => '%'.$searchData.'%'));
            $dataQuery->orWhere('u.name = :userName', array(':userName' => '%'.$searchData.'%'));
        }
        $promoStatus = 1;
        if($_POST['columns']['7']['search']['value']=='inactive') {
            $promoStatus = 0;
        }
        $dataQuery->andWhere('p.status = :status', array(':status' => $promoStatus));
        
        if(!empty($_POST['columns']['8']['search']['value'])){
            $todayDate = $_POST['columns']['8']['search']['value'];
            $fromDate = $_POST['columns']['8']['search']['regex'];
            $dataQuery->andWhere('p.created_at >= :todate', array(':todate' => $todayDate));
            $dataQuery->andWhere('p.created_at <= :fromdate', array(':fromdate' => $fromDate));
        }
        
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        
        $promoListObject = $dataQuery->queryAll();
        
        $promoListObjectCount = count($promoListObject);
        
        $promoListJSONData = CJSON::encode($promoListObject);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $promoListObjectCount . ',
                    "recordsFiltered": ' . $promoListObjectCount . ',
                    "data":' . $promoListJSONData . '
        }';
        exit;
    }
    
    public function actionGetPromoEncryptId() {
        $id = $_POST['id'];
        $enId = BaseClass::mgEncrypt($id);
        echo $enId;
    }
    
    public function actionGetPromoEncryptCode() {
        $code = $_POST['id'];
        $enCode = BaseClass::mgEncrypt($code);
        echo $enCode;
    }
    
    public function actionCheckPromoExist() {
        
        $promoObject = Promo::model()->findByAttributes(array('code' => $_POST['code']));
        
        if(!empty($promoObject)) {
            echo 0; exit;
        } else {
            echo 1; exit;
        }
    }
    
    public function actionUsageList() {
        $this->render('usagelist');
    }
    
    public function actionUsageListData() {
        $promoId = '';
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }
        if(!empty($_REQUEST['id'])) {
            $promoId = BaseClass::mgDecrypt($_REQUEST['id']);
        }
        
        $dataQuery = Yii::app()->db->createCommand()
            ->select('u.name as userName, t.transaction_id as transactionId, p.code as promoCode, o.package_price as packagePrice,'
                    . ' o.promo_cashback as promoCashback, o.created_at as usedDate ')
            ->from('order o')
            ->join('transaction t', 'o.transaction_id = t.id')
            ->join('user u', 't.user_id = u.id')
            ->join('promo p', 'p.id = o.promo_id');
        
        $dataQuery->andWhere('p.id = :id', array(':id' => $promoId));
        
        if(!empty($_POST['search']['value'])){
            $searchData = $_POST['search']['value'];
            $dataQuery->andWhere('u.name LIKE :userName OR t.transaction_id LIKE :transactionId', array(':userName' => '%'.$searchData.'%', ':transactionId' => '%'.$searchData.'%'));
        }

        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        
        $promoUsageList = $dataQuery->queryAll();
        
        $promoUsageListCount = count($promoUsageList);
        
        $promoUsageListJSONData = CJSON::encode($promoUsageList);
        echo '{"draw": '.$draw.',
                    "recordsTotal": ' . $promoUsageListCount . ',
                    "recordsFiltered": ' . $promoUsageListCount . ',
                    "data":' . $promoUsageListJSONData . '
        }';
        exit;
        
    }

}