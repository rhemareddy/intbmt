<?php

class SiteController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'list', 'edit', 'changestatus', 'deletepage'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    
    public function actionIndex() {
        $successMsg = "";
        $staticPageObject = new StaticPages;
        if (!empty($_POST)) {
            $staticPageObject->attributes = $_POST;
            $staticPageObject->status = 1;
            $staticPageObject->created_at = new CDbExpression('NOW()');
            $staticPageObject->updated_at = new CDbExpression('NOW()');
            if (!$staticPageObject->save(false)) {
                    echo "<pre>";
                    print_r($model->getErrors());
                    exit;
            }
        $successMsg = "Page Created Successfully.";
        }
        $this->render('index', array(
            'staticPageObject' => $staticPageObject, 'successMsg' => $successMsg
        ));
    }

    public function actionList() {
        $pageSize = Yii::app()->params['defaultPageSize'];
        $todayDate = date('Y-m-d');
        $fromDate = date('Y-m-d');

        $dataProvider = new CActiveDataProvider('StaticPages', array(
            'pagination' => array('pageSize' => $pageSize),
        ));
       
        if (!empty($_GET)) {
            $todayDate = $_GET['from'];
            $fromDate = $_GET['to'];
        }
          // Default  Condition.
        $condition = "" . 'created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"' . "";

        $dataProvider = new CActiveDataProvider('StaticPages', array(
            'criteria' => array(
                'condition' => ($condition), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));
        
        $this->render('list', array(
            'dataProvider' => $dataProvider, 'todayDate' => $todayDate, 'fromDate' => $fromDate,
        ));
    }
    
     public function actionEdit() {
        $error = "";
        $success = "";
        $staticPageObject = StaticPages::model()->findByPK(array('id' => $_GET['id']));

        if ($_POST) {
            if ($_POST['title'] == '' || $_POST['description'] == '') {
                $error .= "Please fill required(*) marked fields.";
                $this->redirect('/admin/site/list?error=' . $error);
            } else {
                $staticPageObject->attributes = $_POST;
                $staticPageObject->updated_at = new CDbExpression('NOW()');
                $staticPageObject->update();

                $success .= "Page Successfully Updated";
                $this->redirect('/admin/site/list?success=' . $success);
            }
        }

        $this->render('edit', array('success' => $success, 'error' => $error, 'staticPageObject' => $staticPageObject));
    }
    
     public function actionChangeStatus() {
        if ($_REQUEST['id']) {
            $staticPageObject = StaticPages::model()->findByPK($_REQUEST['id']);
            if ($staticPageObject->status == 1) {
                $staticPageObject->status = 0;
            } else {
                $staticPageObject->status = 1;
            }
            $staticPageObject->save(false);

            $this->redirect(array('/admin/site/list', 'msg' => 2));
        }
    }
    
     public function actionDeletePage() {
        if ($_REQUEST['id']) {
            $staticPageObject = StaticPages::model()->findByPK($_REQUEST['id']);
            $staticPageObject->delete();

            $this->redirect(array('/admin/site/list', 'msg' => 1));
        }
    }

}
