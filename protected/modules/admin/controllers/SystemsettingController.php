<?php

class SystemsettingController extends Controller
{
        public $layout = 'main';
        
        public function init() {
            BaseClass::isAdmin();
        }
        
	public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index','list'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'systemlist'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionSystemList(){
        if($_POST) {
            $systemSettingObject = SystemSetting::model()->findByAttributes(array('id' => $_POST['id']));
            if(!empty($systemSettingObject)){
                $systemSettingObject->value = $_POST['system_cashback'];
                if($systemSettingObject->update(false)) {
                    echo 'success';exit;
                } else {
                    echo 'failed';exit;
                }
            }
        }

        $systemSettingObject = SystemSetting::model()->findAll();
        $this->render('system_settings', array('systemSettingObjectList' => $systemSettingObject));
    }
}