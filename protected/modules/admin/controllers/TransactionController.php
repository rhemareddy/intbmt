<?php

class TransactionController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'transaction', 'bid', 'order', 'autocompletebypid'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

       public function actionTransaction() {
        $model = new MoneyTransfer();
        $pageSize = Yii::app()->params['defaultPageSize'];
        $todayDate = date("Y-m-d");
        $fromDate =  date("Y-m-d");
        $status = 1;
        
        if (!empty($_REQUEST)) {
            $todayDate = date("Y-m-d", strtotime($_REQUEST['to']));
            $fromDate = date("Y-m-d", strtotime($_REQUEST['from']));
            $status = $_REQUEST['res_filter'];
        }
        
        $dateSearch = ' AND (t.created_at <= "' . $todayDate . '" AND t.created_at >= "' . $fromDate . '")';
        if(isset($status)){
        $dateSearch .= " AND t.status = ".$status; 
        }

        $criteria = new CDbCriteria;
        $mode = "transfer";
        $criteria->with = array('transaction', 'wallet');
        $criteria->condition = 't.transaction_id = transaction.id AND transaction.mode = "' . $mode . '"'. $dateSearch;
        $criteria->order = 't.id DESC';

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => $pageSize),));

        $this->render('transaction', array(
            'dataProvider' => $dataProvider,
        ));
    }
    
    protected function GetStatusWallet($data, $row) {
        $moneyTransferbject = MoneyTransfer::model()->find(array('condition' => 'id=' . $data['id']));
        if ($moneyTransferbject->status == 1) {
            $title = 'Completed';
        } else {
            $title = 'Pending';
        }
        echo $title;
    }
    
    public function actionBid() {        
        $todayDate=date('Y-m-d');
        $fromDate=date('Y-m-d');
        $searchByPid = "";
        $name = "";
        $status = "";
        $pageSize = Yii::app()->params['defaultPageSize'];
        
        if (!empty($_REQUEST)) {
            $searchByPid=$_REQUEST['searchPid'];
            $name=$_REQUEST['name'];
            $status = $_REQUEST['res_filter'];
            
            if (!empty($_REQUEST['to']) && !empty($_REQUEST['from'])) {
                $todayDate = date("Y-m-d", strtotime($_REQUEST['to']));
                $fromDate = date("Y-m-d", strtotime($_REQUEST['from']));
                
            }
            // Default  Condition.
             $condition = "" . 'ds.start_date >= "' . $todayDate . '" AND ds.close_date < "' .  $fromDate . '"' . "";
           
             //checking PackageId 
            if ($searchByPid) {
                $condition .= ' AND package_id = ' . $searchByPid;
            }
            //Checking status
            if ($status!="") {
            $condition .= ' AND ds.status = ' . $status;
            }
            
                $dataProvider=new CActiveDataProvider('AuctionBids', array(
                'criteria'=>array(
                'condition'=>($condition),
                'select' => 't.auction_id, ds.*',
                'order' => 'ds.id DESC',
                'group'=>'t.auction_id',
                'distinct'=>true,
                'join' => 'LEFT JOIN auction ds on ds.id = t.auction_id',
                 ),
                       'pagination' => array('pageSize' => $pageSize),
            ));
        }
        else{
                   $dataProvider = new CActiveDataProvider('AuctionBids', array(
                   'criteria' => array(
                   'group'=>'auction_id',
                   'distinct'=>true,
                   'order' => 'id DESC',
                ), 'pagination' => array('pageSize' => $pageSize),
            ));
        }

        $this->render('transaction_bid', array('dataProvider' => $dataProvider,'searchByPid' => $searchByPid, 'name' => $name, 'to' => $todayDate, 'from' => $fromDate, 'status' => $status));
    }
    
    protected function getWinner($data, $row) {
        $userId = $data->auction->winner_user_id;
        $userObject = User::model()->findByPk($userId);
        if(!empty($userObject)){
            return $userObject->name;
        }else {
            return "Not Declared";
        }
    }

    public function actionOrder() {
        //$model = new MoneyTransfer();
        $pageSize = Yii::app()->params['defaultPageSize'];
        $todayDate = date("Y-m-d");
        $fromDate =  date("Y-m-d");
        $status = 1;
        if (!empty($_REQUEST)) {
            $todayDate = date("Y-m-d", strtotime($_REQUEST['to']));
            $fromDate = date("Y-m-d", strtotime($_REQUEST['from']));
            $status = $_REQUEST['res_filter'];
        }
        $condition = "" . 'created_at >= "' . $fromDate . '" AND created_at <= "' . $todayDate . '"' . "";
        if(isset($status)){
        $condition .= " AND status = ".$status; 
        }

//        $dataProvider = new CActiveDataProvider($model, array(
//            'criteria' => array(
//                'condition' => ('created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '" AND status = "' . $status . '"' ), 'order' => 'id DESC',
//            ), 'pagination' => array('pageSize' => $pageSize),));
        $dataProvider = new CActiveDataProvider('Order', array(
                'criteria' => array(
                    'condition' => ($condition), 'order' => 'id DESC',
                ), 'pagination' => array('pageSize' => $pageSize),));

        $this->render('transaction_order', array(
            'dataProvider' => $dataProvider,
        ));
    }
     protected function GetStatus($data, $row) {
        $orderObject = Order::model()->find(array('condition' => 'id=' . $data['id']));
        $id = $data->id;
        if ($orderObject->status == 1) {
            $title = 'Completed';
        } else {
            $title = 'Pending';
            //<a class="fancybox btn green fa fa-edit margin-right15" onclick = "showPendingPayment(' . $id . ');">Make Payment</a>
        }
        echo $title;
    }
     public function actionAutoCompleteByPid($term) {
        $match = $term;
        $projectObject = Package::model()->findAll(
                'name LIKE :match', array(':match' => "%$match%")
        );
        $list = array();
        foreach ($projectObject as $q) {
            $data['value'] = $q['id'];
            $data['label'] = $q['name'];
            $list[] = $data;
            unset($data);
        }
        echo json_encode($list);
    }
}
