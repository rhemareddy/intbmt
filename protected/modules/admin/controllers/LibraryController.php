<?php

class LibraryController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'category', 'categorylistdata', 'getencryptid', 'media',
                    'medialistdata', 'getsubcategory', 'getsearchsubcategory'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionCategory() {
        $allMediaCenterCategoryObject = MediaCenterCategory::model()->findAll(array('condition' => 'parent_id = 0 AND 	status = 1'));

        if (isset($_GET['id'])) {
            $mediaCenterCategoryId = BaseClass::mgDecrypt($_GET['id']);
            $mediaCenterCategoryObject = MediaCenterCategory::model()->findByPk($mediaCenterCategoryId);
        } else {
            $mediaCenterCategoryObject = new MediaCenterCategory();
        }


        if (isset($_POST['addedit_submit']) && !empty($_POST['addedit_submit'])) { //resource libarary category/subcategory add or edit
            if (isset($_GET['id'])) {
                $mediaCenterCategoryId = BaseClass::mgDecrypt($_GET['id']);
                $existingMediaCenterCategory = MediaCenterCategory::model()->findAll(array('condition' => 'name = "' . $_POST['name'] . '" AND id <> ' . $mediaCenterCategoryId));
            } else {
                $existingMediaCenterCategory = MediaCenterCategory::model()->findAll(array('condition' => 'name = "' . $_POST['name'] . '"'));
            }

            if (empty($existingMediaCenterCategory)) {
                $newMediaCenterCategoryObject = MediaCenterCategory::model()->create($_POST, $_FILES, $mediaCenterCategoryObject);
                if (isset($newMediaCenterCategoryObject) && !empty($newMediaCenterCategoryObject)) {
                    Yii::app()->user->setFlash('success', 'category or subcategory succesfully updated');
                    $this->redirect('/admin/library/category');
                } else {
                    Yii::app()->user->setFlash('error', 'category or subcategory updation failed');
                    $this->redirect('/admin/library/category');
                }
            } else {
                Yii::app()->user->setFlash('error', 'category or subcategory name already exist');
                $this->redirect('/admin/library/category');
            }
        }


        if (isset($_POST['bulkaction']) && !empty($_POST['bulkaction'])) { //resource libarary category/subcategory active, in active or delete
            $filterArray = "";
            if (isset($_POST['requestids']) && !empty($_POST['requestids'])) {
                $filterArray = $_POST['requestids'];
            }
            $this->LibraryCategoryBulkactions($filterArray, $_POST['bulkaction']);
        }


        $this->render('category', array('allMediaCenterCategoryObject' => $allMediaCenterCategoryObject,
            'mediaCenterCategoryObject' => $mediaCenterCategoryObject));
    }

    public function LibraryCategoryBulkactions($filterArray, $type) {
        if ($type == 'Active' || $type == 'Inactive') {
            $categoryChangeStatus = $this->categoryChangeStatus($filterArray, $type);
            if ($categoryChangeStatus) {
                Yii::app()->user->setFlash('success', "Status has been changed successfully.");
                $this->redirect('/admin/library/category');
            } else {
                Yii::app()->user->setFlash('error', "Please select any category or subcategory from the list.");
                $this->redirect('/admin/library/category');
            }
        } else if ($type == 'Remove') {
            $categoryRemove = $this->removeCategory($filterArray);
            if ($categoryRemove) {
                Yii::app()->user->setFlash('success', "Records deleted successfully.");
                $this->redirect('/admin/library/category');
            } else {
                Yii::app()->user->setFlash('error', "Please select any category or subcategory from the list.");
                $this->redirect('/admin/library/category');
            }
        } else {
            Yii::app()->user->setFlash('error', "Please select any action.");
            $this->redirect('/admin/library/category');
        }
    }

    public function categoryChangeStatus($filterArray, $status) {
        if (isset($filterArray) && !empty($filterArray)) {
            foreach ($filterArray as $key => $filter) {
                $mediaCenterCategoryObject = MediaCenterCategory::model()->findByPk($filter);
                if ($mediaCenterCategoryObject) {
                    if ($status == 'Active') {
                        $mediaCenterCategoryObject->status = 1;
                    } else {
                        $mediaCenterCategoryObject->status = 0;
                    }
                    $mediaCenterCategoryObject->update();
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function removeCategory($filterArray) {
        if (isset($filterArray) && !empty($filterArray)) {
            foreach ($filterArray as $key => $filter) {
                $mediaCenterCategoryObject = MediaCenterCategory::model()->findByPk($filter);
                if ($mediaCenterCategoryObject) {
                    $mediaCenterCategoryObject->delete();
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function actionCategoryListData() {
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if ($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }

        $statusCondition = 'mcc.status = 1';
        if ($_POST['columns'][5]['search']['value'] == 2) {
            $statusCondition = 'mcc.status = 0';
        }
        if ($_POST['columns'][5]['search']['value'] == 'all') {
            $statusCondition = 'mcc.status IN (1,0)';
        }

        $categoryCondition = "";
        if ($_POST['columns'][1]['search']['value'] == 1) {
            $categoryCondition = 'mcc.parent_id = 0';
        }
        if ($_POST['columns'][1]['search']['value'] == 2) {
            $categoryCondition = 'mcc.parent_id != 0';
        }

        $dataQuery = Yii::app()->db->createCommand()
                    ->select('mcc.id, mcc.name, mcc.image, mcc.icon_image, mcc.priority, mcc.created_at,'
                        . '(case when mcc.parent_id != 0 then c.name ELSE "NA" END ) as mainCategory , '
                        . '(case when mcc.parent_id = 0 then "Category" ELSE "Subcategory" END ) as categoryType , '
                        . '(case when mcc.status = 0 then "Inactive" ELSE "Active" END ) as categoryStatus')
                ->from('media_center_category mcc')
                ->leftjoin('media_center_category c', 'c.id=mcc.parent_id');
        $dataQuery->where($statusCondition);
        $dataQuery->andWhere($categoryCondition);
        if (!empty($_POST['search']['value'])) {
            $searchData = $_POST['search']['value'];
            $dataQuery->andWhere('mcc.name LIKE :categoryname', array(':categoryname' => '%' . $searchData . '%'));
        }
        if (!empty($_POST['columns']['4']['search']['value']) && !empty($_POST['columns']['4']['search']['regex'])) {
            $todayDate = $_POST['columns']['4']['search']['value'];
            $fromDate = $_POST['columns']['4']['search']['regex'];
            $dataQuery->andWhere('mcc.created_at <= :todate AND mcc.created_at >= :fromdate', array(':todate' => $todayDate, ':fromdate' => $fromDate));
        }
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $userListObject = $dataQuery->queryAll();
        //Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
                ->select('count(*) as totalRecords')
                ->from('media_center_category mcc')
                ->leftjoin('media_center_category c', 'c.id=mcc.parent_id');
        $countQuery->where($statusCondition);
        $countQuery->andWhere($categoryCondition);
        if (!empty($_POST['search']['value'])) {
            $searchData = $_POST['search']['value'];
            $countQuery->andWhere('mcc.name LIKE :categoryname', array(':categoryname' => '%' . $searchData . '%'));
        }
        if (!empty($_POST['columns']['4']['search']['value']) && !empty($_POST['columns']['4']['search']['regex'])) {
            $todayDate = $_POST['columns']['4']['search']['value'];
            $fromDate = $_POST['columns']['4']['search']['regex'];
            $countQuery->andWhere('mcc.created_at <= :todate AND mcc.created_at >= :fromdate', array(':todate' => $todayDate, ':fromdate' => $fromDate));
        }
        $totalRecordsArray = $countQuery->queryRow();

        $userObjectCount = $totalRecordsArray['totalRecords'];

        $userJSONData = CJSON::encode($userListObject);
        echo '{"draw": ' . $draw . ',
                    "recordsTotal": ' . $userObjectCount . ',
                    "recordsFiltered": ' . $userObjectCount . ',
                    "data":' . $userJSONData . '
        }';
        exit;
    }

    public function actionGetEncryptId() {
        $id = $_POST['id'];
        $enId = BaseClass::mgEncrypt($id);
        echo $enId;
    }

    public function actionMedia() {
        $allMediaCenterCategoryObject = MediaCenterCategory::model()->findAll(array('condition' => 'parent_id = 0 AND 	status = 1'));

        if (isset($_GET['id'])) {
            $mediaCenterDocumentId = BaseClass::mgDecrypt($_GET['id']);
            $mediaCenterDocumentObject = MediaCenterDocument::model()->findByPk($mediaCenterDocumentId);
        } else {
            $mediaCenterDocumentObject = new MediaCenterDocument();
        }

        if (isset($_POST['addedit_submit']) && !empty($_POST['addedit_submit'])) { //resource libarary media add or edit
            $newmediaCenterDocumentObject = MediaCenterDocument::model()->addUpdate($_POST, $_FILES, $mediaCenterDocumentObject);
            if (!empty($newmediaCenterDocumentObject)) {
                Yii::app()->user->setFlash('success', 'Media succesfully updated');
                $this->redirect('/admin/library/media');
            } else {
                Yii::app()->user->setFlash('error', 'Media updation failed');
                $this->redirect('/admin/library/media');
            }
        }

        if (isset($_POST['bulkaction']) && !empty($_POST['bulkaction'])) { //resource libarary category/subcategory active, in active or delete
            $filterArray = "";
            if (isset($_POST['requestids']) && !empty($_POST['requestids'])) {
                $filterArray = $_POST['requestids'];
            }
            $this->LibraryMediaBulkactions($filterArray, $_POST['bulkaction']);
        }


        $this->render('media', array('allMediaCenterCategoryObject' => $allMediaCenterCategoryObject,
            'mediaCenterDocumentObject' => $mediaCenterDocumentObject));
    }

    public function LibraryMediaBulkactions($filterArray, $type) {
        if ($type == 'Active' || $type == 'Inactive') {
            $categoryChangeStatus = $this->documentChangeStatus($filterArray, $type);
            if ($categoryChangeStatus) {
                Yii::app()->user->setFlash('success', "Status has been changed successfully.");
                $this->redirect('/admin/library/media');
            } else {
                Yii::app()->user->setFlash('error', "Please select any category from the list.");
                $this->redirect('/admin/library/media');
            }
        } else if ($type == 'Remove') {
            $categoryRemove = $this->removeMedia($filterArray);
            if ($categoryRemove) {
                Yii::app()->user->setFlash('success', "Records deleted successfully.");
                $this->redirect('/admin/library/media');
            } else {
                Yii::app()->user->setFlash('error', "Please select any category from the list.");
                $this->redirect('/admin/library/media');
            }
        } else {
            Yii::app()->user->setFlash('error', "Please select any action.");
            $this->redirect('/admin/library/media');
        }
    }

    public function documentChangeStatus($filterArray, $status) {
        if (isset($filterArray) && !empty($filterArray)) {
            foreach ($filterArray as $key => $filter) {
                $mediaCenterCategoryObject = MediaCenterDocument::model()->findByPk($filter);
                if ($mediaCenterCategoryObject) {
                    if ($status == 'Active') {
                        $mediaCenterCategoryObject->status = 1;
                    } else {
                        $mediaCenterCategoryObject->status = 0;
                    }
                    $mediaCenterCategoryObject->update();
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function removeMedia($filterArray) {
        if (isset($filterArray) && !empty($filterArray)) {
            foreach ($filterArray as $key => $filter) {
                $mediaCenterCategoryObject = MediaCenterDocument::model()->findByPk($filter);
                if ($mediaCenterCategoryObject) {
                    $mediaCenterCategoryObject->delete();
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function actionMediaListData() {
        $limit = (int) isset($_POST['length']) ? $_POST['length'] : 50;
        $offset = (int) isset($_POST['start']) ? $_POST['start'] : 0;
        $draw = (int) isset($_POST['draw']) ? $_POST['draw'] : 1;
        if ($_POST['order']) {
            $fieldOrderId = $_POST['order'][0]['column'];
            $orderString = $_POST['order'][0]['dir'];
            $orderBy = $_POST['columns'][$fieldOrderId]['data'] . " " . $orderString;
        }

        $statusCondition = 'mcd.status = 1';
        if ($_POST['columns'][7]['search']['value'] == 2) {
            $statusCondition = 'mcd.status = 0';
        }
        if ($_POST['columns'][7]['search']['value'] == 'all') {
            $statusCondition = 'mcd.status IN (1,0)';
        }

        $categoryCondition = "";
        if ($_POST['columns'][1]['search']['value']) {
            $categoryCondition = 'mcd.category = '.$_POST['columns'][1]['search']['value'];
        } 
        
        $subCategoryCondition = "";
        if ($_POST['columns'][2]['search']['value']) {
            $subCategoryCondition = 'mcd.subcategory = '.$_POST['columns'][2]['search']['value'];
        } 

        $dataQuery = Yii::app()->db->createCommand()
                ->select('mcd.id, mcd.title, mcd.description, mcd.language, mcd.created_at,c.name as category, mcd.image as image, mcd.status,mcd.created_at,'
                        . '(case when mcd.is_featured = 1 then "Yes" ELSE "NO" END ) as isPreffered , '
                        . '(case when mcd.subcategory != 0 then sc.name ELSE "" END ) as subcategory , '
                        . '(case when mcd.status = 0 then "InActive" ELSE "ACTIVE" END ) as mcdStatus')
                ->from('media_center_document mcd')
                ->join('media_center_category c', 'c.id=mcd.category')
                ->leftjoin('media_center_category sc', 'sc.id=mcd.subcategory');
        $dataQuery->where($statusCondition);
        $dataQuery->andWhere($categoryCondition);
        $dataQuery->andWhere($subCategoryCondition);
        if (!empty($_POST['search']['value'])) {
//            print_r($_POST['columns']); die;
            $searchData = $_POST['search']['value'];
            $andCondition = !empty($statusCondition)?" AND ".$statusCondition:"";
            $andCondition .= !empty($categoryCondition)?" AND ".$categoryCondition:"";
            $andCondition .= !empty($subCategoryCondition)?" AND ".$subCategoryCondition:"";
            
            $dataQuery->andWhere('c.name LIKE :categoryname', array(':categoryname' => '%' . $searchData . '%'));
            $dataQuery->orWhere('sc.name LIKE :scategoryname'.$andCondition, array(':scategoryname' => '%' . $searchData . '%'));
            $dataQuery->orWhere('mcd.title LIKE :categoryname'.$andCondition, array(':categoryname' => '%' . $searchData . '%'));
            $dataQuery->orWhere('mcd.description LIKE :description'.$andCondition, array(':description' => '%' . $searchData . '%'));
            $dataQuery->orWhere('mcd.language LIKE :language'.$andCondition, array(':language' => '%' . $searchData . '%'));
        }
        if (!empty($_POST['columns']['8']['search']['value']) && !empty($_POST['columns']['8']['search']['regex'])) {
            $todayDate = $_POST['columns']['8']['search']['value'];
            $fromDate = $_POST['columns']['8']['search']['regex'];
            $dataQuery->andWhere('mcd.created_at <= :todate AND mcd.created_at >= :fromdate', array(':todate' => $todayDate, ':fromdate' => $fromDate));
        }
        $dataQuery->order($orderBy)->limit($limit)->offset($offset);
        $userListObject = $dataQuery->queryAll();
        //Get total record in the datablse with condition
        $countQuery = Yii::app()->db->createCommand()
                ->select('count(*) as totalRecords')
                ->from('media_center_document mcd')
                ->join('media_center_category c', 'c.id=mcd.category')
                ->leftjoin('media_center_category sc', 'sc.id=mcd.subcategory');
        $countQuery->where($statusCondition);
        $countQuery->andWhere($categoryCondition);
        $countQuery->andWhere($subCategoryCondition);
        if (!empty($_POST['search']['value'])) {
            $searchData = $_POST['search']['value'];
            $andCondition = !empty($statusCondition)?" AND ".$statusCondition:"";
            $andCondition .= !empty($categoryCondition)?" AND ".$categoryCondition:"";
            $andCondition .= !empty($subCategoryCondition)?" AND ".$subCategoryCondition:"";
            
            $countQuery->andWhere('c.name LIKE :categoryname', array(':categoryname' => '%' . $searchData . '%'));
            $countQuery->orWhere('sc.name LIKE :scategoryname'.$andCondition, array(':scategoryname' => '%' . $searchData . '%'));
            $countQuery->orWhere('mcd.title LIKE :categoryname'.$andCondition, array(':categoryname' => '%' . $searchData . '%'));
            $countQuery->orWhere('mcd.description LIKE :description'.$andCondition, array(':description' => '%' . $searchData . '%'));
            $countQuery->orWhere('mcd.language LIKE :language'.$andCondition, array(':language' => '%' . $searchData . '%'));
        }
        if (!empty($_POST['columns']['8']['search']['value']) && !empty($_POST['columns']['8']['search']['regex'])) {
            $todayDate = $_POST['columns']['8']['search']['value'];
            $fromDate = $_POST['columns']['8']['search']['regex'];
            $countQuery->andWhere('mcd.created_at <= :todate AND mcd.created_at >= :fromdate', array(':todate' => $todayDate, ':fromdate' => $fromDate));
        }
        $totalRecordsArray = $countQuery->queryRow();

        $userObjectCount = $totalRecordsArray['totalRecords'];

        $userJSONData = CJSON::encode($userListObject);
        echo '{"draw": ' . $draw . ',
                    "recordsTotal": ' . $userObjectCount . ',
                    "recordsFiltered": ' . $userObjectCount . ',
                    "data":' . $userJSONData . '
        }';
        exit;
    }

    public function actionGetSubcategory() { // For the get subcategory 
        $html = "";
        $mediaCategoryObject = MediaCenterCategory::model()->findByPk($_POST['id']);
        $mediaSubCategoryObject = MediaCenterCategory::model()->findAll(array('condition' => ' parent_id  =' . $_POST['id'] . ' AND status = 1'));
        if ($mediaSubCategoryObject) {
            $html .= '<select class="form-control" name="subcategory" id="subcategory"  onchange="getSubCategoryData(this.value)" >';
            foreach ($mediaSubCategoryObject as $mediaSubCategory) {
                $html .= '<option value="' . $mediaSubCategory->id . '">' . $mediaSubCategory->name . '</option>';
            }
            $html .= '</select>';
        }

        $subcategoryArray = array('html' => $html, 'categoryName' => $mediaCategoryObject->name);
        echo CJSON::encode($subcategoryArray);
        exit;
    }
    
    public function actionGetSearchSubcategory() { // For the get subcategory 
        $html = "";
        $mediaCategoryObject = MediaCenterCategory::model()->findByPk($_POST['id']);
        $mediaSubCategoryObject = MediaCenterCategory::model()->findAll(array('condition' => ' parent_id  =' . $_POST['id'] . ' AND status = 1'));
        if ($mediaSubCategoryObject) {
            $html .= '<select class="form-control" name="change_sub_category" id="change_sub_category"><option value="" selected="selected">All</option>';
            foreach ($mediaSubCategoryObject as $mediaSubCategory) {
                $html .= '<option value="' . $mediaSubCategory->id . '">' . $mediaSubCategory->name . '</option>';
            }
            $html .= '</select>';
        }

        $subcategoryArray = array('html' => $html, 'categoryName' => $mediaCategoryObject->name);
        echo CJSON::encode($subcategoryArray);
        exit;
    }

}
