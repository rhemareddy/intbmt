<?php

class NewsController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = 'main';

    public function init() {
        BaseClass::isAdmin();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'add', 'list', 'edit', 'changestatus', 'deletenews'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionAdd() {
        $error = "";
        $success = "";
        if (!empty($_POST)) {
            $newsObject = new News;
            $newsObject->title = $_POST['title'];
            $newsObject->news = $_POST['news'];
            $newsObject->created_at = date('Y-m-d');
            $newsObject->updated_at = new CDbExpression('NOW()');
            $newsObject->save(false);

            $success .= "News Added Successfully";
            $this->redirect('add?success=' . $success);
        }

        $this->render('news_add', array('success' => $success, 'error' => $error));
    }

    public function actionList() {
        $model = new News();
        $pageSize = Yii::app()->params['defaultPageSize'];
        $todayDate = date('Y-m-d');
        $fromDate = date('Y-m-d');
        $status = 1;
        if (!empty($_REQUEST)) {
            $todayDate = $_REQUEST['from'];
            $fromDate = $_REQUEST['to'];
        }

        // Default  Condition.
        $condition = "" . 'created_at >= "' . $todayDate . '" AND created_at <= "' . $fromDate . '"' . "";

        $dataProvider = new CActiveDataProvider($model, array(
            'criteria' => array(
                'condition' => ($condition), 'order' => 'id DESC',
            ), 'pagination' => array('pageSize' => $pageSize),));
        $this->render('news_list', array(
            'dataProvider' => $dataProvider, 'todayDate' => $todayDate, 'fromDate' => $fromDate,
        ));
    }

    public function actionEdit() {
        $error = "";
        $success = "";
        $newsObject = News::model()->findByPK(array('id' => $_GET['id']));

        if ($_POST) {
            if ($_POST['title'] == '' || $_POST['news'] == '') {
                $error .= "Please fill required(*) marked fields.";
                $this->redirect('list?error=' . $error);
            } else {
                $newsObject->attributes = $_POST;
                $newsObject->updated_at = new CDbExpression('NOW()');
                $newsObject->update();

                $success .= "News Successfully Updated";
                $this->redirect('list?success=' . $success);
            }
        }

        $this->render('news_edit', array('success' => $success, 'error' => $error, 'newsObject' => $newsObject));
    }

    public function actionChangeStatus() {


        if ($_REQUEST['id']) {
            $newsObject = News::model()->findByPK($_REQUEST['id']);
            if ($newsObject->status == 1) {
                $newsObject->status = 0;
            } else {
                $newsObject->status = 1;
            }
            $newsObject->save(false);

            $this->redirect(array('/admin/news/list', 'msg' => 2));
        }
    }

    public function actionDeleteNews() {

        if ($_REQUEST['id']) {
            $newsObject = News::model()->findByPK($_REQUEST['id']);
            $newsObject->delete();

            $this->redirect(array('/admin/news/list', 'msg' => 1));
        }
    }

}
