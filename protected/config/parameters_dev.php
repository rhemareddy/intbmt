<?php

$ADMINDIR = 'admin';
$payPal = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
$vkCallBackUrl = 'http://dev.mavbid.com/user/vk';
$vkAppId = '5511818';
$vkApiSecret = 'uH02apZJQNF6R0Ekhphg';

$linkedinScope = 'r_basicprofile r_emailaddress';
$linkedinCallBackURL = 'http://dev.mavbid.com/user/linkedin';
$linkedinApiKey = '75uki0974cj4o5';
$linkedinApiSecret = '2Nl7xRtMW95cCUFc';

$facebookAppId = '883214041784266';
$facebookApiSecret = '4042f71c28ac31ef9560423f31dc30f2';

$googleClientId = '226402714357-auacuq2f03lu31ro0a6vsgk6n8j335tg.apps.googleusercontent.com'; //Google CLIENT ID
$googleClientSecret = 'S7U3qTwRTqPnfGYthq542Ty7'; //Google CLIENT SECRET
$googleRedirectUri = 'http://dev.mavbid.com/user/google';  //return url (url to script)
$googleHomeUrl = 'http://dev.mavbid.com';  //return to home

$inviteReferral = "http://dev.mavbid.com/site/join/";
$inviteReferralLink = "http://dev.mavbid.com/site/join/";

$business = 'suraj.asati111-developer@gmail.com';
$domain = "http://dev.mavbid.com/";
$payzaURL = "https://sandbox.Payza.com/sandbox/payprocess.aspx";
$payzaAccount = "seller_1_surya.asati@maverickinfosoft.com";
$paymentResponseUrl = $domain . 'purchase/paymentresponse';

return array(
    'AdminDir' => 'admin',
    'baseUrl' => $domain,
    'imagePath' => array(
        'homePageSlider' => '/upload/homepageSlider/',
        'upload' => 'upload/',
        'package' => 'upload/product/',
        'hotel' => '/upload/profile/',
        'verificationDoc' => '/upload/verification-document/',
        'profileImage' => '/upload/profile-image/',
        'homeBanner' => '/upload/homepageSlider/',
        'homeAdBanner' => '/upload/ads/',
        'homepageSliderPath' => array("city", "state", "country", "general"),
        'city' => 'upload/city/',
        'state' => 'upload/state/',
        'imageNotFound' => '/images/image_not_found.jpg',
        'OfferImage' => 'upload/offer-image/',
        'availOffer' => 'upload/avail_offer/',
        'news' => '/upload/news',
    ),
    'photo_type' => array('jpg', 'jpeg', 'gif', 'png'),
    'contactNumber' => '123456789',
    'facebookPageUrl' => "",
    'googlePageUrl' => "",
    'twitterPageUrl' => "",
    'instagramPageUrl' => "",
    'linkedinPageUrl' => "",
    'percent' => 5,
    'blogUrl' => '/blog/',
    'thumbnails' => array(
        'homepagePackages' => array("70_70" => "70_70", "270_160" => "270_160", "900_500" => "900_500"),
    ),
    'senderMail' => 'no-reply@mavbid.com',
    'senderName' => 'Mav Bid',
    'defaultPageSize' => 50,
    'minPerPage' => 50,
    'startDate' => date('Y-m-d'),
    'startDateLastSevenDay' => date('Y-m-d', strtotime('-7 days')),
    'clientInvoicePercentage' => 5,
    'pageSizeOptions' => array(50 => 50, 100 => 100, 150 => 150),
    'commissionType' => array(
        'description' => 'Description', 'offer' => 'Offer', 'guide' => 'Guide', 'nearby' => 'Nearby', 'transportation' => 'Transportation', 'howto' => 'How to get there?', 'parking' => 'Closed parking lot & Fee'
    ),
    'homeUrl' => '/' . $ADMINDIR . '/users',
    'logoutUrl' => '/' . $ADMINDIR . '/default/login',
    'adminId' => '1',
    'adminSpnId' => '1',
    'adminSpnName' => 'admin',
    'socialSpnId' => '2',
    'socialSpnName' => 'social',
    'paymentGateway' => array(
        'paypal' => array(
            'target_url' => '',
            'call_back_url' => '/controller name/callback',
        )
    ),
    'default' => array(
        'countryId' => 2,
        'cityId' => 13,
        'portalId' => 1,
        'language_id' => 1,
        'invoice_payment_duration' => 18
    ),
    'smsConfig' => array(
        'api_key' => "3cf58dc2",
        'api_secret' => "4a88e03373641a9c",
        'from' => '',
        'apiUrl' => ''
    ),
    'session' => array(
        'class' => 'CDbHttpSession',
        'timeout' => 1800,
    ),
    'sitename' => 'mybidzone',
    'ip' => "162.144.197.77",
    'twitterLink' => 'https://twitter.com/intent/tweet?text=',
    'featuredImage' => "270_160",
    'thumbImage' => "70_70",
    'sliderImage' => "900_500",
    'site_key' => "6LfwOgsTAAAAAHELIrJWjFsfxk3RCeD1mxAtyvHQ",
    'secret_key' => "6LfwOgsTAAAAAAYQad0C2c4KvLks11xwCi86bszd",
    'googleAuthorizationURL' => "https://accounts.google.com/o/oauth2/token",
    'googleGetContactsURL' => "https://www.google.com/m8/feeds/contacts/default/full?max-results=",
    'MaxUploadSize' => 2097152, // 2MB.
    'MaxUploadSizeProfileImg' => 1048576, //1MB   
    'preMonthDate' => date('Y-m-d', strtotime(date('Y-m-d') . " -1 month")),
    'defaultNamePerc' => 5,
    'encryptionPassword' => 9742023299271183,
    'paypalurl' => $payPal,
    'business' => $business,
    'paymentResponse' => $paymentResponseUrl,
    'inviteReferral' => $inviteReferral,
    'inviteReferralLink' => $inviteReferralLink,
    'social' => array(
        'vk' => array(
            'callBackUrl' => $vkCallBackUrl,
            'appId' => $vkAppId,
            'apiSecret' => $vkApiSecret,
        ),
        'linkedin' => array(
            'scope' => $linkedinScope,
            'callBackURL' => $linkedinCallBackURL,
            'apiKey' => $linkedinApiKey,
            'apiSecret' => $linkedinApiSecret,
        ),
        'facebook' => array(
            'appId' => $facebookAppId,
            'appSecret' => $facebookApiSecret,
        ),
        'googlelogin' => array(
            'clientId' => $googleClientId,
            'clientSecret' => $googleClientSecret,
            'redirectUri' => $googleRedirectUri,
            'homeUrl' => $googleHomeUrl,
        ),
        'googlecaptcha' => array(
            'publicKey' => 'https://toponexchange.com/user/vk',
            'privateKey' => '5448973',
        ),
    ),
    'paymentGateway' => array(
        'payza' => array(
            'url' => $payzaURL,
            'merchant_payza' => $payzaAccount,
        ),
        'paypal' => array(
            'url' => $payPal,
            'business' => $business,
        ),
        'perfectmoney' => array(
            'url' => 'https://perfectmoney.is/api/step1.asp',
            'PAYEE_ACCOUNT' => '23423423',
            'PAYEE_NAME' => 'Mav Wealth Limited',
            'PAYMENT_ID' => '251111',
            'STATUS_URL' => 'https://www.mavwealth.com',
        ),
        'payeer' => array(
            'url' => 'https://payeer.com/merchant/',
            'shop' => '234234',
            'key' => 'SuCWTjru3Sb222YLBTb',
        ),
        'okpay' => array(
            'url' => 'https://checkout.okpay.com/',
            'receiver' => '23423432',
        ),
        'fasapay' => array(
            'url' => 'https://sandbox.coinbase.com/checkouts/',
            'account' => 'FP1612222171',
        ),
        'bitcoin' => array(
            'url' => 'https://sandbox.coinbase.com/checkouts/',
        ),
    ),
    // Payament Server
    'payza' => $payzaURL,
    'merchant_payza' => $payzaAccount,
    'paypalurl' => $payPal,
    'business' => $business,
    'perfectmoney' => 'https://perfectmoney.is/api/step1.asp',
    'PAYEE_ACCOUNT' => '23423423',
    'PAYEE_NAME' => 'ABC SOLUTIONS LTD',
    'PAYMENT_ID' => '251111',
    'STATUS_URL' => 'abc@gmail.com',
    /* Front User */
    'payeer' => 'https://payeer.com/merchant/',
    'shop' => '234234',
    'key' => 'SuCWTjru3Sb222YLBTb',
    'okpay' => 'https://checkout.okpay.com/',
    'receiver' => '23423432',
    'fasapayUrl' => 'http://sandbox.fasapay.com/sci',
    'fasapayAccount' => 'FP1612222171',
//    'bitcoinUrl' => 'https://coinbase.com/checkouts/',
    'bitcoinUrl' => 'https://sandbox.coinbase.com/checkouts/',
    'skrill' => 'https://www.moneybookers.com/app/payment.pl',
    'email' => 'accounts@mavwealth.com',
//    'cashu' => 'https://sandbox.cashu.com/cgi-bin/pcashu.cgi',
//    'merchant' => 'ads',
//    'token' => 'cf52b70cfa3baf46a04f8dd17579d840',
//    'solidtrustpay' => 'https://sandbox.cashu.com/cgi-bin/pcashu.cgi',
//    'merchantAccount' => 'sad',         
    'walletIdList' => array(1, 2, 3), //1 For Bid, 2 For Cashback
    'recordsPerPage' => array('50' => 50, '100' => 100, '200' => 200),
    'skype' => 'help.mavwealth',
    'brochure' => '/upload/media-center/mavwealth_brochure_english.pdf',
    'media_center' => array(
        'category' => '/upload/media-center/category/',
        'document' => '/upload/media-center/document/',
    ),
    'auctionPrefix' => 'MW',
    'domainName' => $domain,
    'address' => ' 27, Old Gloucester Street, <br> LONDON, WC1N 3AX, <br> UNITED KINGDOM',
    'phone' => '+447441912666',
    'skype' => 'help.mavwealth',
    'mail' => 'info@mavwealth.com',
    
    /* ADV Cash */
    'advcashurl' => 'https://wallet.advcash.com/sci',
    'advcashAccEmail' => 'billing@mavwealth.com',
    'advcashSciName' => 'Devmav',
    'advcashSciPassword' => '9y$E5tm5Jw',
    'advcashCurrency' => 'USD',
    'cms' => "/upload/cms/",
    'coinPaymentMerchentId' => '5a061331c485679fbd4c957871283242'
);
