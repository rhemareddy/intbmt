<?php

$dbname = "intbmt";
$username = "root";
$pass = "root";

$mailHost = 'mail.intbmt.com';
$mailUserName = "no-reply@intbmt.com";
$mailPassword = "tH)+SLCZv)nt";
$mailPort = '25';
$publickey = "";   // Enter private RSA key here;
$privtekey = "";   // Here comes public RSA key;
$domain = "dev.intbmt.com";
$selector = "mail._doaminkey.intbmt.com";

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'intbmt',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.protected.extensions.*',
        'application.modules.*',
        'application.extensions.*',
    ),
    //'theme'=>'metronic',
    'sourceLanguage' => 'en_us',
    'language' => 'en-US',
    'modules' => array(
        // uncomment the following to enable the Gii tool
        'admin',
        'mobile',
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => false,
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
//			'ipFilters'=>array('127.0.0.1','::1'),
            'ipFilters' => false
        ),
    ),
    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        'dkim' => array(
            'class' => 'ext.dkim.Dkim',
            'open_SSL_pub' => $publickey, // Here comes public RSA key
            'open_SSL_priv' => $privtekey, // Enter private RSA key here
            'domain' => $domain, // Your domain
            'selector' => $selector
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => $mailHost,
                'username' => $mailUserName,
                'password' => $mailPassword,
                'port' => $mailPort,
            ),
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false,
        ),
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'mpdf' => array(
                    'librarySourcePath' => 'application.vendor.mpdf.*',
                    'constants' => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'class' => 'mpdf',
                ),
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendor.html2pdf.*',
                    'classFile' => 'html2pdf.class.php', // For adding to Yii::$classMap
                ),
                'FB_TW' => array(
                    'librarySourcePath' => 'application.vendor.fb_tw.*',
                    'classFile' => 'fb_tw.facebook.facebook.php', 'fb_tw.config.fbconfig.php'  // For adding to Yii::$classMap
                )
            ),
        ),
        'curl' => array(
            'class' => 'application.extensions.curl.Curl',
        //you can setup timeout,http_login,proxy,proxylogin,cookie, and setOPTIONS
        ),
        // uncomment the following to enable URLs in path-format
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
                '<alias:news|contact|about>' => 'site/<alias>',
                'join' => '/site/join',
                'mwadmin' => '/admin/default',
                'mobile' => '/mobile/site',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
            'showScriptName' => false,
        ),
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=' . $dbname,
            'emulatePrepare' => true,
            'username' => $username,
            'password' => $pass,
            'charset' => 'utf8',
            'tablePrefix' => 'tbl_',
            'enableParamLogging' => true,
        ),
        'twitter' => array(
            'class' => 'ext.yiitwitteroauth.YiiTwitter',
            'consumer_key' => 'JsUWA9cpSn8cCoIdCZQRHba32',
            'consumer_secret' => 'IA5qJ66PxsOtlqgwwsqcG1Bk5bVh3hhyaP87UHErE7vECTv8IW',
            'callback' => 'http://localhost/user/callback',
        ),
        // Error handler
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        // the above is unused for 404 errors, as those
        // are handled by Wordpress using custom exception handler
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning,trace',
                ),
                array(
                    'class' => 'CFileLogRoute',
                    'logFile' => 'sms.log',
                    'categories' => 'sms.*',
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . "parameters_dev.php"),
);
